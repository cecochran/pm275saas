/***************************************************
 * File:        $RCSfile: TestCycleDetection.java,v $
 * @author      $Author: tlong2 $,
 * @version     $Id: TestCycleDetection.java,v 1.9 2005/05/14 22:06:28 tlong2 Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * Class for computing the critical path of a project. This could be contained
 * within the ProjectInterface and implemented in project or some other way. But
 * it seems kind of nice to have it declared separately.
 *
 *
 *  <p>
 */

package pm275.common.algorithm;

import junit.framework.TestCase;
import pm275.core.project.*;



public class TestCycleDetection extends TestCase
{
    protected void SetUp() throws Exception
    {
	super.setUp();
    }

    protected void tearDown() throws Exception
    {
	super.tearDown();
    }

    // below test is NOT cyclic
    public void testNOTCyclicCycle()
    {
     

	String exceptionstring = null;
	Project p1 = null;
	
	try {

		ProjectService ps = ProjectService.getInstance();
        p1 = (Project)ps.addProject("proj1");
        ActivityInterface a1 = ps.addActivity(p1.getProjectKey(),
                                     "a1");
        ActivityInterface a2 = ps.addActivity(p1.getProjectKey(),
                                     "a2");
        ActivityKey k1 = a1.getActivityKey();
        ActivityKey k2 = a2.getActivityKey();
	
        p1.associateActivities(k1,k2);

    } catch (Exception e){
            
	    exceptionstring = e.toString();
	    fail("EXCEPTION: testNOTCyclicCycle. " + exceptionstring);
    }
    // 2 activities
    assertEquals(2,p1.getActivitiesCount());

    }
    
    // test below is cyclic
    public void testIsCyclic()
    {
        
	String exceptionstring = null;
	    try {	
	    ProjectService ps = ProjectService.getInstance();
	    Project p1 = ps.addProject("proj1");
	    ActivityInterface a1 = ps.addActivity(p1.getProjectKey(),
					 "a1");
	    ActivityInterface a2 = ps.addActivity(p1.getProjectKey(),
					 "a2");
	    ActivityKey k1 = a1.getActivityKey();
	    ActivityKey k2 = a2.getActivityKey();
	    ActivityInterface a3 = ps.addActivity(p1.getProjectKey(),
					 "a3");
	    ActivityKey k3 = a3.getActivityKey();

            p1.associateActivities(k1,k2);
            p1.associateActivities(k2,k3);
            p1.associateActivities(k3,k1);

        }
        catch (Exception e){
           
	    exceptionstring = e.toString();
	    
       }
        assertNotNull(exceptionstring);
    }

    // test below is cyclic
    public void testCycle3()
    {
 
	String exceptionstring = null;
	
	try {
	
	ProjectService ps = ProjectService.getInstance();
        Project p1 = ps.addProject("proj1");
        ActivityInterface a1 = ps.addActivity(p1.getProjectKey(),
                                     "a1");
        ActivityInterface a2 = ps.addActivity(p1.getProjectKey(),
                                     "a2");
        ActivityKey k1 = a1.getActivityKey();
        ActivityKey k2 = a2.getActivityKey();
        ActivityInterface a3 = ps.addActivity(p1.getProjectKey(),
                                     "a3");
        ActivityKey k3 = a3.getActivityKey();
        ActivityInterface a4 = ps.addActivity(p1.getProjectKey(),
                                     "a4");
        ActivityKey k4 = a4.getActivityKey();
	
		p1.associateActivities(k1,k2);
		p1.associateActivities(k2,k3);
		p1.associateActivities(k1,k3);
		p1.associateActivities(k4,k2);
		p1.associateActivities(k3,k4);

        }
        catch (Exception e){
            
            exceptionstring = e.toString();
        }

        assertNotNull(exceptionstring);

    }

    // test below is NOT cyclic
    public void testCycle4()
    {
       

	String exceptionstring = null;

	try {
        ProjectService ps = ProjectService.getInstance();
        Project p1 = ps.addProject("proj1");
        ActivityInterface a1 = ps.addActivity(p1.getProjectKey(),
                                     "a1");        
        ActivityInterface a2 = ps.addActivity(p1.getProjectKey(),
                                     "a2");
        ActivityKey k1 = a1.getActivityKey();
        ActivityKey k2 = a2.getActivityKey();
        ActivityInterface a3 = ps.addActivity(p1.getProjectKey(),
                                     "a3");
        ActivityKey k3 = a3.getActivityKey();
        ActivityInterface a4 = ps.addActivity(p1.getProjectKey(),
                                     "a4");
        ActivityKey k4 = a4.getActivityKey();

		p1.associateActivities(k1,k2);	
		p1.associateActivities(k2,k3);	
		p1.associateActivities(k1,k3);	
		p1.associateActivities(k4,k2);
		p1.associateActivities(k1,k4);
	
        }
        catch (Exception e){
           
            exceptionstring = e.toString();
        }
	
	assertNull(exceptionstring);
	
    }

    // test below is cyclic                                              
    // taken from Jen VCT test
    public void testCycle5()
    {
	;

        String exceptionstring = null;
	
        try {
	    ProjectService ps = ProjectService.getInstance();
	    Project p1 = ps.addProject("proj1");
	    ActivityInterface design = 
		ps.addActivity(p1.getProjectKey(),"design");
	    ActivityInterface codeui =
		 ps.addActivity(p1.getProjectKey(),"codeui");
            ActivityInterface codemt =
                ps.addActivity(p1.getProjectKey(),"codemt");
            ActivityInterface integration =
                ps.addActivity(p1.getProjectKey(),"integration");
            ActivityInterface systemtest =
                ps.addActivity(p1.getProjectKey(),"systemtest");
            ActivityInterface readyforvat =
                ps.addActivity(p1.getProjectKey(),"readyforvat");

	    ActivityKey designkey = design.getActivityKey();
            ActivityKey codeuikey = codeui.getActivityKey();
            ActivityKey codemtkey = codemt.getActivityKey();
            ActivityKey integrationkey = integration.getActivityKey();
            ActivityKey systemtestkey = systemtest.getActivityKey();
            ActivityKey readyforvatkey = readyforvat.getActivityKey();

	    p1.associateActivities(designkey,codeuikey);
	    p1.associateActivities(designkey,codemtkey);
            p1.associateActivities(codeuikey,integrationkey);
            p1.associateActivities(codemtkey,integrationkey);
            p1.associateActivities(integrationkey,systemtestkey);
            p1.associateActivities(systemtestkey,readyforvatkey);
	    // ok until this line
	    p1.associateActivities(systemtestkey,codeuikey);
	    
	}
	catch (Exception e) {
	    exceptionstring = e.toString();
	}

	assertNotNull(exceptionstring);
    }

    // test below is NOT cyclic                                               
    // taken from Jen VCT test
                                                  
    public void testCycle6()
    {

        String exceptionstring = null;

        try {
            ProjectService ps = ProjectService.getInstance();
            Project p1 = ps.addProject("proj1");

	    ActivityInterface   currActivity = null;
	    ActivityInterface   prevActivity = null;
			
	    for (int i=1; i<=100; i++) {


		currActivity = ps.addActivity(p1.getProjectKey(), 
					      "Light "+String.valueOf(i));
		if (i != 1) {
		    p1.associateActivities(prevActivity.getActivityKey(),
					   currActivity.getActivityKey());
		}
			prevActivity = currActivity;
	    }
	}
	
	catch (Exception e) {
	    exceptionstring = e.toString();
	}

	assertNull(exceptionstring);
    }
}
	/**
 * $Log: TestCycleDetection.java,v $
 * Revision 1.9  2005/05/14 22:06:28  tlong2
 * Removed println
 *
 * Revision 1.8  2005/05/08 08:40:17  tlong2
 * Got rid of compile warnings
 *
 * Revision 1.7  2005/04/19 08:26:07  umangkus
 * Added new test for doing 1000 iteration.
 *
 * Revision 1.6  2005/04/18 06:39:43  umangkus
 * Modified test with new method invocations and add a test copied from Jen
 * VCT test.
 *
 * Revision 1.5  2005/04/16 16:26:31  umangkus
 * Edited test after modified PM275baseKey
 *
 * Revision 1.4  2005/04/16 08:21:30  umangkus
 * Edit test number 4.
 *
 * Revision 1.3  2005/04/16 08:06:44  ccochran
 * add headers
 *
 
 * Revision 1.1  2005/03/22 04:30:59  urip
 * Updated header to include CVS ID.
 *
 * Updated footer to include CVS log.
 *
 **/

