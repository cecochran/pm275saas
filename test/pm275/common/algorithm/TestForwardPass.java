/***************************************************
 * File:        $RCSfile: TestForwardPass.java,v $
 * @author      $Author: vchumako $,
 * @version     $Id: TestForwardPass.java,v 1.3 2005/05/09 16:49:17 vchumako Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * Class for computing the critical path of a project. This could be contained
 * within the ProjectInterface and implemented in project or some other way. But
 * it seems kind of nice to have it declared separately.
 *
 *
 *  <p>
 */
package pm275.common.algorithm;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import pm275.common.exception.CycleDependencyException;
import pm275.common.exception.DuplicateKeyException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.ResourceException;
import pm275.core.project.ActivityInterface;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ProjectService;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourceService;

import junit.framework.TestCase;

/**
 * @author Chris
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TestForwardPass extends TestCase {
    DateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
    
	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();

	}

	
	
	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	
    /*
     * Build a test project and return it
     * @return Test Project
     */
    private ProjectInterface buildProject() 
    		throws 	ResourceException, 
					ParseException, 
					InternalProjectException, 
					KeyNotFoundException,
					DuplicateKeyException,
					InternalActivityException,
					CycleDependencyException {
    	
	    ResourceService resourceService = ResourceService.getInstance();
	    ProjectService projectService = ProjectService.getInstance();

	    ProjectInterface project1 = projectService.addProject("TestBackwardPass Test Project 1");
	   
   	    ResourcePool resourcePool = resourceService.createResourcePool();
	    ResourceInterface chris  = resourceService.createResource("RC1", "Chris C");
	    ResourceInterface tom  = resourceService.createResource("RT1", "Tom L");

    	try {
    	    resourcePool.addResource(chris);
    	    resourcePool.addResource(tom);
    	    resourceService.addResourcePool(resourcePool);
    	    
    	    DateFormat DF = new SimpleDateFormat("yyyy-MM-dd");
            // Thursday April 14, 2005
            Date startDate = DF.parse("2005-04-14");    
            
            project1.setStartDate(startDate);
 
            ActivityInterface sm = project1.getStartMilestone();
            sm.setStartDate(startDate);
            ActivityInterface em = project1.getEndMilestone();
            em.setStartDate(startDate);
            
            // Add Resource Pool to Project
            projectService.addResources(project1.getProjectKey(), resourcePool.getResourcePoolKey());

            ActivityInterface act1 = projectService.addActivity(project1.getProjectKey(), "act 1");
            act1.setStartDate(startDate);
            
            ActivityInterface act2 = projectService.addActivity(project1.getProjectKey(), "act 2");
            act2.setDuration(16);
            
            project1.associateActivities(project1.getStartMilestone().getActivityKey(),act1.getActivityKey());
            project1.associateActivities(act1.getActivityKey(),act2.getActivityKey());
          
            resourceService.assign(chris.getKey(), act1.getActivityKey(), project1.getProjectKey(), 100);
            resourceService.assign(tom.getKey(), act2.getActivityKey(), project1.getProjectKey(), 100);

            
    	} catch (ResourceException re) {
    		fail("ResourceException in buildProject()" + re.getMessage());
    	} catch (ParseException pe) {
    		fail("ParseException in buildProject()" + pe.getMessage());
    	} catch (InternalProjectException ipe) {
    		fail("InternalProjectException in buildProject()" + ipe.getMessage());
    	} catch (KeyNotFoundException knfe) {
    		fail("KeyNotFoundException in buildProject()" + knfe.getMessage());
    	} catch (DuplicateKeyException dke) {
    		fail("DuplicateKeyException in buildProject()" + dke.getMessage());
    	} catch (InternalActivityException iae) {
    		fail("InternalActivityException in buildProject()" + iae.getMessage());
    	} catch (CycleDependencyException cde) {
    		fail("CycleDependencyException in buildProject()" + cde.getMessage());
    	}
    	return project1;
    }
    
	
	
	/**
	 * Constructor for testForwardPass.
	 * @param arg0
	 */
	public TestForwardPass(String arg0) {
		super(arg0);
	}
	public void testForwardPass() {
	
		try
		{
		   ProjectInterface project1 = buildProject();	
		   List listOfActivities = project1.getActivities(null,true);
		   ForwardPass fp = new ForwardPass(listOfActivities);
	       assertNotNull(fp);
	       
		}catch (Exception e) {
	       assertTrue(false);   
	    }
	
	}
	

	public void testHasNext() {
		
		try
		{
		   ProjectInterface project1 = buildProject();
		   List listOfActivities = project1.getActivities(null,true);
		   ForwardPass fp = new ForwardPass(listOfActivities);
	       assertTrue(fp.hasNext());
	       
		}catch (Exception e) {
	       assertTrue(false);   
	    }
	}
/*
	public void testNext() {
		try
		{
		   ProjectInterface project1 = buildProject();	
		   List listOfActivities = project1.getActivities(null,true);
		   ForwardPass fp = new ForwardPass(listOfActivities);
		   Activity act =  fp.next();
		   assertNotNull(act);
	       
		}catch (Exception e) {
	       assertTrue(false);   
	    }
	}
*/
	
	public void testReset() {
		try
		{
		   ProjectInterface project1 = buildProject();	
		   List listOfActivities = project1.getActivities(null,true);
		   ForwardPass fp = new ForwardPass(listOfActivities);
	       fp.reset();
	       
		   assertTrue(true);
	       
		}catch (Exception e) {
	       assertTrue(false);   
	    }
	
	}

}
/**
 * $Log: TestForwardPass.java,v $
 * Revision 1.3  2005/05/09 16:49:17  vchumako
 * Assigned unique ID to sample resources
 *
 * Revision 1.2  2005/05/08 08:40:17  tlong2
 * Got rid of compile warnings
 *
 * Revision 1.1  2005/05/08 06:35:02  tlong2
 * Cleaned up the algorithm tests
 *
 * Revision 1.7  2005/04/27 12:45:25  tlong2
 * updates to account for resourceService.assign() changes
 *
 * Revision 1.6  2005/04/24 22:27:10  tlong2
 * updated tests to match changes to CP algorithm
 *
 * Revision 1.5  2005/04/19 16:39:10  ccochran
 * updated tests
 *
 * Revision 1.4  2005/04/19 15:34:34  ccochran
 * update iterator tests
 *
 * Revision 1.3  2005/04/17 19:40:37  ccochran
 * updated tests suite
 *
 * Revision 1.2  2005/04/16 08:20:19  ccochran
 * updated header
 *
 * Revision 1.1  2005/04/16 08:11:12  ccochran
 * add to cvs
 *
 
 * Revision 1.1  2005/03/22 04:30:59  dnathan
 * Updated header to include CVS ID.
 *
 * Updated footer to include CVS log.
 *
 **/
