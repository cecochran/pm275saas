/***************************************************
 * File:        $RCSfile: TestProjectCriticalPath.java,v $
 * @author      $Author: achou $,
 * @version     $Id: TestProjectCriticalPath.java,v 1.14 2005/05/09 16:47:51 achou Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * Class for computing the critical path of a project. This could be contained
 * within the ProjectInterface and implemented in project or some other way. But
 * it seems kind of nice to have it declared separately.
 *
 *
 *  <p>
 */
package pm275.common.algorithm;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import pm275.core.project.*;
import pm275.core.resource.*;
import pm275.common.exception.CycleDependencyException;
import pm275.common.exception.DuplicateKeyException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.ResourceException;
import junit.framework.TestCase;


public class TestProjectCriticalPath extends TestCase {
        private static final ProjectService  projectService = ProjectService.getInstance();
        private static final ResourceService resourceService = ResourceService.getInstance();
        private static ProjectInterface      project1 = null;
        private static ProjectInterface      project2 = null;
        private static ProjectInterface      project3 = null;
        private static ProjectKey    project1Key = null;
        private static ProjectKey    project2Key = null;
        private static ProjectKey    project3Key = null;
        //project 4 needs to use some of project 1's activities
        private static ActivityKey   project4Act1Key = null;
        private static ActivityKey   project4Act2Key = null;
        //private static Date startDate = null;
        //private static final Date startDate = pm275.gui.common.DateFormatter.getDateForString("2005-04-18");
        private static final DateFormat DF = new SimpleDateFormat ("yyyy-MM-dd");

    protected void SetUp() throws Exception
    {
        super.setUp();
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
    /**
     * Constructor for TestProjectCriticalPath.
     * @param arg0
     */
    public TestProjectCriticalPath(String arg0) {
        super(arg0);

    }
    /*
     * Build a test project and return it
     * @return Test Project
     */
    private ProjectInterface buildProject()
            throws  ResourceException,
                    ParseException,
                    InternalProjectException,
                    KeyNotFoundException,
                    DuplicateKeyException,
                    InternalActivityException,
                    CycleDependencyException {

        ResourceService resourceService = ResourceService.getInstance();
        ProjectService projectService = ProjectService.getInstance();

        ProjectInterface project1 = projectService.addProject("TestBackwardPass Test Project 1");

        ResourcePool resourcePool = resourceService.createResourcePool();
        ResourceInterface chris  = resourceService.createResource("Chris");
        ResourceInterface tom  = resourceService.createResource("Tom");

        try {
            resourcePool.addResource(chris);
            resourcePool.addResource(tom);
            resourceService.addResourcePool(resourcePool);

            DateFormat DF = new SimpleDateFormat("yyyy-MM-dd");
            // Thursday April 14, 2005
            Date startDate = DF.parse("2005-04-14");

            project1.setStartDate(startDate);

            ActivityInterface sm = project1.getStartMilestone();
            sm.setStartDate(startDate);
            ActivityInterface em = project1.getEndMilestone();
            em.setStartDate(startDate);

            // Add Resource Pool to Project
            projectService.addResources(project1.getProjectKey(), resourcePool.getResourcePoolKey());

            ActivityInterface act1 = projectService.addActivity(project1.getProjectKey(), "act 1");
            act1.setStartDate(startDate);

            ActivityInterface act2 = projectService.addActivity(project1.getProjectKey(), "act 2");
            act2.setDuration(16);

             project1.associateActivities(act1.getActivityKey(),act2.getActivityKey());

            resourceService.assign(chris.getKey(), act1.getActivityKey(), project1.getProjectKey(), 100);
            resourceService.assign(tom.getKey(), act2.getActivityKey(), project1.getProjectKey(), 100);


        } catch (ResourceException re) {
            fail("ResourceException in buildProject()" + re.getMessage());
        } catch (ParseException pe) {
            fail("ParseException in buildProject()" + pe.getMessage());
        } catch (InternalProjectException ipe) {
            fail("InternalProjectException in buildProject()" + ipe.getMessage());
        } catch (KeyNotFoundException knfe) {
            fail("KeyNotFoundException in buildProject()" + knfe.getMessage());
        } catch (DuplicateKeyException dke) {
            fail("DuplicateKeyException in buildProject()" + dke.getMessage());
        } catch (InternalActivityException iae) {
            fail("InternalActivityException in buildProject()" + iae.getMessage());
        } catch (CycleDependencyException cde) {
            fail("CycleDependencyException in buildProject()" + cde.getMessage());
        }
        return project1;
    }

    public void testCalculateCriticalPath()
    {
        try{

            ProjectInterface p = buildProject();
            LinkedList list = p.calculateCriticalPath();
            assertNotNull(list);

        }catch (Exception e) {
            fail("EXCEPTION testCalculateCriticalPath()");

        }
    }

    public void testCalculateCriticalPath2()
    {
        try{

            ProjectInterface p = buildProject();
            LinkedList list = p.calculateCriticalPath();

            // Remove start and end milestones.
            if (list.contains(p.getStartMilestone()))
            {
                list.remove(p.getStartMilestone());
            }
            if (list.contains(p.getEndMilestone()))
            {
                list.remove(p.getEndMilestone());
            }

            // Two activities on CP.
            assertEquals(list.size(), 2);

        }catch (Exception e) {
            fail("EXCEPTION testCalculateCriticalPath2()");

        }
    }

    public void testProject1()
    {
        ActivityInterface design = null;
        ActivityInterface ui = null;
        ActivityInterface mt = null;
        ActivityInterface inte = null;
        ActivityInterface test = null;
        ActivityInterface uat = null;

        ProjectInterface p = fabricateProject1();
        Iterator activities = p.getActivities( null , false ).iterator();

        while( activities.hasNext() )
        {
            ActivityInterface comp = (ActivityInterface) activities.next();

            if( comp.getName().equals( "Design" ) )
            {
                design = comp;
            }
            if( comp.getName().equals( "Code UI" ) )
            {
                ui = comp;
            }
            if( comp.getName().equals( "Code Middle Tier Stuff" ) )
            {
                mt = comp;
            }
            if( comp.getName().equals( "Integration of UI and Middle Tier" ) )
            {
                inte = comp;
            }
            if( comp.getName().equals( "System Test" ) )
            {
                test = comp;
            }
            if( comp.getName().equals( "Ready for UAT" ) )
            {
                uat = comp;
            }
        }

        // check durations
        assertEquals( design.getDuration() , 40 );
        assertEquals( ui.getDuration() , 56 );
        assertEquals( mt.getDuration() , 80 );
        assertEquals( inte.getDuration() , 16 );
        assertEquals( test.getDuration() , 40 );
        assertEquals( uat.getDuration() , 0 );

        // check early finish flag
        assertFalse( design.getEarlyFinishFlag() );
        assertFalse( ui.getEarlyFinishFlag() );
        assertFalse( mt.getEarlyFinishFlag() );
        assertFalse( inte.getEarlyFinishFlag() );
        assertFalse( test.getEarlyFinishFlag() );
        assertFalse( uat.getEarlyFinishFlag() );

        ProjectCriticalPath path = new ProjectCriticalPath(p);
        path.calculateCriticalPath();

        // check durations
        assertEquals( design.getDuration() , 40 );
        assertEquals( ui.getDuration() , 56 );
        assertEquals( mt.getDuration() , 80 );
        assertEquals( inte.getDuration() , 16 );
        assertEquals( test.getDuration() , 40 );
        assertEquals( uat.getDuration() , 0 );

        // check early start
        assertEquals( design.getEarlyStartHours() , 0 );
        assertEquals( ui.getEarlyStartHours() , 40 );
        assertEquals( mt.getEarlyStartHours() , 40 );
        assertEquals( inte.getEarlyStartHours() , 120 );
        assertEquals( test.getEarlyStartHours() , 136 );
        assertEquals( uat.getEarlyStartHours() , 176 );

        // check early finish
        assertEquals( design.getEarlyFinishHours() , 40 );
        assertEquals( ui.getEarlyFinishHours() , 96 );
        assertEquals( mt.getEarlyFinishHours() , 120 );
        assertEquals( inte.getEarlyFinishHours() , 136 );
        assertEquals( test.getEarlyFinishHours() , 176 );
        assertEquals( uat.getEarlyFinishHours() , 176 );

        // check early finish flag
        assertTrue( design.getEarlyFinishFlag() );
        assertTrue( ui.getEarlyFinishFlag() );
        assertTrue( mt.getEarlyFinishFlag() );
        assertTrue( inte.getEarlyFinishFlag() );
        assertTrue( test.getEarlyFinishFlag() );
        assertTrue( uat.getEarlyFinishFlag() );

        // check all for end milestone
        assertTrue( p.getEndMilestone().getEarlyFinishFlag() );
        assertEquals( p.getEndMilestone().getEarlyStartHours() , 176 );
        assertEquals( p.getEndMilestone().getEarlyFinishHours() , 176 );

        // check late finish
        assertEquals( design.getLateFinishHours() , 40 );
        assertEquals( ui.getLateFinishHours() , 120 );
        assertEquals( mt.getLateFinishHours() , 120 );
        assertEquals( inte.getLateFinishHours() , 136 );
        assertEquals( test.getLateFinishHours() , 176 );
        assertEquals( uat.getLateFinishHours() , 176 );

        // check late start
        assertEquals( design.getLateStartHours() , 0 );
        assertEquals( ui.getLateStartHours() , 64 );
        assertEquals( mt.getLateStartHours() , 40 );
        assertEquals( inte.getLateStartHours() , 120 );
        assertEquals( test.getLateStartHours() , 136 );
        assertEquals( uat.getEarlyStartHours() , 176 );

    }



    public static ProjectInterface fabricateProject1() {
        ResourcePool    project1Pool;
        ResourcePoolKey project1PoolKey;

        try {
          // Create Resource Pool and add Peter and Paul
          project1Pool    = resourceService.createResourcePool();
          project1PoolKey = project1Pool.getResourcePoolKey();
          Resource paul  = resourceService.createResource("Paul");
          project1Pool.addResource(paul);
          Resource peter = resourceService.createResource("Peter");
          project1Pool.addResource(peter);
          resourceService.addResourcePool(project1Pool);

          // Create Project
          project1 = projectService.addProject("Test Project 1");
          project1Key = project1.getProjectKey();
          Date startDate = DF.parse("2005-04-18");
          project1.setStartDate(startDate);

          // Add Resource Pool to Project
          projectService.addResources(project1Key, project1PoolKey);
          /**
           * Now start adding the Activities with durations in hours
           * 1. Start date: 4/18
           * 2. Design (Peter and Paul), 1week
           * 3. Code UI (Peter), 7 days, after design is complete
           * 4. Code Middle Tier Stuff (Paul), 10 days, after design is complete
           * 5. Integration of UI and Middle Tier (Peter and Paul), 2 days, after UI and Middle Tier are complete
           * 6. System Test(Peter and Paul), 5 days, after Integration is complete
           * 7. Ready for UAT, milestone date depending on System Test completion
           *
           */
          ActivityInterface design = projectService.addActivity(project1Key, "Design");
          design.setDuration(40);
          ActivityInterface codeUI = projectService.addActivity(project1Key, "Code UI");
          codeUI.setDuration(56);
          ActivityInterface codeMT = projectService.addActivity(project1Key, "Code Middle Tier Stuff");
          codeMT.setDuration(80);
          ActivityInterface integration = projectService.addActivity(project1Key, "Integration of UI and Middle Tier");
          integration.setDuration(16);
          ActivityInterface systemTest = projectService.addActivity(project1Key, "System Test");
          systemTest.setDuration(40);
          ActivityInterface readyForUAT = projectService.addActivity(project1Key, "Ready for UAT");
          readyForUAT.setDuration(0);


          /**
           * set resources
           */
          resourceService.assign(paul.getKey(), design.getActivityKey(), project1Key,100);
          resourceService.assign(peter.getKey(), design.getActivityKey(),project1Key, 100);
          resourceService.assign(peter.getKey(), codeUI.getActivityKey(),project1Key, 100);
          resourceService.assign(paul.getKey(), codeMT.getActivityKey(),project1Key, 100);
          resourceService.assign(paul.getKey(), integration.getActivityKey(),project1Key, 100);
          resourceService.assign(peter.getKey(), integration.getActivityKey(),project1Key, 100);
          resourceService.assign(paul.getKey(), systemTest.getActivityKey(), project1Key,100);
          resourceService.assign(peter.getKey(), systemTest.getActivityKey(),project1Key, 100);

          /**
           * finally, add the dependencies
           */
          project1.associateActivities(design.getActivityKey(),codeUI.getActivityKey());
          project1.associateActivities(design.getActivityKey(),codeMT.getActivityKey());
          project1.associateActivities(codeUI.getActivityKey(),integration.getActivityKey());
          project1.associateActivities(codeMT.getActivityKey(),integration.getActivityKey());
          project1.associateActivities(integration.getActivityKey(),systemTest.getActivityKey());
          project1.associateActivities(systemTest.getActivityKey(),readyForUAT.getActivityKey());

          //project 4 will need these
          // project4Act1Key = codeUI.getActivityKey();
          // project4Act2Key = readyForUAT.getActivityKey();
          // log.info("VCT Project1 created successfully");

        }
        catch (CycleDependencyException cde) {
          // log.warn("Warning: " + cde);
        }
        catch (Exception e) {
          // log.fatal("Error fabricating VCT Project1: " + e);
          e.printStackTrace();
          System.exit(1);
        }
        return project1;
    }


}

/**
 * $Log: TestProjectCriticalPath.java,v $
 * Revision 1.14  2005/05/09 16:47:51  achou
 * Fixed testCalculateCriticalPath2() to remove start/end milestones and expected size of critical path.
 *
 * Revision 1.13  2005/05/08 16:37:50  tlong2
 * Cleaned up
 *
 * Revision 1.12  2005/05/08 06:35:02  tlong2
 * Cleaned up the algorithm tests
 *
 * Revision 1.11  2005/04/28 05:40:45  achou
 * Removed dependency on ProjectMaker and DateFormatter.  Copied fabricateProject1() from ProjectMaker to this class.
 *
 * Revision 1.10  2005/04/24 20:24:40  dnathan
 * Moved everything into one test.
 *
 * Revision 1.9  2005/04/19 04:03:42  ccochran
 * updated successor
 *
 * Revision 1.8  2005/04/19 02:27:42  ccochran
 * updated test
 *
 * Revision 1.7  2005/04/18 19:51:07  ccochran
 * test
 *
 * Revision 1.6  2005/04/18 15:14:18  ccochran
 * building
 *
 * Revision 1.5  2005/04/18 08:14:42  ccochran
 * building out test
 *
 * Revision 1.4  2005/04/18 07:32:14  ccochran
 * updated to us  vct code
 *
 * Revision 1.3  2005/04/17 19:10:51  ccochran
 * fixed merge problem
 *
 * Revision 1.2  2005/04/17 18:30:50  dnathan
 * Fixed compilation errors.
 *
 * Revision 1.1  2005/04/17 18:26:35  ccochran
 * layer 1
 *

 * Revision 1.1  2005/03/22 04:30:59  dnathan
 * Updated header to include CVS ID.
 *
 * Updated footer to include CVS log.
 *
 **/

