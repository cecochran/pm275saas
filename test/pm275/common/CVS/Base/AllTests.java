/**
 * CSCIE 275 - Software Architecture and Engineering
 *
 * File: $RCSfile: AllTests.java,v $
 * @author $Author: tlong2 $,
 * @version $Revision: 1.7 $
 *
 * <p></p>
 *
 * $Id: AllTests.java,v 1.7 2005/05/08 06:35:29 tlong2 Exp $
 */
package pm275.common;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite("Common tests for test.pm275.common");
        //$JUnit-BEGIN$

        
        // Algorighm Tests
        suite.addTestSuite(pm275.common.algorithm.TestBackwardPass.class);
        suite.addTestSuite(pm275.common.algorithm.TestCycleDetection.class);
        suite.addTestSuite(pm275.common.algorithm.TestForwardPass.class);
        suite.addTestSuite(pm275.common.algorithm.ProjectCriticalPath.class);
        
        
        // Persistance Tests
        suite.addTestSuite(pm275.common.persistance.TestDBConnection.class);
        suite.addTestSuite(pm275.common.persistance.TestPersistanceService.class);
        suite.addTestSuite(pm275.common.persistance.TestProjectVisitorDBMS.class);
        suite.addTestSuite(pm275.common.persistance.TestProjectVisitorToXML.class);
        suite.addTestSuite(pm275.common.persistance.TestProjectXmlReader.class);
        suite.addTestSuite(pm275.common.persistance.TestProjectXmlWriter.class);

        
        // Reporting Tests
        suite.addTestSuite(pm275.common.reporting.TestReportingService.class);
        
        
        // Security Tests
        suite.addTestSuite(pm275.common.security.TestSecurityService.class);
        
        //$JUnit-END$
        return suite;
    }
}

/**
 * $Log: AllTests.java,v $
 * Revision 1.7  2005/05/08 06:35:29  tlong2
 * Updated so all common tests are run
 *
 * Revision 1.6  2005/05/06 23:56:15  achou
 * Added TestProjectXmlReader.
 *
 * Revision 1.5  2005/05/06 06:06:35  achou
 * Changed TestProjectVisitorToXML to TestProjectXmlWriter.
 *
 * Revision 1.4  2005/05/01 08:55:16  achou
 * Added TestProjectVisitorToXML and commented out TestCycleDetection (because it was taking too long to complete).
 *
 * Revision 1.3  2005/04/16 05:58:58  umangkus
 * Add test for CycleDetection.java
 *
 * Revision 1.2  2005/03/30 16:04:06  dnathan
 * Removed unneeded imports.
 *
 * Revision 1.1  2005/03/30 06:29:43  tlong2
 * Reorganized AllTests.java to be at common root to match test.xml
 *
 * Revision 1.1  2005/03/27 18:43:37  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.4  2005/03/26 19:42:26  vchumako
 * Add AssignmentKey test
 *
 * Revision 1.3  2005/03/26 19:02:41  vchumako
 * AssignmentKey test added
 *
 * Revision 1.2  2005/03/26 09:02:28  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.1  2005/03/22 06:25:17  dnathan
 * New file.
 *
 * Revision 1.2  2005/03/22 05:10:41  dnathan
 * Updated CVS footer.
 *
 **/
