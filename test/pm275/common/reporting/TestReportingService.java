/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestReportingService.java,v $ 
 * @author $Author: ccochran $, 
 * @version $Revision: 1.2 $
 * 
 * <p></p>
 * 
 * $Id: TestReportingService.java,v 1.2 2005/03/28 18:03:48 ccochran Exp $
 */
package pm275.common.reporting;

import junit.framework.TestCase;


public class TestReportingService extends TestCase {

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testGetInstance() {
		//TODO Implement getInstance().
	}

}

/**
 * $Log: TestReportingService.java,v $
 * Revision 1.2  2005/03/28 18:03:48  ccochran
 * fixed some packages
 *
 * Revision 1.1  2005/03/27 18:43:20  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:18  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:30  dnathan
 * Updated CVS footer.
 *
 **/