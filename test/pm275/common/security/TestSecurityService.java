/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile$ 
 * @author $Author$, 
 * @version $Revision$
 * 
 * <p>Test for Security Service</p>
 * 
 * $Id$
 */
package pm275.common.security;

import junit.framework.TestCase;


public class TestSecurityService extends TestCase 
{
	
	public static void main(String[] args) {
		junit.textui.TestRunner.run(TestSecurityService.class);
	}

	public void testLoginSuccess() throws Exception 
	{
		SecurityService service = SecurityService.getInstance();
		boolean loginSuccess = service.login("admin", "password");
		assertEquals("Error" , true, loginSuccess);
		loginSuccess = service.login("user", "welcome");
		assertEquals("Error" , true, loginSuccess);
	}
	
	
	public void testLoginFailed() throws Exception 
	{
		SecurityService service = SecurityService.getInstance();
		boolean failed = service.login("admin", "abcd");
		assertEquals("Error" , false, failed);
		failed = service.login("admin12", "abcd");
		assertEquals("Error" , false, failed);
		
	}
	
	public void testLoginException() throws Exception 
	{
		SecurityService service = SecurityService.getInstance();
		String userMsg = "Username cannot be null or blank";
		String pwdMsg = "Password cannot be null or blank";
		try
		{
			boolean failed = service.login("", "abcd");
		}catch(SecurityException se)
		{
			assertEquals("Error", userMsg,se.getMessage());
		}
		try
		{
			boolean failed = service.login(null, "abcd");
		}catch(SecurityException se)
		{
			assertEquals("Error", userMsg,se.getMessage());
		}
		try
		{
			boolean failed = service.login("admin", "");
		}catch(SecurityException se)
		{
			assertEquals("Error", pwdMsg,se.getMessage());
		}
		try
		{
			boolean failed = service.login("admin", null);
		}catch(SecurityException se)
		{
			assertEquals("Error", pwdMsg,se.getMessage());
		}
		
	}

}
/**
 * $Log$
 * Revision 1.5  2005/03/29 03:54:19  rcoutinh
 * Added comment
 *
 * Revision 1.4  2005/03/29 03:46:57  vchumako
 * Restored test for SecurityService
 *
 * Revision 1.2  2005/03/27 02:11:34  rcoutinh
 * Added CVS Header and Footer
 * 
 */