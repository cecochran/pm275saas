/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestPersistanceService.java,v $ 
 * @author $Author: vchumako $, 
 * @version $Revision: 1.3 $
 * 
 * <p></p>
 * 
 * $Id: TestPersistanceService.java,v 1.3 2005/05/01 05:11:35 vchumako Exp $
 */
package pm275.common.persistance;

import junit.framework.TestCase;


public class TestPersistanceService extends TestCase {

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testGetInstance() {
		PersistanceService ps = PersistanceService.getInstance();
		assertNotNull(ps);
	}
	
	

}

/**
 * $Log: TestPersistanceService.java,v $
 * Revision 1.3  2005/05/01 05:11:35  vchumako
 * Fixed DAO exception name, added resource to dbms, still need to fix jndi ro get back to plain jdbc
 *
 * Revision 1.2  2005/03/28 18:03:48  ccochran
 * fixed some packages
 *
 * Revision 1.1  2005/03/27 18:43:37  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:20  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:31  dnathan
 * Updated CVS footer.
 *
 **/