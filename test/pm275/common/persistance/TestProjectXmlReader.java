/*
 *  CSCIE 275 - Software Architecture and Engineering
 *
 *  File: $RCSfile: TestProjectXmlReader.java,v $
 *  @author $Author: tlong2 $,
 *  @version $Revision: 1.5 $
 *
 *  <p></p>
 *
 *  $Id: TestProjectXmlReader.java,v 1.5 2005/05/14 22:59:25 tlong2 Exp $
 */
package pm275.common.persistance;

import junit.framework.TestCase;

import java.util.Iterator;
import java.io.File;

import pm275.core.project.ProjectInterface;
import pm275.core.project.ActivityInterface;
import pm275.core.project.Activity;
import pm275.core.resource.ResourceInterface;
import pm275.common.exception.PersistanceException;
import pm275.common.exception.CycleDependencyException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.ResourceException;
import pm275.common.exception.KeyNotFoundException;

public class TestProjectXmlReader extends TestCase
{
    private File inputFile1;
    private File inputFile2;


    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        inputFile1 = new File("pm275.test1-out.xml");
        inputFile2 = new File("pm275.test2-out.xml");
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /*
     * Verify project 1 created
     */
    public void testProjectFromXmlProject1()
            throws PersistanceException, CycleDependencyException,
                   InternalActivityException, ResourceException,
                   KeyNotFoundException
    {
        // Read project 1.
        ProjectXmlReader reader1 = new ProjectXmlReader(inputFile1);
        ProjectInterface project1 = reader1.readProject();
      // displayProject(project1);
        
        assertNotNull(project1);
    }
    /*
     * Verify project created
     */
    public void testProjectFromXmlProject2()
    throws PersistanceException, CycleDependencyException,
           InternalActivityException, ResourceException,
           KeyNotFoundException
    {  
	
		// Read project 2.
		ProjectXmlReader reader2 = new ProjectXmlReader(inputFile2);
		ProjectInterface project2 = reader2.readProject();
	//	displayProject(project2);
		
		assertNotNull(project2);
	}
    /*
     * Verify 3 activites in project 1
     */
    public void testProjectFromXmlProject1Activities()
    throws PersistanceException, CycleDependencyException,
           InternalActivityException, ResourceException,
           KeyNotFoundException
	{
		// Read project
		ProjectXmlReader reader1 = new ProjectXmlReader(inputFile1);
		ProjectInterface project1 = reader1.readProject();
		
		assertEquals(3,project1.getActivitiesCount());
	} 
    /*
     * Verify 6 activites in project 2
     */
    public void testProjectFromXmlProject2Activities()
    throws PersistanceException, CycleDependencyException,
           InternalActivityException, ResourceException,
           KeyNotFoundException
	{
		// Read project
		ProjectXmlReader reader2 = new ProjectXmlReader(inputFile2);
		ProjectInterface project2 = reader2.readProject();
		
		assertEquals(6,project2.getActivitiesCount());
	}  
    /*
     * Verify 8 resources in project 1
     */
    public void testProjectFromXmlProject1Resources()
    throws PersistanceException, CycleDependencyException,
           InternalActivityException, ResourceException,
           KeyNotFoundException
	{
		// Read project
		ProjectXmlReader reader1 = new ProjectXmlReader(inputFile1);
		ProjectInterface project1 = reader1.readProject();
		
		assertEquals(8,project1.getResources().size());
	} 
    /*
     * Verify 8 resources in project 2
     */
    public void testProjectFromXmlProject2Resources()
    throws PersistanceException, CycleDependencyException,
           InternalActivityException, ResourceException,
           KeyNotFoundException
	{
		// Read project
		ProjectXmlReader reader2 = new ProjectXmlReader(inputFile2);
		ProjectInterface project2 = reader2.readProject();
		
		assertEquals(8,project2.getResources().size());
	}     
    /*
     * Verify project 1 calendar created
     */  
    public void testProjectFromXmlProject1Calendar()
    throws PersistanceException, CycleDependencyException,
           InternalActivityException, ResourceException,
           KeyNotFoundException
	{
		// Read project
		ProjectXmlReader reader1 = new ProjectXmlReader(inputFile1);
		ProjectInterface project1 = reader1.readProject();
		
		assertNotNull(project1.getCalendarKey());
	} 
    /*
     * Verify project 2 calendar created
     */
    public void testProjectFromXmlProject2Calendar()
    throws PersistanceException, CycleDependencyException,
           InternalActivityException, ResourceException,
           KeyNotFoundException
	{
		// Read project
		ProjectXmlReader reader2 = new ProjectXmlReader(inputFile2);
		ProjectInterface project2 = reader2.readProject();
		
		assertNotNull(project2.getCalendarKey());
	}   


    
    
    private void displayProject(ProjectInterface project)
            throws KeyNotFoundException
    {


        // Show activities.
        for (Iterator i = project.getActivities(Activity.OrderComparator.ascending()).iterator(); i.hasNext(); )
        {
            ActivityInterface activity = (ActivityInterface) i.next();
            //System.out.println("    --------------------");
            //System.out.println("    Activity:");
            //System.out.println("");
            //System.out.println(activity.toString());
            //System.out.println("    --------------------");
        }

        // Show resources.
        //System.out.println("    --------------------");
        //System.out.println("    Resource pool:");
        for (Iterator i = project.getResources().iterator(); i.hasNext(); )
        {
            ResourceInterface resource = (ResourceInterface) i.next();
            //System.out.println("    Resource: " + resource.toString());
        }
        //System.out.println("    --------------------");

        // Show calendar.
        /*
        CalendarService calendarService = CalendarService.getInstance();
        CalendarInterface calendar = calendarService.getCalendar(project.getCalendarKey());
        //System.out.println("    --------------------");
        //System.out.println("    Calendar: " + calendar.getDaysPerWeek() +
                           " days per week, " + calendar.getHoursPerDay() +
                           " hours per day.");
        //System.out.println("    --------------------");
        */

      //  //System.out.println("--------------------");
    }
}

/*
 *  $Log: TestProjectXmlReader.java,v $
 *  Revision 1.5  2005/05/14 22:59:25  tlong2
 *  cleaned up tests
 *
 *  Revision 1.4  2005/05/08 22:50:51  achou
 *  Added a second test project.
 *
 *  Revision 1.3  2005/05/08 19:38:31  achou
 *  Added code to display project's resources.
 *
 *  Revision 1.2  2005/05/08 16:34:36  tlong2
 *  Updated imports and exceptions
 *
 *  Revision 1.1  2005/05/06 23:55:10  achou
 *  New JUnit test for ProjectXmlReader.
 *
 */
