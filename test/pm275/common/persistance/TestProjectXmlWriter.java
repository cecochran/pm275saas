/*
 *  CSCIE 275 - Software Architecture and Engineering
 *
 *  File: $RCSfile: TestProjectXmlWriter.java,v $
 *  @author $Author: tlong2 $,
 *  @version $Revision: 1.4 $
 *
 *  <p></p>
 *
 *  $Id: TestProjectXmlWriter.java,v 1.4 2005/05/14 22:59:25 tlong2 Exp $
 */
package pm275.common.persistance;

import junit.framework.TestCase;


import java.io.File;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import pm275.core.project.ProjectService;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ActivityInterface;
import pm275.core.resource.ResourceService;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourceInterface;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.DuplicateKeyException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.ResourceException;
import pm275.common.exception.CycleDependencyException;
import pm275.common.exception.PersistanceException;


public class TestProjectXmlWriter extends TestCase
{
    // Test projects.
    private ProjectInterface testProject1;
    private ProjectInterface testProject2;

    // Resources.
    private ResourcePool resourcePool;
    private ResourceInterface alex;
    private ResourceInterface chris;
    private ResourceInterface dan;
    private ResourceInterface radha;
    private ResourceInterface vladimir;
    private ResourceInterface tom;
    private ResourceInterface urip;
    private ResourceInterface jen;

    // XML files.
    private File outputFile1;
    private File outputFile2;

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");


    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        setupResourcePool();
        testProject1 = createTestProject1();
        outputFile1 = new File("pm275.test1-out.xml");
        testProject2 = createTestProject2();
        outputFile2 = new File("pm275.test2-out.xml");
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     *  Comprehensive test.
     */
    public void testProject1ToXml() throws PersistanceException
    {
    	String sException = null;
    	try {
    	       // Write project 1.
            ProjectXmlWriter writer1 = new ProjectXmlWriter(outputFile1);
            writer1.writeProject(testProject1);
   		
    	}
    	catch (Exception e) {
    		fail("Exception: testProject1ToXml. " + e.getMessage());
    	}
    	
    	assertNull(sException);
    	

    }
    public void testProject2ToXml() throws PersistanceException
    {
    	String sException = null;
    	try {
    	    // Write project 2.
    	    ProjectXmlWriter writer2 = new ProjectXmlWriter(outputFile2);
    	    writer2.writeProject(testProject2);
    	     		
    	}
    	catch (Exception e) {
    		fail("Exception: testProject1ToXml. " + e.getMessage());
    	}
    	
    	assertNull(sException);
    	

    }

    /**
     *  Sets up a test project.
     */
    private final ProjectInterface createTestProject1()
            throws ParseException, KeyNotFoundException, DuplicateKeyException,
                   InternalProjectException, InternalActivityException,
                   ResourceException, CycleDependencyException
    {
        // Create a new project.
        ProjectService projectService = ProjectService.getInstance();
        ProjectInterface project = projectService.addProject("PM-275");

        // Set the start date.
        project.setStartDate(SDF.parse("2005-03-08"));

        // Set resources for the project.
        projectService.addResources(project.getProjectKey(),
                resourcePool.getResourcePoolKey());

        // Customize the calendar - 2 hours per day, 6 days a week.
        /*
        CalendarService calendarService = CalendarService.getInstance();
        CalendarInterface calendar = calendarService.getCalendar(project.getCalendarKey());
        calendar.setHoursPerDay(2);
        calendar.setDaysPerWeek(6);
        */

        // Create first activity.
        ActivityInterface activity1 =
                projectService.addActivity(project.getProjectKey(),
                                           "Iteration 1");
        activity1.setIsFixedStartDate(true);
        activity1.setIsFixedDuration(true);
        activity1.setDuration(120);  // 3 weeks

        // Create second activity.
        ActivityInterface activity2 =
                projectService.addActivity(project.getProjectKey(),
                                           "Iteration 2");
        activity2.setIsFixedStartDate(false);
        activity2.setIsFixedDuration(true);
        activity2.setDuration(120);  // 3 weeks
        project.associateActivities(activity1.getActivityKey(),
                                    activity2.getActivityKey());

        // Create third activity.
        ActivityInterface activity3 =
                projectService.addActivity(project.getProjectKey(),
                                           "Iteration 3");
        activity3.setIsFixedStartDate(false);
        activity3.setIsFixedDuration(true);
        activity3.setDuration(120);  // 3 weeks
        project.associateActivities(activity2.getActivityKey(),
                                    activity3.getActivityKey());

        // Return the project.
        return project;
    }

    private final ProjectInterface createTestProject2()
            throws ParseException, KeyNotFoundException, DuplicateKeyException,
                   InternalProjectException, InternalActivityException,
                   ResourceException, CycleDependencyException
    {
        // Create a new project.
        ProjectService projectService = ProjectService.getInstance();
        ProjectInterface project = projectService.addProject("PM-275, Iteration 3");

        // Set the start date.
        project.setStartDate(SDF.parse("2005-04-19"));

        // Set resources for the project.
        projectService.addResources(project.getProjectKey(),
                resourcePool.getResourcePoolKey());

        // Resource service.
        ResourceService resourceService = ResourceService.getInstance();

        // Create "bug fix" activity.
        ActivityInterface bugFixActivity =
                projectService.addActivity(project.getProjectKey(),
                                           "Fix Iteration 2 bugs");
        bugFixActivity.setIsFixedStartDate(false);
        bugFixActivity.setIsFixedDuration(true);
        bugFixActivity.setDuration(40);  // 1 week

        // Assign resources.
        resourceService.assign(alex.getKey(), bugFixActivity.getActivityKey(),
                               project.getProjectKey(), 100);
        resourceService.assign(chris.getKey(), bugFixActivity.getActivityKey(),
                               project.getProjectKey(), 100);
        resourceService.assign(dan.getKey(), bugFixActivity.getActivityKey(),
                               project.getProjectKey(), 100);
        resourceService.assign(radha.getKey(), bugFixActivity.getActivityKey(),
                               project.getProjectKey(), 100);
        resourceService.assign(vladimir.getKey(), bugFixActivity.getActivityKey(),
                               project.getProjectKey(), 100);
        resourceService.assign(tom.getKey(), bugFixActivity.getActivityKey(),
                               project.getProjectKey(), 100);
        resourceService.assign(urip.getKey(), bugFixActivity.getActivityKey(),
                               project.getProjectKey(), 100);
        resourceService.assign(jen.getKey(), bugFixActivity.getActivityKey(),
                               project.getProjectKey(), 100);

        // Create "DB persistence" activity.
        ActivityInterface dbActivity =
                projectService.addActivity(project.getProjectKey(),
                                           "Database persistence");
        dbActivity.setIsFixedStartDate(false);
        dbActivity.setIsFixedDuration(false);
        dbActivity.setDuration(120);  // 3 weeks
        project.associateActivities(bugFixActivity.getActivityKey(),
                                    dbActivity.getActivityKey());

        // Assign resources.
        resourceService.assign(chris.getKey(), dbActivity.getActivityKey(),
                               project.getProjectKey(), 100);
        resourceService.assign(vladimir.getKey(), dbActivity.getActivityKey(),
                               project.getProjectKey(), 100);
        resourceService.assign(urip.getKey(), dbActivity.getActivityKey(),
                               project.getProjectKey(), 100);

        // Create "XML persistence" activity.
        ActivityInterface xmlActivity =
                projectService.addActivity(project.getProjectKey(),
                                           "XML persistence");
        xmlActivity.setIsFixedStartDate(false);
        xmlActivity.setIsFixedDuration(false);
        xmlActivity.setDuration(120);  // 3 weeks
        project.associateActivities(bugFixActivity.getActivityKey(),
                                    xmlActivity.getActivityKey());

        // Assign resources.
        resourceService.assign(alex.getKey(), xmlActivity.getActivityKey(),
                               project.getProjectKey(), 100);
        resourceService.assign(tom.getKey(), xmlActivity.getActivityKey(),
                               project.getProjectKey(), 100);
        resourceService.assign(radha.getKey(), xmlActivity.getActivityKey(),
                               project.getProjectKey(), 100);

        // Create "GUI enhancements" activity.
        ActivityInterface guiActivity =
                projectService.addActivity(project.getProjectKey(),
                                           "GUI enhancements");
        guiActivity.setIsFixedStartDate(false);
        guiActivity.setIsFixedDuration(false);
        guiActivity.setDuration(80);  // 2 weeks
        project.associateActivities(bugFixActivity.getActivityKey(),
                                    guiActivity.getActivityKey());

        // Assign resources.
        resourceService.assign(dan.getKey(), guiActivity.getActivityKey(),
                               project.getProjectKey(), 100);
        resourceService.assign(jen.getKey(), guiActivity.getActivityKey(),
                               project.getProjectKey(), 100);

        // Create "VCT" activity.
        ActivityInterface vctActivity =
                projectService.addActivity(project.getProjectKey(),
                                           "Virtual Customer Team");
        vctActivity.setIsFixedStartDate(false);
        vctActivity.setIsFixedDuration(false);
        vctActivity.setDuration(40);  // 1 week
        project.associateActivities(dbActivity.getActivityKey(),
                                    vctActivity.getActivityKey());
        project.associateActivities(xmlActivity.getActivityKey(),
                                    vctActivity.getActivityKey());

        // No resources for VCT activity.

        // Create "project management" activity.
        ActivityInterface pmActivity =
                projectService.addActivity(project.getProjectKey(),
                                           "Project management");
        pmActivity.setIsFixedStartDate(true);
        pmActivity.setIsFixedDuration(true);
        pmActivity.setDuration(120);  // 3 weeks

        // Assign resource.
        resourceService.assign(radha.getKey(), pmActivity.getActivityKey(),
                               project.getProjectKey(), 100);

        // Return the project.
        return project;
    }

    /**
     *  Creates generic resource pool.
     */
    private final void setupResourcePool()
            throws ResourceException
    {
        // Create new resource pool.
        ResourceService resourceService = ResourceService.getInstance();
        resourcePool = resourceService.createResourcePool();

        // Create some resources.
        alex = resourceService.createResource("Alex Chou");
        chris = resourceService.createResource("Chris Cochran");
        dan = resourceService.createResource("Dan Nathan");
        radha = resourceService.createResource("Radha Coutinho");
        vladimir= resourceService.createResource("Vladimir Chumakov");
        tom = resourceService.createResource("Tom Long");
        urip = resourceService.createResource("Urip Mangkusubroto");
        jen = resourceService.createResource("Jennifer Keiser");

        // Add the resources to the pool.
        resourcePool.addResource(alex);
        resourcePool.addResource(chris);
        resourcePool.addResource(dan);
        resourcePool.addResource(radha);
        resourcePool.addResource(vladimir);
        resourcePool.addResource(tom);
        resourcePool.addResource(urip);
        resourcePool.addResource(jen);
    }
}

/*
 *  $Log: TestProjectXmlWriter.java,v $
 *  Revision 1.4  2005/05/14 22:59:25  tlong2
 *  cleaned up tests
 *
 *  Revision 1.3  2005/05/09 00:04:07  achou
 *  Changed name of second test project.
 *
 *  Revision 1.2  2005/05/08 22:50:51  achou
 *  Added a second test project.
 *
 *  Revision 1.1  2005/05/06 06:05:49  achou
 *  New JUnit test for ProjectXmlWriter.
 *
 */
