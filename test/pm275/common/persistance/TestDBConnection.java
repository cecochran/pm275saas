/*
 * File: $RCSfile: TestDBConnection.java,v $
 * @author $Author: vchumako $,
 * @version $Id: TestDBConnection.java,v 1.1 2005/05/01 05:11:36 vchumako Exp $
 * Assignment: CSCI E-275 Team 1 Project
 * <p>
 * Contents: methods to test DBConnection.java
 * </p>
 * <p>
 * Purpose: make sure DBConnection works properly
 * </p>
 */
package pm275.common.persistance;

import junit.framework.TestCase;
import java.sql.Connection;

import pm275.common.exception.PersistanceException;

/**
 * @author vc
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TestDBConnection extends TestCase {
	private static final String url = "jdbc:mysql:///pm275_team1";

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Constructor for TestDBConnection.
	 * @param arg0
	 */
	public TestDBConnection(String arg0) {
		super(arg0);
	}
	
	public void testGetConnection() {
		try {
			Connection conn = DBConnection.getConnection(url);
			assertNotNull(conn);
		} catch(PersistanceException pe) {
			fail("Can't get '" + url + "' connection: " + pe.getMessage());		
		}
	}
}
