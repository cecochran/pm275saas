/*
 *  CSCIE 275 - Software Architecture and Engineering
 *
 *  File: $RCSfile: TestProjectXmlReader.java,v $
 *  @author $Author: achou $,
 *  @version $Revision: 1.1 $
 *
 *  <p></p>
 *
 *  $Id: TestProjectXmlReader.java,v 1.1 2005/05/06 23:55:10 achou Exp $
 */
package pm275.common.persistance;

import junit.framework.TestCase;

import java.util.List;
import java.util.Iterator;
import java.io.File;

import pm275.core.project.ProjectInterface;
import pm275.core.project.ActivityInterface;
import pm275.core.project.Activity;
import pm275.core.resource.ResourceInterface;
import pm275.core.calendar.CalendarInterface;
import pm275.common.exception.PersistanceException;


public class TestProjectXmlReader extends TestCase
{
    private File inputFile;


    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        inputFile = new File("pm275.test-out.xml");
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    /**
     *  Comprehensive test.
     */
    public void testProjectFromXml() throws PersistanceException
    {
        // Create the reader.
        ProjectXmlReader reader = new ProjectXmlReader(inputFile);

        // Read the project.
        ProjectInterface project = reader.readProject();

        // Display what we got.
        System.out.println("--------------------");
        System.out.println("Project:");
        System.out.println("");
        System.out.println(project.toString());
        System.out.println("");
        for (Iterator i = project.getActivities(Activity.OrderComparator.ascending()).iterator(); i.hasNext(); )
        {
            ActivityInterface activity = (ActivityInterface) i.next();
            System.out.println("    --------------------");
            System.out.println("    Activity:");
            System.out.println("");
            System.out.println(activity.toString());
            System.out.println("    --------------------");
        }
        System.out.println("--------------------");
    }
}

/*
 *  $Log: TestProjectXmlReader.java,v $
 *  Revision 1.1  2005/05/06 23:55:10  achou
 *  New JUnit test for ProjectXmlReader.
 *
 */
