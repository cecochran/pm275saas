/**
 * File:        $RCSfile: TestTableColumnData.java,v $
 * @author      Jennifer Keiser
 * @version     $Id: TestTableColumnData.java,v 1.3 2005/04/02 15:23:33 jkeiser Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * 
 * Contents:	Tests for the TableColumnData class.
 * Purpose:		Tests the TableColumnData class.
 */
package pm275.gui.panels;

import java.lang.Exception;
import junit.framework.TestCase;

public class TestTableColumnData extends TestCase 
{
    /**
     * Tests that table column data can be established without error 
     */
	public void testTableColumnData() 
    {
        try {
        	TableColumnData tblColData = new TableColumnData("test", 1, 30 );
    		// test the components that should be present
    		assertNotNull( tblColData.colTitle );
    		assertTrue( tblColData.colWidth != 0 );
    		assertTrue( tblColData.colAlignment != 0 );		
        }      
        catch (Exception e) {
        	fail( "Establishing table structure should not have failed" );
		} 
    }
}
/*
 *  $Log: TestTableColumnData.java,v $
 *  Revision 1.3  2005/04/02 15:23:33  jkeiser
 *  Cleaned up tests.  Added CVS $Log.
 *
 */