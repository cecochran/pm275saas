/**
 * File:        $RCSfile: TestProjectTableData.java,v $
 * @author      Jennifer Keiser
 * @version     $Id: TestProjectTableData.java,v 1.1 2005/04/02 15:21:48 jkeiser Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * 
 * Contents:	Tests for the ProjectTableData class.
 * Purpose:		Tests the ProjectTableData class.
 */
package pm275.gui.panels;

import java.lang.Exception;
import java.text.DateFormat;
import pm275.core.project.*;
import junit.framework.TestCase;

public class TestProjectTableData extends TestCase 
{
	private ProjectService 	projectService;
	private Project 		testProject;
	private ProjectTableData prjTblData;
	private Activity		activity2, activity3;
	
	/*	
     * Set up for tests
     */	
	protected void Setup() {
			
        try
        {
            projectService = new ProjectService();
        	testProject = projectService.addProject("project 1");
            testProject.setStartDate(DateFormat.getDateInstance().parse("2005-04-01"));
    		projectService.addActivity(testProject.getProjectKey(), "activity1.1");
        }
        catch (Exception ex)
        {
        	fail( "Creating project should not have failed" );
        }
	}
	
	/**
     * Tests that project table data can be established without error 
     */
	public void testEstablishTableData() 
    {
//Need to debug -- this is failing in ProjectTableData at:
//this.activities = project.getActivities(Activity.KeyComparator.ascending());
		//try {   	   	
        //	prjTblData = new ProjectTableData(testProject);
        //}      
        //catch (Exception e) {
        // 	fail( "Establishing table structure should not have failed" );
		//} 
   		// Test the components that should be present
		//assertNotNull( prjTblData.project );
		//assertNotNull( prjTblData.activities );        
    }
	
	/**
     * Tests that all get methods return valid responses 
     */
	public void testGetters() {
		//assertTrue( prjTblData.getColumnCount() != 0 );	
		//assertTrue( prjTblData.getRowCount() != 0 );
		//assertTrue( prjTblData.getColumnWidth(0) != 0 );
		//assertTrue( prjTblData.getColumnAlign(0) != 0 );		
	}
	
	/**
     * Tests that all methods that add activities do so successfully  
     */
	public void testAdds() 
	{
		Activity testActivity2, testActivity3;
		try {	
			//add tests if the addRow methods remain
	    }      
	    catch (Exception e) {
	    	fail( "Establishing/adding activities should not have failed" );
		} 

	}
}
/*
 *  $Log: TestProjectTableData.java,v $
 *  Revision 1.1  2005/04/02 15:21:48  jkeiser
 *  Initial checkin.
 *
 */