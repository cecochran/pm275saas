/**
 * File:        $RCSfile: TestProjectPanel.java,v $
 * @author      Jennifer Keiser
 * @version     $Id: TestProjectPanel.java,v 1.3 2005/03/30 16:04:05 dnathan Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * 
 * Contents:	Tests for the ProjectPanel class.
 * Purpose:		Tests the ProjectPanel class.
 */
package pm275.gui.panels;

import java.lang.Exception;
import junit.framework.TestCase;

public class TestProjectPanel extends TestCase 
{
	//MainDriver driver = new MainDriver();
	
    /**
     * Tests that an empty project can be established and displayed 
     */
	public void testEmptyProject() throws Exception
    {
        /*
        // This test has been commented since it requires user intervention.
        
        super.setUp();
        
        MainPanel mainpanel = new MainPanel(driver);
  
        try 
		{
        	ProjectService projectService = ProjectService.getInstance();
        	ProjectPanel panel = new ProjectPanel(mainpanel, null );

    		// test the components that should be present
    		assertNotNull( panel.tblProject );
    		assertNotNull( panel.tableData );
    		
		}
        catch (Exception e) 
		{
        	fail( "Establishing project should not have failed" );
		} */
    }
        
    /**
     * Tests that an nonempty project can be established and displayed 
     */
	public void testNonEmptyProject() throws Exception
    {
		/*
        // This test has been commented since it requires user intervention.
        
        super.setUp();
        MainPanel mainpanel = new MainPanel(driver);
  
        try 
		{
        	ProjectService projectService = ProjectService.getInstance();
        	Project project1 = projectService.addProject("project1");
        	projectService.addActivity(project1.getProjectKey(), "activity1.1");
        	ProjectPanel panel = new ProjectPanel(mainpanel, project1 );        	
    		
    		// test the components that should be present
    		assertNotNull( panel.tblProject );
    		assertNotNull( panel.tableData );
    		assertNotNull( panel.getProject() );
    		
		}
        catch (Exception e) 
		{
        	fail( "Establishing project should not have failed" );
		} 
		*/
    }
        
}
