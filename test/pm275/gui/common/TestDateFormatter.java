/***************************************************
 * File:        $RCSfile: TestDateFormatter.java,v $
 * @author      $Author: dnathan $,
 * @version     $Id: TestDateFormatter.java,v 1.5 2005/03/30 17:31:46 dnathan Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:    Functions for testing the pm275.gui.common.DateFormatter class.
 * </p>
 * <p>
 * Purpose:     Tests the pm275.gui.common.DateFormatter class.
 * </p>
 */
package pm275.gui.common;

import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;

import junit.framework.TestCase;

public class TestDateFormatter extends TestCase
{
    /**
     * Test that the class returns an empty string for a null date.
     */
    public void testGetStringForNullDate()
    {
        Date testDate = null;
        String testValue = DateFormatter.getStringForDate( testDate );
        assertTrue( new String("").equals( testValue ) );
    }

    /**
     * Tests that the class returns a valid string for a valid date.
     */
    public void testGetStringForValidDate()
    {
		Calendar cal = Calendar.getInstance();
		cal.set( Calendar.YEAR , 2004 );
		cal.set( Calendar.MONTH , 11 );
		cal.set( Calendar.DATE , 25 );
        String testValue = DateFormatter.getStringForDate( cal.getTime() );
        assertNotNull( testValue );
		assertTrue( "2004-12-25".equals(testValue) );
    }

    /**
     * Tests that the class returns a valid response for an invalid date.
     */
    public void testGetStringForInvalidDate()
    {
        Date testDate = new Date(-1);
        String testValue = DateFormatter.getStringForDate( testDate );
        assertNotNull( testValue );
    }

    /**
     * Tests that the method returns a null Date for a null String
     */
    public void testGetDateForNullString()
    {
        String testString = null;
        Date testValue = null;
        try
        {
            testValue = DateFormatter.getDateForString( testString );
        }
        catch (ParseException ex)
        {
			// test failed
			fail( "Should not have gotten a ParseException for a null string" );
        }

        assertNull( testValue );
    }

    /**
     *  Tests that the method returns a null Date for an empty String
     */
    public void testGetDateForEmptyString()
    {
        String testString = "";
        Date testValue = null;
        try
        {
            testValue = DateFormatter.getDateForString( testString );
        }
        catch (ParseException ex)
        {
			// test failed
			fail( "Should not have gotten a ParseException for an empty string" );
        }

        assertNull( testValue );
    }

    /**
     * Tests that the method returns the right Date for a valid String.
     */
    public void testGetDateForValidString()
    {
		String testString = "2004-11-25";
        Date testValue = null;
        try
        {
            testValue = DateFormatter.getDateForString( testString );
        }
        catch (ParseException ex)
        {
			// test failed
			fail( "Should not have gotten a ParseException for a valid string" );
        }
		
		Calendar cal = Calendar.getInstance();
		cal.setTime( testValue );
		
        assertEquals( 2004 , cal.get( Calendar.YEAR ) );
        assertEquals( 10 , cal.get( Calendar.MONTH )  );
        assertEquals( 25 , cal.get( Calendar.DATE )  );
    }

    /**
     * Tests that the method returns a null Date for an invalid String.
     */
    public void testGetDateForInvalidString()
    {
        String testString = "not a date";
        Date testValue = null;
        try
        {
            testValue = DateFormatter.getDateForString( testString );
        }
		catch (ParseException e) 
		{
			// test suceeded
			return;
		}
		
		fail( "Test of invalid datestring should have failed" );
    }
}


/**
 * $Log: TestDateFormatter.java,v $
 * Revision 1.5  2005/03/30 17:31:46  dnathan
 * Fixed year problem.
 *
 * Revision 1.4  2005/03/30 16:04:06  dnathan
 * Removed unneeded imports.
 *
 * Revision 1.3  2005/03/29 05:10:44  dnathan
 * Changes to failure behavior.
 *
 * Revision 1.2  2005/03/29 04:57:35  achou
 * Added try...catch for ParseException thrown by DateFormatter.getDateForString().
 *
 * Revision 1.1  2005/03/29 03:17:33  dnathan
 * New file.
 *
 *
 **/
