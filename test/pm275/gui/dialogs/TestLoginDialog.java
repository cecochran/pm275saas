/**
 * File:        $RCSfile: TestLoginDialog.java,v $
 * @author      $Author: dnathan $,
 * @version     $Id: TestLoginDialog.java,v 1.3 2005/03/29 06:02:45 dnathan Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:	Tests for the LoginDialog class.
 * </p>
 * <p>
 * Purpose:		Tests the LoginDialog class.
 * </p>
 */
package pm275.gui.dialogs;

import javax.swing.JFrame;
import junit.framework.TestCase;

public class TestLoginDialog extends TestCase 
{
    /**
     * Tests that all the expected components are being displayed.
     */
    public void testComponentPresence()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Please Login";
		LoginDialog dialog = new LoginDialog( frame , dialogTitle , false , null );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		// here are the components that should be present in the dialog:
		assertNotNull( dialog.okButton );
		assertNotNull( dialog.cancelButton );
		assertNotNull( dialog.usernameLabel );
		assertNotNull( dialog.usernameField );
		assertNotNull( dialog.passwordLabel );
		assertNotNull( dialog.passwordField );
		
		// make sure that the presented text is correct
		assertTrue( dialog.getTitle().equals(dialogTitle) );
		assertTrue( dialog.usernameField.getText().equals("") );
		assertTrue( dialog.passwordField.getText().equals("") );
		assertTrue( dialog.usernameLabel.getText().trim().equals("Username:") );
		assertTrue( dialog.passwordLabel.getText().trim().equals("Password:") );
		
		// make sure that the fields can be edited
		assertTrue( dialog.usernameField.isEditable() );
		assertTrue( dialog.passwordField.isEditable() );
		
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that all expected components are visible when a config is passed in.
	 */
	public void testComponentPresenceWithConfig()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Please Login";
		String username = "admin";
		String password = "password";
		
		LoginDialogResult config = new LoginDialogResult();
		
		try 
		{
			config.setUsername(username);
			config.setPassword(password);
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid username and password should not have failed" );
		}
		
		LoginDialog dialog = new LoginDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		// here are the components that should be present in the dialog:
		assertNotNull( dialog.okButton );
		assertNotNull( dialog.cancelButton );
		assertNotNull( dialog.usernameLabel );
		assertNotNull( dialog.usernameField );
		assertNotNull( dialog.passwordLabel );
		assertNotNull( dialog.passwordField );
		
		// make sure that the presented text is correct
		assertTrue( dialog.getTitle().equals(dialogTitle) );
		assertTrue( dialog.usernameField.getText().equals(username) );
		assertTrue( dialog.passwordField.getText().equals(password) );
		assertTrue( dialog.usernameLabel.getText().trim().equals("Username:") );
		assertTrue( dialog.passwordLabel.getText().trim().equals("Password:") );
		
		// make sure that the fields can be edited
		assertTrue( dialog.usernameField.isEditable() );
		assertTrue( dialog.passwordField.isEditable() );
		
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that changing the state of an input and pressing the OK button causes the 
	 * config to be modified.
	 */
	public void testModifyConfig_OK()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Please Login";
		String username = "admin";
		String username2 = "admin2";
		String password = "password";
		String password2 = "password2";
		
		LoginDialogResult config = new LoginDialogResult();
		
		try 
		{
			config.setUsername(username);
			config.setPassword(password);
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid username and password should not have failed" );
		}
		
		LoginDialog dialog = new LoginDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		dialog.usernameField.setText( username2 );
		dialog.passwordField.setText( password2 );
		
		dialog.okButton.doClick();
		
		assertTrue( config.getIsCompleted() );
		assertTrue( config.getUsername().equals(username2) );
		assertTrue( config.getPassword().equals(password2) );
				
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that clicking the cancel button causes the config to not be modified.
	 */
	public void testModifyConfig_Cancel()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Please Login";
		String username = "admin";
		String username2 = "admin2";
		String password = "password";
		String password2 = "password2";
		
		LoginDialogResult config = new LoginDialogResult();
		
		try 
		{
			config.setUsername(username);
			config.setPassword(password);
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid username and password should not have failed" );
		}
		
		LoginDialog dialog = new LoginDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		dialog.usernameField.setText( username2 );
		dialog.passwordField.setText( password2 );
		
		dialog.cancelButton.doClick();
		
		assertFalse( config.getIsCompleted() );
				
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that invalid usernames are not accepted when the OK button is pressed.
	 */
	public void testModifyConfig_OK_InvalidUsername()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Please Login";
		String username = "admin";
		String username2 = "";
		String password = "password";
		String password2 = "password2";
		
		LoginDialogResult config = new LoginDialogResult();
		
		try 
		{
			config.setUsername(username);
			config.setPassword(password);
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid username and password should not have failed" );
		}
		
		LoginDialog dialog = new LoginDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		dialog.usernameField.setText( username2 );
		dialog.passwordField.setText( password2 );
				
		dialog.okButton.doClick();
		
		assertTrue( dialog.hasError );
				
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that invalid passwords are not accepted when the OK button is pressed.
	 */
	public void testModifyConfig_OK_InvalidPassword()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Please Login";
		String username = "admin";
		String username2 = "admin2";
		String password = "password";
		String password2 = "";
		
		LoginDialogResult config = new LoginDialogResult();
		
		try 
		{
			config.setUsername(username);
			config.setPassword(password);
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid username and password should not have failed" );
		}
		
		LoginDialog dialog = new LoginDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		dialog.usernameField.setText( username2 );
		dialog.passwordField.setText( password2 );
				
		dialog.okButton.doClick();
		
		assertTrue( dialog.hasError );
				
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that invalid usernames are accepted when the cancel button is pressed.
	 */
	public void testModifyConfig_Cancel_InvalidUsername()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Please Login";
		String username = "admin";
		String username2 = "";
		String password = "password";
		String password2 = "password2";
		
		LoginDialogResult config = new LoginDialogResult();
		
		try 
		{
			config.setUsername(username);
			config.setPassword(password);
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid username and password should not have failed" );
		}
		
		LoginDialog dialog = new LoginDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		dialog.usernameField.setText( username2 );
		dialog.passwordField.setText( password2 );
				
		dialog.cancelButton.doClick();
		
		assertFalse( dialog.hasError );
				
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that invalid passwords are accepted when the cancel button is pressed.
	 */
	public void testModifyConfig_Cancel_InvalidPassword()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Please Login";
		String username = "admin";
		String username2 = "admin2";
		String password = "password";
		String password2 = "";
		
		LoginDialogResult config = new LoginDialogResult();
		
		try 
		{
			config.setUsername(username);
			config.setPassword(password);
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid username and password should not have failed" );
		}
		
		LoginDialog dialog = new LoginDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		dialog.usernameField.setText( username2 );
		dialog.passwordField.setText( password2 );
				
		dialog.cancelButton.doClick();
		
		assertFalse( dialog.hasError );
				
		// clean up
		dialog.dispose();
	}
}

/**
 * $Log: TestLoginDialog.java,v $
 * Revision 1.3  2005/03/29 06:02:45  dnathan
 * Now tests a live JDialog.
 *
 * Revision 1.2  2005/03/22 06:26:49  dnathan
 * Moved package.
 *
 * Revision 1.1  2005/03/22 05:55:30  dnathan
 * New file.
 *
 **/