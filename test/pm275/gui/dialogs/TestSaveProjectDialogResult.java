/*
 *  File      :  TestSaveProjectDialogResult.java
 *  Version   :  $Id: TestSaveProjectDialogResult.java,v 1.1 2005/05/01 18:13:35 dnathan Exp $
 *  Author    :  Dan Nathan
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */
package pm275.gui.dialogs;

import junit.framework.TestCase;

public class TestSaveProjectDialogResult extends TestCase 
{
    private SaveProjectDialogResult result = null;
    
    protected void setUp() throws Exception 
    {
        result = new SaveProjectDialogResult();
    }
    
    public void testGetIsCompleted() 
    {
        assertFalse( result.getIsCompleted() );
        result.setIsCompleted(true);
        assertTrue( result.getIsCompleted() );
    }

    public void testSetIsCompleted() 
    {
        result.setIsCompleted(true);
        assertTrue( result.getIsCompleted() );
    }

    public void testGetName() throws Exception
    {
        assertNull( this.result.getName() );
        this.result.setName( "New Name" );
        assertTrue( "New Name".equals( this.result.getName() ) );
    }

    public void testSetName() throws Exception
    {
        this.result.setName( "New Name" );
        assertTrue( "New Name".equals( this.result.getName() ) );
        try
        {
            this.result.setName(null);
            fail( "Should have gotten an exception" );
        }
        catch(Exception e){}
        try
        {
            this.result.setName("");
            fail( "Should have gotten an exception" );
        }
        catch(Exception e){}
    }

}


/**
 * $Log: TestSaveProjectDialogResult.java,v $
 * Revision 1.1  2005/05/01 18:13:35  dnathan
 * New file.
 *
 *
 **/