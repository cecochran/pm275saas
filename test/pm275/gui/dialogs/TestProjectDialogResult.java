/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestProjectDialogResult.java,v $ 
 * @author $Author: dnathan $, 
 * @version $Revision: 1.3 $
 * 
 * <p>
 * This class tests the ProjectDialogResult class.
 * </p>
 * 
 * $Id: TestProjectDialogResult.java,v 1.3 2005/03/22 07:19:41 dnathan Exp $
 */
package pm275.gui.dialogs;

import java.util.GregorianCalendar;
import java.util.Date;

import junit.framework.TestCase;

public class TestProjectDialogResult extends TestCase 
{
	private ProjectDialogResult projectDialogResult = null;
	
	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception 
	{
		super.setUp();
		this.projectDialogResult = new ProjectDialogResult();
		this.projectDialogResult.setName( "name" );
		this.projectDialogResult.setStartDate( GregorianCalendar.getInstance().getTime() );
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception 
	{
		super.tearDown();
		this.projectDialogResult = null;
	}

	public void testGetName() 
	{
		if( this.projectDialogResult.getName() == null )
		{
			fail( "ProjectDialogResult.getName failed.  Got null result." );
		}
	}
	
	public void testProjectDialogResult()
	{
		if( new ProjectDialogResult() == null )
		{
			fail( "ProjectDialogResult() constructor failed" );
		}
	}

	public void testSetName() 
	{
		String newName = this.projectDialogResult.getName() + "This is a new name.";
		
		try
		{
			this.projectDialogResult.setName( newName );
		}
		catch( ParameterFormatException e )
		{
			fail( "A valid name caused a ParmaterFormatException" );
		}
		
		if( ! newName.equals( this.projectDialogResult.getName() ) )
		{
			fail( "ProjectDialogResult.setName failed.  Result was not saved." );
		}
	}
	
	public void testEmptyName()
	{
		try
		{
			this.projectDialogResult.setName( "" );
			fail( "Passing an empty name to ProjectDialogResult.setName() should have failed" );
		}
		catch( ParameterFormatException e )
		{
			return;
		}
	}
	
	public void testNullName()
	{
		try
		{
			this.projectDialogResult.setName( null );
			fail( "Passing an null name to ProjectDialogResult.setName() should have failed" );
		}
		catch( ParameterFormatException e )
		{
			return;
		}
	}

	public void testGetStartDate() 
	{
		if( this.projectDialogResult.getStartDate() == null )
		{
			fail( "ProjectDialogResult.getStartDate failed.  Got null result." );
		}
	}

	public void testSetStartDate()
	{
		Date newDate = GregorianCalendar.getInstance().getTime();
		
		try
		{
			this.projectDialogResult.setStartDate( newDate );
		}
		catch( ParameterFormatException e )
		{
			fail( "A valid date caused a ParmaterFormatException" );
		}
		
		if( ! newDate.equals( this.projectDialogResult.getStartDate() ) )
		{
			fail( "ProjectDialogResult.setStartDate failed.  Result was not saved." );
		}
	}
	
	public void testNullStartDate()
	{
		try
		{
			this.projectDialogResult.setStartDate( null );
			fail( "Passing null to ProjectDialogResult.setStartDate() should have failed" );
		}
		catch( ParameterFormatException e )
		{
			return;
		}
	}
}

/**
 * $Log: TestProjectDialogResult.java,v $
 * Revision 1.3  2005/03/22 07:19:41  dnathan
 * Added tests to check for null or empty names and start dates.
 *
 * Revision 1.2  2005/03/22 06:26:49  dnathan
 * Moved package.
 *
 * Revision 1.1  2005/03/22 05:55:29  dnathan
 * New file.
 *
 **/