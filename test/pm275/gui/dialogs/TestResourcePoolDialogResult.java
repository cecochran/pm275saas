/***************************************************
 * File: 		$RCSfile: TestResourcePoolDialogResult.java,v $ 
 * @author 		$Author: dnathan $, 
 * @version 	$Id: TestResourcePoolDialogResult.java,v 1.1 2005/04/03 04:38:55 dnathan Exp $
 * Assignment:	CSCI E-275 Team 1 Project
 * <p>
 * Contents:	A class for communicating between the main GUI and the resource edit/create dialog.
 * </p>
 * <p>
 * Purpose: 	This class is used to communicate Resource details between the "New Resource"
 * 				and "Resource Activity" dialogs and the application.
 * </p>
 */
package pm275.gui.dialogs;

import pm275.core.resource.ResourceKey;
import junit.framework.TestCase;

public class TestResourcePoolDialogResult extends TestCase 
{
	private ResourcePoolDialogResult result = null;
	
	public void setUp() throws Exception
	{
		this.result = new ResourcePoolDialogResult();
		this.result.setName( "Valid name" );
		ResourceKey[] keys = new ResourceKey[2];
		keys[0] = new ResourceKey();
		keys[1] = new ResourceKey();
		this.result.setResources( keys );
	}
	
	public void testResourcePoolDialogResult()
	{
		if( new ResourcePoolDialogResult() == null )
		{
			fail( "ResourcePoolDialogResult constructor failed" );
		}
	}

	public void testGetName() 
	{
		if( this.result.getName() == null )
		{
			fail( "ResourcePoolDialogResult.getName failed.  Got null result." );
		}
	}

	public void testSetName() 
	{
		String newName = this.result.getName() + "This is a new name.";
		
		try
		{
			this.result.setName( newName );
		}
		catch( ParameterFormatException e )
		{
			fail( "A valid name caused a ParmaterFormatException" );
		}
		
		if( ! newName.equals( this.result.getName() ) )
		{
			fail( "ResourcePoolDialogResult.setName failed.  Result was not saved." );
		}
	}
	
	public void testNullName()
	{
		try
		{
			this.result.setName(null);
			fail( "ResourcePoolDialogResult should have failed when setName was called with a null" );
		}
		catch( ParameterFormatException e )
		{
			return;
		}
	}
	
	public void testEmptyName()
	{
		try
		{
			this.result.setName("");
			fail( "ResourcePoolDialogResult should have failed when setName was called with an empty value" );
		}
		catch( ParameterFormatException e )
		{
			return;
		}
	}
	
	public void testGetResources()
	{
		assertNotNull( this.result.getResources() );
		assertTrue( this.result.getResources().length == 2 );
	}
	
	public void testSetResources()
	{
		ResourceKey[] keys = new ResourceKey[2];
		keys[0] = new ResourceKey();
		keys[1] = new ResourceKey();
		
		try 
		{
			this.result.setResources( keys );
		}
		catch (ParameterFormatException e) 
		{
			fail( "Got error with a valid set of resources" );
		}
		
		assertTrue( this.result.getResources().length == keys.length );
		
		for( int x = 0 ; x < keys.length ; x++ )
		{
			assertTrue( keys[x] == this.result.getResources()[x] );
		}
	}
	
	public void testSetNullResources()
	{
		try 
		{
			this.result.setResources( null );
		}
		catch (ParameterFormatException e) 
		{
			// success
			return;
		}
		
		fail( "Should have gotten error with null value" );
	}
}
/**
 * $Log: TestResourcePoolDialogResult.java,v $
 * Revision 1.1  2005/04/03 04:38:55  dnathan
 * New file.
 *
 *
 */