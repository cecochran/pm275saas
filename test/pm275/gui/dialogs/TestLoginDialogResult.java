/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestLoginDialogResult.java,v $ 
 * @author $Author: dnathan $, 
 * @version $Revision: 1.3 $
 * 
 * <p>
 * This class tests the pm275.gui.dialogs.LoginDialogResult class.
 * </p>
 * 
 * $Id: TestLoginDialogResult.java,v 1.3 2005/03/22 07:09:55 dnathan Exp $
 */
package pm275.gui.dialogs;

import junit.framework.TestCase;
import pm275.gui.dialogs.LoginDialogResult;
import pm275.gui.dialogs.ParameterFormatException;

/**
 * This class tests the pm275.gui.dialogs.LoginDialogResult class.
 */
public class TestLoginDialogResult extends TestCase 
{
	private LoginDialogResult result = null;
	
	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception 
	{
		super.setUp();
		this.result = new LoginDialogResult();
		this.result.setUsername( "username" );
		this.result.setPassword( "password" );
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception 
	{
		super.tearDown();
		this.result = null;
	}
	
	public void testLoginDialogResult() throws Exception
	{
		if( new LoginDialogResult() == null )
		{
			fail( "LoginDialogResult() constructor failed" );
		}
	}
	
	public void testUsername() throws Exception
	{
		String oldUsername = this.result.getUsername();
		String newUsername = oldUsername + "newUsername";
		
		this.result.setUsername( newUsername );
		
		if( oldUsername.equals( this.result.getUsername() ) )
		{
			fail( "LoginDialogResult.setUsername operation failed" );
		}
		
		if( ! newUsername.equals(this.result.getUsername() ) )
		{
			fail( "LoginDialogResult.getUsername operation failed" );
		}
	}
	
	public void testPassword() throws Exception
	{
		String oldPassword = this.result.getPassword();
		String newPassword = oldPassword + "newPassword";
		
		this.result.setPassword( newPassword );
		
		if( oldPassword.equals( this.result.getPassword() ) )
		{
			fail( "LoginDialogResult.setPassword operation failed" );
		}
		
		if( ! newPassword.equals(this.result.getPassword() ) )
		{
			fail( "LoginDialogResult.getPassword operation failed" );
		}
	}
	
	public void testNullPassword()
	{
		try
		{
			this.result.setPassword(null);
		}
		catch( ParameterFormatException e )
		{
			return;
		}
		
		fail( "Passing a null password to LoginDialogResult.setPassword() should have failed" );
	}
	
	public void testEmptyPassword()
	{
		try
		{
			this.result.setPassword("");
		}
		catch( ParameterFormatException e )
		{
			return;
		}
		
		fail( "Passing a empty password to LoginDialogResult.setPassword() should have failed" );
	}
	
	public void testNullUsername()
	{
		try
		{
			this.result.setUsername(null);
		}
		catch( ParameterFormatException e )
		{
			return;
		}
		
		fail( "Passing a null username to LoginDialogResult.setUsername() should have failed" );
	}
	
	public void testEmptyUsername()
	{
		try
		{
			this.result.setUsername("");
		}
		catch( ParameterFormatException e )
		{
			return;
		}
		
		fail( "Passing a null username to LoginDialogResult.setUsername() should have failed" );
	}
}

/**
 * $Log: TestLoginDialogResult.java,v $
 * Revision 1.3  2005/03/22 07:09:55  dnathan
 * Added tests to check for null or empty usernames and passwords.
 *
 * Revision 1.2  2005/03/22 06:27:14  dnathan
 * Implemented tests.
 *
 * Revision 1.1  2005/03/22 05:55:29  dnathan
 * New file.
 *
 **/