/***************************************************
 * File: 		$RCSfile: TestResourceDialogResult.java,v $ 
 * @author 		$Author: dnathan $, 
 * @version 	$Id: TestResourceDialogResult.java,v 1.1 2005/04/03 04:38:55 dnathan Exp $
 * Assignment:	CSCI E-275 Team 1 Project
 * <p>
 * Contents:	A class for testing the class used for communicating between the main GUI 
 * 				and the resource edit/create dialog.
 * </p>
 * <p>
 * Purpose: 	This class is used to test the class that is used to communicate Resource 
 * 				details between the "New Resource" and "Resource Activity" dialogs and the 
 * 				application.
 * </p>
 */
package pm275.gui.dialogs;

import junit.framework.TestCase;

public class TestResourceDialogResult extends TestCase 
{
	private ResourceDialogResult result = null;
	
	public void setUp() throws Exception
	{
		this.result = new ResourceDialogResult();
		this.result.setName( "Valid name" );
	}
	
	public void testResourceDialogResult()
	{
		if( new ResourceDialogResult() == null )
		{
			fail( "ResourceDialogResult constructor failed" );
		}
	}

	public void testGetName() 
	{
		if( this.result.getName() == null )
		{
			fail( "TestResourceDialogResult.getName failed.  Got null result." );
		}
	}

	public void testSetName() 
	{
		String newName = this.result.getName() + "This is a new name.";
		
		try
		{
			this.result.setName( newName );
		}
		catch( ParameterFormatException e )
		{
			fail( "A valid name caused a ParmaterFormatException" );
		}
		
		if( ! newName.equals( this.result.getName() ) )
		{
			fail( "ResourceDialogResult.setName failed.  Result was not saved." );
		}
	}
	
	public void testNullName()
	{
		try
		{
			this.result.setName(null);
			fail( "ResourceDialogResult should have failed when setName was called with a null" );
		}
		catch( ParameterFormatException e )
		{
			return;
		}
	}
	
	public void testEmptyName()
	{
		try
		{
			this.result.setName("");
			fail( "ResourceDialogResult should have failed when setName was called with an empty value" );
		}
		catch( ParameterFormatException e )
		{
			return;
		}
	}
}
/**
 * $Log: TestResourceDialogResult.java,v $
 * Revision 1.1  2005/04/03 04:38:55  dnathan
 * New file.
 *
 *
 */