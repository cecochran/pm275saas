/**
 * File:        $RCSfile: TestActivityDialog.java,v $
 * @author      $Author: dnathan $,
 * @version     $Id: TestActivityDialog.java,v 1.3 2005/03/29 06:26:08 dnathan Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:	Tests for the ActivityDialog class.
 * </p>
 * <p>
 * Purpose:		Tests the ActivityDialog class.
 * </p>
 */
package pm275.gui.dialogs;

import java.text.ParseException;

import javax.swing.JFrame;

import pm275.gui.common.DateFormatter;
import junit.framework.TestCase;

public class TestActivityDialog extends TestCase 
{
    /**
     * Tests that all the expected components are being displayed.
     */
    public void testComponentPresence()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Activity Details";
		ActivityDialog dialog = new ActivityDialog( frame , dialogTitle , false , null );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		// here are the components that should be present in the dialog:
		assertNotNull( dialog.okButton );
		assertNotNull( dialog.cancelButton );
		assertNotNull( dialog.nameField );
		assertNotNull( dialog.nameLabel );
		assertNotNull( dialog.startDateField );
		assertNotNull( dialog.startDateLabel );
		
		// make sure that the presented text is correct
		assertTrue( dialog.getTitle().equals(dialogTitle) );
		assertTrue( dialog.nameField.getText().equals("") );
		assertTrue( dialog.startDateField.getText().equals("") );
		assertTrue( dialog.nameLabel.getText().trim().equals("Activity Name:") );
		assertTrue( dialog.startDateLabel.getText().trim().equals("Activity Start Date:") );
		
		// make sure that the fields can be edited
		assertTrue( dialog.nameField.isEditable() );
		assertTrue( dialog.startDateField.isEditable() );
		
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that all expected components are visible when a config is passed in.
	 */
	public void testComponentPresenceWithConfig()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Activity Details";
		String name = "admin";
		String startDate = "2004-12-12";
		
		ActivityDialogResult config = new ActivityDialogResult();
		
		try 
		{
			config.setName(name);
			config.setStartDate( DateFormatter.getDateForString( startDate ) );
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		} 
		catch (ParseException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		}
		
		ActivityDialog dialog = new ActivityDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		// here are the components that should be present in the dialog:
		assertNotNull( dialog.okButton );
		assertNotNull( dialog.cancelButton );
		assertNotNull( dialog.nameField );
		assertNotNull( dialog.nameLabel );
		assertNotNull( dialog.startDateField );
		assertNotNull( dialog.startDateLabel );
		
		// make sure that the presented text is correct
		assertTrue( dialog.getTitle().equals(dialogTitle) );
		assertTrue( dialog.nameField.getText().equals(name) );
		assertTrue( dialog.startDateField.getText().equals(startDate) );
		assertTrue( dialog.nameLabel.getText().trim().equals("Activity Name:") );
		assertTrue( dialog.startDateLabel.getText().trim().equals("Activity Start Date:") );
		
		// make sure that the fields can be edited
		assertTrue( dialog.nameField.isEditable() );
		assertTrue( dialog.startDateField.isEditable() );
		
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that changing the state of an input and pressing the OK button causes the 
	 * config to be modified.
	 */
	public void testModifyConfig_OK()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Activity Details";
		String name = "admin";
		String name2 = "admin2";
		String startDate = "2004-12-12";
		String startDate2 = "2004-12-11";
		
		ActivityDialogResult config = new ActivityDialogResult();
		
		try 
		{
			config.setName(name);
			config.setStartDate( DateFormatter.getDateForString( startDate ) );
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		} 
		catch (ParseException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		}
		
		ActivityDialog dialog = new ActivityDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		dialog.nameField.setText( name2 );
		dialog.startDateField.setText( startDate2 );
		
		dialog.okButton.doClick();
		
		assertTrue( config.getIsCompleted() );
		assertTrue( config.getName().equals(name2) );
		try
		{
			assertTrue( config.getStartDate().equals( DateFormatter.getDateForString(startDate2)) );
		} 
		catch (ParseException e) 
		{
			fail( e.getMessage() );
		}
		
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that clicking the cancel button causes the config to not be modified.
	 */
	public void testModifyConfig_Cancel()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Activity Details";
		String name = "admin";
		String name2 = "admin2";
		String startDate = "2004-12-12";
		String startDate2 = "2004-12-11";
		
		ActivityDialogResult config = new ActivityDialogResult();
		
		try 
		{
			config.setName(name);
			config.setStartDate( DateFormatter.getDateForString( startDate ) );
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		} 
		catch (ParseException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		}
		
		ActivityDialog dialog = new ActivityDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		dialog.nameField.setText( name2 );
		dialog.startDateField.setText( startDate2 );
		
		dialog.cancelButton.doClick();
		
		assertFalse( config.getIsCompleted() );
		
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that invalid usernames are not accepted when the OK button is pressed.
	 */
	public void testModifyConfig_OK_InvalidName()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Activity Details";
		String name = "admin";
		String name2 = "";
		String startDate = "2004-12-12";
		String startDate2 = "2004-12-11";
		
		ActivityDialogResult config = new ActivityDialogResult();
		
		try 
		{
			config.setName(name);
			config.setStartDate( DateFormatter.getDateForString( startDate ) );
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		} 
		catch (ParseException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		}
		
		ActivityDialog dialog = new ActivityDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		dialog.nameField.setText( name2 );
		dialog.startDateField.setText( startDate2 );
		
		dialog.okButton.doClick();
		
		assertTrue( dialog.hasError );
		
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that invalid passwords are not accepted when the OK button is pressed.
	 */
	public void testModifyConfig_OK_InvalidStartDate()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Activity Details";
		String name = "admin";
		String name2 = "admin2";
		String startDate = "2004-12-12";
		String startDate2 = "";
		
		ActivityDialogResult config = new ActivityDialogResult();
		
		try 
		{
			config.setName(name);
			config.setStartDate( DateFormatter.getDateForString( startDate ) );
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		} 
		catch (ParseException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		}
		
		ActivityDialog dialog = new ActivityDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		dialog.nameField.setText( name2 );
		dialog.startDateField.setText( startDate2 );
		
		dialog.okButton.doClick();
		
		assertTrue( dialog.hasError );
		
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that invalid usernames are accepted when the cancel button is pressed.
	 */
	public void testModifyConfig_Cancel_InvalidName()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Activity Details";
		String name = "admin";
		String name2 = "";
		String startDate = "2004-12-12";
		String startDate2 = "2004-12-11";
		
		ActivityDialogResult config = new ActivityDialogResult();
		
		try 
		{
			config.setName(name);
			config.setStartDate( DateFormatter.getDateForString( startDate ) );
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		} 
		catch (ParseException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		}
		
		ActivityDialog dialog = new ActivityDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		dialog.nameField.setText( name2 );
		dialog.startDateField.setText( startDate2 );
		
		dialog.cancelButton.doClick();
		
		assertFalse( config.getIsCompleted() );
		
		// clean up
		dialog.dispose();
	}
	
	/**
	 * Tests that invalid passwords are accepted when the cancel button is pressed.
	 */
	public void testModifyConfig_Cancel_InvalidPassword()
	{
		JFrame frame = new JFrame();
		String dialogTitle = "Activity Details";
		String name = "admin";
		String name2 = "";
		String startDate = "2004-12-12";
		String startDate2 = "2004-12-11";
		
		ActivityDialogResult config = new ActivityDialogResult();
		
		try 
		{
			config.setName(name);
			config.setStartDate( DateFormatter.getDateForString( startDate ) );
		}
		catch (ParameterFormatException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		} 
		catch (ParseException e) 
		{
			fail( "Setting valid name and start date should not have failed" );
		}
		
		ActivityDialog dialog = new ActivityDialog( frame , dialogTitle , false , config );
		dialog.inTesting = true;
		dialog.hasError = false;
		
		dialog.nameField.setText( name2 );
		dialog.startDateField.setText( startDate2 );
		
		dialog.cancelButton.doClick();

		assertFalse( config.getIsCompleted() );
		
		// clean up
		dialog.dispose();
	}
}

/**
 * $Log: TestActivityDialog.java,v $
 * Revision 1.3  2005/03/29 06:26:08  dnathan
 * Now tests a live JDialog.
 *
 *
 **/