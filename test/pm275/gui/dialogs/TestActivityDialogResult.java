/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestActivityDialogResult.java,v $ 
 * @author $Author: dnathan $, 
 * @version $Revision: 1.6 $
 * 
 * <p></p>
 * 
 * $Id: TestActivityDialogResult.java,v 1.6 2005/04/16 17:36:43 dnathan Exp $
 */
package pm275.gui.dialogs;

import pm275.core.project.ActivityKey;
import junit.framework.TestCase;

public class TestActivityDialogResult extends TestCase {
	private ActivityDialogResult activityDialogResult = null;
	
	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		this.activityDialogResult = new ActivityDialogResult();
		this.activityDialogResult.setName( "activityDialogResult" );
		this.activityDialogResult.setDuration( 10 );
		this.activityDialogResult.setDurationType( ActivityDialogResult.FIXED_DURATION );
		this.activityDialogResult.setDurationMeasure( ActivityDialogResult.HOURS );

	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testActivityDialogResult()
	{
		if( new ActivityDialogResult() == null )
		{
			fail( "ActivityDialogResult constructor failed" );
		}
	}

	public void testGetName() 
	{
		if( this.activityDialogResult.getName() == null )
		{
			fail( "ProjectDialogResult.getName failed.  Got null result." );
		}
	}

	public void testSetName() 
	{
		String newName = this.activityDialogResult.getName() + "This is a new name.";
		
		try
		{
			this.activityDialogResult.setName( newName );
		}
		catch( ParameterFormatException e )
		{
			fail( "A valid name caused a ParmaterFormatException" );
		}
		
		if( ! newName.equals( this.activityDialogResult.getName() ) )
		{
			fail( "ProjectDialogResult.setName failed.  Result was not saved." );
		}
	}
	
	public void testNullName()
	{
		try
		{
			this.activityDialogResult.setName(null);
			fail( "ActivityDialogResult should have failed when setName was called with a null" );
		}
		catch( ParameterFormatException e )
		{
			return;
		}
	}
	
	public void testEmptyName()
	{
		try
		{
			this.activityDialogResult.setName("");
			fail( "ActivityDialogResult should have failed when setName was called with an empty value" );
		}
		catch( ParameterFormatException e )
		{
			return;
		}
	}
	
	public void testGetDuration()
	{
		assertEquals( this.activityDialogResult.getDuration() , 10 );
	}
	
	public void testSetDuration()
	{
		try 
		{
			this.activityDialogResult.setDuration( 100 );
		}
		catch (ParameterFormatException e) 
		{
			fail( "Valid duration should not have caused Exception" );
		}
	}
	
	public void testSetDurationInvalid()
	{
		try 
		{
			this.activityDialogResult.setDuration( -100 );
		}
		catch (ParameterFormatException e) 
		{
			// success
			return;
		}
		
		fail( "Invalid duration should have caused Exception" );
	}
	
	public void testGetDurationType()
	{
		assertEquals( this.activityDialogResult.getDurationType() , ActivityDialogResult.FIXED_DURATION );		
	}
	
	public void testSetDurationType()
	{
		try 
		{
			this.activityDialogResult.setDurationType( ActivityDialogResult.EFFORT );
		}
		catch (ParameterFormatException e) 
		{
			fail( "Should not have gotten an Exception on valid duration type" );
		}
		try 
		{
			this.activityDialogResult.setDurationType( ActivityDialogResult.FIXED_DURATION );
		}
		catch (ParameterFormatException e) 
		{
			fail( "Should not have gotten an Exception on valid duration type" );
		}
	}
	
	public void testSetDurationTypeInvalid()
	{
		int NOSUCH = 42;
		try 
		{
			this.activityDialogResult.setDurationType( NOSUCH );
		}
		catch (ParameterFormatException e) 
		{
			// success
			return;
		}
		
		fail( "Should have gotten an Exception for invalid duration type" );
	}
	
	public void testGetPredecessors()
	{
		//assertNotNull( this.activityDialogResult.getPredecessors() );
	}
	
	public void testSetPredecessors()
	{
		ActivityKey[] a = new ActivityKey[3];
		a[0] = new ActivityKey();
		a[1] = new ActivityKey();
		a[2] = new ActivityKey();
		
		/*try 
		{
			//this.activityDialogResult.setPredecessors(a);
		} 
		catch (ParameterFormatException e) 
		{
			fail( "Should have succeeded with valid array" );
		}*/
		
		//assertEquals( this.activityDialogResult.getPredecessors().length , a.length );
		
		for( int x = 0 ; x < a.length ; x++ )
		{
			//assertEquals( a[x] , this.activityDialogResult.getPredecessors()[x] );
		}
	}
	
	public void testSetPredecessorsNull()
	{
		//try 
		//{
			//this.activityDialogResult.setPredecessors(null);
		//}
		//catch (ParameterFormatException e) 
		//{
			// success
		//	return;
		//}
		
		//fail( "Should have gotten Exception for null array" );
	}
	
	public void testSetPredecessorsEmpty()
	{
		/*try 
		{
			ActivityKey[] a = new ActivityKey[0];
			this.activityDialogResult.setPredecessors(a);
		}
		catch (ParameterFormatException e) 
		{
			// success
			return;
		}
		
		fail( "Should have gotten Exception for zero-length array" );*/
	}
	
	public void testGetDurationMeasure()
	{
		assertEquals( ActivityDialogResult.HOURS , this.activityDialogResult.getDurationMeasure() );
	}
	
	public void testSetDurationMeasure()
	{
		try
		{
			this.activityDialogResult.setDurationMeasure( ActivityDialogResult.DAYS );
		}
		catch( ParameterFormatException e )
		{
			fail( "Should not have gotten exception for valid duration measure" );
		}
		assertEquals( ActivityDialogResult.DAYS , this.activityDialogResult.getDurationMeasure() );
	}
	
	public void testSetDurationMeasureInvalid()
	{
		try
		{
			this.activityDialogResult.setDurationMeasure( -1 );
		}
		catch( ParameterFormatException e )
		{
			// success
			return;
		}
		
		fail( "Should have gotten exception for invalid duration measure" );
	}
}

/**
 * $Log: TestActivityDialogResult.java,v $
 * Revision 1.6  2005/04/16 17:36:43  dnathan
 * Various changes to make it compile.
 *
 * Revision 1.5  2005/04/03 20:37:13  dnathan
 * Added code to support measurement types of hours and days.
 *
 * Revision 1.4  2005/04/03 05:18:21  dnathan
 * Added new properties for duration, duration type and predecessors.
 *
 * Revision 1.3  2005/03/22 07:27:44  dnathan
 * Added tests to check for null or empty names.
 *
 * Revision 1.2  2005/03/22 06:26:49  dnathan
 * Moved package.
 *
 * Revision 1.1  2005/03/22 05:55:29  dnathan
 * New file.
 *
 **/