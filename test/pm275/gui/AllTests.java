/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: AllTests.java,v $ 
 * @author $Author: dnathan $, 
 * @version $Revision: 1.4 $
 * 
 * <p></p>
 * 
 * $Id: AllTests.java,v 1.4 2005/04/03 05:17:43 dnathan Exp $
 */
package pm275.gui;

import junit.framework.Test;
import junit.framework.TestSuite;
import pm275.gui.dialogs.*;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for gui dialogs");
		//$JUnit-BEGIN$
		suite.addTestSuite(TestActivityDialog.class);
		suite.addTestSuite(TestActivityDialogResult.class);
		suite.addTestSuite(TestLoginDialog.class);
		suite.addTestSuite(TestLoginDialogResult.class);
		suite.addTestSuite(TestProjectDialog.class);
		suite.addTestSuite(TestProjectDialogResult.class);
		suite.addTestSuite(TestResourceDialogResult.class);
		suite.addTestSuite(TestResourcePoolDialogResult.class);
		//$JUnit-END$
		return suite;
	}
}

/**
 * $Log: AllTests.java,v $
 * Revision 1.4  2005/04/03 05:17:43  dnathan
 * Added all dialog tests
 *
 * Revision 1.3  2005/03/30 16:04:06  dnathan
 * Removed unneeded imports.
 *
 * Revision 1.2  2005/03/22 07:28:18  dnathan
 * Updated to reflect currently available tests.
 *
 * Revision 1.1  2005/03/22 06:31:41  dnathan
 * New file.
 *
 * Revision 1.2  2005/03/22 06:26:49  dnathan
 * Moved package.
 *
 * Revision 1.1  2005/03/22 05:55:30  dnathan
 * New file.
 *
 **/