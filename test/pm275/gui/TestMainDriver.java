/**
 * CSCIE 275 - Software Architecture and Engineering
 *
 * File: $RCSfile: TestMainDriver.java,v $
 * @author $Author: dnathan $,
 * @version $Revision: 1.3 $
 *
 * <p></p>
 *
 * $Id: TestMainDriver.java,v 1.3 2005/03/30 15:07:59 dnathan Exp $
 */

package pm275.gui;

import junit.framework.TestCase;


public class TestMainDriver extends TestCase
{
    private MainDriver driver;


    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        super.setUp();
        //driver = new MainDriver();
        //driver.dispose();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
	
	public void testCreate()
	{
		/*
		 * this test commented since it requires user intervention
		 */
	}
}

/**
 * $Log: TestMainDriver.java,v $
 * Revision 1.3  2005/03/30 15:07:59  dnathan
 * Commented interactive tests.
 *
 * Revision 1.2  2005/03/25 20:53:24  achou
 * Fixed compile error.
 *
 * Revision 1.1  2005/03/24 02:05:53  achou
 * Initial check-in.
 *
 * Revision 1.2  2005/03/22 06:26:49  dnathan
 * Moved package.
 *
 * Revision 1.1  2005/03/22 05:55:30  dnathan
 * New file.
 *
 **/
