/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestCalendarKey.java,v $ 
 * @author $Author: rcoutinh $, 
 * @version $Revision: 1.3 $
 * 
 * <p></p>
 * 
 * $Id: TestCalendarKey.java,v 1.3 2005/03/29 03:58:35 rcoutinh Exp $
 */
package pm275.core.calendar;

import junit.framework.TestCase;


public class TestCalendarKey extends TestCase {
	
	/**
	 * Test constructor with no param
	 *
	 */
	public void testCalendarKeyConstructorNoParam()
	{
		CalendarKey key = new CalendarKey();
		assertNotNull(key);
	}
	
	/*
	 * Class under test for boolean equals(CalendarKey)
	 */
	public void testEqualsCalendarKey() {
		String keyStr = String.valueOf(System.currentTimeMillis());
		CalendarKey key = new CalendarKey(keyStr);
		assertEquals("Error", true,key.equals(new CalendarKey(keyStr)));
		
	}

	/*
	 * Class under test for String toString()
	 */
	public void testToString() {
		String keyStr = String.valueOf(System.currentTimeMillis());
		CalendarKey key = new CalendarKey(keyStr);
		assertEquals("Error", keyStr, key.toString());
	}
	
	public static void main(String[] args) {
		junit.textui.TestRunner.run(TestCalendarKey.class);
	}
}

/**
 * $Log: TestCalendarKey.java,v $
 * Revision 1.3  2005/03/29 03:58:35  rcoutinh
 * Test case for CalendarKey
 *
 * Revision 1.2  2005/03/28 01:19:14  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:33  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:21  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:32  dnathan
 * Updated CVS footer.
 *
 **/