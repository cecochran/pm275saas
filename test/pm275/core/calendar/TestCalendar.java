/**
 * CSCIE 275 - Software Architecture and Engineering
 *
 * File: $RCSfile: TestCalendar.java,v $
 * @author $Author: tlong2 $,
 * @version $Revision: 1.7 $
 *
 * <p></p>
 *
 * $Id: TestCalendar.java,v 1.7 2005/05/14 14:53:54 tlong2 Exp $
 */
package pm275.core.calendar;

import junit.framework.TestCase;
import pm275.core.calendar.Calendar;
import pm275.core.calendar.Holiday;

import java.util.Date;

public class TestCalendar extends TestCase {

    public void testIsCorporate() {
        Calendar calendar = new Calendar();
        boolean isCorporate = calendar.isCorporate();
        assertEquals("Error", false, isCorporate);
        calendar.setCalendarType(Enumeration.CORPORATECALENDAR);
        isCorporate = calendar.isCorporate();
        assertEquals("Error", true, isCorporate);
    }

    public void testIsPersonal() {
        Calendar calendar = new Calendar();
        boolean isPersonal = calendar.isPersonal();
        assertEquals("Error", true, isPersonal);
        calendar.setCalendarType(Enumeration.PERSONALCALENDAR);
        isPersonal = calendar.isPersonal();
        assertEquals("Error", true, isPersonal);

    }

    public void testIsProject() {
        Calendar calendar = new Calendar();
        boolean isProject = calendar.isProject();
        assertEquals("Error", false, isProject);
        calendar.setCalendarType(Enumeration.PROJECTCALENDAR);
        isProject = calendar.isProject();
        assertEquals("Error", true, isProject);

    }

    public void testGetName() {
        Calendar calendar = new Calendar();
        calendar.setName("TestCalendar");
        assertEquals("Error", "TestCalendar" , calendar.getName());
    }

    public void testGetCalendarType()
    {
        Calendar calendar = new Calendar();
        calendar.setCalendarType(Enumeration.CORPORATECALENDAR);
        assertEquals("Error", Enumeration.CORPORATECALENDAR, calendar.getCalendarType());
        calendar.setCalendarType(Enumeration.PROJECTCALENDAR);
        assertEquals("Error", Enumeration.PROJECTCALENDAR, calendar.getCalendarType());
        calendar.setCalendarType(Enumeration.PERSONALCALENDAR);
        assertEquals("Error", Enumeration.PERSONALCALENDAR, calendar.getCalendarType());

    }

    public void testGetHoursPerDay() {
        Calendar calendar = new Calendar();
        calendar.setHoursPerDay(8);
        assertEquals("Error", 8,calendar.getHoursPerDay());
    }

    public void testGetDaysPerWeek() {
        Calendar calendar = new Calendar();
        calendar.setDaysPerWeek(5);
        assertEquals("Error", 5, calendar.getDaysPerWeek());
    }

    // method to check 
    //
    public void testCalendarListRestricted()
    {
	// add 2 holidays
	java.util.Calendar ucal = java.util.Calendar.getInstance();
	ucal.set(java.util.Calendar.MONTH, 
		 java.util.Calendar.DECEMBER);
	ucal.set(java.util.Calendar.DATE,25);
	Date xmas = ucal.getTime();
	//System.out.println(xmas);
	
	ucal.set(java.util.Calendar.MONTH,
                 java.util.Calendar.JULY);
        ucal.set(java.util.Calendar.DATE,4);
	Date iday = ucal.getTime();
	//System.out.println(iday);
	
	Calendar cal = new Calendar();
	cal.addRestrictedDay(xmas,"Christmas");
	cal.addRestrictedDay(iday,"Independent Day");

	java.util.Collection holidays = cal.listRestrictedDays();

	java.util.Iterator iterator = holidays.iterator();

	while (iterator.hasNext()) {
	    Holiday hol = (Holiday)iterator.next();
	   // System.out.println(hol);
	}
    }

    public void testAddHoursToDate()
    {
        // TODO: Implement test for addHoursToDate().
    }

    public void testAddDaysToDate()
    {
        // TODO: Implement test for addDaysToDate().
    }

    public void testGetNextWorkDay()
    {
        // TODO: Implement test for getNextWorkDay().
    }

    public static void main(String[] args) {
        junit.textui.TestRunner.run(TestCalendar.class);
    }
}

/**
 * $Log: TestCalendar.java,v $
 * Revision 1.7  2005/05/14 14:53:54  tlong2
 * Removed println
 *
 * Revision 1.6  2005/05/03 03:11:04  umangkus
 * Add a test to access Calendar.listRestrictedDays() method
 *
 * Revision 1.5  2005/05/03 01:41:52  umangkus
 * add new test for testing if corporate calendar is already included
 *
 * Revision 1.4  2005/04/19 09:23:36  achou
 * Added test placeholders for new Calendar methods.
 *
 * Revision 1.3  2005/03/29 04:42:07  rcoutinh
 * Updated test cases for calendar
 *
 * Revision 1.2  2005/03/28 01:19:14  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:33  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.4  2005/03/27 02:28:44  rcoutinh
 * Added test cases for Calendar
 *
 * Revision 1.3  2005/03/22 06:26:20  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:32  dnathan
 * Updated CVS footer.
 *
 **/
