/***************************************************
 * File:        $RCSfile$
 * @author      $Author$,
 * @version     $Id$
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * JUnit test case for CalendarDAOToDB
 * </p>
 */
package pm275.core.calendar;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import pm275.common.persistance.DBConnection;
import pm275.common.persistance.PM275DBSchema;

import pm275.core.project.ProjectService;
import pm275.core.project.Project;
import pm275.core.project.ProjectDAOtoDB;

import junit.framework.TestCase;

public class TestCalendarDAOToDB extends TestCase {

	public static void main(String[] args) {
		junit.textui.TestRunner.run(TestCalendarDAOToDB.class);
	}
	
	public void testSet()
	{
		CalendarService cs = CalendarService.getInstance();
		CalendarInterface cIfc = cs.getCorporateCalendar();
		CalendarDAOToDB calDB = new CalendarDAOToDB();
		calDB.set(cIfc.getKey());
		
		try
		{
			PreparedStatement ps  = null;
			DBConnection conn = new DBConnection();
			conn.connect();
			ps = conn.getPreparedPreparedStatement("Select * from calendar where ID = ?");
			ps.setString(1,cIfc.getKey().toString());
			ResultSet rs = ps.executeQuery();
			if(rs.next())
			{
				int type = rs.getInt(PM275DBSchema.CALENDAR_TYPE);
				assertEquals("Error:", type, cIfc.getCalendarType());
				int hourPerDay = rs.getInt(PM275DBSchema.CALENDAR_HOURS);
				assertEquals("Error: ", hourPerDay,cIfc.getHoursPerDay());
			}
			else 
			{
				fail();
			}
		}catch(Exception e)
		{  
			fail("Failed to save Calendar");
			e.printStackTrace();
		}
	}
	
	public void testGet()
	{
		try
		{
			ProjectService ps = ProjectService.getInstance();
			Project p = ps.addProject("TestProject");
			ps.writeProjectDB(p.getProjectKey());
			CalendarDAOToDB calDB = new CalendarDAOToDB();
			
			CalendarKey cKey = calDB.get(p.getProjectKey());
			assertEquals("Error", p.getCalendarKey().toString(),cKey.toString());
		}catch(Exception e)
		{  
			fail("Failed to save Calendar");
			e.printStackTrace();
		}
	}

}


/**
 * $Log$
 * Revision 1.1  2005/05/14 22:16:38  rcoutinh
 * Junit test for CalendarDAOToDB
 *
 *
 */
