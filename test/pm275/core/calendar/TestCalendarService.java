/**
 * CSCIE 275 - Software Architecture and Engineering
 *
 * File: $RCSfile: TestCalendarService.java,v $
 * @author $Author: umangkus $,
 * @version $Revision: 1.6 $
 *
 * <p></p>
 *
 * $Id: TestCalendarService.java,v 1.6 2005/05/03 01:41:53 umangkus Exp $
 */
package pm275.core.calendar;

import pm275.core.calendar.Calendar;
import pm275.core.calendar.CalendarService;
import pm275.core.calendar.CalendarKey;
import junit.framework.TestCase;

import java.util.Collection;

public class TestCalendarService extends TestCase {

    /**
     * Test instance is not null
     *
     */
    public void testGetInstance1() {
        CalendarService cs;
        cs = CalendarService.getInstance();
        assertNotNull(cs);
    }

    /**
     * Test service has single instance
     *
     */
    public void testGetInstance2() {
        CalendarService cs1 = CalendarService.getInstance();

        CalendarService cs2 = CalendarService.getInstance();

        // Both instances should be to the same object
        assertEquals("Error",cs1,cs2);

    }

    public void testAddCalendar()
        throws Exception {

        /*CalendarService cs = CalendarService.getInstance();
        cs.addCalendar(new CalendarKey());
        //System.out.println("list Calendars" + cs.listCalendars().size());
        int count = cs.listCalendars().size();
        assertEquals("Error",1,count);
        */
    }

    public void testRemoveCalendar()
        throws Exception
    {

	CalendarKey key = new CalendarKey();
	CalendarService cs = CalendarService.getInstance();
	cs.addCalendar(key);
	Calendar c = cs.removeCalendar(key);
	assertNotNull(c);
	
    }

    public void testGetCalendar()
        throws Exception
    {
        CalendarKey key = new CalendarKey();
        CalendarService cs = CalendarService.getInstance();
        cs.addCalendar(key);
        Calendar c = cs.getCalendar(key);
        assertNotNull(c);
        assertEquals("Error", key,c.getKey());
    }

    // test if a corporate calendar is already included
    //
    public void testGetCalendarList()
	throws Exception
    {
	CalendarService cs = CalendarService.getInstance();
	Collection calendars = 
	    cs.listCalendars();

	/* remove comment to print names
	java.util.Iterator iterator = calendars.iterator();

	while (iterator.hasNext()) {
	    Calendar cal = (Calendar)iterator.next();
	    System.out.println(cal.getName());
	}
	*/

	assertEquals(2,calendars.size());
    }

    /*
     *  Class under test for Date getNextWorkDay(Date).
     */
    public void testGetNextWorkDay()
    {
        // TODO: Implement test for getNextWorkDay().
    }

    /*
     * Class under test for boolean isWorkDay(Date, ResourceKey)
     */
    public void testIsWorkDayDateResourceKey() {
        //TODO Implement isWorkDay().
    }

    /*
     * Class under test for boolean isWorkDay(Date)
     */
    public void testIsWorkDayDate() {
        //TODO Implement isWorkDay().
    }

    public void testAddRestrictedDay() {
        //TODO Implement addRestrictedDay().
    }

    public static void main(String[] args) {
        junit.textui.TestRunner.run(TestCalendarService.class);
    }
}

/**
 * $Log: TestCalendarService.java,v $
 * Revision 1.6  2005/05/03 01:41:53  umangkus
 * add new test for testing if corporate calendar is already included
 *
 * Revision 1.5  2005/04/19 09:23:12  achou
 * Removed obsolete test placeholders and added a new one.
 *
 * Revision 1.4  2005/03/30 17:48:31  dnathan
 * Nuked test that was breaking build.
 *
 * Revision 1.3  2005/03/29 04:42:26  rcoutinh
 * updated test case for calendarservice
 *
 * Revision 1.2  2005/03/28 01:19:14  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:34  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:19  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:30  dnathan
 * Updated CVS footer.
 *
 **/
