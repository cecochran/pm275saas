/**
 * CSCIE 275 - Software Architecture and Engineering
 *
 * File: $RCSfile: TestResourceService.java,v $
 * @author $Author: tlong2 $,
 * @version $Revision: 1.18 $
 *
 * <p></p>
 *
 * $Id: TestResourceService.java,v 1.18 2005/05/05 06:11:52 tlong2 Exp $
 */
package pm275.core.resource;

import java.util.Collection;
import java.util.Iterator;

import pm275.core.project.ActivityInterface;
import pm275.core.project.ActivityKey;
import pm275.core.project.Project;
import pm275.core.project.ProjectKey;
import pm275.core.project.ProjectService;
import pm275.common.exception.PersistanceException;
import pm275.common.exception.ResourceException;
import junit.framework.TestCase;
import pm275.common.persistance.PersistanceService;

public class TestResourceService extends TestCase {

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testGetInstance() {
        ResourceService rs;
        rs = ResourceService.getInstance();
        assertNotNull(rs);
    }

    public void testAddResourcePool() {

        ResourceService rService = null;
        ResourcePool resPool = null;
        try
        {
			// get service
            rService = ResourceService.getInstance();
            resPool = rService.createResourcePool();
            //Put in a resource in Resource service
            assertNotNull(rService.getResourcePool(resPool.getResourcePoolKey()));
        }
        catch(ResourceException ex)
        {
        	fail(ex.getMessage());
        }
    }

    public void testRemoveResourcePool() {

        ResourceService rService = null;
        ResourceInterface res = null;

        try
        {
            //  get service
            rService = ResourceService.getInstance();

            //Put in a resource in Resource service
            res = rService.createResource("BugsBunny");
            ResourceKey rk = res.getKey();
            ResourcePool resPool = rService.createResourcePool();
            resPool.addResource(res);
            ResourcePoolKey Rpk = resPool.getResourcePoolKey();
            rService.setResourcePool(resPool);
            rService.removeResourcePool(Rpk);
            ResourcePool rsp2 = rService.getResourcePool(Rpk);
            assertNull(rsp2);
        }
        catch(ResourceException ex)
        {
        	fail(ex.getMessage());
        }
    }

    public void testGetResourcePool() {

        try
        {
            // get service
            ResourceService rService = ResourceService.getInstance();

            //Put in a resource in Resource service
            ResourceInterface res = new Resource("BugsBunny");
            ResourceKey rk = res.getKey();

            ResourcePool resPool = new ResourcePool();
            resPool.addResource(res);

            ResourcePoolKey Rpk = resPool.getResourcePoolKey();

            rService.setResourcePool(resPool);
            ResourcePool rsp2 = rService.getResourcePool(Rpk);

            assertNotNull(rsp2);
        }
        catch(ResourceException ex)
        {
            fail(ex.getMessage());
        }
    }

    public void testSetResourcePool() {


        //get service
        ResourceService rService = null;

        try
        {
            rService = ResourceService.getInstance();

            //Put in a resource in Resource service
            ResourceInterface res = new Resource("BugsBunny");
            ResourceKey rk = res.getKey();
            ResourcePool resPool = new ResourcePool();

            resPool.addResource(res);
            ResourcePoolKey Rpk = resPool.getResourcePoolKey();
            rService.setResourcePool(resPool);

            ResourcePool rsp2 = rService.getResourcePool(Rpk);
            assertNotNull(rsp2);
        }
        catch(ResourceException ex)
        {
			fail(ex.getMessage());
        }

    }

		
    public void testListResourcePools() {

        try
        {

            //get service
            ResourceService rService = ResourceService.getInstance();

            //Put in a resource in Resource service
            ResourceInterface res = rService.createResource("BugsBunny");
            ResourceKey rk = res.getKey();
            ResourcePool resPool = rService.createResourcePool();
            resPool.addResource(res);
            rService.setResourcePool(resPool);

              //get iterator
            java.util.Iterator it = rService.listResourcesPools().iterator();

            //cycle thru all the propjects
            while (it.hasNext()) {
               //get project and call recalcualte method
                ResourcePool rs2 = (ResourcePool) it.next();
                assertNotNull(rs2);
            }
        }
        catch(ResourceException ex)
        {
        	fail(ex.getMessage());
        }
    }

    public void testRemoveResource() {

        try
        {
            //get service
            ResourceService rService = ResourceService.getInstance();

            //Resource res= new Resource("PM275");
            ResourceInterface res = rService.createResource("BugsBunny");
 
            //remove resource
            ResourceKey rk = res.getKey();
            rService.removeResource(res.getKey());

            //Should be null
            ResourceInterface res2 = rService.getResource(rk);

            //Should be null
            assertNull(res2);
        }
        catch(ResourceException ex)
        {
        	fail(ex.getMessage());
        }
    }

    public void testGetResource() {

        try
        {
            //get service
            ResourceService rService = ResourceService.getInstance();

            ResourceInterface res = new Resource("BugsBunny");
            rService.setResource(res);

            //remove resource
            ResourceKey rk = res.getKey();
            ResourceInterface res2 = rService.getResource(rk);

            //Should not be null
            assertNotNull(res2);
        }
        catch(ResourceException ex)
        {
			fail(ex.getMessage());
        }
    }

    public void testSetResource() {

        try
        {
    //      get service
            ResourceService rService = ResourceService.getInstance();

            //Put in a resource in Resource service
            ResourceInterface res = new Resource("BugsBunny");
            ResourceKey rk = res.getKey();
            rService.setResource(res);

            //Now Get Key
            ResourceInterface res2 = rService.getResource(rk);

            //Should not null
            assertNotNull(res2);
        }
        catch(ResourceException ex)
        {
			fail(ex.getMessage());
        }
    }

    public void testListResources() {

        try
        {
            //get service
            ResourceService rService = ResourceService.getInstance();

            //Put in a resource in Resource service
            ResourceInterface res = new Resource("BugsBunny");
            ResourceKey rk = res.getKey();
            rService.setResource(res);

              //get iterator
            java.util.Iterator it = rService.listResources().iterator();

            //cycle thru all the propjects
            while (it.hasNext()) {
               //get project and call recalcualte method
                ResourceInterface rs2 = (ResourceInterface) it.next();
                assertNotNull(rs2);
            }
        }
        catch(ResourceException ex)
        {
			fail(ex.getMessage());
        }
    }

    /*
     * Class under test for void assign(ResourceKey, ResourcePoolKey)
     */
    public void testAssignResourceKeyResourcePoolKey() {
        //TODO Implement assign().
    }

    /*
     * Class under test for void assign(ResourceKey, ActivityKey,ProjectKey, double)
     */
    public void testAssignResourceKeyActivityKeydouble() 
		throws Exception
	{
		// get service
        ResourceService rService = ResourceService.getInstance();
        ResourcePool project1Pool    = rService.createResourcePool();
        ResourcePoolKey project1PoolKey = project1Pool.getResourcePoolKey();

        //Put in a resource in Resource service
        ResourceInterface res = rService.createResource("BugsBunny");
        ResourceKey rk = res.getKey();
		project1Pool.addResource(res);
		rService.addResourcePool(project1Pool);

		// Create Project
		ProjectService ps = ProjectService.getInstance();
        Project project1 = ps.addProject("Test Project 1");
        ProjectKey project1Key = project1.getProjectKey();
		ps.addResources(project1Key, project1PoolKey);
		
		//Assign reso
		ActivityInterface activity1 = ps.addActivity(project1Key, "activity1");
		rService.assign(rk,activity1.getActivityKey(),project1Key,100);
        
		//Verify activity has resource
		Collection resources = activity1.getResources();
		Iterator iterator = resources.iterator();
		assertEquals("error", resources.size(),1);
		Resource activityRes = (Resource)iterator.next();
		assertEquals("error",res,activityRes);
		
		//Verify resourceservice has assignment;
		AssignmentInterface aIfc = rService.getAssignment(rk,activity1.getActivityKey());
		assertNotNull(aIfc);
    }
	
	
	public void testDeAssign(ResourceKey resourceKey, ActivityKey activityKey, ProjectKey pKey)
		throws Exception
	{
		// get service
        ResourceService rService = ResourceService.getInstance();
        ResourcePool project1Pool    = rService.createResourcePool();
        ResourcePoolKey project1PoolKey = project1Pool.getResourcePoolKey();

        //Put in a resource in Resource service
        ResourceInterface res = rService.createResource("BugsBunny");
        ResourceKey rk = res.getKey();
		project1Pool.addResource(res);
		rService.addResourcePool(project1Pool);

		// Create Project
		ProjectService ps = ProjectService.getInstance();
        Project project1 = ps.addProject("Test Project 1");
        ProjectKey project1Key = project1.getProjectKey();
		ps.addResources(project1Key, project1PoolKey);
		
		//Assign reso
		ActivityInterface activity1 = ps.addActivity(project1Key, "activity1");
		rService.assign(rk,activity1.getActivityKey(),project1Key,100);
		
		rService.deassign(rk,activity1.getActivityKey(),project1Key);
		
		//	Verify activity has no resource
		Collection resources = activity1.getResources();
		assertTrue("Error", resources.isEmpty());
		
		// Verify resourceService has no assignment
        assertNull("Error", rService.getAssignment(rk,activity1.getActivityKey()));
	}
	
	
	public void testDeAssignAll(ResourceKey resourceKey, ActivityKey activityKey, ProjectKey pKey)
	throws Exception
	{
		// get service
		ResourceService rService = ResourceService.getInstance();
		ResourcePool project1Pool    = rService.createResourcePool();
		ResourcePoolKey project1PoolKey = project1Pool.getResourcePoolKey();
		
		//Put in a resource in Resource service
		ResourceInterface res = rService.createResource("BugsBunny");
		ResourceInterface res2 = rService.createResource("DaffyDuck");
		ResourceInterface res3 = rService.createResource("Goofy");
		
		ResourceKey rk = res.getKey();
		project1Pool.addResource(res);
		rService.addResourcePool(project1Pool);
		
		// Create Project
		ProjectService ps = ProjectService.getInstance();
		Project project1 = ps.addProject("Test Project 1");
		ProjectKey project1Key = project1.getProjectKey();
		ps.addResources(project1Key, project1PoolKey);
		
		//Assign reso
		ActivityInterface activity1 = ps.addActivity(project1Key, "activity1");
		rService.assign(rk,activity1.getActivityKey(),project1Key,100);
		rService.assign(res2.getKey(),activity1.getActivityKey(),project1Key,100);
		rService.assign(res3.getKey(),activity1.getActivityKey(),project1Key,100);
		
		rService.deassignAll(activity1.getActivityKey(),project1Key);
		
		//	Verify activity has no resource
		Collection resources = activity1.getResources();
		assertTrue("Error", resources.isEmpty());
		
		// Verify resourceService has no assignment
		assertNull("Error", rService.getAssignment(rk,activity1.getActivityKey()));
		assertNull("Error", rService.getAssignment(res2.getKey(),activity1.getActivityKey()));
		assertNull("Error", rService.getAssignment(res3.getKey(),activity1.getActivityKey()));

	}
}

/**
 * $Log: TestResourceService.java,v $
 * Revision 1.18  2005/05/05 06:11:52  tlong2
 * Removed LoadResourcePool() because it wasn't working or clearly useful
 *
 * Revision 1.17  2005/04/30 17:55:49  rcoutinh
 * Added tests for deAssign and deAssignAll
 *
 * Revision 1.16  2005/04/30 17:05:57  rcoutinh
 * added test for assign method
 *
 * Revision 1.15  2005/04/29 03:22:55  tlong2
 * fixed a couple more
 *
 * Revision 1.14  2005/04/18 03:21:40  bbuchana
 * Added the Load resource Pool test
 *
 * Revision 1.13  2005/04/17 03:23:14  vchumako
 * Changed Resource creation to appropriate methods.
 *
 * Revision 1.12  2005/04/17 02:20:49  vchumako
 * add createResourcePool() and createResource() methods to the ResourceService.
 * They should be used from now on to create Resources.
 *
 * Revision 1.11  2005/04/13 23:14:33  vchumako
 * fix Resource interfaces and test case
 *
 * Revision 1.10  2005/04/10 21:31:23  vchumako
 * Added Assignment and Resource Interface whenever it's possible
 *
 * Revision 1.9  2005/03/30 16:04:06  dnathan
 * Removed unneeded imports.
 *
 * Revision 1.8  2005/03/30 02:14:56  achou
 * Fixed compilation errors.
 *
 * Revision 1.7  2005/03/29 22:56:03  bbuchana
 * Fixed defect in JTest.
 *
 * Revision 1.6  2005/03/29 22:37:19  bbuchana
 * Fixed defects in JTest.
 *
 * Revision 1.5  2005/03/29 22:07:14  bbuchana
 * Added Exception handling.
 *
 * Revision 1.4  2005/03/29 13:33:14  ccochran
 * Added some test code for resource pools
 *
 * Revision 1.3  2005/03/29 12:31:11  ccochran
 * added test Instance
 *
 * Revision 1.2  2005/03/28 01:19:15  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:36  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:20  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:31  dnathan
 * Updated CVS footer.
 *
 **/
