/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestAssignment.java,v $ 
 * @author $Author: vchumako $, 
 * @version $Revision: 1.4 $
 * 
 * <p></p>
 * 
 * $Id: TestAssignment.java,v 1.4 2005/04/10 21:31:22 vchumako Exp $
 */
package pm275.core.resource;

import junit.framework.TestCase;
import pm275.core.project.*;


public class TestAssignment extends TestCase {

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testGetActivity() {
		ActivityKey ak = new ActivityKey();
		ResourceKey rk = new ResourceKey();
		AssignmentInterface as = new Assignment(ak,rk);
		ActivityKey ak2 = as.getActivityKey();
		assertNotNull(ak2);
	}

	public void testGetResource() {
		ActivityKey ak = new ActivityKey();
		ResourceKey rk = new ResourceKey();
		AssignmentInterface as = new Assignment(ak,rk);
		ResourceKey rk2 = as.getResource();
		assertNotNull(rk2);
	}

	public void testGetKey() {
		ActivityKey ak = new ActivityKey();
		ResourceKey rk = new ResourceKey();
		AssignmentInterface as = new Assignment(ak,rk);
		AssignmentKey ak2 = as.getAssignmentKey();
		assertNotNull(ak2);
	}

	public void testGetPercent() {
		ActivityKey ak = new ActivityKey();
		ResourceKey rk = new ResourceKey();
		AssignmentInterface as = new Assignment(ak,rk);
		as.setPercent(100);
		double pr = as.getPercent();
		assertEquals(pr,100,100);
	}

	public void testSetPercent() {
		//TODO Implement setPercent().
		ActivityKey ak = new ActivityKey();
		ResourceKey rk = new ResourceKey();
		AssignmentInterface as = new Assignment(ak,rk);
		as.setPercent(100);
		double pr = as.getPercent();
		assertEquals(pr,100,100);
	}

}

/**
 * $Log: TestAssignment.java,v $
 * Revision 1.4  2005/04/10 21:31:22  vchumako
 * Added Assignment and Resource Interface whenever it's possible
 *
 * Revision 1.3  2005/03/29 18:21:46  ccochran
 * Added baseline test code
 *
 * Revision 1.2  2005/03/28 01:19:16  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:36  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:21  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:32  dnathan
 * Updated CVS footer.
 *
 **/