/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestResourcePoolKey.java,v $ 
 * @author $Author: ccochran $, 
 * @version $Revision: 1.4 $
 * 
 * <p></p>
 * 
 * $Id: TestResourcePoolKey.java,v 1.4 2005/03/29 13:48:41 ccochran Exp $
 */
package pm275.core.resource;

import junit.framework.TestCase;


public class TestResourcePoolKey extends TestCase {

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/*
	 * Class under test for boolean equals(ResourcePoolKey)
	 */
	public void testEqualsResourcePoolKey() {
		ResourcePoolKey rpk  = new ResourcePoolKey("ResourcePool Key Test 2");
		boolean bResult = rpk.equals(rpk);
		assertTrue(bResult);
	}

	/*
	 * Class under test for String toString()
	 */
	public void testToString() {
		//TODO Implement toString().
		ResourcePoolKey pk = new ResourcePoolKey();
		String sResult = pk.toString();
		assertNotNull(sResult,pk);
	}

}

/**
 * $Log: TestResourcePoolKey.java,v $
 * Revision 1.4  2005/03/29 13:48:41  ccochran
 * Added Test Code for Keys
 *
 * Revision 1.3  2005/03/29 13:45:01  ccochran
 * Added test code model after Project service
 *
 * Revision 1.2  2005/03/28 01:19:15  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:36  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:19  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:30  dnathan
 * Updated CVS footer.
 *
 **/