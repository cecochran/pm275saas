/**
 * CSCIE 275 - Software Architecture and Engineering
 *
 * File: $RCSfile: TestResourcePool.java,v $
 * @author $Author: tlong2 $,
 * @version $Revision: 1.15 $
 *
 * <p></p>
 *
 * $Id: TestResourcePool.java,v 1.15 2005/05/14 21:37:05 tlong2 Exp $
 */
package pm275.core.resource;


import pm275.common.exception.ResourceException;
import junit.framework.TestCase;


public class TestResourcePool extends TestCase {

    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testAddResource() {

        try
        {
            ResourceService rs = ResourceService.getInstance();
            //Put in a resource in Resource service
            ResourcePool resPool = rs.createResourcePool();
            ResourceInterface res = rs.createResource("DonaldDuck");
            ResourceKey rk = res.getKey();
            resPool.addResource(res);
            ResourceInterface res2 = resPool.getResource(rk);
            assertNotNull(res2);
        }
        catch(ResourceException ex)
        {
            // TODO:  Place code to fail test.
        }
    }

/*
    public void testRemoveResource() {
	ResourceKey rk = null;
	ResourcePool resPool = null;
        try
        {
            ResourceService rService = ResourceService.getInstance();
            //Put in a resource in Resource service
            resPool = rService.createResourcePool();
            ResourceInterface rs = rService.createResource("DonaldDuck");
            rk = rs.getKey();


            //Add resouce
            resPool.addResource(rs);

            //remove resource
            resPool.removeResouce(rk);

        }
        catch(ResourceException ex)
        {
            fail(ex.getMessage());
        }

	try {
            //Try get same resource; Should throw an exception
            ResourceInterface res2 = resPool.getResource(rk);
            assertNotNull(null);
	} catch(ResourceException ex) {
	    // test succeeded
	}
    }
*/
    public void testListResources() {

        try
        {
            ResourceService rService = ResourceService.getInstance();
            //Put in a resource in Resource service
            ResourcePool resPool = rService.createResourcePool();
            ResourceInterface rs = rService.createResource("DonaldDuck");
            ResourceKey rk = rs.getKey();
            //Add resouce
            resPool.addResource(rs);

            //get iterator
            java.util.Iterator it = resPool.listResourceKeys().iterator();

            //cycle thru all the propjects
	    ResourceKey key = null;
            while (it.hasNext()) {
               //get project and call recalcualte method
               //ResourceKey rp2 = (ResourceKey) it.next();
               key = (ResourceKey) it.next();
               assertNotNull(key);
            }
	    assertEquals(key, rk);
        }
        catch(ResourceException ex)
        {
            fail("testListResources() " + ex.getMessage());
        }

    }

}

/**
 * $Log: TestResourcePool.java,v $
 * Revision 1.15  2005/05/14 21:37:05  tlong2
 * commented out old test
 *
 * Revision 1.14  2005/04/18 03:37:21  vchumako
 * Fixed equal keys test
 *
 * Revision 1.13  2005/04/18 03:27:39  vchumako
 * Fixex removeResource test
 *
 * Revision 1.12  2005/04/17 19:28:08  vchumako
 * Fixed a few tests
 *
 * Revision 1.11  2005/04/17 11:26:32  achou
 * Handles new return type for ResourcePool.listResourceKeys().
 *
 * Revision 1.10  2005/04/17 02:20:49  vchumako
 * add createResourcePool() and createResource() methods to the ResourceService.
 * They should be used from now on to create Resources.
 *
 * Revision 1.9  2005/04/13 23:14:33  vchumako
 * fix Resource interfaces and test case
 *
 * Revision 1.8  2005/04/10 21:31:22  vchumako
 * Added Assignment and Resource Interface whenever it's possible
 *
 * Revision 1.7  2005/03/29 22:19:04  bbuchana
 * Fixed bug in JTest.
 *
 * Revision 1.6  2005/03/29 22:04:41  bbuchana
 * Added Exception handling.
 *
 * Revision 1.5  2005/03/29 17:48:22  ccochran
 * Added test code for resource pool
 *
 * Revision 1.4  2005/03/29 17:33:25  ccochran
 * Added test code for resources
 *
 * Revision 1.3  2005/03/28 03:10:49  vchumako
 * Fixed package name
 *
 * Revision 1.2  2005/03/28 01:19:16  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:35  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:20  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:31  dnathan
 * Updated CVS footer.
 *
 **/
