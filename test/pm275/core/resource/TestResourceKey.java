/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestResourceKey.java,v $ 
 * @author $Author: ccochran $, 
 * @version $Revision: 1.3 $
 * 
 * <p></p>
 * 
 * $Id: TestResourceKey.java,v 1.3 2005/03/29 13:48:41 ccochran Exp $
 */
package pm275.core.resource;

import junit.framework.TestCase;


public class TestResourceKey extends TestCase {

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/*
	 * Class under test for boolean equals(int)
	 */
	public void testEqualsint() {
		ResourceKey rpk  = new ResourceKey("Resource Key Test 2");
		boolean bResult = rpk.equals(rpk);
		assertTrue(bResult);
	}

	/*
	 * Class under test for String toString()
	 */
	public void testToString() {
		ResourceKey pk = new ResourceKey();
		String sResult = pk.toString();
		assertNotNull(sResult,pk);
	}

}

/**
 * $Log: TestResourceKey.java,v $
 * Revision 1.3  2005/03/29 13:48:41  ccochran
 * Added Test Code for Keys
 *
 * Revision 1.2  2005/03/28 01:19:16  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:35  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:21  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:32  dnathan
 * Updated CVS footer.
 *
 **/