/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestAssignmentKey.java,v $ 
 * @author $Author: vchumako $, 
 * @version $Revision: 1.5 $
 * 
 * <p></p>
 * 
 * $Id: TestAssignmentKey.java,v 1.5 2005/04/18 03:37:21 vchumako Exp $
 */
package pm275.core.resource;

import junit.framework.TestCase;


public class TestAssignmentKey extends TestCase {

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/*
	 * Class under test for boolean equals(AsssignmentKey)
	 */
	public void testEqualsAssignmentKey() {
		AssignmentKey pk = new AssignmentKey();
		String str = pk.getKeyId();
		AssignmentKey pk2 = new AssignmentKey(str);
		assertEquals(pk.toString(), pk2.toString());
	}

	/*
	 * Class under test for String toString()
	 */
	public void testToString() {
		String str  = new String("testKey");
		AssignmentKey pk = new AssignmentKey(str);
		String sResult = pk.toString();
		assertEquals(sResult,str);
	}

}

/**
 * $Log: TestAssignmentKey.java,v $
 * Revision 1.5  2005/04/18 03:37:21  vchumako
 * Fixed equal keys test
 *
 * Revision 1.4  2005/04/17 19:28:08  vchumako
 * Fixed a few tests
 *
 * Revision 1.3  2005/03/29 13:52:14  ccochran
 * Added more test code
 *
 * Revision 1.2  2005/03/28 01:19:15  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:34  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:19  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:30  dnathan
 * Updated CVS footer.
 *
 **/
