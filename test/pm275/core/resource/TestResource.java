/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestResource.java,v $ 
 * @author $Author: vchumako $, 
 * @version $Revision: 1.6 $
 * 
 * <p></p>
 * 
 * $Id: TestResource.java,v 1.6 2005/05/01 05:11:36 vchumako Exp $
 */
package pm275.core.resource;

import pm275.common.exception.ResourceException;
import junit.framework.TestCase;


public class TestResource extends TestCase {

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testGetName() {
		ResourceInterface rs = new Resource("ScoobyDo");
		String str = rs.getName();
		assertEquals(str,"ScoobyDo");
		
	}

	public void testSetName() {
		ResourceInterface rs = new Resource("ScoobyDo");
		rs.setName("Shaggy");
		String str = rs.getName();
		assertEquals(str,"Shaggy");
	}

	public void testGetKey() {
		ResourceInterface rs = new Resource("ScoobyDo");
		ResourceKey rk = rs.getKey();
		assertNotNull(rk);
	}

	public void testAsssign() {
		//TODO Implement asssign().
		ResourceInterface rs = new Resource("ScoobyDo");
		AssignmentKey ak = new AssignmentKey();
	    rs.assign(ak);
	    
	    //get iterator 
    	java.util.Iterator it = rs.listAssignments();
    	
	}

	public void testDeassign() 
	{
		try
		{
		//	Put in a resource in Resource service
			ResourceInterface rs = new Resource("DonaldDuck");
		    ResourceKey rk = rs.getKey();
		    AssignmentKey ak = new AssignmentKey();
		    rs.assign(ak);
		    rs.deassign(ak);
		    //get iterator 
			int size = rs.assignmentsSize();
			assertEquals(size,0);
		}
		catch(ResourceException ex)
		{
			fail("Can't deassign!");
		}
	}

	public void testListAsssignments() {
		//Put in a resource in Resource service
		ResourceInterface rs = new Resource("DonaldDuck");
        ResourceKey rk = rs.getKey();
        AssignmentKey ak = new AssignmentKey();
        rs.assign(ak);
        
        //get iterator 
    	int size = rs.assignmentsSize();
    	assertEquals(size,1);
    	
	}

}

/**
 * $Log: TestResource.java,v $
 * Revision 1.6  2005/05/01 05:11:36  vchumako
 * Fixed DAO exception name, added resource to dbms, still need to fix jndi ro get back to plain jdbc
 *
 * Revision 1.5  2005/04/10 21:31:23  vchumako
 * Added Assignment and Resource Interface whenever it's possible
 *
 * Revision 1.4  2005/03/29 22:03:43  bbuchana
 * Added Exception handling.
 *
 * Revision 1.3  2005/03/29 18:04:26  ccochran
 * First cut at test code
 *
 * Revision 1.2  2005/03/28 01:19:16  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:35  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:20  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:31  dnathan
 * Updated CVS footer.
 *
 **/