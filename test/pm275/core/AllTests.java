package pm275.core;

import junit.framework.Test;
import junit.framework.TestSuite;


/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: AllTests.java,v $ 
 * @author $Author: tlong2 $, 
 * @version $Revision: 1.9 $
 * 
 * <p></p>
 * 
 * $Id: AllTests.java,v 1.9 2005/05/09 17:52:44 tlong2 Exp $
 */



public class AllTests extends TestSuite {

	public AllTests(String s) {
		
		super(s);
		
	}
	public static Test suite() {
		TestSuite suite = new TestSuite("Core tests for test.pm275.core.*");
		//$JUnit-BEGIN$
		
		// core.calendar tests
		suite.addTestSuite(pm275.core.calendar.TestCalendar.class);
		suite.addTestSuite(pm275.core.calendar.TestCalendarKey.class);
		suite.addTestSuite(pm275.core.calendar.TestCalendarService.class);

		
		// core.resource tests
		suite.addTestSuite(pm275.core.resource.TestAssignment.class);
		suite.addTestSuite(pm275.core.resource.TestAssignmentKey.class);
		suite.addTestSuite(pm275.core.resource.TestResource.class);
		suite.addTestSuite(pm275.core.resource.TestResourceKey.class);
		suite.addTestSuite(pm275.core.resource.TestResourcePool.class);
		suite.addTestSuite(pm275.core.resource.TestResourcePoolKey.class);
		suite.addTestSuite(pm275.core.resource.TestResourceService.class);
		
		
		// core.project tests
		suite.addTestSuite(pm275.core.project.TestActivity.class);
		suite.addTestSuite(pm275.core.project.TestTimeReport.class);
		suite.addTestSuite(pm275.core.project.TestActivityKey.class);
		suite.addTestSuite(pm275.core.project.TestProjectKey.class);
		suite.addTestSuite(pm275.core.project.TestProject.class);
		suite.addTestSuite(pm275.core.project.TestProjectService.class);
		suite.addTestSuite(pm275.core.project.TestProjectService2.class);
			
		

		
		


		
		//$JUnit-END$
		return suite;
	}
}

/**
 * $Log: AllTests.java,v $
 * Revision 1.9  2005/05/09 17:52:44  tlong2
 * really final changes
 *
 * Revision 1.8  2005/05/05 05:58:19  tlong2
 * Fixed all core tests except 2.
 *
 * Revision 1.7  2005/03/28 03:28:50  vchumako
 * Fixed compile error
 *
 * Revision 1.6  2005/03/28 01:19:18  tlong2
 * Updated package info
 *
 * Revision 1.5  2005/03/27 18:43:27  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.4  2005/03/26 19:42:26  vchumako
 * Add AssignmentKey test
 *
 * Revision 1.3  2005/03/26 19:02:41  vchumako
 * AssignmentKey test added
 *
 * Revision 1.2  2005/03/26 09:02:28  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.1  2005/03/22 06:25:17  dnathan
 * New file.
 *
 * Revision 1.2  2005/03/22 05:10:41  dnathan
 * Updated CVS footer.
 *
 **/
