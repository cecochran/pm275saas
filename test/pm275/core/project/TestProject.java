/**
 * CSCIE 275 - Software Architecture and Engineering
 *
 * File: $RCSfile: TestProject.java,v $
 * @author $Author: tlong2 $,
 * @version $Revision: 1.20 $
 *
 * <p></p>
 *
 * $Id: TestProject.java,v 1.20 2005/05/09 18:55:59 tlong2 Exp $
 */
package pm275.core.project;

import junit.framework.TestCase;
import pm275.common.exception.CycleDependencyException;
import pm275.common.exception.DuplicateKeyException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.ResourceException;
import pm275.core.calendar.*;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourcePoolKey;
import pm275.core.resource.ResourceService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;


public class TestProject extends TestCase {

    private ProjectService projectService;
    private ResourceService resourceService;
    private CalendarService calendarService;

    private ResourcePool    project1Pool;
    private ResourcePoolKey project1PoolKey;
    private ResourceInterface paul;
    private ResourceInterface peter;
    private ResourceInterface mary;
    private ActivityInterface activity1;
    private ProjectInterface project1;
    private ProjectKey project1Key;
   

    private static final java.text.DateFormat DF =
            new java.text.SimpleDateFormat("yyyy-MM-dd");


    // startDate = Thursday April 14 2005
    private Date startDate;
    private Date startDate2;
    private Date thursday04142005;
    private CalendarKey cKey;
    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
        resourceService = ResourceService.getInstance();
        projectService = ProjectService.getInstance();

        // Thursday April 14, 2005
        startDate = DF.parse("2005-04-14");

        // Saturday April 23, 2005
        startDate2 = DF.parse("2005-04-23");

        thursday04142005 = DF.parse("2005-04-14");
        CalendarService calendarService = CalendarService.getInstance();
        cKey = new CalendarKey();

    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /*
     * Call this to set up a base project.
     */
    protected void setupTestProject1() throws Exception {
    	
        project1Pool = null;
        project1PoolKey = null;
        paul = null;
        peter = null;
        mary = null;
        activity1 = null;
        project1 = null;
        project1Key = null;
        
    	ActivityInterface predAct1;
    	ActivityInterface succAct1;

        try {
            // Create Resource Pool and add Peter and Paul
            project1Pool    = resourceService.createResourcePool();
            project1PoolKey = project1Pool.getResourcePoolKey();

            paul  = resourceService.createResource("Paul");
            project1Pool.addResource(paul);
            peter = resourceService.createResource("Peter");
            project1Pool.addResource(peter);
            resourceService.addResourcePool(project1Pool);

            // Create Project
            project1 = projectService.addProject("Test Project 1");
            project1Key = project1.getProjectKey();

            project1.setStartDate(startDate);
            ActivityInterface sm = project1.getStartMilestone();
            sm.setStartDate(startDate);
            ActivityInterface em = project1.getEndMilestone();
            em.setStartDate(startDate);
            
            // Add Resource Pool to Project
            projectService.addResources(project1Key, project1PoolKey);

            activity1 = projectService.addActivity(project1Key, "activity1");
            activity1.setStartDate(thursday04142005);
            
            predAct1 = projectService.addActivity(project1Key, "do something before activity1");
            predAct1.setDuration(0);
            
            succAct1 = projectService.addActivity(project1Key, "do something after activity1");
            succAct1.setDuration(0);
            
            project1.associateActivities(predAct1.getActivityKey(),activity1.getActivityKey());
            project1.associateActivities(activity1.getActivityKey(),succAct1.getActivityKey());
 

            /**
             * set resources
             */
            resourceService.assign(paul.getKey(), activity1.getActivityKey(), project1Key, 100);
            resourceService.assign(peter.getKey(), activity1.getActivityKey(), project1Key, 100);


        }
        catch (Exception e) {

            // Fail the test setup
            assertEquals("EXCEPTION : SetupTestProject", e.getMessage());
        }

    }

    private ProjectInterface buildProject(String sProjectName) 
	throws 	ResourceException, 
			ParseException, 
			InternalProjectException, 
			KeyNotFoundException,
			DuplicateKeyException,
			InternalActivityException,
			CycleDependencyException {

    	ResourceService resourceService = ResourceService.getInstance();
	ProjectService projectService = ProjectService.getInstance();

	ProjectInterface project1 = projectService.addProject(sProjectName);

   ResourcePool resourcePool = resourceService.createResourcePool();
   ResourceInterface abe  = resourceService.createResource("Abe");
   ResourceInterface bob  = resourceService.createResource("Bob");


	try {
	    resourcePool.addResource(abe);
	    resourcePool.addResource(bob);
	    resourceService.addResourcePool(resourcePool);
	    
	    DateFormat DF = new SimpleDateFormat("yyyy-MM-dd");
	    // Thursday April 14, 2005
	    Date startDate = DF.parse("2005-04-14");    
	    
	    project1.setStartDate(startDate);
	
	    ActivityInterface sm = project1.getStartMilestone();
	    sm.setStartDate(startDate);
	    ActivityInterface em = project1.getEndMilestone();
	    em.setStartDate(startDate);
	    
	    // Add Resource Pool to Project
	    projectService.addResources(project1.getProjectKey(), resourcePool.getResourcePoolKey());
	
	    ActivityInterface act1 = projectService.addActivity(project1.getProjectKey(), "act 1");
	    act1.setStartDate(startDate);
	    
	    ActivityInterface act2 = projectService.addActivity(project1.getProjectKey(), "act 2");
	    act2.setDuration(16);
	    
	    
	    project1.associateActivities(act1.getActivityKey(),act2.getActivityKey());
	  
	    resourceService.assign(abe.getKey(), act1.getActivityKey(), project1.getProjectKey(), 100);
	    resourceService.assign(bob.getKey(), act2.getActivityKey(), project1.getProjectKey(), 100);
	
	    projectService.addProjectToPool(project1);
	    
	} catch (ResourceException re) {
		fail("ResourceException in buildProject()" + re.getMessage());
	} catch (ParseException pe) {
		fail("ParseException in buildProject()" + pe.getMessage());
	} catch (InternalProjectException ipe) {
		fail("InternalProjectException in buildProject()" + ipe.getMessage());
	} catch (KeyNotFoundException knfe) {
		fail("KeyNotFoundException in buildProject()" + knfe.getMessage());
	} catch (DuplicateKeyException dke) {
		fail("DuplicateKeyException in buildProject()" + dke.getMessage());
	} catch (InternalActivityException iae) {
		fail("InternalActivityException in buildProject()" + iae.getMessage());
	} catch (CycleDependencyException cde) {
		fail("CycleDependencyException in buildProject()" + cde.getMessage());
	}
	
	
	return project1;
}

    
    /// BEGIN TESTCASES


    public void testGetName() throws Exception {
        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            assertEquals("Test Project 1",project1.getName());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetName", e.getMessage());
        }
    }

    public void testSetName() throws Exception {
        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            project1.setName("New Project 1 Name");
            assertEquals("New Project 1 Name",project1.getName());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testSetName", e.getMessage());
        }
    }


    public void testProjectKey1() throws Exception {

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            assertEquals(project1.getProjectKey(),project1Key);

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testProjectKey1", e.getMessage());
        }
    }

    public void testProjectKey2() throws Exception {

        try {

        	ProjectInterface p2 = buildProject("testProjectKey2");

            projectService = ProjectService.getInstance();
            
            ProjectInterface p3 = projectService.getProject(p2.getProjectKey());

            assertEquals(p2.getProjectKey(),p3.getProjectKey());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testProjectKey2", e.getMessage());
        }
    }

    public void testProjectKey1And2NotSame() throws Exception {

        try {

        	ProjectInterface p1 = buildProject("P1-testProjectKey1And2NotSame");
        	ProjectInterface p2 = buildProject("P2-testProjectKey1And2NotSame");

            projectService = ProjectService.getInstance();
            p1 = projectService.getProject(p1.getProjectKey());
            p2 = projectService.getProject(p2.getProjectKey());

            assertNotSame(p1.getProjectKey(),p2.getProjectKey());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testProjectKey1And2NotSame", e.getMessage());
        }
    }

    public void testGetStartDate() throws Exception {
        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);


            assertEquals(startDate,project1.getStartDate());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetStartDate", e.getMessage());
        }
    }

    public void testSetStartDate() throws Exception {
        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);


            // Monday April 25, 2005
            Date startDateNew = DF.parse("2005-04-25");

            project1.setStartDate(startDateNew);

            assertEquals(startDateNew,project1.getStartDate());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testSetStartDate", e.getMessage());
        }
    }

    public void testGetEndDate() throws Exception {
        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            project1.setStartDate(thursday04142005);
            
            // Friday April 15, 2005
            Date endDate = DF.parse("2005-04-15");


            assertEquals(endDate,project1.getEndDate());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetEndDate", e.getMessage());
        }


    }

/*
    public void testSetEndDate() throws Exception {
        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);


            projectService.recalculateProject(project1Key);

            // Friday April 29, 2005
            Date endDate = DF.parse("2005-04-29");

            project1.setEndDate(endDate);

            assertEquals(endDate,project1.getEndDate());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testSetEndDate", e.getMessage());
        }
    }
*/
    public void testGetResourcePoolKey() throws Exception  {

        int ARE_EQUAL = 0;
        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            assertEquals(project1.getResourcePoolKey().compareTo(project1PoolKey),ARE_EQUAL);

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetResourcePoolKey", e.getMessage());
        }
    }

    public void testGetCalendarKey() throws Exception  {

        try {
            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);


            assertNotNull(project1.getCalendarKey());
        }
        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetCalendarKey", e.getMessage());
        }
    }

    public void testSetCalendarKey() throws Exception  {

        try {
            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            CalendarKey prevKey = project1.getCalendarKey();
            CalendarKey newKey = new CalendarKey();

            project1.setCalendarKey(newKey);

            assertEquals(newKey,project1.getCalendarKey());
        }
        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testSetCalendarKey", e.getMessage());
        }

    }

    /*
     * Every Activity except 1 is on the CP
     */
    public void testCalculateCriticalPath1() throws Exception  {

        Date startDate1a;
        Date startDate1b;

        try {

        	ProjectInterface p1 = buildProject("testCalculateCriticalPath1");
/*
            projectService = ProjectService.getInstance();
            

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(p1.getProjectKey(), "act1a");
            act1a.setDuration(4);

            // Monday April 24, 2005
            startDate1b = DF.parse("2005-04-24");
            ActivityInterface act1b = projectService.addActivity(p1.getProjectKey(), "act1b");
            act1b.setDuration(6);

            p1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());
            p1.associateActivities(activity1.getActivityKey(),act1b.getActivityKey());
*/
            projectService.recalculateProject(p1.getProjectKey());
            LinkedList cp = p1.calculateCriticalPath();

            /* Debug 
            ListIterator li = cp.listIterator();
            while (li.hasNext()) {
            	Activity a = (Activity)li.next();
            	System.out.println(a.getName());
            }
            */
            
            // The Critical Path size should be 4 ( Start\End act1, act2)
            
            assertEquals(4,cp.size());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testCalculateCriticalPath1", e.getMessage());
        }
    }

    /*
     * Every Activity is on the CP
     */
    public void testCalculateCriticalPath2() throws Exception  {

        Date startDate1a;
        Date startDate1b;

        try {

 
        	setupTestProject1();
        	
            projectService = ProjectService.getInstance();
            
            ProjectInterface p1 = projectService.getProject(project1.getProjectKey());

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(p1.getProjectKey(), "act1a");
            act1a.setStartDate(startDate1a);

            // Friday April 29, 2005
            startDate1b = DF.parse("2005-04-29");
            ActivityInterface act1b = projectService.addActivity(p1.getProjectKey(), "act1b");
            act1b.setStartDate(startDate1b);

            p1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());
            p1.associateActivities(act1a.getActivityKey(),act1b.getActivityKey());

            projectService.recalculateProject(p1.getProjectKey());
            LinkedList cp = p1.calculateCriticalPath();

            /* Debug
            ListIterator li = cp.listIterator();
            while (li.hasNext()) {
            	Activity a = (Activity)li.next();
            	System.out.println(a.getName());
            }
            */
            // The Critical Path size should be 6 (act1a, act1b, End Project Milestone, activity1, do something before activity1activity1)
            
            assertEquals(6,cp.size());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testCalculateCriticalPath2", e.getMessage());
        }
    }

    // Successor is placed correctly
    public void testAssociateActivities1() throws Exception  {

        Date startDate1a;

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            act1a.setStartDate(startDate1a);


            project1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());


            // Verify that activity 1 has 3 successors (activity2 and endMilestone and succAct1)
            assertEquals(3,activity1.getSuccessors().size());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testAssociateActivities1", e.getMessage());
        }
    }

    // Predecessor is placed correctly
    public void testAssociateActivities2() throws Exception  {

        Date startDate1a;

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            act1a.setStartDate(startDate1a);


            project1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());


            // Verify that act1a has 2 predecessors (startMilestone & activity1)
            assertEquals(2,act1a.getPredecessors().size());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testAssociateActivities2", e.getMessage());
        }
    }


    public void testAddActivity() throws Exception  {

        Date startDate1a;

        try {
            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = new Activity(startDate1a,project1Key);

            project1.addActivity(act1a.getActivityKey(), act1a);


            // getActivitiesCount should be 4 (activity1, act1a, succAct1, predAct1) - Milestones not counted
            assertEquals(4,project1.getActivitiesCount());
        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetActivitiesCount", e.getMessage());
        }
    }

    public void testGetActivitiesCount() throws Exception  {

        Date startDate1a;
        Date startDate1b;

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            act1a.setStartDate(startDate1a);

            // Friday April 29, 2005
            startDate1b = DF.parse("2005-04-29");
            ActivityInterface act1b = projectService.addActivity(project1Key, "act1b");
            act1b.setStartDate(startDate1b);

            project1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());
            project1.associateActivities(act1a.getActivityKey(),act1b.getActivityKey());



            // getActivitiesCount should be 5 (activity1, act1a, act1b, succAct1, predAct2) - Milestones not counted
            assertEquals(5,project1.getActivitiesCount());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetActivitiesCount", e.getMessage());
        }
    }

    // Get all activities excluding Milestones
    public void testGetActivities_Comparator() throws Exception  {

        Date startDate1a;
        Date startDate1b;

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            act1a.setStartDate(startDate1a);

            // Friday April 29, 2005
            startDate1b = DF.parse("2005-04-29");
            ActivityInterface act1b = projectService.addActivity(project1Key, "act1b");
            act1b.setStartDate(startDate1b);

            project1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());
            project1.associateActivities(act1a.getActivityKey(),act1b.getActivityKey());



            // getActivities should be 5 (activity1, act1a, act1b, succAct1, predAct1) - Milestones not counted
            assertEquals(5,project1.getActivities(null).size());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetActivities_Comparator", e.getMessage());
        }

    }

    // Get all activities including Milestones
    public void testGetActivities_ComparatorTrue() throws Exception  {

        Date startDate1a;
        Date startDate1b;

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            act1a.setStartDate(startDate1a);

            // Friday April 29, 2005
            startDate1b = DF.parse("2005-04-29");
            ActivityInterface act1b = projectService.addActivity(project1Key, "act1b");
            act1b.setStartDate(startDate1b);

            project1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());
            project1.associateActivities(act1a.getActivityKey(),act1b.getActivityKey());



            // getActivities should be 7 (startMilestone activity1, act1a, act1b, succAct1, predAct1, endMilestone)
            assertEquals(7,project1.getActivities(null,true).size());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetActivities_ComparatorTrue", e.getMessage());
        }
    }

    // Get all activities excluding Milestones
    public void testGetActivitiesComparatorFalse()  throws Exception  {

        Date startDate1a;
        Date startDate1b;

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            act1a.setStartDate(startDate1a);

            // Friday April 29, 2005
            startDate1b = DF.parse("2005-04-29");
            ActivityInterface act1b = projectService.addActivity(project1Key, "act1b");
            act1b.setStartDate(startDate1b);

            project1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());
            project1.associateActivities(act1a.getActivityKey(),act1b.getActivityKey());



            // getActivities should be 5 (activity1, act1a, act1b,succAct1, predAct1) - Milestones not counted
            assertEquals(5,project1.getActivities(null,false).size());

        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetActivities_ComparatorFalse", e.getMessage());
        }

    }

    /*
     * Test Activity is expected activity
     */
    public void testGetActivity1() throws Exception  {

        Date startDate1a;
        Date startDate1b;

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            act1a.setStartDate(startDate1a);

            // Friday April 29, 2005
            startDate1b = DF.parse("2005-04-29");
            ActivityInterface act1b = projectService.addActivity(project1Key, "act1b");
            act1b.setStartDate(startDate1b);

            project1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());
            project1.associateActivities(act1a.getActivityKey(),act1b.getActivityKey());



            // get Activity should get the same activity1 as declared activity1
            assertEquals(project1.getActivity(activity1.getActivityKey()),activity1);


        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetActivity1", e.getMessage());
        }
    }
    /*
     * Test Activity is not another activity
     */
    public void testGetActivity2() throws Exception  {

        Date startDate1a;

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            act1a.setStartDate(startDate1a);

            project1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());


            // get Activity should get the same activity1 as declared activity1
            assertNotSame(project1.getActivity(activity1.getActivityKey()),project1.getActivity(act1a.getActivityKey()));


        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetActivity2", e.getMessage());
        }
    }
    public void testRemoveActivity() throws Exception  {

        Date startDate1a;

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            act1a.setStartDate(startDate1a);

            project1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());


            project1.removeActivity(act1a.getActivityKey());

            // getActivitiesCount should be 3 (activity1,succAct1, predAct1) - Milestones not counted
            assertEquals(3,project1.getActivitiesCount());
        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetActivity2", e.getMessage());
        }
    }
    public void testAddRemoveActivity() throws Exception  {

        Date startDate1a;

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            act1a.setStartDate(startDate1a);

            project1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());

            // getActivitiesCount should be 4 (activity1,succAct1, predAct1, act1a) - Milestones not counted
            assertEquals(4,project1.getActivitiesCount());
 

            project1.removeActivity(act1a.getActivityKey());

            // getActivitiesCount should be 3 (activity1,succAct1, predAct1) - Milestones not counted
            assertEquals(3,project1.getActivitiesCount());
        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testGetActivity2", e.getMessage());
        }
    }
    public void testUpdateActivity()  throws Exception  {


        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);


            activity1.setDuration(99);

            project1.updateActivity(activity1.getActivityKey(),activity1);

            Activity foo = project1.getActivity(activity1.getActivityKey());


            // getDuration for Activity foo should have the updated value of 99
            assertEquals(foo.getDuration(),99);


        }

        catch (Exception e) {
            // Fail the test
            assertEquals("EXCEPTION : testUpdateActivity", e.getMessage());
        }
    }

}

/**
 * $Log: TestProject.java,v $
 * Revision 1.20  2005/05/09 18:55:59  tlong2
 * really really final
 *
 * Revision 1.19  2005/05/09 17:37:08  tlong2
 * final updates
 *
 * Revision 1.18  2005/05/08 21:37:18  rcoutinh
 * Corrected compile errors
 *
 * Revision 1.17  2005/05/05 05:58:19  tlong2
 * Fixed all core tests except 2.
 *
 * Revision 1.16  2005/04/29 03:22:55  tlong2
 * fixed a couple more
 *
 * Revision 1.15  2005/04/29 02:47:50  tlong2
 * can't leave these tests alone
 *
 * Revision 1.14  2005/04/29 01:21:30  tlong2
 * fixed calls to old pre-ProjectKey param Activity constructor calls
 *
 * Revision 1.13  2005/04/28 05:42:54  achou
 * Removed dependency on DateFormatter.
 *
 * Revision 1.12  2005/04/27 12:45:25  tlong2
 * updates to account for resourceService.assign() changes
 *
 * Revision 1.11  2005/04/27 11:45:15  tlong2
 * updates
 *
 * Revision 1.10  2005/04/24 23:34:18  tlong2
 * a few more updates
 *
 * Revision 1.9  2005/04/24 22:36:14  tlong2
 * updates to tests
 *
 * Revision 1.8  2005/04/24 16:13:05  rcoutinh
 * updates due to Project and Activity calendar changes
 *
 * Revision 1.7  2005/04/24 14:14:03  tlong2
 * Added tests for all Project Methods
 *
 * Revision 1.6  2005/04/23 11:28:24  tlong2
 * Added stub methods to complete
 *
 * Revision 1.5  2005/03/29 18:55:44  ccochran
 * fixed bug in test code
 *
 * Revision 1.4  2005/03/29 12:16:44  ccochran
 * Added code for tests
 *
 * Revision 1.3  2005/03/28 06:28:49  tlong2
 * Completed Iteration 1 tests
 *
 * Revision 1.2  2005/03/28 01:19:17  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:39  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:21  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:32  dnathan
 * Updated CVS footer.
 *
 **/