/**
 * CSCIE 275 - Software Architecture and Engineering
 *
 * File: $RCSfile: TestActivity.java,v $
 * @author $Author: tlong2 $,
 * @version $Revision: 1.25 $
 *
 * <p></p>
 *
 * $Id: TestActivity.java,v 1.25 2005/05/14 14:55:57 tlong2 Exp $
 */
package pm275.core.project;

import junit.framework.TestCase;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import pm275.common.exception.CycleDependencyException;
import pm275.common.exception.DuplicateKeyException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.ResourceException;
import pm275.core.calendar.CalendarService;
import pm275.core.resource.ResourcePoolKey;
import pm275.core.resource.ResourceService;
import pm275.core.resource.ResourcePool;

import pm275.core.resource.ResourceInterface;

public class TestActivity extends TestCase {
    private static final int DEFAULT_DURATION = 8;

    private ResourceService resourceService;
    private ProjectService projectService;

    private ResourcePool    project1Pool;
    private ResourcePoolKey project1PoolKey;
    private ResourceInterface paul;
    private ResourceInterface peter;
    private ResourceInterface mary;
    private ActivityInterface activity1;
    private ProjectInterface project1;
    private ProjectKey project1Key;
    
    
    private static final java.text.DateFormat DF =
            new java.text.SimpleDateFormat("yyyy-MM-dd");


    // startDate = Thursday April 14 2005
    private Date startDate;
    private Date thursday04142005;
    private Date friday04152005;
    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
        resourceService = ResourceService.getInstance();
        projectService = ProjectService.getInstance();
        startDate = DF.parse("2005-04-14");
        thursday04142005 = DF.parse("2005-04-14");
        friday04152005 = DF.parse("2005-04-15");
        setupTestProject();
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /*
     * Build a test project and return it
     * @return Test Project
     */
    private ProjectInterface buildProject(String sProjectName) 
    		throws 	ResourceException, 
					ParseException, 
					InternalProjectException, 
					KeyNotFoundException,
					DuplicateKeyException,
					InternalActivityException,
					CycleDependencyException {
    	
	    ResourceService resourceService = ResourceService.getInstance();
	    ProjectService projectService = ProjectService.getInstance();

	    ProjectInterface project1 = projectService.addProject(sProjectName);
	   
   	    ResourcePool resourcePool = resourceService.createResourcePool();
	    ResourceInterface abe  = resourceService.createResource("Abe");
	    ResourceInterface bob  = resourceService.createResource("Bob");

        
    	try {
    	    resourcePool.addResource(abe);
    	    resourcePool.addResource(bob);
    	    resourceService.addResourcePool(resourcePool);
    	    
    	    DateFormat DF = new SimpleDateFormat("yyyy-MM-dd");
            // Thursday April 14, 2005
            Date startDate = DF.parse("2005-04-14");    
            
            project1.setStartDate(startDate);
 
            ActivityInterface sm = project1.getStartMilestone();
            sm.setStartDate(startDate);
            ActivityInterface em = project1.getEndMilestone();
            em.setStartDate(startDate);
            
            // Add Resource Pool to Project
            projectService.addResources(project1.getProjectKey(), resourcePool.getResourcePoolKey());

            ActivityInterface act1 = projectService.addActivity(project1.getProjectKey(), "act 1");
            act1.setStartDate(startDate);
            
            ActivityInterface act2 = projectService.addActivity(project1.getProjectKey(), "act 2");
            act2.setDuration(16);
            
            
            project1.associateActivities(act1.getActivityKey(),act2.getActivityKey());
          
            resourceService.assign(abe.getKey(), act1.getActivityKey(), project1.getProjectKey(), 100);
            resourceService.assign(bob.getKey(), act2.getActivityKey(), project1.getProjectKey(), 100);

            projectService.addProjectToPool(project1);
            
    	} catch (ResourceException re) {
    		fail("ResourceException in buildProject()" + re.getMessage());
    	} catch (ParseException pe) {
    		fail("ParseException in buildProject()" + pe.getMessage());
    	} catch (InternalProjectException ipe) {
    		fail("InternalProjectException in buildProject()" + ipe.getMessage());
    	} catch (KeyNotFoundException knfe) {
    		fail("KeyNotFoundException in buildProject()" + knfe.getMessage());
    	} catch (DuplicateKeyException dke) {
    		fail("DuplicateKeyException in buildProject()" + dke.getMessage());
    	} catch (InternalActivityException iae) {
    		fail("InternalActivityException in buildProject()" + iae.getMessage());
    	} catch (CycleDependencyException cde) {
    		fail("CycleDependencyException in buildProject()" + cde.getMessage());
    	}
    	
    	
    	return project1;
    }
    
    protected void setupTestProject() throws Exception {
    	ActivityInterface predAct1;
    	ActivityInterface succAct1;
    	
        project1Pool = null;
        project1PoolKey = null;
        paul = null;
        peter = null;
        activity1 = null;
        project1 = null;
        project1Key = null;
        
        try {
            // Create Resource Pool and add Peter and Paul
            project1Pool    = resourceService.createResourcePool();
            project1PoolKey = project1Pool.getResourcePoolKey();

            paul  = resourceService.createResource("Paul");
            project1Pool.addResource(paul);
            peter = resourceService.createResource("Peter");
            project1Pool.addResource(peter);
            resourceService.addResourcePool(project1Pool);

            // Create Project
            project1 = projectService.addProject("Test Project 1");
            project1Key = project1.getProjectKey();

            // Friday, Apr. 22, 2005 a workday
            Date startDate = DF.parse("2005-04-22");
            project1.setStartDate(startDate);
            
            ActivityInterface sm = project1.getStartMilestone();
            sm.setStartDate(startDate);

            
            // Add Resource Pool to Project
            projectService.addResources(project1Key, project1PoolKey);

            activity1 = projectService.addActivity(project1Key, "activity1");

            predAct1 = projectService.addActivity(project1Key, "do something before activity1");
            predAct1.setDuration(4);
            
            succAct1 = projectService.addActivity(project1Key, "do something after activity1");
            succAct1.setDuration(4);
            
            project1.associateActivities(predAct1.getActivityKey(),activity1.getActivityKey());
            project1.associateActivities(activity1.getActivityKey(),succAct1.getActivityKey());
          

            /**
             * set resources
             */
            resourceService.assign(paul.getKey(), activity1.getActivityKey(), project1Key, 100);
            resourceService.assign(peter.getKey(), activity1.getActivityKey(), project1Key, 100);
        }
        catch (Exception e) {

            // Fail the test setup
            assertEquals("EXCEPTION : SetupTestProject", e.getMessage());
        }

    }

    /// BEGIN TESTCASES

    public void testActivityConstructor() {
        Date theDay = thursday04142005;

        ActivityInterface a = new Activity(theDay,null);
        String sResult = a.toString();
        assertNotNull(sResult,a);

    }

    public void testComputeEndDate()
    {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);
        assertNotNull(a.getStartDate());
        a.setName("Activity1");
        a.setDuration(40);
        Calendar cal = Calendar.getInstance();
        cal.setTime(theDay);
        // 7 because  don't count weekends, if so, then 5
        cal.add(Calendar.DAY_OF_MONTH,7);
        assertEquals("Error", cal.getTime() , a.getEndDate());

    }

    public void testActivityConstructor4Params() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,3,false,project1Key);
        String sResult = a.toString();
        assertNotNull(sResult,a);

    }

    public void testActivityConstructor4ParamsDuration() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,3,false,project1Key);
        assertEquals("Test that ActivityInterface constructor sets duration to 3",3,a.getDuration());


    }


    public void testGetName() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);
        a.setName("Activity1");
        String sResult = a.getName();
        assertEquals("Activity1", sResult);
    }

    public void testSetNameIsSetInConstructor() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);
        String sResult = a.getName();

        // default name set in constructor
        assertEquals("Activity name not set", sResult);
    }

    public void testSetStartDate() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);

        // Sets start date to theDay by default. Verify not null
        assertNotNull(a.getStartDate().toString());
    }

    // More precise test than testSetStartDate()
    public void testGetStartDate() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);
        // Sets start date to theDay
        CalendarService cs = CalendarService.getInstance();
        theDay = cs.getNextWorkDay(theDay);
        String sResult = a.getStartDate().toString();
        assertEquals(theDay.toString(), sResult);
    }


    public void testGetEndDateNotSameAsStartDateByDefault() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);

        // If default duration = 8 default start and end date should be different
        assertNotSame(a.getStartDate().toString(), a.getEndDate().toString());
    }

    // Should be day after theDay by default. (default duration == 8)
    public void testGetEndDate() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);
        CalendarService cs = CalendarService.getInstance();
        theDay = cs.getNextWorkDay(theDay);
        // Sets end date to theDay by default
        String sResult = a.getEndDate().toString();
        assertEquals(friday04152005.toString(), sResult);
    }

    public void testGetIsFixedDurationTrue() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);
        assertTrue(a.getIsFixedDuration());

    }
    public void testGetIsFixedDurationFalse() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,3,false,project1Key);

        assertFalse(a.getIsFixedDuration());

    }
    public void testGetDuration() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);
        int dur = 0;
        // default duration is 8 hrs
        assertEquals("Test that ActivityInterface constructor sets default duration",DEFAULT_DURATION,a.getDuration());
        a.setDuration(40);
        assertEquals(40,a.getDuration());



    }

    public void testSetDuration() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);

        a.setDuration(2);
        assertEquals(2, a.getDuration());
    }

    public void testAddResource() throws Exception
    {
        int TOTAL_RESOURCES = 2;

        try {

            // This will add 2 resources
            //setupTestProject();

            // Verify that Activity1 has 2 resources
            assertEquals(TOTAL_RESOURCES,activity1.getResources().size());


        }

        catch (Exception e) {

            // Fail the test
            assertEquals("EXCEPTION : AddResource", e.getMessage());
        }

    }

    public void testGetResources() throws Exception {

         int TOTAL_RESOURCES = 2;


        try {

           // setupTestProject();

            projectService = ProjectService.getInstance();


            // Should have 2 resources peter & paul
            assertEquals(TOTAL_RESOURCES,activity1.getResources().size());


        }
        catch (Exception e) {

            // Fail the test
            assertEquals("EXCEPTION : GetResources", e.getMessage());
        }

    }

    public void testSetResources() throws Exception {

        int TOTAL_RESOURCES = 2;


        try {

            //setupTestProject();


            activity1.setResources();

            // Verify that ActivityInterface 1 has 2 resources
            assertEquals(activity1.getResources().size(),TOTAL_RESOURCES);


        }

        catch (Exception e) {

            // Fail the test
            fail("EXCEPTION : AddResource. " + e.getMessage());
        }


    }

    public void testRemoveResource() throws Exception {

        int TOTAL_RESOURCES = 2;
        try {


        	ProjectInterface p = buildProject("testRemoveResource");

            projectService = ProjectService.getInstance();
  
            // Remove a resource
            ResourceService rs = ResourceService.getInstance();
            Iterator it = p.getActivities(null,false).iterator();
            
            ActivityInterface a = (ActivityInterface) it.next();
            Iterator it2 = a.getResources().iterator();
            
            ResourceInterface res = (ResourceInterface)it2.next();
            
            rs.deassign(res.getKey(),a.getActivityKey(),p.getProjectKey());
            

            // Verify that ActivityInterface 1 has 0 resources
            assertEquals(0,a.getResources().size());
            
            

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : RemoveResource. " + e.getMessage());

        }
    }

    public void testClearResources()  throws Exception {

        int TOTAL_RESOURCES = 2;

        try {


            setupTestProject();

            projectService = ProjectService.getInstance();
            Project p = projectService.getProject(project1Key);
  
            activity1.setResources();

            // Remove a resource
            activity1.clearResources();

            // Verify that ActivityInterface 1 has NO resources
            assertEquals(activity1.getResources().size(),0);
        }
        catch (Exception e) {
            // Fail the test
			e.printStackTrace();
            fail("EXCEPTION : ClearResources. " + e.getMessage());

        }

    }

    public void testAddPredecessor() throws Exception
    {

        try {


            setupTestProject();

            Date activity2StartDate = DF.parse("2005-04-13");
            ActivityInterface activity2 = projectService.addActivity(project1Key, "activity2");


            activity2.setStartDate(activity2StartDate);

            activity1.addPredecessor(activity2.getActivityKey());

            // 3 (startMilestone & activity2 & predAct1)
            assertEquals(3, activity1.getPredecessors().size());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testAddPredecessor" + e.getMessage());

        }

    }
    public void testGetPredecessors() throws Exception
    {



        try {


          	ProjectInterface p = buildProject("testGetPredecessors");
 
            projectService = ProjectService.getInstance();
               
            Iterator it1 = p.getActivities(null,true).iterator();
                
            ActivityInterface a2 = null;
            while (it1.hasNext()) {
              	 a2 = (ActivityInterface)it1.next();
               	if (a2.isEndMilestone()) {
              		break;
               	}
                	
            }

            // Should be 2
         assertEquals(2,a2.getPredecessors().size());

        } catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testGetPredecessors" + e.getMessage());

        }


    }
    public void testRemovePredecessor() throws Exception
    {

        try {


            setupTestProject();
            Date act2StartDate = DF.parse("2005-04-13");
            ActivityInterface act2 = projectService.addActivity(project1.getProjectKey(), "act2");
            act2.setStartDate(act2StartDate);

            projectService = ProjectService.getInstance();
            ProjectInterface p = projectService.getProject(project1.getProjectKey());
            // Associate activities
            p.associateActivities(act2.getActivityKey(), activity1.getActivityKey());
            projectService.recalculateProject(p.getProjectKey());


            activity1.removePredecessor(act2.getActivityKey());

            // confirm that the getPredecessors has 1 (startMilestone & predAct1)
            assertEquals(2,activity1.getPredecessors().size());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testRemovePredecessor" + e.getMessage());

        }

    }

    public void testAddRemovePredecessor() throws Exception
    {

        try {


            setupTestProject();
            Date activity2StartDate = DF.parse("2005-04-13");
            ActivityInterface activity2 = projectService.addActivity(project1Key, "activity2");
            activity2.setStartDate(activity2StartDate);

            projectService = ProjectService.getInstance();
            ProjectInterface p = projectService.getProject(project1Key);
            // Associate activities
            p.associateActivities(activity2.getActivityKey(), activity1.getActivityKey());


            // confirm that the getPredecessors has 3 (startMilestone & activity2 & predAct1)
            assertEquals(3,activity1.getPredecessors().size());


            activity1.removePredecessor(activity2.getActivityKey());

            // confirm that the getPredecessors has 1 (startMilestone & predAct1)
            assertEquals(2,activity1.getPredecessors().size());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testRemovePredecessor" + e.getMessage());

        }

    }

    
    public void testAddSuccessor() throws Exception {

        try {


            setupTestProject();
            Date activity2StartDate = DF.parse("2005-04-13");
            ActivityInterface activity2 = projectService.addActivity(project1Key, "activity2");
            activity2.setStartDate(activity2StartDate);


            activity1.addSuccessor(activity2.getActivityKey());

            // Should be 3 (endMilestone & activity2 & succAct1)
            assertEquals(3, activity1.getSuccessors().size());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testAddSuccessor" + e.getMessage());

        }

    }

    public void testGetSuccessor() throws Exception {
        {

            try {


            	ProjectInterface p = buildProject("testGetSuccessor");
 
                projectService = ProjectService.getInstance();
                
                Iterator it1 = p.getActivities(null,true).iterator();
                
                ActivityInterface a2 = null;
                while (it1.hasNext()) {
                	 a2 = (ActivityInterface)it1.next();
                	if (a2.isStartMilestone()) {
                		break;
                	}
                	
                }
                	
                
                
                // should be 2
                assertEquals(2,a2.getSuccessors().size());

            }
            catch (Exception e) {
                // Fail the test
                fail("EXCEPTION : testGetSuccessor" + e.getMessage());

            }
        }
    }

    public void testRemoveSuccessor() throws Exception {

        try {


            setupTestProject();
            Date activity2StartDate = DF.parse("2005-04-13");
            ActivityInterface activity2 = projectService.addActivity(project1Key, "activity2");
            activity2.setStartDate(activity2StartDate);

            activity1.addSuccessor(activity2.getActivityKey());

            activity1.removeSuccessor(activity2.getActivityKey());

            // confirm that the getSuccessor has 1 (endMilestone, succAct1)
            assertEquals(2,activity1.getPredecessors().size());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testRemoveSuccessor" + e.getMessage());

        }

    }
    public void testAddRemoveSuccessor() throws Exception {

        try {


            setupTestProject();
            Date activity2StartDate = DF.parse("2005-04-13");
            ActivityInterface activity2 = projectService.addActivity(project1Key, "activity2");
            activity2.setStartDate(activity2StartDate);

            project1.associateActivities(activity1.getActivityKey(),activity2.getActivityKey());
    
            // confirm that the getSuccessor has 3 (endMilestone, succAct1, activity2)
            assertEquals(3,activity1.getSuccessors().size());

            activity1.removeSuccessor(activity2.getActivityKey());

            // confirm that the getSuccessor has 2 (endMilestone, succAct1)
            assertEquals(2,activity1.getSuccessors().size());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testRemoveSuccessor" + e.getMessage());

        }

    }
    public void testCalculateSlackTime() throws Exception  {
        Date startDate1a;
        Date startDate1b;

        try {


            setupTestProject();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            // Monday April 24, 2005
            startDate1a = DF.parse("2005-04-24");
            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            act1a.setStartDate(startDate1a);

            // Friday April 29, 2005
            startDate1b = DF.parse("2005-04-29");
            ActivityInterface act1b = projectService.addActivity(project1Key, "act1b");
            act1b.setStartDate(startDate1b);

            project1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());
            project1.associateActivities(act1a.getActivityKey(),act1b.getActivityKey());

            projectService.recalculateProject(project1Key);

            assertEquals(0, act1b.calculateSlackTime());

        }
        catch (Exception e) {
            // Fail the test
			e.printStackTrace();
            fail("EXCEPTION : testCalculateSlackTime" + e.getMessage());

        }
    }

    public void testDisassociateActivity() throws Exception {

        try {

        	ProjectInterface p = buildProject("testDisassociateActivity");
            
            projectService = ProjectService.getInstance();

            Iterator it = p.getActivities(null,false).iterator();
            
            ActivityInterface a = (ActivityInterface) it.next();
            
 
            // calls disassociateActivity
            projectService.removeActivity(p.getProjectKey(),a.getActivityKey());



            // should be 1 
            assertEquals(1, p.getActivitiesCount());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testDisassociateActivity. " + e.getMessage());

        }
    }



    public void testGetProjectKey()throws Exception {
        try {

            setupTestProject();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);


            // confirm that the project key is same
            assertEquals(activity1.getProjectKey().toString(),project1.getProjectKey().toString());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testGetProjectKey. " + e.getMessage());

        }
    }



    public void testGetActivityKey() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);

        assertNotNull(a.getActivityKey());
    }

    public void testSetActivityKey() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);
        ActivityKey ak = new ActivityKey("New Key");

        a.setActivityKey(ak);


        assertEquals(a.getActivityKey().toString(),"New Key");
    }



    public void testGetStatus() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);

        int status = 0;
        assertEquals(status, a.getStatus());

    }

    public void testSetStatus() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);

        a.setStatus(2);
        assertEquals(2, a.getStatus());
    }

    public void testIsStartMilestone() throws Exception{
        try {

            setupTestProject();

            projectService = ProjectService.getInstance();

            project1 = projectService.getProject(project1Key);

            // return milestones
            List list = project1.getActivities(null,true);

            Iterator it = list.iterator();

            ActivityInterface a = null;

            while (it.hasNext()) {
                a = (Activity) it.next();
                if (a.isStartMilestone())
                    break;
            }


            // confirm that the first element is the startmilestone
            assertTrue(a.isStartMilestone());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testIsStartMilestone. " + e.getMessage());

        }
    }

    public void testIsEndMilestone() throws Exception {
        try {

            setupTestProject();

            projectService = ProjectService.getInstance();

            project1 = projectService.getProject(project1Key);

            // return milestones
            List list = project1.getActivities(null,true);

            Iterator it = list.iterator();

            ActivityInterface a = null;

            while (it.hasNext()) {
                a = (Activity) it.next();
                if (a.isEndMilestone())
                    break;
            }

            // confirm that the first element is the startmilestone
            assertTrue(a.isEndMilestone());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testIsEndMilestone. " + e.getMessage());

        }
    }

    public void testResetCriticalPathValuesEFF() {
        try {

            setupTestProject();


            activity1.resetCriticalPathValues();


            assertFalse(activity1.getEarlyFinishFlag());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testResetCriticalPathValuesEFF. " + e.getMessage());

        }
    }

    public void testResetCriticalPathValuesLSF() {
        try {

            setupTestProject();


            activity1.resetCriticalPathValues();


            assertFalse(activity1.getLateStartFlag());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testResetCriticalPathValuesEFF. " + e.getMessage());

        }
    }


    public void testResetCriticalPathValuesESH() {
        try {

            activity1.resetCriticalPathValues();


            assertEquals(-1,activity1.getEarlyStartHours());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testResetCriticalPathValuesESH. " + e.getMessage());

        }
    }

    public void testResetCriticalPathValuesEFH() {
        try {

            setupTestProject();

            projectService = ProjectService.getInstance();

            project1 = projectService.getProject(project1Key);

            project1.calculateCriticalPath();
            
            activity1.resetCriticalPathValues();


            assertEquals(activity1.getEarlyFinishHours(),-1);

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testResetCriticalPathValuesEFH. " + e.getMessage());

        }
    }


    public void testGetEarlyStartHours() throws Exception {
        try {


        Date activity1StartDate = DF.parse("2005-04-15");
        ActivityInterface activity1 = new Activity(activity1StartDate,null);
        //  -1 is the default
        assertEquals(-1,activity1.getEarlyStartHours());
        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testGetEarlyStartHours. " + e.getMessage());

        }
    }

    public void testGetEarlyFinishHours() throws Exception {
        try {


        Date activity1StartDate = DF.parse("2005-04-15");
        ActivityInterface activity1 = new Activity(activity1StartDate,null);

        // -1 is the default
        assertEquals(-1,activity1.getEarlyFinishHours());
        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testGetEarlyFinishHours. " + e.getMessage());

        }
    }

    public void testGetLateStartHours()  throws Exception {
        try {


        Date activity1StartDate = DF.parse("2005-04-15");
        ActivityInterface activity1 = new Activity(activity1StartDate,null);

        assertEquals(-1,activity1.getLateStartHours());
        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testGetLateStartHours. " + e.getMessage());

        }
    }

    public void testGetLateFinishHours() throws Exception {
        try {


        Date activity1StartDate = DF.parse("2005-04-15");
        ActivityInterface activity1 = new Activity(activity1StartDate,null);

        assertEquals(-1,activity1.getLateFinishHours());
        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testGetLateFinishHours. " + e.getMessage());

        }
    }

    public void testGetEarlyFinishFlag() throws Exception {
        try {


        Date activity1StartDate = DF.parse("2005-04-15");
        ActivityInterface activity1 = new Activity(activity1StartDate,null);

        assertFalse(activity1.getEarlyFinishFlag());
        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testGetEarlyFinishFlag. " + e.getMessage());

        }
    }

    public void testGetLateStartFlag() throws Exception {
        try {


        Date activity1StartDate = DF.parse("2005-04-15");
        ActivityInterface activity1 = new Activity(activity1StartDate,null);

        assertFalse(activity1.getLateStartFlag());
        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testGetLateStartFlag. " + e.getMessage());

        }
    }


    public void testGetDurationMeasure() throws Exception {
        try {

        Date activity1StartDate = DF.parse("2005-04-15");
        ActivityInterface activity1 = new Activity(activity1StartDate,null);


        activity1.setDurationMeasure(100);

        assertEquals(activity1.getDurationMeasure(),100);
        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testGetDurationMeasure. " + e.getMessage());

        }
    }

    public void testToString() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);
        ActivityKey ak = new ActivityKey("New Key");


        assertNotNull(a.toString());
    }

    public void testGetSlackTime() throws Exception {
        try {

            setupTestProject();

            projectService = ProjectService.getInstance();

            project1 = projectService.getProject(project1Key);

            project1.calculateCriticalPath();

            ActivityInterface a = project1.getActivity(activity1.getActivityKey());

            assertEquals(a.calculateSlackTime(),a.getSlackTime());

        }
        catch (Exception e) {
            // Fail the test
            fail("EXCEPTION : testGetSlackTime. " + e.getMessage());

        }
    }

    public void testGetDisplayOrder() {
        Date theDay = thursday04142005;
        ActivityInterface a = new Activity(theDay,null);

        a.setDisplayOrder(3);

        assertEquals(a.getDisplayOrder(),3);
    }

    public static void main(String[] args) {
        junit.textui.TestRunner.run(TestActivity.class);
    }
}

/**
 * $Log: TestActivity.java,v $
 * Revision 1.25  2005/05/14 14:55:57  tlong2
 * Removed println
 *
 * Revision 1.24  2005/05/09 17:52:44  tlong2
 * really final changes
 *
 * Revision 1.23  2005/05/09 17:37:00  tlong2
 * final updates
 *
 * Revision 1.22  2005/05/05 05:58:19  tlong2
 * Fixed all core tests except 2.
 *
 * Revision 1.21  2005/04/30 15:45:14  rcoutinh
 * Some cleanup - Only one failure remains  with critical path test
 *
 * Revision 1.20  2005/04/29 03:22:55  tlong2
 * fixed a couple more
 *
 * Revision 1.19  2005/04/29 02:47:50  tlong2
 * can't leave these tests alone
 *
 * Revision 1.18  2005/04/29 01:21:30  tlong2
 * fixed calls to old pre-ProjectKey param Activity constructor calls
 *
 * Revision 1.17  2005/04/28 05:42:54  achou
 * Removed dependency on DateFormatter.
 *
 * Revision 1.16  2005/04/27 12:45:25  tlong2
 * updates to account for resourceService.assign() changes
 *
 * Revision 1.15  2005/04/27 11:45:15  tlong2
 * updates
 *
 * Revision 1.14  2005/04/24 22:36:14  tlong2
 * updates to tests
 *
 * Revision 1.13  2005/04/24 16:13:04  rcoutinh
 * updates due to Project and Activity calendar changes
 *
 * Revision 1.12  2005/04/24 14:14:26  tlong2
 * minor updates
 *
 * Revision 1.11  2005/04/24 02:10:33  tlong2
 * Added a bunch of unit tests
 *
 * Revision 1.10  2005/04/23 22:31:45  tlong2
 * update some tests. more to go
 *
 * Revision 1.9  2005/04/23 11:11:50  tlong2
 * Added stub test methods
 *
 * Revision 1.8  2005/04/19 09:22:57  achou
 * Changed code to get next work day.
 *
 * Revision 1.7  2005/04/18 04:22:32  rcoutinh
 * added test case for computeEndDate
 * and handled case where theDay is not workday in test case for testGetStartDate and testGetEndDate
 *
 * Revision 1.6  2005/04/17 22:13:26  tlong2
 * Added Activity tests
 *
 * Revision 1.5  2005/03/29 18:29:14  ccochran
 * fix status bug
 *
 * Revision 1.4  2005/03/29 17:05:41  ccochran
 * added test for status methods
 *
 * Revision 1.3  2005/03/28 06:28:27  tlong2
 * Completed Iteration 1 tests
 *
 * Revision 1.2  2005/03/28 01:19:17  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:38  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.7  2005/03/27 02:58:31  dnathan
 * Added import for new Activity package.
 *
 * Revision 1.6  2005/03/26 09:02:28  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.5  2005/03/25 14:27:50  ccochran
 * CEC Fixed some minor things
 *
 * Revision 1.4  2005/03/23 04:45:27  tlong2
 * First attempt at Junit tests
 *
 * Revision 1.3  2005/03/22 06:26:19  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:31  dnathan
 * Updated CVS footer.
 *
 **/