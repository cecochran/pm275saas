package pm275.core.project;

import java.util.HashMap;
import java.util.Map;
import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import pm275.core.project.ActivityInterface;
import pm275.core.project.ActivityDAOtoDB;
import pm275.core.project.ProjectDAOtoDB;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ProjectKey;
import pm275.core.project.ProjectService;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourceService;
import pm275.common.exception.ResourceException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.DuplicateKeyException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.CycleDependencyException;
import org.apache.log4j.Logger;

import junit.framework.TestCase;
/***************************************************
 * File:        $RCSfile: TestActivityDAOtoDB.java,v $
 * @author      $Author: vchumako $,
 * @version     $Id: TestActivityDAOtoDB.java,v 1.7 2005/05/09 16:46:18 vchumako Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * In the spirt of the layering pattern, this class provides a self contained 
 * mechanism for all Activity based presistance to DBMS. Assumes a predefined
 * database structure that this class must mirror.
 * CREATE TABLE `activity` (
  `activitykey` varchar(255) NOT NULL default '',
  `projectkey` varchar(255) NOT NULL default '',
  `name` varchar(255) default NULL,
  `startdate` datetime default NULL,
  `enddate` datetime default NULL,
  `duration` int(11) default NULL,
  `slacktime` int(11) default NULL,
  `status` tinyint(4) default '0',
  `resourcepoolkey` varchar(255) default NULL,
  `fixedduration` tinyint(4) default '0',
  `fixedstartdate` tinyint(4) default '0',
  PRIMARY KEY  (`activitykey`),
 * * */


public class TestActivityDAOtoDB extends TestCase {

    private static final Logger log = Logger.getLogger(TestActivityDAOtoDB.class);
    private static final String ATTR = "TestActivityDAOtoDB";
    
    protected void setUp() throws Exception {
        super.setUp();
    }

    /*
     * Build a test project and return it
     * @return Test Project
     */
    private ProjectInterface buildProject() 
    		throws 	ResourceException, 
					ParseException, 
					InternalProjectException, 
					KeyNotFoundException,
					DuplicateKeyException,
					InternalActivityException,
					CycleDependencyException {
    	
	    ResourceService resourceService = ResourceService.getInstance();
	    ProjectService projectService = ProjectService.getInstance();

	    ProjectInterface project1 = projectService.addProject("DAOtoDB Test Project 1");
	   
   	    ResourcePool resourcePool = resourceService.createResourcePool();
	    ResourceInterface chris  = resourceService.createResource("RC1", "Chris C");
	    ResourceInterface tom  = resourceService.createResource("RT1", "Tom L");

        
    	try {
    	    resourcePool.addResource(chris);
    	    resourcePool.addResource(tom);
    	    resourceService.addResourcePool(resourcePool);
    	    
    	    DateFormat DF = new SimpleDateFormat("yyyy-MM-dd");
            // Thursday April 14, 2005
            Date startDate = DF.parse("2005-04-14");    
            
            project1.setStartDate(startDate);
 
            ActivityInterface sm = project1.getStartMilestone();
            sm.setStartDate(startDate);
            ActivityInterface em = project1.getEndMilestone();
            em.setStartDate(startDate);
            
            // Add Resource Pool to Project
            projectService.addResources(project1.getProjectKey(), resourcePool.getResourcePoolKey());

            ActivityInterface act1 = projectService.addActivity(project1.getProjectKey(), "act 1");
            act1.setStartDate(startDate);
            
            ActivityInterface act2 = projectService.addActivity(project1.getProjectKey(), "act 2");
            act2.setDuration(16);
            
            
            project1.associateActivities(act1.getActivityKey(),act2.getActivityKey());
          
            resourceService.assign(chris.getKey(), act1.getActivityKey(), project1.getProjectKey(), 100);
            resourceService.assign(tom.getKey(), act2.getActivityKey(), project1.getProjectKey(), 100);

            
    	} catch (ResourceException re) {
    		fail("ResourceException in buildProject()" + re.getMessage());
    	} catch (ParseException pe) {
    		fail("ParseException in buildProject()" + pe.getMessage());
    	} catch (InternalProjectException ipe) {
    		fail("InternalProjectException in buildProject()" + ipe.getMessage());
    	} catch (KeyNotFoundException knfe) {
    		fail("KeyNotFoundException in buildProject()" + knfe.getMessage());
    	} catch (DuplicateKeyException dke) {
    		fail("DuplicateKeyException in buildProject()" + dke.getMessage());
    	} catch (InternalActivityException iae) {
    		fail("InternalActivityException in buildProject()" + iae.getMessage());
    	} catch (CycleDependencyException cde) {
    		fail("CycleDependencyException in buildProject()" + cde.getMessage());
    	}
    	return project1;
    }
    
    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testGet() throws Exception{
    	
    	//setup build project 
    	ProjectInterface  project1 = buildProject();
	  	ProjectDAOtoDB pdba = new ProjectDAOtoDB();
	    int num = pdba.set(project1);

	    //build a project
	    ProjectDAOtoDB pdba2 = new ProjectDAOtoDB();
	    ProjectInterface project2 = pdba.get(project1.getProjectKey());
	    ProjectKey pkey = new ProjectKey("Scooby Doo2");
	    project2.setProjectKey(pkey);
	    project2.setName("Scooby Doo2");
	    num = pdba2.set(project2);
	    //check for error code
	    assertTrue(num != -1);
	    
	    //Now try to load the project back
	    ProjectInterface proj = pdba2.get(pkey);

	    //check for error code
	    assertNotNull(proj);
	    
	    
    }
    public void testSet() throws Exception{
       //setup build project 
       ProjectInterface  project1 = buildProject();
	  	
	   Map actMap = project1.getActivities();
	   HashMap map = new HashMap(actMap);
	   
	   int cnt = 1;
	   int res = 0;
	   //get iterator
        java.util.Iterator it =  map.values().iterator();
       //cycle thru all the Activities
        while (it.hasNext()) {
            
        	ActivityInterface act = (ActivityInterface) it.next();
        	//get project and call recalcualte method
        	ActivityDAOtoDB actDB = new ActivityDAOtoDB();  
        	res = actDB.set(act);

        	//check for error code 
    	    assertTrue(res == 1);
    	    
        	//log.debug("Count:"+cnt +" Write result" + res);
        }

	    
	    
    	
    }
    public void testDelete() throws Exception{
    	//test activity code
    	ProjectInterface  project1 = buildProject();
    	
    	
	   Map actMap = project1.getActivities();
	   HashMap map = new HashMap(actMap);
	   
	   int cnt = 1;
	   int res = 0;
	   //get iterator
        java.util.Iterator it =  map.values().iterator();
       //cycle thru all the Activities
        while (it.hasNext()) {
            
        	ActivityInterface act = (ActivityInterface) it.next();
        	//get project and call recalcualte method
        	ActivityDAOtoDB actDB = new ActivityDAOtoDB();  
        	res = actDB.set(act);
            log.debug("Count: "+cnt +" Set result: " + res);
            
            actDB.delete(act.getActivityKey());
           log.debug("Count: "+cnt +" Delete result: " + res);
            cnt++;
        }

	    
    }
    
    public void testtoString() throws Exception{
     //is this needed ?
    /*	ActivityDAOtoDB actDB = new ActivityDAOtoDB();
	  	int num = actDB.set(project1);*/
    	
    	assertTrue(true);
    }
}
/**
* $Log: TestActivityDAOtoDB.java,v $
* Revision 1.7  2005/05/09 16:46:18  vchumako
* Assigned unique ID to sample resources
*
* Revision 1.6  2005/05/08 05:33:11  tlong2
* fixed bug in buildProject()
*
* Revision 1.5  2005/05/08 03:42:57  tlong2
* Fixed a few compile errors
*
* Revision 1.4  2005/05/08 02:24:07  tlong2
* Fixed testcase bugs
*
* Revision 1.3  2005/05/06 19:41:09  ccochran
* removed system out
*
* Revision 1.2  2005/05/06 19:40:19  ccochran
* added base code
*
* Revision 1.1  2005/05/05 06:03:23  ccochran
* base code needs more work
*
* Revision 1.3  2005/05/04 12:09:01  ccochran
* insert, update, delete skeleton
*
* Revision 1.2  2005/05/04 09:41:34  ccochran
* added consturctor
*
* Revision 1.1  2005/05/04 05:54:29  ccochran
* added footers
*
*
* Updated footer to include CVS log.
*
**/

