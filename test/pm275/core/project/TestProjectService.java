/**
 * CSCIE 275 - Software Architecture and Engineering
 *
 * File: $RCSfile: TestProjectService.java,v $
 * @author $Author: ccochran $,
 * @version $Revision: 1.18 $
 *
 * <p></p>
 *
 * $Id: TestProjectService.java,v 1.18 2005/05/09 16:26:42 ccochran Exp $
 */
package pm275.core.project;

//import com.sun.rsasign.p;

import java.util.Date;

import junit.framework.TestCase;
import pm275.common.exception.ProjectCreationException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.KeyNotFoundException;
import pm275.core.calendar.CalendarService;
import pm275.core.calendar.CalendarInterface;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourcePoolKey;
import pm275.core.resource.ResourceService;

public class TestProjectService extends TestCase {
    private ProjectService projectService;
    private ResourceService resourceService;
    private CalendarService calendarServiceI;
    private CalendarService calendarService;
    private CalendarInterface calendar1;

    private ResourcePool    project1Pool;
    private ResourcePoolKey project1PoolKey;
    private ResourceInterface paul;
    private ResourceInterface peter;
    private ResourceInterface mary;
    private ActivityInterface activity1;
    private ProjectInterface project1;
    private ProjectKey project1Key;
    
    private ResourcePool    project2Pool;
    private ResourcePoolKey project2PoolKey;
    private ResourceInterface paul2;
    private ResourceInterface peter2;
    private ResourceInterface mary2;
    private ActivityInterface activity2;
    private ProjectInterface project2;
    private ProjectKey project2Key;

    private static final java.text.DateFormat DF =
            new java.text.SimpleDateFormat("yyyy-MM-dd");


    // startDate = Thursday April 14 2005
    private Date startDate;
    private Date startDate2;
    private Date thursday04142005;
    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
        resourceService = ResourceService.getInstance();
        projectService = ProjectService.getInstance();

        // Thursday April 14, 2005
        startDate = DF.parse("2005-04-14");

        // Saturday April 23, 2005
        startDate2 = DF.parse("2005-04-23");

        thursday04142005 = DF.parse("2005-04-14");
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /*
     * Call this to set up a base project.
     * 1 project  : Test Project 1
     * 2 Resources: Peter, Paul (assigned 100% to activity1)
     * 1 Activity : activity1
     * 1 Resource pool assigned to project : project1Pool (peter, paul, mary)
     */
    protected void setupTestProject1() throws Exception {
        project1Pool = null;
        project1PoolKey = null;
        paul = null;
        peter = null;
        mary = null;
        activity1 = null;
        project1 = null;
        project1Key = null;
        resourceService = ResourceService.getInstance();
        projectService = ProjectService.getInstance();
        
        try {
            // Create Resource Pool and add Peter and Paul
            project1Pool    = resourceService.createResourcePool();
            project1PoolKey = project1Pool.getResourcePoolKey();

            paul  = resourceService.createResource("Paul");
            project1Pool.addResource(paul);
            peter = resourceService.createResource("Peter");
            project1Pool.addResource(peter);
            mary  = resourceService.createResource("Mary");
            project1Pool.addResource(mary);
            resourceService.addResourcePool(project1Pool);

            // Create Project
            project1 = projectService.addProject("Test Project 1");
            project1Key = project1.getProjectKey();

            project1.setStartDate(startDate);

            // Add Resource Pool to Project
            projectService.addResources(project1Key, project1PoolKey);

            activity1 = projectService.addActivity(project1Key, "activity1");

            calendarService = CalendarService.getInstance();



            /**
             * set resources
             */
            resourceService.assign(paul.getKey(), activity1.getActivityKey(), project1Key, 100);
            resourceService.assign(peter.getKey(), activity1.getActivityKey(), project1Key, 100);


        }
        catch (Exception e) {

            // Fail the test setup
            assertEquals("EXCEPTION : SetupTestProject", e.getMessage());
        }

    }

 
    /*
     * Call this to set up a base project.
     * 1 project  : Test Project 1
     * 2 Resources: Peter2, Paul2 (assigned 100% to activity1)
     * 1 Activity : activity1
     * 1 Resource pool assigned to project : project1Pool (peter2, paul2, mary2)
     */
    protected void setupTestProject2() throws Exception {
        project2Pool = null;
        project2PoolKey = null;
        paul2 = null;
        peter2 = null;
        mary2 = null;
        activity2 = null;
        project2 = null;
        project2Key = null;
        try {
            // Create Resource Pool and add Peter and Paul
            project2Pool    = resourceService.createResourcePool();
            project2PoolKey = project2Pool.getResourcePoolKey();

            paul2  = resourceService.createResource("Paul2");
            project2Pool.addResource(paul2);
            peter2 = resourceService.createResource("Peter2");
            project2Pool.addResource(peter2);
            mary2  = resourceService.createResource("Mary2");
            project2Pool.addResource(mary2);
            resourceService.addResourcePool(project2Pool);

            // Create Project
            project2 = projectService.addProject("Test Project 2");
            project2Key = project2.getProjectKey();

            project2.setStartDate(startDate);

            // Add Resource Pool to Project
            projectService.addResources(project2Key, project2PoolKey);

            activity2 = projectService.addActivity(project2Key, "activity2");

            calendarService = CalendarService.getInstance();



            /**
             * set resources
             */
            resourceService.assign(paul2.getKey(), activity2.getActivityKey(), project2Key, 100);
            resourceService.assign(peter2.getKey(), activity2.getActivityKey(), project2Key, 100);


        }
        catch (Exception e) {

            // Fail the test setup
            assertEquals("EXCEPTION : SetupTestProject 2", e.getMessage());
        }

    }

    public void testGetInstance1() {
        ProjectService ps;
        ps = ProjectService.getInstance();
        assertNotNull(ps);
    }

    public void testGetInstance2() {
        ProjectService ps1;
        ps1 = ProjectService.getInstance();

        ProjectService ps2;
        ps2 = ProjectService.getInstance();

        String sResult = ps1.toString();

        // Both instances should be to the same object
        assertEquals(ps2.toString(),sResult);

    }
    public void testAddProject()
        throws ProjectCreationException {

        ProjectService ps = new ProjectService();
        ps.addProject("Project 1");

        boolean bResult = ps.listProjects().isEmpty();
        assertFalse(bResult);

    }

    public void testRemoveProject()
        throws ProjectCreationException, InternalProjectException {

            ProjectService ps = new ProjectService();
            Project pr = ps.addProject("Project 2");
            ProjectKey pk = pr.getProjectKey();
            try{
               ProjectInterface p = ps.removeProject(pk);
            }catch (KeyNotFoundException e){
                assertTrue(true);
            }


    }

    public void testGetProject() throws ProjectCreationException, InternalProjectException,KeyNotFoundException {
        ProjectService ps = new ProjectService();
        Project pr = ps.addProject("Project 2");
        ProjectKey pk = pr.getProjectKey();
        Project p = ps.getProject(pk);
        assertNotNull(p);
        }

    public void testSetProject() {
        ProjectService ps = new ProjectService();
        Project pr = ps.addProject("Project 2");
        ProjectKey pk = pr.getProjectKey();
        ps.setProject(pk,pr);
        try{
            Project pr2 = ps.getProject(pk);
            assertEquals(pr,pr2);
        }catch (Exception e){
            fail(e.getMessage());
        }


    }




    public void testAddActivity() {
        ProjectService ps = ProjectService.getInstance();
        Project pr = ps.addProject("Test Project 2");
        ProjectKey pk = pr.getProjectKey();

        try{
            ActivityInterface act = projectService.addActivity(pk, "activity2");

            ps.setProject(pk,pr);
            Project pr2 = ps.getProject(pk);

            Activity act2 = pr2.getActivity(act.getActivityKey());
            assertEquals(act,act2);
        }catch (Exception e){
            fail(e.getMessage());
        }

    }
    public void testAddActivity2() throws Exception {

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);


            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            ActivityInterface act1b = projectService.addActivity(project1Key, "act1b");
            ActivityInterface act1c = projectService.addActivity(project1Key, "act1c");


            // should be 4 (activity1, act1a, act1b, act1c)
            assertEquals(4,projectService.getProject(project1Key).getActivitiesCount());

        }

        catch (Exception e) {
            fail(e.getMessage());
        }

    }

    public void testAddActivityAt() throws Exception {

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);


            ActivityInterface act1a = projectService.addActivityAt(project1Key, "act1a",2);
            ActivityInterface act1b = projectService.addActivityAt(project1Key, "act1b",2);
            ActivityInterface act1c = projectService.addActivityAt(project1Key, "act1c",2);

            projectService.recalculateProject(project1Key);

            assertEquals(act1c.getDisplayOrder(),2);

        }

        catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public void testRemoveActivity() throws Exception  {


        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();

            projectService.removeActivity(project1Key,activity1.getActivityKey());

            // getActivitiesCount should be 0 since activity1 removed
            assertEquals(0,project1.getActivitiesCount());


        }

        catch (Exception e) {
            fail(e.getMessage());
        }

    }
    public void testGetActivity() throws Exception {

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();

            ActivityInterface a = projectService.getActivity(project1Key,activity1.getActivityKey());

            // Should be equal
            assertEquals(a,activity1);


        }

        catch (Exception e) {
            fail(e.getMessage());
        }

    }

    public void testGetActivity2()throws Exception {

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);


            ActivityInterface act1a = projectService.addActivity(project1Key, "act1a");
            ActivityInterface act1b = projectService.addActivity(project1Key, "act1b");
            ActivityInterface act1c = projectService.addActivity(project1Key, "act1c");

            //projectService.recalculateProject(project1Key);

            Activity act1bFoo = project1.getActivity(act1b.getActivityKey());

            // should be the same activity
            assertEquals(act1b,act1bFoo);
        }

        catch (Exception e) {
            fail(e.getMessage());
        }
    }
    public void testAddResources() throws Exception {

        try {
            // this adds resources
            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            // 3 resource (peter, paul, mary)
            assertEquals(project1Pool.listResources().size(),3);

        }

        catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public void testRemoveResource() throws Exception {

/*
            // Create Resource Pool and add Peter and Paul
        	ResourcePool project1aPool    = resourceService.createResourcePool();
        	ResourcePoolKey project1aPoolKey = project1Pool.getResourcePoolKey();

        	ResourceInterface paul1a  = resourceService.createResource("Paul1a");
            project1Pool.addResource(paul1a);
            ResourceInterface  peter1a = resourceService.createResource("Peter1a");
            project1Pool.addResource(peter1a);
            ResourceInterface mary1a  = resourceService.createResource("Mary1a");
            project1Pool.addResource(mary1a);
            
            resourceService = ResourceService.getInstance();
            resourceService.addResourcePool(project1aPool);

            // Create Project
            ProjectInterface project1a = projectService.addProject("Test Project 1a");
            ProjectKey project1aKey = project1a.getProjectKey();

            project1a.setStartDate(startDate);

            // Add Resource Pool to Project
            projectService.addResources(project1aKey, project1aPoolKey);

            ActivityInterface activity1a = projectService.addActivity(project1aKey, "activity1a");

            calendarService = CalendarService.getInstance();



         
            resourceService.assign(paul1a.getKey(), activity1a.getActivityKey(), project1aKey, 100);
            resourceService.assign(peter1a.getKey(), activity1a.getActivityKey(), project1aKey, 100);




        try {
            projectService = ProjectService.getInstance();
            project1a = projectService.getProject(project1aKey);

           // projectService.removeResource(project1a.getProjectKey(),mary1a.getKey());


            // 2 resource (peter, paul)
            assertEquals(2,project1aPool.listResources().size());


        }

        catch (Exception e) {
            // Fail the test
        	fail("Exception 2 " + e.getMessage());
        }
        
        */
		
    }
    public void testGetResource() throws Exception {

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            ResourceInterface paul2 = projectService.getResource(project1.getProjectKey(),paul.getKey());

            // (Name of paul2 == paul)
            assertEquals(paul2.getName(),paul.getName());


        }

        catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public void testListProjects() {
        ProjectService ps = new ProjectService();
        Project pr = ps.addProject("Project 2");
        ProjectKey pk = pr.getProjectKey();

        ps.listProjects();

          //get iterator
        java.util.Iterator it = ps.listProjects().iterator();

        //cycle thru all the propjects
        while (it.hasNext()) {
           //get project and call recalcualte method
            Project pr2 = (Project) it.next();
            assertNotNull(pr2);
        }


    }

    public void testGetProjectCalendar() throws Exception {

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);

            project1.setCalendarKey(calendarService.getCorporateCalendar().getKey());

            assertNotNull(projectService.getProjectCalendar(project1Key));

        }

        catch (Exception e) {
            fail(e.getMessage());
        }
    }
}

/**
 * $Log: TestProjectService.java,v $
 * Revision 1.18  2005/05/09 16:26:42  ccochran
 * method remove resource no longer exsists
 *
 * Revision 1.17  2005/05/08 21:37:36  rcoutinh
 * corrected some compile errors
 *
 * Revision 1.16  2005/05/05 05:58:18  tlong2
 * Fixed all core tests except 2.
 *
 * Revision 1.15  2005/04/29 03:22:55  tlong2
 * fixed a couple more
 *
 * Revision 1.14  2005/04/29 02:47:50  tlong2
 * can't leave these tests alone
 *
 * Revision 1.13  2005/04/28 05:42:54  achou
 * Removed dependency on DateFormatter.
 *
 * Revision 1.12  2005/04/27 12:45:24  tlong2
 * updates to account for resourceService.assign() changes
 *
 * Revision 1.11  2005/04/27 11:45:15  tlong2
 * updates
 *
 * Revision 1.10  2005/04/24 22:36:13  tlong2
 * updates to tests
 *
 * Revision 1.9  2005/04/24 15:57:34  rcoutinh
 * Updated for changes in Activity
 *
 * Revision 1.8  2005/04/16 01:03:47  dnathan
 * Fixed test.
 *
 * Revision 1.7  2005/03/29 19:23:15  ccochran
 * fixed bug in test code, stopped eating exceptions
 *
 * Revision 1.6  2005/03/29 12:24:18  ccochran
 * Commented out unused packages imports
 *
 * Revision 1.5  2005/03/29 11:24:25  ccochran
 * Add some more code for tests
 *
 * Revision 1.4  2005/03/29 10:59:53  ccochran
 * updated test code
 *
 * Revision 1.3  2005/03/29 00:05:54  tlong2
 * Additional Testcases
 *
 * Revision 1.2  2005/03/28 01:19:17  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:39  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:20  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:31  dnathan
 * Updated CVS footer.
 *
 **/