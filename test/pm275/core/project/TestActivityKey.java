/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestActivityKey.java,v $ 
 * @author $Author: rcoutinh $, 
 * @version $Revision: 1.4 $
 * 
 * <p></p>
 * 
 * $Id: TestActivityKey.java,v 1.4 2005/04/24 15:57:34 rcoutinh Exp $
 */
package pm275.core.project;

import java.util.Date;

import junit.framework.TestCase;


public class TestActivityKey extends TestCase {

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/*
	 * Class under test for boolean equals(ActivityKey)
	 */
	public void testEquals() {
		Date today = new Date();
		Activity a = new Activity(today,null);
		boolean bResults = a.myKey.equals(a.myKey);
		assertTrue(bResults);
	}

	/*
	 * Class under test for String toString()
	 * test Activity Constructor Creates New Key
	 */
	public void testToStringNotNull() { 
		
		Date today = new Date();
		Activity a = new Activity(today,null);
		String sResult = a.myKey.toString();
		assertNotNull(sResult,a.myKey);
		
	}
}

/**
 * $Log: TestActivityKey.java,v $
 * Revision 1.4  2005/04/24 15:57:34  rcoutinh
 * Updated for changes in Activity
 *
 * Revision 1.3  2005/03/28 06:28:38  tlong2
 * Completed Iteration 1 tests
 *
 * Revision 1.2  2005/03/28 01:19:17  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:37  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.4  2005/03/23 04:45:36  tlong2
 * First attempt at Junit tests
 *
 * Revision 1.3  2005/03/22 06:26:18  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:29  dnathan
 * Updated CVS footer.
 *
 **/