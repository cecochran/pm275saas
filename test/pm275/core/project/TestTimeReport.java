/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestTimeReport.java,v $ 
 * @author $Author: vchumako $, 
 * @version $Revision: 1.7 $
 * 
 * <p></p>
 * 
 * $Id: TestTimeReport.java,v 1.7 2005/04/17 03:23:13 vchumako Exp $
 */
package pm275.core.project;

//import java.util.Date;

import junit.framework.TestCase;
import pm275.core.resource.*;

public class TestTimeReport extends TestCase {
	TimeReport timeReport1;
	ResourceService rService;
	ResourceInterface resource;
	TimeReport timeReport2;
	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		rService = ResourceService.getInstance();
		timeReport1 = new TimeReport();
		resource = rService.createResource("Test resource");
		timeReport2 = new TimeReport(resource.getKey());
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		timeReport1 = null;
		resource = null;
		timeReport2 = null;
	}

	public void testConstructorNoParam() {
		assertNotNull(timeReport1);
	}
	public void testConstructorParam() {
		assertNotNull(timeReport2);
	}	
	
	public void testGetSetAssignmentKey() {
		AssignmentKey ak = new AssignmentKey("Test Assignment Key");
		timeReport1.setAssignmentKey(ak);
		String sResult = timeReport1.getAssignmentKey().toString();
		assertEquals("Test Assignment Key", sResult);
	}	
	public void testGetResourceKey() {
		ResourceKey rKey = timeReport2.getResourceKey();
		assertNotNull(rKey);
	}	
}

/**
 * $Log: TestTimeReport.java,v $
 * Revision 1.7  2005/04/17 03:23:13  vchumako
 * Changed Resource creation to appropriate methods.
 *
 * Revision 1.6  2005/04/10 21:31:22  vchumako
 * Added Assignment and Resource Interface whenever it's possible
 *
 * Revision 1.5  2005/03/30 06:18:26  ccochran
 * comment out unused import
 *
 * Revision 1.4  2005/03/30 03:55:47  tlong2
 * fixed bug
 *
 * Revision 1.3  2005/03/30 03:36:50  tlong2
 * Final TeamReport updates and testcases
 *
 * Revision 1.2  2005/03/28 01:19:18  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:38  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:20  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:31  dnathan
 * Updated CVS footer.
 *
 **/
