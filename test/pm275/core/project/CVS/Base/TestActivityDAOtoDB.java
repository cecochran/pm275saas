import java.util.HashMap;
import java.util.Map;

import pm275.core.project.Activity;
import pm275.core.project.ActivityDAOtoDB;
import pm275.core.project.ProjectDAOtoDB;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ProjectKey;
import pm275.core.resource.ResourceService;


import junit.framework.TestCase;
/***************************************************
 * File:        $RCSfile: TestActivityDAOtoDB.java,v $
 * @author      $Author: ccochran $,
 * @version     $Id: TestActivityDAOtoDB.java,v 1.3 2005/05/06 19:41:09 ccochran Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * In the spirt of the layering pattern, this class provides a self contained 
 * mechanism for all Activity based presistance to DBMS. Assumes a predefined
 * database structure that this class must mirror.
 * CREATE TABLE `activity` (
  `activitykey` varchar(255) NOT NULL default '',
  `projectkey` varchar(255) NOT NULL default '',
  `name` varchar(255) default NULL,
  `startdate` datetime default NULL,
  `enddate` datetime default NULL,
  `duration` int(11) default NULL,
  `slacktime` int(11) default NULL,
  `status` tinyint(4) default '0',
  `resourcepoolkey` varchar(255) default NULL,
  `fixedduration` tinyint(4) default '0',
  `fixedstartdate` tinyint(4) default '0',
  PRIMARY KEY  (`activitykey`),
 * * */
public class TestActivityDAOtoDB extends TestCase {

	
    protected void setUp() throws Exception {
        super.setUp();
        /*resourceService = ResourceService.getInstance();
        projectService = ProjectService.getInstance();
        startDate = DF.parse("2005-04-14");
        thursday04142005 = DF.parse("2005-04-14");
        friday04152005 = DF.parse("2005-04-15");
        */
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testGet() throws Exception{
    	
    	//setup build project 
    	ProjectInterface  project1 = buildProject();
	  	ProjectDAOtoDB pdba = new ProjectDAOtoDB();
	    int num = pdba.set(project1);

	    //build a project
	    ProjectDAOtoDB pdba2 = new ProjectDAOtoDB();
	    ProjectInterface project2 = pdba.get(project1.getProjectKey());
	    ProjectKey pkey = new ProjectKey("Scooby Doo2")
	    project2.setProjectKey(pley);
	    project2.setName("Scooby Doo2");
	    int num pdba2.set(project2);
	    //check for error code
	    assert(num != -1);
	    
	    //Now try to load the project back
	    ProjectInteface proj = pdba2.get(pkey);

	    //check for error code
	    assertNotNull(proj)
	    
	    
    }
    public void testSet() throws Exception{
       //setup build project 
       ProjectInterface  project1 = buildProject();
	  	
	   Map actMap = project1.getActivities();
	   HashMap map = new HashMap(actMap);
	   
	   int cnt = 1;
	   int res = 0;
	   //get iterator
        java.util.Iterator it =  map.values().iterator();
       //cycle thru all the Activities
        while (it.hasNext()) {
            
        	Activity act = (Activity) it.next();
        	//get project and call recalcualte method
        	ActivityDAOtoDB actDB = new ActivityDAOtoDB();  
        	res = actDB.set(act);

        	//check for error code 
    	    assertTrue(res == 1);
    	    
        	//log.debug("Count:"+cnt +" Write result" + res);
        }

	    
	    
    	
    }
    public void testDelete() throws Exception{
    	//test activity code
    	ProjectInterface  project1 = buildProject();
    	
    	
	   Map actMap = project1.getActivities();
	   HashMap map = new HashMap(actMap);
	   
	   int cnt = 1;
	   int res = 0;
	   //get iterator
        java.util.Iterator it =  map.values().iterator();
       //cycle thru all the Activities
        while (it.hasNext()) {
            
        	Activity act = (Activity) it.next();
        	//get project and call recalcualte method
        	ActivityDAOtoDB actDB = new ActivityDAOtoDB();  
        	res = actDB.set(act);
            log.debug("Count: "+cnt +" Set result: " + res);
            
            actDB.delete(act.getActivityKey());
           log.debug("Count: "+cnt +" Delete result: " + res);
            cnt++;
        }

	    
    }
    
    public void testtoString() throws Exception{
     //is this needed ?
    /*	ActivityDAOtoDB actDB = new ActivityDAOtoDB();
	  	int num = actDB.set(project1);*/
    	
    	assertTrue(true);
    }
}
/**
* $Log: TestActivityDAOtoDB.java,v $
* Revision 1.3  2005/05/06 19:41:09  ccochran
* removed system out
*
* Revision 1.2  2005/05/06 19:40:19  ccochran
* added base code
*
* Revision 1.1  2005/05/05 06:03:23  ccochran
* base code needs more work
*
* Revision 1.3  2005/05/04 12:09:01  ccochran
* insert, update, delete skeleton
*
* Revision 1.2  2005/05/04 09:41:34  ccochran
* added consturctor
*
* Revision 1.1  2005/05/04 05:54:29  ccochran
* added footers
*
*
* Updated footer to include CVS log.
*
**/

