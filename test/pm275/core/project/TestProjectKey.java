/**
 * CSCIE 275 - Software Architecture and Engineering
 * 
 * File: $RCSfile: TestProjectKey.java,v $ 
 * @author $Author: ccochran $, 
 * @version $Revision: 1.5 $
 * 
 * <p></p>
 * 
 * $Id: TestProjectKey.java,v 1.5 2005/03/29 12:24:19 ccochran Exp $
 */
package pm275.core.project;

//import java.util.Date; 

import junit.framework.TestCase;


public class TestProjectKey extends TestCase {

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	
	public void testProjectKeyConstructorNoParam() {
		ProjectKey pk = new ProjectKey();
		String sResult = pk.toString();
		assertNotNull(sResult,pk);
	}
	public void testProjectKeyConstructor() {
		ProjectKey pk = new ProjectKey("Project Key Test");
		String sResult = pk.toString();
		assertEquals("Project Key Test", sResult);

	}	
	/*
	 * Class under test for boolean equals(ProjectKey)
	 */
	public void testEqualsProjectKey() {
		
		ProjectKey pk  = new ProjectKey("Project Key Test 2");
		boolean bResult = pk.equals(pk);
		assertTrue(bResult);
	}

	/*
	 * Class under test for String toString() Already tested in testProjectKeyConstructor()
	 */
	public void testToString() {
		ProjectKey pk = new ProjectKey();
		String sResult = pk.toString();
		assertNotNull(sResult,pk);
	}

}

/**
 * $Log: TestProjectKey.java,v $
 * Revision 1.5  2005/03/29 12:24:19  ccochran
 * Commented out unused packages imports
 *
 * Revision 1.4  2005/03/29 00:05:54  tlong2
 * Additional Testcases
 *
 * Revision 1.3  2005/03/28 23:36:06  tlong2
 * Added Testcases
 *
 * Revision 1.2  2005/03/28 01:19:16  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:38  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:19  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:30  dnathan
 * Updated CVS footer.
 *
 **/