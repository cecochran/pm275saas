/**
 * CSCIE 275 - Software Architecture and Engineering
 *
 * File: $RCSfile: TestProjectService2.java,v $
 * @author $Author: tlong2 $,
 * @version $Revision: 1.5 $
 *
 * <p></p>
 *
 * $Id: TestProjectService2.java,v 1.5 2005/05/09 18:55:58 tlong2 Exp $
 */
package pm275.core.project;

//import com.sun.rsasign.p;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import junit.framework.TestCase;
import pm275.common.exception.CycleDependencyException;
import pm275.common.exception.DuplicateKeyException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.ResourceException;
import pm275.core.calendar.CalendarService;
import pm275.core.calendar.CalendarInterface;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourcePoolKey;
import pm275.core.resource.ResourceService;

public class TestProjectService2 extends TestCase {
    private ProjectService projectService;
    private ResourceService resourceService;
    private CalendarService calendarServiceI;
    private CalendarService calendarService;
    private CalendarInterface calendar1;

    private ResourcePool    project1Pool;
    private ResourcePoolKey project1PoolKey;
    private ResourceInterface paul;
    private ResourceInterface peter;
    private ResourceInterface mary;
    private ActivityInterface activity1;
    private ProjectInterface project1;
    private ProjectKey project1Key;
    
    private ResourcePool    project2Pool;
    private ResourcePoolKey project2PoolKey;
    private ResourceInterface paul2;
    private ResourceInterface peter2;
    private ResourceInterface mary2;
    private ActivityInterface activity2;
    private ProjectInterface project2;
    private ProjectKey project2Key;

    private static final java.text.DateFormat DF =
            new java.text.SimpleDateFormat("yyyy-MM-dd");


    // startDate = Thursday April 14 2005
    private Date startDate;
    private Date startDate2;
    private Date thursday04142005;
    /*
     * @see TestCase#setUp()
     */
    protected void setUp() throws Exception {
        super.setUp();
        resourceService = ResourceService.getInstance();
        projectService = ProjectService.getInstance();

        // Thursday April 14, 2005
        startDate = DF.parse("2005-04-14");

        // Saturday April 23, 2005
        startDate2 = DF.parse("2005-04-23");

        thursday04142005 = DF.parse("2005-04-14");
    }

    /*
     * @see TestCase#tearDown()
     */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /*
     * Call this to set up a base project.
     * 1 project  : Test Project 1
     * 2 Resources: Peter, Paul (assigned 100% to activity1)
     * 1 Activity : activity1
     * 1 Resource pool assigned to project : project1Pool (peter, paul, mary)
     */
    protected void setupTestProject1() throws Exception {
        project1Pool = null;
        project1PoolKey = null;
        paul = null;
        peter = null;
        mary = null;
        activity1 = null;
        project1 = null;
        project1Key = null;
        resourceService = ResourceService.getInstance();
        projectService = ProjectService.getInstance();
        
        try {
            // Create Resource Pool and add Peter and Paul
            project1Pool    = resourceService.createResourcePool();
            project1PoolKey = project1Pool.getResourcePoolKey();

            paul  = resourceService.createResource("Paul X");
            project1Pool.addResource(paul);
            peter = resourceService.createResource("Peter X");
            project1Pool.addResource(peter);
            mary  = resourceService.createResource("Mary X");
            project1Pool.addResource(mary);
            resourceService.addResourcePool(project1Pool);

            // Create Project
            project1 = projectService.addProject("Test Project 001");
            project1Key = project1.getProjectKey();

            project1.setStartDate(startDate);

            // Add Resource Pool to Project
            projectService.addResources(project1Key, project1PoolKey);

            activity1 = projectService.addActivity(project1Key, "activity 001");

            calendarService = CalendarService.getInstance();



            /**
             * set resources
             */
            resourceService.assign(paul.getKey(), activity1.getActivityKey(), project1Key, 100);
            resourceService.assign(peter.getKey(), activity1.getActivityKey(), project1Key, 100);


        }
        catch (Exception e) {

            // Fail the test setup
            assertEquals("EXCEPTION : SetupTestProject", e.getMessage());
        }

    }

 
    private ProjectInterface buildProject(String sProjectName) 
	throws 	ResourceException, 
			ParseException, 
			InternalProjectException, 
			KeyNotFoundException,
			DuplicateKeyException,
			InternalActivityException,
			CycleDependencyException {

	ResourceService resourceService = ResourceService.getInstance();
	ProjectService projectService = ProjectService.getInstance();
	
	ProjectInterface project1 = projectService.addProject(sProjectName);
	
	   ResourcePool resourcePool = resourceService.createResourcePool();
	ResourceInterface abe  = resourceService.createResource("Abe");
	ResourceInterface bob  = resourceService.createResource("Bob");
	
	
	try {
	    resourcePool.addResource(abe);
	    resourcePool.addResource(bob);
	    resourceService.addResourcePool(resourcePool);
	    
	    DateFormat DF = new SimpleDateFormat("yyyy-MM-dd");
	    // Thursday April 14, 2005
	    Date startDate = DF.parse("2005-04-14");    
	    
	    project1.setStartDate(startDate);
	
	    ActivityInterface sm = project1.getStartMilestone();
	    sm.setStartDate(startDate);
	    ActivityInterface em = project1.getEndMilestone();
	    em.setStartDate(startDate);
	    
	    // Add Resource Pool to Project
	    projectService.addResources(project1.getProjectKey(), resourcePool.getResourcePoolKey());
	
	    ActivityInterface act1 = projectService.addActivity(project1.getProjectKey(), "act 1");
	    act1.setStartDate(startDate);
	    
	    ActivityInterface act2 = projectService.addActivity(project1.getProjectKey(), "act 2");
	    act2.setDuration(16);
	    
	    
	    project1.associateActivities(act1.getActivityKey(),act2.getActivityKey());
	  
	    resourceService.assign(abe.getKey(), act1.getActivityKey(), project1.getProjectKey(), 100);
	    resourceService.assign(bob.getKey(), act2.getActivityKey(), project1.getProjectKey(), 100);
	
	    projectService.addProjectToPool(project1);
	    
	} catch (ResourceException re) {
		fail("ResourceException in buildProject()" + re.getMessage());
	} catch (ParseException pe) {
		fail("ParseException in buildProject()" + pe.getMessage());
	} catch (InternalProjectException ipe) {
		fail("InternalProjectException in buildProject()" + ipe.getMessage());
	} catch (KeyNotFoundException knfe) {
		fail("KeyNotFoundException in buildProject()" + knfe.getMessage());
	} catch (DuplicateKeyException dke) {
		fail("DuplicateKeyException in buildProject()" + dke.getMessage());
	} catch (InternalActivityException iae) {
		fail("InternalActivityException in buildProject()" + iae.getMessage());
	} catch (CycleDependencyException cde) {
		fail("CycleDependencyException in buildProject()" + cde.getMessage());
	}


	return project1;
}


    public void testRecalculate_project1() throws Exception {

        try {

        	ProjectInterface p1 = buildProject("P1-testRecalculate_project1");
            ProjectInterface p2 = buildProject("P2-testRecalculate_project1");           
 
            ProjectService projectService = ProjectService.getInstance();

            // big negative test. If this doesn't throw an exception, we have tested alot
            projectService.recalculateProject(p1.getProjectKey()); //recalculate();

            

            //  should be 2 activities in project 1
            assertFalse(projectService.getProject(p1.getProjectKey()) == projectService.getProject(p2.getProjectKey()));

        }

        catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public void testRecalculated_projectsDiffer() throws Exception {

        try {
        	ProjectInterface p1 = buildProject("P1-testRecalculated_projectsDiffer");
            ProjectInterface p2 = buildProject("P2-testRecalculated_projectsDiffer");           
 
            ProjectService projectService = ProjectService.getInstance();

            //  should be 2 activities in project 1
            assertFalse(projectService.getProject(p1.getProjectKey()) == projectService.getProject(p2.getProjectKey()));

        }

        catch (Exception e) {
            fail(e.getMessage());
        }
    }
    public void testRecalculateProject() throws Exception {

        try {

            setupTestProject1();

            projectService = ProjectService.getInstance();
            project1 = projectService.getProject(project1Key);


            ActivityInterface act1a = projectService.addActivityAt(project1Key, "act1a",2);

            ActivityInterface act1b = projectService.addActivityAt(project1Key, "act1b",2);

            ActivityInterface act1c = projectService.addActivityAt(project1Key, "act1c",2);
            act1c.setDuration(10);

            project1.associateActivities(activity1.getActivityKey(),act1a.getActivityKey());
            project1.associateActivities(activity1.getActivityKey(),act1b.getActivityKey());
            project1.associateActivities(activity1.getActivityKey(),act1c.getActivityKey());

            // should be 4 actitivities on CP activity1 and act1c & EndMilestone
            LinkedList cp = projectService.recalculateProject(project1Key);

            /* Debug 
            java.util.ListIterator li = cp.listIterator();
            while (li.hasNext()) {
            	Activity a = (Activity)li.next();
            	System.out.println(a.getName());
            }
            */
            
            assertEquals(4,cp.size());

        }

        catch (Exception e) {
            fail(e.getMessage());
        }
    }
}

/**
 * $Log: TestProjectService2.java,v $
 * Revision 1.5  2005/05/09 18:55:58  tlong2
 * really really final
 *
 * Revision 1.4  2005/05/09 17:52:43  tlong2
 * really final changes
 *
 * Revision 1.3  2005/05/08 21:39:40  rcoutinh
 * fixed compile errors
 *
 * Revision 1.2  2005/05/08 08:50:43  tlong2
 * got rid of compile Warnings
 *
 * Revision 1.1  2005/05/05 05:58:18  tlong2
 * Fixed all core tests except 2.
 *
 * Revision 1.15  2005/04/29 03:22:55  tlong2
 * fixed a couple more
 *
 * Revision 1.14  2005/04/29 02:47:50  tlong2
 * can't leave these tests alone
 *
 * Revision 1.13  2005/04/28 05:42:54  achou
 * Removed dependency on DateFormatter.
 *
 * Revision 1.12  2005/04/27 12:45:24  tlong2
 * updates to account for resourceService.assign() changes
 *
 * Revision 1.11  2005/04/27 11:45:15  tlong2
 * updates
 *
 * Revision 1.10  2005/04/24 22:36:13  tlong2
 * updates to tests
 *
 * Revision 1.9  2005/04/24 15:57:34  rcoutinh
 * Updated for changes in Activity
 *
 * Revision 1.8  2005/04/16 01:03:47  dnathan
 * Fixed test.
 *
 * Revision 1.7  2005/03/29 19:23:15  ccochran
 * fixed bug in test code, stopped eating exceptions
 *
 * Revision 1.6  2005/03/29 12:24:18  ccochran
 * Commented out unused packages imports
 *
 * Revision 1.5  2005/03/29 11:24:25  ccochran
 * Add some more code for tests
 *
 * Revision 1.4  2005/03/29 10:59:53  ccochran
 * updated test code
 *
 * Revision 1.3  2005/03/29 00:05:54  tlong2
 * Additional Testcases
 *
 * Revision 1.2  2005/03/28 01:19:17  tlong2
 * Updated package info
 *
 * Revision 1.1  2005/03/27 18:43:39  tlong2
 * Organized test directories to match src directories...
 *
 * Revision 1.3  2005/03/22 06:26:20  dnathan
 * Moved package.
 *
 * Revision 1.2  2005/03/22 04:59:31  dnathan
 * Updated CVS footer.
 *
 **/