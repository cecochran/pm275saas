All third party jars go in this directory.  This includes things like
junit.jar
optional.jar
a jar file for the mysqlJDBC driver
...

Note that adding a jar file requires using the cvs command

cvs add -kb filename

which adds the jar files as binaries.
