# $Id: drop_pm275_tables.sql,v 1.2 2005/04/20 20:39:28 vchumako Exp $

USE pm275_team1;

DROP TABLE project;
DROP TABLE activity;
DROP TABLE activityRef;
DROP TABLE resource;
DROP TABLE assignment;
DROP TABLE calendar;

DROP DATABASE pm275_team1;

# $Log: drop_pm275_tables.sql,v $
# Revision 1.2  2005/04/20 20:39:28  vchumako
# Fix MySQL schema
#
# Revision 1.1  2005/04/20 20:18:22  vchumako
# Preliminary MySQL schema as scripts
#