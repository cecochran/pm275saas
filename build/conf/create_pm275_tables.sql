
# $Id: create_pm275_tables.sql,v 1.15 2005/05/15 20:11:12 ccochran Exp $
# Contents: PM275 team1 MySQL schema

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS team1;
CREATE DATABASE team1;

USE team1;


#
# Structure for the `project` table :
#

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `projectkey` varchar(255) NOT NULL default '',
  `name` varchar(255) NOT NULL default '',
  `startMilestonekey` varchar(255) default NULL,
  `endMilestonekey` varchar(255) default NULL,
  `resourcepoolkey` varchar(255) default NULL,
  `calendarkey` varchar(255) default NULL,
  `startdate` datetime default NULL,
  PRIMARY KEY  (`projectkey`),
  UNIQUE KEY `ProjectKey` (`projectkey`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Structure for the `activity` table :
#

DROP TABLE IF EXISTS `activity`;

CREATE TABLE `activity` (
  `activitykey` varchar(255) NOT NULL default '',
  `projectkey` varchar(255) NOT NULL default '',
  `name` varchar(255) default NULL,
  `startdate` datetime default NULL,
  `enddate` datetime default NULL,
  `duration` int(11) default NULL,
  `slacktime` int(11) default NULL,
  `status` tinyint(4) default '0',
  `resourcepoolkey` varchar(255) default NULL,
  `fixedduration` tinyint(4) default '0',
  `fixedstartdate` tinyint(4) default '0',
  `displayorder` tinyint(4) default '0',
  PRIMARY KEY  (`activitykey`),
  UNIQUE KEY `activitykey` (`activitykey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Structure for the `activityref` table :
#

DROP TABLE IF EXISTS `activityref`;

CREATE TABLE `activityref` (
  `activitykey` varchar(255) NOT NULL default '',
  `projectKey` varchar(255) default NULL,
  `predflag` int(11) default NULL,
  `value` varchar(255) default NULL
);

# resource schema

DROP TABLE IF EXISTS `resource`;

CREATE TABLE resource (
	ID varchar(64) NOT NULL default '',
	first varchar(32),
	last varchar(32) NOT NULL,
    PRIMARY KEY  (`ID`),
    UNIQUE KEY `ID` (`ID`)
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `assignment`;

CREATE TABLE assignment (
	ID varchar(64) NOT NULL default '',
	resourceKey varchar(64) NOT NULL,
	activityKey varchar(64) NOT NULL,
	effort double NOT NULL DEFAULT 100,
	 PRIMARY KEY  (`ID`),
    UNIQUE KEY `ID` (`ID`)
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `calendar`;
CREATE TABLE calendar (
	ID varchar(64) NOT NULL default '',
#	projectID varchar(64) NOT NULL,
	calendarName varchar(64) NOT NULL DEFAULT "Corporate calendar",
	calendarType tinyint NOT NULL DEFAULT 0,
	daysInWeek int NOT NULL DEFAULT 5,
	hoursInDay int NOT NULL DEFAULT 8,
	workWeek int NOT NULL DEFAULT 0,
    PRIMARY KEY  (`ID`),
    UNIQUE KEY `ID`(`ID`)
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `holidays`;
CREATE TABLE holidays (
	calendarKey varchar(64) NOT NULL default '',
	holiday datetime NOT NULL
	
  ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO resource VALUES ('r1', 'Julie', 'First'), 
	('r2', 'Dave', 'Second'),('r3', 'Ali', 'Third');



# $Log: create_pm275_tables.sql,v $
# Revision 1.15  2005/05/15 20:11:12  ccochran
# added start date for project
#
# Revision 1.14  2005/05/15 02:21:30  ccochran
# added drop tables
#
# Revision 1.13  2005/05/14 22:20:15  ccochran
# updated to allow for display order
#
# Revision 1.12  2005/05/09 04:31:43  vchumako
# Set the default calendar name
#
# Revision 1.11  2005/05/09 03:53:00  vchumako
# Fixed typo
#
# Revision 1.10  2005/05/09 03:45:55  vchumako
# Removed foreign key from calendar table
#
# Revision 1.9  2005/05/09 02:41:43  vchumako
# Updated calendar table, added holiday table
#
# Revision 1.8  2005/05/08 05:56:40  vchumako
# Updated assignment table. Don't allow null keys
#
# Revision 1.7  2005/05/07 15:54:55  dnathan
# Fixed FC stuff.
#
# Revision 1.6  2005/05/07 14:53:09  vchumako
# Integrated Chris shcema
#
# Revision 1.5  2005/05/02 23:53:38  vchumako
# Changed db name to 'team1'. Added few resources to load.
#
# Revision 1.4  2005/04/25 21:10:55  vchumako
# Keeps name consistent
#
# Revision 1.3  2005/04/20 21:19:35  vchumako
# Fixed activityRef table
#
#
