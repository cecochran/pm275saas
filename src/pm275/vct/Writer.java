/*
 *  File      :  Writer.java
 *  @version  :  $Id: Writer.java,v 1.2 2005/05/04 23:46:37 tlong2 Exp $
 *  @author   :  Bruce Buchanan
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  
 *  Purpose   :  
 */
package pm275.vct;


public class Writer 
{
	
	public static void write(String line)
	{
		System.out.println(line);
	}
	
	public static void write(String title, String line)
	{
		System.out.println(title + "\n");
		Writer.write(line);
	}

}
/**
* $Log: Writer.java,v $
* Revision 1.2  2005/05/04 23:46:37  tlong2
* Added CVS Header\Footer and fixed bug to get rid of compile errror
*
*
*/