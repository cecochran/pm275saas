/*
 *  File      :  IterationThreeDriver.java
 *  @version  :  $Id: IterationThreeDriver.java,v 1.3 2005/05/15 23:30:02 tlong2 Exp $
 *  @author   :  Bruce Buchanan
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  
 *  Purpose   :  
 */
package pm275.vct;


import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import pm275.common.exception.CycleDependencyException;
import pm275.common.persistance.PersistanceService;
import pm275.core.calendar.Calendar;
import pm275.core.calendar.CalendarKey;
import pm275.core.calendar.CalendarService;
import pm275.core.project.ActivityInterface;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ProjectKey;
import pm275.core.project.ProjectService;
import pm275.core.resource.Resource;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourcePoolKey;
import pm275.core.resource.ResourceService;
import pm275.gui.common.DateFormatter;

public class IterationThreeDriver 
{
	
	  private static final Logger log = Logger.getLogger( IterationThreeDriver.class );
	    private static final String ATTR = "IterationThreeDriver";
	
	public static void main(String args[])
	{
		try
		{
			ProjectService.getInstance().addProjectToPool(ProjectMaker.fabricateProject1());
			ProjectService.getInstance().addProjectToPool(ProjectMaker.fabricateProject2());
			//ProjectService.getInstance().addProjectToPool(ProjectMaker.fabricateProject3());
			//ProjectService.getInstance().addProjectToPool(ProjectMaker.fabricateProject4());
			
			Collection projectPool = ProjectService.getInstance().listProjects();
			Iterator projPoolIterator = projectPool.iterator();
			
			while(projPoolIterator.hasNext())
			{
				ProjectInterface project = (ProjectInterface)projPoolIterator.next();
			
				Map activityList = project.getActivities();
				Collection coll = activityList.values();
				Iterator iterator = coll.iterator();
				
				Writer.write("Project start date", DateFormatter.getStringForDate(project.getStartDate()));
				Writer.write("Project end date", DateFormatter.getStringForDate(project.getEndDate()));
				
				
				CalendarKey calendarKey = project.getCalendarKey();
				Calendar calendar = CalendarService.getInstance().getCalendar(calendarKey);
			
				java.util.Calendar date = java.util.Calendar.getInstance();
				date.setTime(project.getStartDate());
				
				
				java.util.Calendar endDate = java.util.Calendar.getInstance();
				endDate.setTime(project.getEndDate());
			
				while(date.before(endDate))
				{
					Collection resourcePool = project.getResources();
					Iterator poolIterator = resourcePool.iterator();
				
					while (poolIterator.hasNext())
					{
						Resource resource = (Resource)poolIterator.next();
						double hoursWorkedToday = ResourceService.getInstance().calculateHoursWorkedToday(project, resource, date);
						
						if(hoursWorkedToday > 0)
						{
							Writer.write("Date " + date.getTime().toString());
							Writer.write("Resource " + resource.getName());
							Writer.write(String.valueOf(hoursWorkedToday) + " hours");
						}
					}
					date.add(java.util.Calendar.DAY_OF_WEEK, +1);
				}
			}	
		}
		catch(Exception e)
		{
			Writer.write(e.getMessage());
		}
	}
	

	private static ProjectInterface fabricateProject1() 
	{
		ProjectInterface project = null;
		try 
		{
			ProjectService projectService = ProjectService.getInstance();
			ResourceService resourceService = ResourceService.getInstance();
		  // Create Project
		  project = PersistanceService.getInstance().loadProject();
		  ProjectKey projectKey = project.getProjectKey();
		  
		  ActivityInterface design = projectService.addActivity(projectKey, "Design");
		  design.setDuration(40);
		  ActivityInterface codeUI = projectService.addActivity(projectKey, "Code UI");
		  codeUI.setDuration(40);
		  //ActivityInterface codeMT = projectService.addActivity(projectKey, "Code Middle Tier");
		  //codeMT.setDuration(80);
		 // ActivityInterface integration = projectService.addActivity(projectKey, "Integration of UI and Middle Tier");
		 // integration.setDuration(16);
		  ActivityInterface systemTest = projectService.addActivity(projectKey, "System Test");
		  systemTest.setDuration(40);
		  ActivityInterface readyForUAT = projectService.addActivity(projectKey, "Ready for UAT");
		  readyForUAT.setDuration(0);

		  /**
		   * set resources
		   */
		  ResourcePoolKey resourceKey = project.getResourcePoolKey();
		  ResourcePool pool = ResourceService.getInstance().getResourcePool(resourceKey);
		  
		  	Collection coll = pool.listResources();
			Object[] resources = coll.toArray();
		  
		  resourceService.assign(((Resource)resources[0]).getKey(), design.getActivityKey(), projectKey,100);
		 // resourceService.assign(((Resource)resources[2]).getKey(), design.getActivityKey(),projectKey, 100);
		  resourceService.assign(((Resource)resources[0]).getKey(), codeUI.getActivityKey(),projectKey, 100);
		 // resourceService.assign(((Resource)resources[1]).getKey(), codeMT.getActivityKey(),projectKey, 100);
		  //resourceService.assign(((Resource)resources[3]).getKey(), integration.getActivityKey(),projectKey, 100);
		  //resourceService.assign(((Resource)resources[4]).getKey(), integration.getActivityKey(),projectKey, 100);
		  resourceService.assign(((Resource)resources[0]).getKey(), systemTest.getActivityKey(), projectKey,100);
		  //resourceService.assign(((Resource)resources[0]).getKey(), readyForUAT.getActivityKey(),projectKey, 100);

		  /**
		   * finally, add the dependencies
		   */
		  project.associateActivities(design.getActivityKey(),codeUI.getActivityKey());
		 // project.associateActivities(design.getActivityKey(),codeMT.getActivityKey());
		  //project.associateActivities(codeUI.getActivityKey(),integration.getActivityKey());
		  //project.associateActivities(codeMT.getActivityKey(),integration.getActivityKey());
		  //project.associateActivities(integration.getActivityKey(),systemTest.getActivityKey());
		  project.associateActivities(codeUI.getActivityKey(),systemTest.getActivityKey());
		  
		//  project.

		}
		catch (CycleDependencyException e) {
		  //log.warn("Warning: " + cde);
		  e.printStackTrace();
				   System.exit(1);
		}
		catch (Exception e) {
		  //log.fatal("Error fabricating VCT Project1: " + e);
		  e.printStackTrace();
		  System.exit(1);
		}
		return project;
	}

}


/**
* $Log: IterationThreeDriver.java,v $
* Revision 1.3  2005/05/15 23:30:02  tlong2
* cleaned up compile warnings
*
* Revision 1.2  2005/05/04 23:46:37  tlong2
* Added CVS Header\Footer and fixed bug to get rid of compile errror
*
*
*/