/*
 *  File      :  ProjectMaker.java
 *  @version  :  $Id: ProjectMaker.java,v 1.17 2005/05/09 16:39:19 vchumako Exp $
 *  @author   :  Jennifer Keiser
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Fabricates 3 test projects.  Then calls PrintWriter to print details
 *               about each project. Finally, causes and catches a Cycle Dependency Exception.
 *  Purpose   :  To validate the PM-275 project from a Virtual Customer's perspective.
 */

package pm275.vct;

import java.util.*;
import java.io.*;
import org.apache.log4j.Logger;
import pm275.core.project.*;
import pm275.core.resource.*;
import pm275.gui.common.*;
import pm275.common.exception.*;

public class ProjectMaker {

    private static final ProjectService  projectService = ProjectService.getInstance();
    private static final ResourceService resourceService = ResourceService.getInstance();
    private static ProjectInterface      project1 = null;
    private static ProjectInterface      project2 = null;
    private static ProjectInterface      project3 = null;
    private static ProjectKey    project1Key = null;
    private static ProjectKey    project2Key = null;
    private static ProjectKey    project3Key = null;
    //project 4 needs to use some of project 1's activities
    private static ActivityKey   project4Act1Key = null;
    private static ActivityKey   project4Act2Key = null;

    private static FileWriter   file;
    private static PrintWriter  out;
    private static final Logger log = Logger.getLogger(ProjectMaker.class);
    private static final String ATTR = "ProjectMaker";

    protected ProjectMaker() {
        super();
    }
    
    /*
     * Create text file to hold output.  Call each of four fabricateProject methods to 
     * test the proper functioning of the PM-275 project. 
     */
    public static void main(String args[]) {

        //open a file for output
        String f = new String("output.txt");

        try {
            file = new FileWriter(f);
            out = new PrintWriter(file, true);
        }
        catch (IOException ioe) {
            log.fatal("Exception opening output file");
            System.exit(1);
        }

        //create the first project and "print" it
        ProjectInterface project1 = fabricateProject1();

        try {
            projectService.recalculateProject(project1Key);
        }
        catch (InternalProjectException ipe) {
            log.fatal("ERROR : ProjectMaker.main - InternalProjectException in recalculateProject for project1");
        }
        /*
        catch (ResourceException re) {
            log.fatal("ERROR : ProjectMaker.main - ResourceException in recalculateProject for project1");
        } */
        catch (KeyNotFoundException knfe) {
            log.fatal("ERROR : ProjectMaker.main - KeyNotFoundException in recalculateProject for project1");
        }

        //ProjectWriter pw1 = new ProjectWriter(project1, true, file, out);

        //*************************************

        //create the second (alphabet) project and "print" it
        ProjectInterface project2 = fabricateProject2();

        try {
            projectService.recalculateProject(project2Key);
        }
        catch (InternalProjectException ipe) {
            log.fatal("ERROR : ProjectMaker.main - InternalProjectException in recalculateProject for project2");
        }
        /*
        catch (ResourceException re) {
            log.fatal("ERROR : ProjectMaker.main - ResourceException in recalculateProject for project2");
        } */
        catch (KeyNotFoundException knfe) {
            log.fatal("ERROR : ProjectMaker.main - KeyNotFoundException in recalculateProject for project2");
        }
        //ProjectWriter pw2 = new ProjectWriter(project2, true, file, out);

        //**************************************************************

        //create the third (points of light) project but don't print it
        ProjectInterface project3 = fabricateProject3();
        //ProjectWriter pw3 = new ProjectWriter(project3, false, file, out);

        //build on the first project, but test for a cycle
        project1 = fabricateProject4();

        out.flush();
        out.close();
      }

    /*
     * Create an enhancement project using two resources and 6 tasks with some  
     * dependencies. 
     */
    public static ProjectInterface fabricateProject1() {
        ResourcePool    project1Pool;
        ResourcePoolKey project1PoolKey;

        try {
          // Create Resource Pool and add Peter and Paul
          project1Pool    = resourceService.createResourcePool();
          project1PoolKey = project1Pool.getResourcePoolKey();
          Resource paul  = resourceService.createResource("RPL1", "Paul L1");
          project1Pool.addResource(paul);
          Resource peter = resourceService.createResource("RPL2", "Peter L2");
          project1Pool.addResource(peter);
          resourceService.addResourcePool(project1Pool);

          // Create Project
          project1 = projectService.addProject("Test Project 1");
          project1Key = project1.getProjectKey();
          Date startDate = DateFormatter.getDateForString("2005-04-18");
          project1.setStartDate(startDate);

          // Add Resource Pool to Project
          projectService.addResources(project1Key, project1PoolKey);
          /**
           * Now start adding the Activities with durations in hours
           * 1. Start date: 4/18
           * 2. Design (Peter and Paul), 1week
           * 3. Code UI (Peter), 7 days, after design is complete
           * 4. Code Middle Tier Stuff (Paul), 10 days, after design is complete
           * 5. Integration of UI and Middle Tier (Peter and Paul), 2 days, after UI and Middle Tier are complete
           * 6. System Test(Peter and Paul), 5 days, after Integration is complete
           * 7. Ready for UAT, milestone date depending on System Test completion
           *
           */
          ActivityInterface design = projectService.addActivity(project1Key, "Design");
          design.setDuration(40);
          ActivityInterface codeUI = projectService.addActivity(project1Key, "Code UI");
          codeUI.setDuration(56);
          ActivityInterface codeMT = projectService.addActivity(project1Key, "Code Middle Tier Stuff");
          codeMT.setDuration(80);
          ActivityInterface integration = projectService.addActivity(project1Key, "Integration of UI and Middle Tier");
          integration.setDuration(16);
          ActivityInterface systemTest = projectService.addActivity(project1Key, "System Test");
          systemTest.setDuration(40);
          ActivityInterface readyForUAT = projectService.addActivity(project1Key, "Ready for UAT");
          readyForUAT.setDuration(0);


          /**
           * set resources
           */
          resourceService.assign(paul.getKey(), design.getActivityKey(), project1Key,100);
          resourceService.assign(peter.getKey(), design.getActivityKey(),project1Key, 100);
          resourceService.assign(peter.getKey(), codeUI.getActivityKey(),project1Key, 100);
          resourceService.assign(paul.getKey(), codeMT.getActivityKey(),project1Key, 100);
          resourceService.assign(paul.getKey(), integration.getActivityKey(),project1Key, 100);
          resourceService.assign(peter.getKey(), integration.getActivityKey(),project1Key, 100);
          resourceService.assign(paul.getKey(), systemTest.getActivityKey(), project1Key,100);
          resourceService.assign(peter.getKey(), systemTest.getActivityKey(),project1Key, 100);

          /**
           * finally, add the dependencies
           */
          project1.associateActivities(design.getActivityKey(),codeUI.getActivityKey());
          project1.associateActivities(design.getActivityKey(),codeMT.getActivityKey());
          project1.associateActivities(codeUI.getActivityKey(),integration.getActivityKey());
          project1.associateActivities(codeMT.getActivityKey(),integration.getActivityKey());
          project1.associateActivities(integration.getActivityKey(),systemTest.getActivityKey());
          project1.associateActivities(systemTest.getActivityKey(),readyForUAT.getActivityKey());

          //project 4 will need these
          project4Act1Key = codeUI.getActivityKey();
          project4Act2Key = readyForUAT.getActivityKey();
          log.info("VCT Project1 created successfully");

        }
        catch (CycleDependencyException cde) {
          log.warn("Warning: " + cde);
        }
        catch (Exception e) {
          log.fatal("Error fabricating VCT Project1: " + e);
          e.printStackTrace();
          System.exit(1);
        }
        return project1;
    }

    /*
     * Create an "alphabet" project using 8 interdependent tasks and no resources.
     */
    public static ProjectInterface fabricateProject2() {
        try {
            project2 = projectService.addProject("Test Project 2");
            project2Key = project2.getProjectKey();

            /**
             * Now start adding the Activities with durations in hours:
             * 1.   A, 2 days
             * 2.   B, 1 day
             * 3.   C, 3 days
             * 4.   D, 3 days
             * 5.   E, 4 days
             * 6.   F, 1 day
             * 7.   G, 2 days
             * 8.   H, 4 days
             */
            ActivityInterface A = projectService.addActivity(project2Key, "A");
            A.setDuration(16);
            ActivityInterface B = projectService.addActivity(project2Key, "B");
            B.setDuration(8);
            ActivityInterface C = projectService.addActivity(project2Key, "C");
            C.setDuration(24);
            ActivityInterface D = projectService.addActivity(project2Key, "D");
            D.setDuration(24);
            ActivityInterface E = projectService.addActivity(project2Key, "E");
            E.setDuration(32);
            ActivityInterface F = projectService.addActivity(project2Key, "F");
            F.setDuration(8);
            ActivityInterface G = projectService.addActivity(project2Key, "G");
            G.setDuration(16);
            ActivityInterface H = projectService.addActivity(project2Key, "H");
            H.setDuration(32);

            /**
             * finally, add the dependencies
             */
            project2.associateActivities(A.getActivityKey(),B.getActivityKey());
            project2.associateActivities(A.getActivityKey(),E.getActivityKey());
            project2.associateActivities(B.getActivityKey(),C.getActivityKey());
            project2.associateActivities(B.getActivityKey(),E.getActivityKey());
            project2.associateActivities(D.getActivityKey(),E.getActivityKey());
            project2.associateActivities(E.getActivityKey(),F.getActivityKey());
            project2.associateActivities(G.getActivityKey(),E.getActivityKey());
            project2.associateActivities(H.getActivityKey(),F.getActivityKey());

            log.info("VCT Project2 created successfully");
          }
        catch (Exception e) {
            log.fatal("Error fabricating VCT Project2: " + e);
            e.printStackTrace();
            System.exit(1);
        }
        return project2;
    }

    /*
     * Create a Thousand Points of Light project using 1000 tasks of equal duration
     * and no resources.
     */
    public static ProjectInterface fabricateProject3() {
        ActivityInterface   currActivity = null;
        ActivityInterface   prevActivity = null;
        try {
            project3 = projectService.addProject("Test Project 3");
            project3Key = project3.getProjectKey();
            Date startDate = DateFormatter.getDateForString("2005-04-18");
            project3.setStartDate(startDate);

            for (int i=1; i<=1000; i++) {
                currActivity = projectService.addActivity(project3Key, "Light "+String.valueOf(i));
                if (i != 1) {
                    project3.associateActivities(prevActivity.getActivityKey(),currActivity.getActivityKey());
                }
                prevActivity = currActivity;
            }
            log.info("VCT Project3 created successfully");
          }
        catch (Exception e) {
            log.fatal("Error fabricating VCT Project3: " + e);
            e.printStackTrace();
            System.exit(1);
        }
        return project3;
    }

    /*
     * Intentionally add a circular dependency to project 1 and make sure that a 
     * warning is presented.
     */
    public static ProjectInterface fabricateProject4() {
        //demonstrate that a warning properly occurs when an invalid connection is made
        try {
            project1.associateActivities(project4Act2Key, project4Act1Key);
        }
        catch (CycleDependencyException cde){
            out.println("Project 4: Cycle created when connecting Ready for UAT and Code UI\n");
        }
        catch (Exception e) {
            log.fatal("Error fabricating VCT Project4: " + e);
            e.printStackTrace();
            System.exit(1);
        }
        return project1;
    }

}
/*
 *  $Log: ProjectMaker.java,v $
 *  Revision 1.17  2005/05/09 16:39:19  vchumako
 *  Assigned unique ID to sample resources
 *
 *  Revision 1.16  2005/05/08 08:41:03  tlong2
 *  Commented out ResourceException that was causing compile error
 *
 *  Revision 1.15  2005/05/02 19:59:58  bbuchana
 *  Adjusted for VCT functionality: Iteration 3
 *
 *  Revision 1.14  2005/04/27 02:02:11  rcoutinh
 *  ResourceService.assign now accepts ProjectKey
 *
 *  Revision 1.13  2005/04/19 08:30:32  jkeiser
 *  Added comment block to all methods.  Corrected duration of activity H in project 2.
 *
 *  Revision 1.12  2005/04/18 22:32:34  umangkus
 *  fabricateProject4() is good to go.
 *
 *  Revision 1.11  2005/04/18 04:12:46  achou
 *  Converted rest of the Activity references to ActivityInterface.
 *
 *  Revision 1.10  2005/04/18 03:49:50  tlong2
 *  Critical Path tests and CP debugging out.println statements
 *
 *  Revision 1.9  2005/04/18 02:11:08  tlong2
 *  Added projectService.recalculateProject(project1Key); // Debugging CP algorithm - still not working
 *
 *  Revision 1.8  2005/04/17 20:16:41  jkeiser
 *  Replaced Activity references with ActivityInterface.  Moved output.txt file access from constructor to main.  Commented out fabricateProject4 temporarily due to errors.
 *
 *  Revision 1.7  2005/04/17 14:20:49  jkeiser
 *  Un-commented calls to resourceService.assign.  Modified call to ProjectWriter to allow writing out project but not detailed activity info.
 *
 *  Revision 1.6  2005/04/17 02:20:27  vchumako
 *  add createResourcePool() and createResource() methods to the ResourceService.
 *  They should be used from now on to create Resources.
 *
 *  Revision 1.5  2005/04/16 22:22:31  jkeiser
 *  Misc updates to accommodate debugging and change from Enumerators to Collections.
 *
 *  Revision 1.4  2005/04/16 15:39:42  jkeiser
 *  Commented out resourceService.assign calls (for now) due to errors.  Added code to pass PrintWriter and FileWriter to ProjectWriter class.
 *
 *  Revision 1.3  2005/04/16 12:59:08  jkeiser
 *  Added method calls to create a pool and assign resources to activities.  Added calls to set activity durations.  Moved all dump__() methods to ProjectWriter.java.
 *
 *  Revision 1.2  2005/04/15 22:29:39  ccochran
 *  Fixed to use Project Interface
 *
 *  Revision 1.1  2005/04/14 02:43:30  jkeiser
 *  Initial checkin
 *
 */
