/*
 *  File      :  ProjectWriter.java
 *  @version  :  $Id: ProjectWriter.java,v 1.8 2005/04/23 00:35:18 jkeiser Exp $
 *  @author   :  Jennifer Keiser
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Methods to dump (write to a text file) information about a project, its
 *               activities, and its resources. 
 *  Purpose   :  To validate the PM-275 project from a Virtual Customer's perspective. 
 */

package pm275.vct;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Comparator;

import org.apache.log4j.Logger;

import pm275.core.project.Activity;
import pm275.core.project.ActivityInterface;
import pm275.core.project.ProjectInterface;
import pm275.core.resource.Resource;
import pm275.gui.common.DateFormatter;

public class ProjectWriter {
	
	private FileWriter 	 file;
	private PrintWriter  out;
	private int			 activityCount;
	private static final Logger log = Logger.getLogger(ProjectWriter.class);
	private static final String ATTR = "ProjectWriter";	
	/**
	 * For the Project provided as input, prints to the output file the Project's name, start date, 
	 * end date, and primary contact.  Also asks dumpActivity() to do its job for each Activity.  
	 */
	public ProjectWriter(ProjectInterface p, boolean printActivities, FileWriter f, PrintWriter o) {
		file = f;
		out = o;
	    try {
	 	   	//out = new PrintWriter(file, true);
	    	out.println(" ");
	    	out.println(" ");
		    out.println("Project Name: " + p.getName());
		   	out.println("Start Date: " + DateFormatter.getStringForDate(p.getStartDate()));
		 	out.println(" End Date : " + DateFormatter.getStringForDate(p.getEndDate()));
		
		 	if (printActivities) {
				// Run through the list of Activity objects that have been added 
				// to the Project and lists their attribute and association information.  
			 	Comparator keyOrder = Activity.KeyComparator.ascending(); 
				ListIterator activities = p.getActivities(keyOrder).listIterator();
				activityCount = 0;
				ActivityInterface temp = null;
				while (activities.hasNext()) {
		    	  	activityCount++;
		    	 	temp = (ActivityInterface)activities.next();
		    		dumpActivity(temp);
				}
		 	}
		}
	    catch (Exception e) {
	    	log.fatal("Exception opening output file");
		    System.exit(1);
	    }	    
	}
	
    /**
	 * For the Activity provided as input, prints to the output file the Activity's
	 * name, start date, duration, end date, and resources.  Also asks dumpPredecessors() and
	 * dumpSuccessors() to do their respective jobs.
	 */
	public void dumpActivity(ActivityInterface a) {
	    try {
	    	out.println(" ");
			out.println("Activity " + activityCount + ": " + a.getName());
			out.println("  Start Date = " + DateFormatter.getStringForDate(a.getStartDate()));
			out.println("  Duration = " + a.getDuration() + " hours");
			out.println("  End Date = " + DateFormatter.getStringForDate(a.getEndDate()));

			// TLong : Code below is for debugging Critical Path algorithm
			//		   Comment out before final submission
			/*
			out.println("    -----------------------------");
			out.println("  earlyStartHours = " + a.getEarlyStartHours());
			out.println("  earlyFinishHours = " + a.getEarlyFinishHours());
			out.println("  lateStartHours = " + a.getLateStartHours());
			out.println("  lateFinishHours = " + a.getLateFinishHours());
			out.println("  isStartMilestone = " + a.isStartMilestone());
			out.println("  isEndMilestone = " + a.isEndMilestone());
			out.println("    -----------------------------");
			*/
			
			out.println("  Slack Time = " + a.calculateSlackTime());
			out.println("  Resources Assigned:");
			Collection resources = a.getResources();
			for (Iterator i = resources.iterator(); i.hasNext(); )
		    {
	 		 	Resource r = (Resource)i.next();		    	
		    	dumpResource(r);		    	
		    }			
	   		dumpPredecessors(a);
	    	dumpSuccessors(a);
	    }
	    catch (Exception e) {
	    	log.fatal("Exception opening output file");
		    System.exit(1);
	    }	    
	}
	
	/**
	 * prints out the names of all the Activities that precede this Activity
	 */
	private void dumpPredecessors(ActivityInterface a) {
	  try {
		  Collection predecessors = a.getPredecessors();
		  if (predecessors.isEmpty()) {
		    out.println("  Predecessors: None");
		  }  
		  else {
			out.println("  Predecessors: ");    
		    for (Iterator i = predecessors.iterator(); i.hasNext(); )
		    {
		    	ActivityInterface p = (ActivityInterface)i.next(); 
		    	out.println("\t" + p.getName());		    	
		    }
		  }
	  }
	  catch (Exception e) {
	  	out.println("Error reading predecessors: " + e);
	  }
	}
	  
	/**
	 * dumpSuccessors() prints out the names of all the 
	 * Activities that succeed this Activity
	 */
	private void dumpSuccessors(ActivityInterface a) {
	  try {
		  Collection successors = a.getSuccessors();
		  if (successors.isEmpty()) {
		    out.println("  Successors: None");	      
		  }
		  else {
			out.println("  Successors: ");
		    for (Iterator i = successors.iterator(); i.hasNext(); )
		    {
		    	ActivityInterface s = (ActivityInterface)i.next(); 
		    	out.println("\t" + s.getName());		    	
		    }		  
		  }
	  }
	  catch (Exception e) {
	  	out.println("Error reading successors: " + e);
	  }
	}

	/**
	 * For the Resource provided as input, prints the resource's name to the output file.
	 */
	public void dumpResource(Resource r) {
	    try {
	   		out.println("\t" + r.getName());	    
		}
	    catch (Exception e) {
	    	log.fatal("Exception opening output file");
		    System.exit(1);
	    }	     
	}
	
}

/**
* $Log: ProjectWriter.java,v $
* Revision 1.8  2005/04/23 00:35:18  jkeiser
* Added attempt to sort activities by name (KeyComparator.ascending()).  Not working yet.
*
* Revision 1.7  2005/04/18 21:32:56  tlong2
* Critical Path working!
*
* Revision 1.6  2005/04/18 03:49:49  tlong2
* Critical Path tests and CP debugging out.println statements
*
* Revision 1.5  2005/04/17 20:18:52  jkeiser
* Replaced Activity references with ActivityInterface.  Redirected ouput to file rather than console.
*
* Revision 1.4  2005/04/17 14:23:42  jkeiser
* Added call to Activity.calculateSlackTime().  Updated sections that iterate through Activities to properly retrieve their names.  Added boolean parameter to constructor to allow writing out project but not detailed activity info.
*
* Revision 1.3  2005/04/16 22:22:55  jkeiser
* Misc updates to accommodate debugging and change from Enumerators to Collections.  Am temporarily sending output to console instead of file.
*
* Revision 1.2  2005/04/16 15:40:30  jkeiser
* Added code to accept PrintWriter and FileWriter from ProjectWriter class.  Misc other cleanup.
*
* Revision 1.1  2005/04/16 13:05:20  jkeiser
* Initial checkin.  Contains all dump__() methods that were previously in ProjectMaker.
*
*
**/