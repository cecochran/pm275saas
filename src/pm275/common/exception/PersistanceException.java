/*
 * Created on Apr 13, 2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package pm275.common.exception;

import org.apache.log4j.Logger;

/**
 * @author brubuc
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PersistanceException extends PM275BaseException
{
    private static final long serialVersionUID = 3256438127391159350L;
    private static final Logger log = Logger.getLogger(PersistanceException.class );

    //  The constructor for an PersistanceException with a specific message
     public PersistanceException (String msg) {
         super(msg);
         log.error(msg);
     }

    /**
     *  Creates a persistence exception with the specified message and root
     *  cause.
     *  @param msg Error message.
     *  @param cause Exception that caused a persistence exception.
     */
    public PersistanceException (String msg, Throwable cause)
    {
        super(msg, cause);
        log.error(msg);
    }
}

/*
 *  $Log: PersistanceException.java,v $
 *  Revision 1.3  2005/05/06 06:02:57  achou
 *  Added constructor that takes a message and a cause.
 *
 */
