package pm275.common.exception;
/***************************************************
 * File:        $RCSfile: CycleDependencyException.java,v $
 * @author      $Author: dnathan $,
 * @version     $Id: CycleDependencyException.java,v 1.3 2005/04/12 03:39:40 dnathan Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:     Exception Handler when a Cycle is detected when connecting
 * 				activities.
 * </p>
 */
public class CycleDependencyException 
    extends PM275BaseException
{
	private static final long serialVersionUID = 3690192161651830838L;

	public CycleDependencyException(String msg)
    {
	super(msg);
    }
}
/**
* $Log: CycleDependencyException.java,v $
* Revision 1.3  2005/04/12 03:39:40  dnathan
* Added serialization ID.
*
* Revision 1.2  2005/04/09 00:40:18  tlong2
* Added CVS header and footer
*
* Revision 1.1  2005/04/03 22:34:55  tlong2
* Exception created
*
*
*/
