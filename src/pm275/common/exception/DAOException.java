package pm275.common.exception;
import org.apache.log4j.Logger;

/***************************************************
 * File:        $RCSfile: DAOException.java,v $
 * @author      $Author: vchumako $,
 * @version     $Id: DAOException.java,v 1.1 2005/05/01 05:11:10 vchumako Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose: Exception Handler when a duplicate Hash Key is found
 * </p>
 */
public class DAOException extends PM275BaseException 
{
	/**
	 * This field auto-generated by Eclipse.  DN.
	 */
	//private static final long serialVersionUID = 3256437023450347571M;
	private Throwable keyNotFoundException; 
	private static final Logger log = Logger.getLogger( KeyNotFoundException.class );
	private static final String ATTR = "KeyNotFoundException";
	
	
	public DAOException()
	{
		super();
	}
	
	public DAOException(String msg)
	{
		super(msg);
		log.error(msg);
	}
	
    public DAOException(String message, Throwable cause)
    {
        super(message, cause);
        log.error(message);
    }

}
