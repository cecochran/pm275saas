package pm275.common.exception;
import org.apache.log4j.Logger;

/***************************************************
 * File:        $RCSfile: CriticalPathException.java,v $
 * @author      $Author: dnathan $,
 * @version     $Id: CriticalPathException.java,v 1.2 2005/04/16 21:06:06 dnathan Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose: Exception during critical path algorithm 
 * </p>
 */
public class CriticalPathException extends PM275BaseException{

	private static final long serialVersionUID = 4049920463226286388L;
	private Throwable CriticalPathException; 
	private static final Logger log = Logger.getLogger( CriticalPathException.class );
	private static final String ATTR = "CriticalPathException";
	
	
	public CriticalPathException()
	{
		super();
	}
	
	public CriticalPathException(String msg)
	{
		super(msg);
		log.error(msg);
	}
	
}

/**
* $Log: CriticalPathException.java,v $
* Revision 1.2  2005/04/16 21:06:06  dnathan
* Warning hunting.
*
* Revision 1.1  2005/04/13 00:29:04  tlong2
* Log.error updates to avoid unused imports of log4j.logger warnings
*
* Revision 1.1  2005/04/12 22:34:55  tlong2
* Exception created
*
*
*/