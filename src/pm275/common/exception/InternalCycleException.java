package pm275.common.exception;
/***************************************************
 * File:        $RCSfile: InternalCycleException.java,v $
 * @author      $Author: dnathan $,
 * @version     $Id: InternalCycleException.java,v 1.3 2005/04/16 21:06:06 dnathan Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents: InternalCycleException
 * </p>
 * <p>
 * Purpose: Exception thrown internally by CycleDetection.java if caught a cycle in given project.
 * </p>
 */
public class InternalCycleException
    extends PM275BaseException
{
	private static final long serialVersionUID = 3258416114332809017L;

	public InternalCycleException(String msg)
    {
	super(msg);
    }
}
/**
 * $Log: InternalCycleException.java,v $
 * Revision 1.3  2005/04/16 21:06:06  dnathan
 * Warning hunting.
 *
 * Revision 1.2  2005/04/16 05:41:01  umangkus
 * Add some comment.
 *
 * Revision 1.1  2005/04/15 07:44:34  umangkus
 * Add this exception, to be used internally by CycleDepedency.java
 *
 *
 **/
