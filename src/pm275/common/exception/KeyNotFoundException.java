package pm275.common.exception;
import org.apache.log4j.Logger;

/***************************************************
 * File:        $RCSfile: KeyNotFoundException.java,v $
 * @author      $Author: tlong2 $,
 * @version     $Id: KeyNotFoundException.java,v 1.6 2005/04/13 00:29:03 tlong2 Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * Exception for Failure to find key
 * </p>
 */
public class KeyNotFoundException extends PM275BaseException{
	/**
	 * This field auto-generated by Eclipse.  DN.
	 */
	private static final long serialVersionUID = 3256437023450347571L;
	private Throwable keyNotFoundException; 
	private static final Logger log = Logger.getLogger( KeyNotFoundException.class );
	private static final String ATTR = "KeyNotFoundException";
	
	
	public KeyNotFoundException()
	{
		super();
	}
	
	public KeyNotFoundException(String msg)
	{
		super(msg);
		log.error(msg);
	}
	
}