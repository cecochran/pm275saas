# EMS MySQL Manager Pro 3.2.0.1
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : team1


SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `team1`;

CREATE DATABASE `team1`
    CHARACTER SET 'latin1'
    COLLATE 'latin1_swedish_ci';

USE `team1`;

#
# Structure for the `activity` table : 
#

DROP TABLE IF EXISTS `activity`;

CREATE TABLE `activity` (
  `activitykey` varchar(255) NOT NULL default '',
  `projectkey` varchar(255) NOT NULL default '',
  `name` varchar(255) default NULL,
  `startdate` datetime default NULL,
  `enddate` datetime default NULL,
  `duration` int(11) default NULL,
  `slacktime` int(11) default NULL,
  `status` tinyint(4) default '0',
  `resourcepoolkey` varchar(255) default NULL,
  `fixedduration` tinyint(4) default '0',
  `fixedstartdate` tinyint(4) default '0',
  PRIMARY KEY  (`activitykey`),
  UNIQUE KEY `activitykey` (`activitykey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Structure for the `activityref` table : 
#

DROP TABLE IF EXISTS `activityref`;

CREATE TABLE `activityref` (
  `activitykey` varchar(255) NOT NULL default '',
  `projectKey` varchar(255) default NULL,
  `predflag` int(11) default NULL,
  `value` varchar(255) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1
#
# Structure for the `project` table : 
#

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `projectkey` varchar(255) NOT NULL default '',
  `name` varchar(255) NOT NULL default '',
  `startMilestonekey` varchar(255) default NULL,
  `endMilestonekey` varchar(255) default NULL,
  `resourcepoolkey` varchar(255) default NULL,
  `calendarkey` varchar(255) default NULL,
  PRIMARY KEY  (`projectkey`),
  UNIQUE KEY `ProjectKey` (`projectkey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
