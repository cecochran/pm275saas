/*
 * Created on May 2, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pm275.common.persistance;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.*;
import java.util.*;

import org.apache.log4j.Logger;

import pm275.common.exception.*;
import pm275.common.persistance.*;
import pm275.core.project.*;
import pm275.core.resource.Resource;
import pm275.core.resource.ResourceDAOtoDB;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourcePoolKey;
import pm275.core.resource.ResourceService;
import pm275.gui.common.DateFormatter;
import pm275.vct.ProjectMaker;
import pm275.*;


/**
 * @author Chris
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class verifyConnect {

	   private static final Logger log = Logger.getLogger(Project.class);
	    private static final String ATTR = "Project";
	    

	
	public static ProjectInterface buildProject(){
		  ProjectService  projectService = ProjectService.getInstance();
		    ResourceService resourceService = ResourceService.getInstance();
		ProjectInterface      project1 = null;
		   ProjectInterface      project2 = null;
		    ProjectInterface      project3 = null;
		     ProjectKey    project1Key = null;
		   ProjectKey    project2Key = null;
		    ProjectKey    project3Key = null;
		    //project 4 needs to use some of project 1's activities
		     ActivityKey   project4Act1Key = null;
		     ActivityKey   project4Act2Key = null;

		  
		
		ResourcePool    project1Pool;
        ResourcePoolKey project1PoolKey;

        try {
          // Create Resource Pool and add Peter and Paul
          project1Pool    = resourceService.createResourcePool();
          project1PoolKey = project1Pool.getResourcePoolKey();
          Resource paul  = resourceService.createResource("RPL1", "Paul L1");
          project1Pool.addResource(paul);
          Resource peter = resourceService.createResource("RPL2", "Peter L4");
          project1Pool.addResource(peter);
          resourceService.addResourcePool(project1Pool);

          // Create Project
          project1 = projectService.addProject("Test Project 1");
          project1Key = project1.getProjectKey();
          java.util.Date startDate = DateFormatter.getDateForString("2005-04-18");
          project1.setStartDate(startDate);

                    
          // Add Resource Pool to Project
          projectService.addResources(project1Key, project1PoolKey);
          /**
           * Now start adding the Activities with durations in hours
           * 1. Start date: 4/18
           * 2. Design (Peter and Paul), 1week
           * 3. Code UI (Peter), 7 days, after design is complete
           * 4. Code Middle Tier Stuff (Paul), 10 days, after design is complete
           * 5. Integration of UI and Middle Tier (Peter and Paul), 2 days, after UI and Middle Tier are complete
           * 6. System Test(Peter and Paul), 5 days, after Integration is complete
           * 7. Ready for UAT, milestone date depending on System Test completion
           *
           */
          ActivityInterface design = projectService.addActivity(project1Key, "Design");
          design.setDuration(40);
          ActivityInterface codeUI = projectService.addActivity(project1Key, "Code UI");
          codeUI.setDuration(56);
          ActivityInterface codeMT = projectService.addActivity(project1Key, "Code Middle Tier Stuff");
          codeMT.setDuration(80);
          ActivityInterface integration = projectService.addActivity(project1Key, "Integration of UI and Middle Tier");
          integration.setDuration(16);
          ActivityInterface systemTest = projectService.addActivity(project1Key, "System Test");
          systemTest.setDuration(40);
          ActivityInterface readyForUAT = projectService.addActivity(project1Key, "Ready for UAT");
          readyForUAT.setDuration(0);


          /**
           * set resources
           */
          resourceService.assign(paul.getKey(), design.getActivityKey(), project1Key,100);
          resourceService.assign(peter.getKey(), design.getActivityKey(),project1Key, 100);
          resourceService.assign(peter.getKey(), codeUI.getActivityKey(),project1Key, 100);
          resourceService.assign(paul.getKey(), codeMT.getActivityKey(),project1Key, 100);
          resourceService.assign(paul.getKey(), integration.getActivityKey(),project1Key, 100);
          resourceService.assign(peter.getKey(), integration.getActivityKey(),project1Key, 100);
          resourceService.assign(paul.getKey(), systemTest.getActivityKey(), project1Key,100);
          resourceService.assign(peter.getKey(), systemTest.getActivityKey(),project1Key, 100);

          /**
           * finally, add the dependencies
           */
          project1.associateActivities(design.getActivityKey(),codeUI.getActivityKey());
          project1.associateActivities(design.getActivityKey(),codeMT.getActivityKey());
          project1.associateActivities(codeUI.getActivityKey(),integration.getActivityKey());
          project1.associateActivities(codeMT.getActivityKey(),integration.getActivityKey());
          project1.associateActivities(integration.getActivityKey(),systemTest.getActivityKey());
          project1.associateActivities(systemTest.getActivityKey(),readyForUAT.getActivityKey());

          //project 4 will need these
          project4Act1Key = codeUI.getActivityKey();
          project4Act2Key = readyForUAT.getActivityKey();
          log.info("VCT Project1 created successfully");

        }
        catch (CycleDependencyException cde) {
          log.warn("Warning: " + cde);
        }
        catch (Exception e) {
          log.fatal("Error fabricating VCT Project1: " + e);
          e.printStackTrace();
          System.exit(1);
        }
        return project1;
	}
	
	public static void main(String[] args) {
	    Connection con = null;

	 	    try {
	    	Class.forName("com.mysql.jdbc.Driver").newInstance();
         
	    //BASE CODE	
	   con = DriverManager.getConnection(PM275DBProperties.DATABASE,PM275DBProperties.USER, PM275DBProperties.PASSWORD );
	  	if(!con.isClosed()){
	        System.out.println("Successfully connected to " +
	          "MySQL server using TCP/IP...");
	          }
	          con.close();
	   	    	
	          System.out.println("PROJECT LOAD");
	          ProjectDAOtoDB pdba = new ProjectDAOtoDB();
	          
	         // ProjectKey pkey = new  ProjectKey("26143550 1115933879015");
	          //ProjectInterface  project1 = pdba.get(pkey);
	         // System.out.println(pdba.getAbbaList().size());
	          
	          Resource res = new Resource("12","Chris Cochran");
	          ResourceDAOtoDB resDB = new ResourceDAOtoDB();
	          resDB.set(res);
	          
	          resDB.set(res);
	          
	          System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
	          System.out.println(res.toString());
	          
	    	
		  /*  ProjectInterface  project1 = buildProject();
		  	ProjectDAOtoDB pdba = new ProjectDAOtoDB();
		    int num = pdba.set(project1);
		    System.out.println("Return result:" + num );
		    System.out.println("Project 1:" + project1.toString() );
		    ProjectDAOtoDB pdba2 = new ProjectDAOtoDB();
		   
		    Map actMap = project1.getActivities();
			   HashMap map = new HashMap(actMap);
			   
			   int cnt = 1;
			   int res = 0;
			   //get iterator
		        java.util.Iterator it =  map.values().iterator();
		       //cycle thru all the Activities
		        while (it.hasNext()) {
		            
		        	Activity act = (Activity) it.next();
		        	//get project and call recalcualte method
		        	ActivityDAOtoDB actDB = new ActivityDAOtoDB();  
		        	res = actDB.set(act);
		            System.out.println("Count: "+cnt +" Set result: " + res);
		            
		            //actDB.delete(act.getActivityKey());
		          // System.out.println("Count: "+cnt +" Delete result: " + res);
		            cnt++;
		        }
		        
		       */ 
		    
	/*	    
		    //Use old key
		    ProjectKey pKey = new ProjectKey("Test Project 1");			//project1.getProjectKey()
		    ProjectInterface project2 = pdba.get(pKey);
		    System.out.println("*********** PROJECT2 **********"+"\n");
 	    	System.out.println(project2);
   	*/
	 	   } catch(Exception e) {
		      System.err.println("SQL Exception: " + e.getMessage());
		       
		  } 
	 	   
	 	   /*catch(InstantiationException e) {
		      System.err.println("InstantiationException Exception: " + e.getMessage());
	    }catch(IllegalAccessException e) {
		      System.err.println("IllegalAccessException: " + e.getMessage());
	    }catch(ClassNotFoundException e) {
		      System.err.println("ClassNotFoundException Exception: " + e.getMessage());
	    }*/ 
	  }

	
}
