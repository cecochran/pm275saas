/*
 *  File:        $RCSfile: ProjectXmlWriter.java,v $
 *  @author      $Author: achou $,
 *  @version     $Id: ProjectXmlWriter.java,v 1.4 2005/05/09 02:50:18 achou Exp $
 *  Assignment:  CSCI E-275 Team 1 Project
 *  <p>
 *  Contents:
 *  </p>
 *  <p>
 *  Purpose:
 *  </p>
 */

package pm275.common.persistance;

import java.util.Collection;
import java.util.List;
import java.util.Iterator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import pm275.core.calendar.CalendarService;
import pm275.core.calendar.CalendarInterface;
import pm275.core.calendar.CalendarKey;
import pm275.core.resource.ResourceService;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.AssignmentInterface;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ActivityInterface;
import pm275.core.project.Activity;
import pm275.common.exception.ResourceException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.PersistanceException;

import org.apache.log4j.Logger;

import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.ListIterator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.XMLOutputter;
import org.jdom.output.Format;


/**
 *  This class writes the XML representation of a project and its various
 *  components (activities, resources, calendars) to a file.
 */
public class ProjectXmlWriter
{
    //
    //  Data members.
    //

    private final File outputFile;

    // Namespace for XML schema.
    private static final Namespace XSI_NAMESPACE =
            Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");

    // Logger.
    private static final Logger log = Logger.getLogger( ProjectXmlWriter.class );

    // Date format.
    private static final DateFormat DATE_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");


    //
    //  Methods.
    //

    /**
     *  Creates a writer that outputs to the specified file.
     *  @param file The target file.
     */
    public ProjectXmlWriter(File file)
    {
        this.outputFile = file;
    }

    /**
     *  Creates an XML document for the specified project, then outputs it to
     *  the file.
     *  @param project Project to write to file.
     *  @throws PersistanceException If the project cannot be written to file.
     */
    public void writeProject(ProjectInterface project)
            throws PersistanceException
    {
        try
        {
            // First, create the various components of the XML document.

            // Create the <project> element.
            Element projectElement = projectToElement(project);

            // Create the <activities> element.
            Element activitiesElement = new Element("activities");
            List activities = project.getActivities(Activity.OrderComparator.ascending());
            for (Iterator i = activities.iterator(); i.hasNext(); )
            {
                ActivityInterface a = (ActivityInterface) i.next();
                activitiesElement.addContent(activityToElement(a));
            }

            // Create the <resources> element.
            Element resourcesElement = new Element("resourcePool");
            Collection resources = project.getResources();
            for (Iterator i = resources.iterator(); i.hasNext(); )
            {
                ResourceInterface r = (ResourceInterface) i.next();
                resourcesElement.addContent(resourceToElement(r));
            }

            // Create the <calendars> element.
            Element calendarsElement = new Element("calendars");
            CalendarKey calendarKey = project.getCalendarKey();
            CalendarInterface calendar;
            if (calendarKey != null)
            {
                try
                {
                    calendar = CalendarService.getInstance().getCalendar(calendarKey);
                }
                catch (KeyNotFoundException ex)
                {
                    log.error("Cannot find project calendar for key: " + calendarKey.toString());
                    calendar = CalendarService.getInstance().getCorporateCalendar();
                }
            }
            else
            {
                calendar = CalendarService.getInstance().getCorporateCalendar();
            }
            calendarsElement.addContent(calendarToElement(calendar));

            // Now create the docuemnt's root element and add everything above.

            // Create the document's root element.
            Element documentRoot = new Element("pm275");
            documentRoot.setAttribute(
                    "noNamespaceSchemaLocation", "pm275.xsd", XSI_NAMESPACE);
            documentRoot.addContent(projectElement);
            documentRoot.addContent(activitiesElement);
            documentRoot.addContent(resourcesElement);
            documentRoot.addContent(calendarsElement);

            // Set the root element, then output the document.
            Document document = new Document();
            document.setRootElement(documentRoot);
            XMLOutputter out = new XMLOutputter(Format.getPrettyFormat());
            out.output(document, new FileWriter(outputFile));
        }
        catch (IOException ex)
        {
            String message = "Cannot write project " + project.getName() +
                             "to file " + outputFile.getAbsolutePath();
            log.error(message);
            throw new PersistanceException(message, ex);
        }
    }

    /**
     *  Creates and returns an XML element for the specified resource.
     *  @param resource The resource.
     *  @return XML element for the resource.
     */
    private Element resourceToElement(ResourceInterface resource)
    {
        // Create elements for relevant resource attributes.

        // In Team 1 there is only 1 name value for resource. So
        // assume getName() contains first and last name
        // separated by white space and parse it
        StringTokenizer st = new StringTokenizer(resource.getName());
        int i= 1;
        String token = "";
        String sFirst = "";
        String sLast = "";
        while ( st.hasMoreTokens() ) {
            token = st.nextToken();
            if (i == 1)
                sFirst = token;
            if (i == 2)
                sLast = token;
            i++;
        }



        //<first>
        Element first = new Element("first");
        first.setText(sFirst);

        // <last>
        Element last = new Element("last");
        last.setText(sLast);

        // <resourceCalendar>
        Element resourceCalendar = new Element("resourceCalendar");

            // <calendarRef>
        Element calendarRef = new Element("calendarRef");
        calendarRef.setAttribute("calendarKey",String.valueOf(resource.getCalendarKey()));

        resourceCalendar.addContent(calendarRef);

        // <resource>
        // Create element for this resource and add to the resource list.
        Element resourceElement = new Element("resource");
        resourceElement.setAttribute("resourceKey",String.valueOf(resource.getKey()));
        resourceElement.addContent(first);
        resourceElement.addContent(last);
        resourceElement.addContent(resourceCalendar);

        // Add the resource to the resources pool
        // resources.addContent(resourceElement);
        return resourceElement;
    }

    /**
     *  Creates and returns an XML element for the specified activity.
     *  @param activity The activity.
     *  @return XML element for the activity.
     */
    private Element activityToElement(ActivityInterface activity)
    {
        // Create elements for relevant calendar attributes.
        Iterator it;
        // <name>
        Element name = new Element("name");
        name.setText(String.valueOf(activity.getName()));

        // <startDate>
        Element startDate = new Element("startDate");
        startDate.setAttribute("fixed",String.valueOf(activity.getIsFixedStartDate()));
        startDate.setText(DATE_FORMAT.format(activity.getStartDate()));

        // <endDate>
        Element endDate = new Element("endDate");
        // TODO WARNING Hardcoded value
        //      NOTE: This is always computed in team1's implementation, so
        //      setting to false. Assuming team2's implementation is different
        endDate.setAttribute("fixed",String.valueOf(false));
        endDate.setText(DATE_FORMAT.format(activity.getEndDate()));

        // <duration>
        Element duration = new Element("duration");
        duration.setAttribute("fixed",String.valueOf(activity.getIsFixedDuration()));
        duration.setText(String.valueOf(activity.getDuration()));

        // <subactivities>
        Element subactivities = new Element("subactivities");
        subactivities.setText("");

        // <dependencies> = predecessors
        Element dependencies = new Element("dependencies");
        it = activity.getPredecessors().iterator();

        while (it.hasNext())
        {
            ActivityInterface a = (ActivityInterface)it.next();

            // Only do this if predecessor is not start milestone, which is
            // always implied.
            if (! a.isStartMilestone())
            {
                // <activityRef>
                Element activityRef = new Element("activityRef");
                activityRef.setAttribute("activityKey",String.valueOf(a.getActivityKey()));

                dependencies.addContent(activityRef);
            }
        }


        // <assignments>
        Element assignments = new Element("assignments");

        ResourceService rs = ResourceService.getInstance();
        it = activity.getResources().iterator();

        while (it.hasNext())
        {
            try {

                ResourceInterface res = (ResourceInterface)it.next();

                AssignmentInterface assn = rs.getAssignment(res.getKey(),activity.getActivityKey());
                // <assignment>
                Element assignment = new Element("assignment");
                assignment.setAttribute("assignmentKey",String.valueOf(assn.getAssignmentKey()));
                assignment.setAttribute("resourceKey",String.valueOf(res.getKey()));
                // <effort> = getPercent()
                Element effort = new Element("effort");
                effort.setText(String.valueOf(assn.getPercent()));
                assignment.addContent(effort);
                assignments.addContent(assignment);
            }
            catch (ResourceException re) {
                log.error("ResourceException thrown in ProjectXmlWriter.activityToElement " + re.getMessage());
            }

        }

        // <activity>
        // Create element for this activity and add to the activity list.
        Element activityElement = new Element("activity");
        activityElement.setAttribute("activityKey",String.valueOf(activity.getActivityKey()));
        activityElement.addContent(name);
        activityElement.addContent(startDate);
        activityElement.addContent(endDate);
        activityElement.addContent(duration);
        activityElement.addContent(subactivities);
        activityElement.addContent(dependencies);
        activityElement.addContent(assignments);

        // add the activity to the activities
        // activities.addContent(activityElement);
        return activityElement;
    }

    /**
     *  Creates and returns an XML element for the specified calendar.
     *  @param calendar The calendar.
     *  @return XML element for the calendar.
     */
    private Element calendarToElement(CalendarInterface calendar)
    {
        // Create elements for relevant calendar attributes.
        // <daysInWeek>
        Element daysInWeek = new Element("daysInWeek");
        daysInWeek.setText(String.valueOf(calendar.getDaysPerWeek()));

        // <hoursInDay>
        Element hoursInDay = new Element("hoursInDay");
        hoursInDay.setText(String.valueOf(calendar.getHoursPerDay()));

        // Create element for this calendar and add to the calendars list.
        //<calendarElement>
        Element calendarElement = new Element("calendar");
        calendarElement.setAttribute("calendarKey",
                                     calendar.getKey().toString());
        calendarElement.addContent(daysInWeek);
        calendarElement.addContent(hoursInDay);

        // Add the calendar to the calendars
        // calendars.addContent(calendarElement);
        return calendarElement;
    }

    /**
     *  Sets up and returns the XML element for the specified project.
     *  @param project The project.
     *  @return XML element for the project.
     */
    private Element projectToElement(ProjectInterface project)
    {
        // Create elements for the project
        Element projectElement = new Element("project");
        projectElement.setAttribute("projectKey",String.valueOf(project.getProjectKey()));


        // <name>
        Element name = new Element("name");
        name.setText(project.getName());

        //<startDate>
        Element startDate = new Element("startDate");

        // TODO WARNING hardcoded value> Don't see why it is needed
        //              for team 1 so assiging as false.
        startDate.setAttribute("fixed",String.valueOf(false));

        startDate.setText(DATE_FORMAT.format(project.getStartDate()));

        //<startDate>
        Element endDate = new Element("endDate");

        // TODO WARNING hardcoded value> Don't see why it is needed
        //              for team 1 so assiging as false.
        endDate.setAttribute("fixed",String.valueOf(false));

        try {
            endDate.setText(DATE_FORMAT.format(project.getEndDate()));
        } catch (InternalProjectException ipe) {
            log.error("InternalProjectException thrown in visitProject.visitActivity " + ipe.getMessage());

        } catch (KeyNotFoundException knfe) {
            log.error("KeyNotFoundException thrown in visitProject.visitActivity " + knfe.getMessage());

        }

        //<activityList>
        Element activityList = new Element("activityList");

        ListIterator li = project.getActivities(null,false).listIterator();


        while (li.hasNext())
        {

            ActivityInterface act = (ActivityInterface)li.next();

            // <activityRef>
            Element activityRef = new Element("activityRef");
            activityRef.setAttribute("activityKey",String.valueOf(act.getActivityKey()));

            // add the reference to the activityList
            activityList.addContent(activityRef);
        }

        // <projectCalendar>
        Element projectCalendar = new Element("projectCalendar");

            // <calendarRef>
            Element calendarRef = new Element("calendarRef");
            calendarRef.setAttribute("calendarKey",String.valueOf(project.getCalendarKey()));

        projectCalendar.addContent(calendarRef);

        // add the elements to the <project>
        projectElement.addContent(name);
        projectElement.addContent(startDate);
        projectElement.addContent(endDate);
        projectElement.addContent(activityList);
        projectElement.addContent(projectCalendar);

        return projectElement;
    }

}

/*
 * $Log: ProjectXmlWriter.java,v $
 * Revision 1.4  2005/05/09 02:50:18  achou
 * Removed erroneous ProjectVisitorToXML references.
 *
 * Revision 1.3  2005/05/08 22:41:17  achou
 * Moved writeProject() to top of class.  Export project calendar instead of corporate calendar.
 *
 * Revision 1.2  2005/05/06 23:42:27  achou
 * Format project and activity start/end dates according to XML specification (using MySQL default timestamp format).
 *
 * Revision 1.1  2005/05/06 06:04:53  achou
 * New file to replace ProjectVisitorToXML.java.
 *
 */
