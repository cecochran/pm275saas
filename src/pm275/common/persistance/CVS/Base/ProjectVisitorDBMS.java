/*
 * File: $RCSfile: ProjectVisitorDBMS.java,v $
 * @author $Author: ccochran $,
 * @version $Id: ProjectVisitorDBMS.java,v 1.16 2005/05/13 20:34:11 ccochran Exp $
 * Assignment: CSCI E-275 Team 1 Project
 * <p>
 * Contents: Visitor implementation for DB case
 * </p>
 * <p>
 * Purpose: Marshal/Unmarshall PM275 object to/from dbms
 * </p>
 */
package pm275.common.persistance;

import java.util.Map;
import java.util.TreeMap;

import pm275.core.calendar.CalendarInterface;
import pm275.core.calendar.CalendarKey;
import pm275.core.calendar.CalendarService;
import pm275.core.project.ActivityInterface;
import pm275.core.project.ActivityKey;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ProjectKey;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.ResourceKey;
import pm275.core.resource.AssignmentInterface;
import pm275.core.resource.ResourceService;
import pm275.common.exception.PersistanceException;
import pm275.common.exception.ResourceException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

/**
 * @author vc
  */
public class ProjectVisitorDBMS implements ProjectVisitorInterface, PM275DBSchema {
	  private static final Logger log = Logger.getLogger( ProjectVisitorDBMS.class );
	  private static final String ATTR = "ProjectVisitorDBMS";
	  
	  private Connection conn;

	
	ProjectVisitorDBMS() {
		try {
		    conn = DBConnection.getConnection();
		} catch(PersistanceException pe) {
		    log.error("Can't get Database connection!");
		}
		
	}
	/* (non-Javadoc)
	 * @see pm275.common.persistance.ProjectVisitorInterface#visitCalendar(pm275.core.calendar.CalendarInterface)
	 */
	public void visitCalendar(CalendarInterface c) {
	    PreparedStatement ps = null;
	    try {
		if(c.getName() == null) {
		    c.setName("Corporate calendar");
		}
		ps = conn.prepareStatement(SELECT_CALENDAR_WITH_ID);
		ps.setString(1, c.getKey().toString());
		ResultSet rs = ps.executeQuery();
		log.debug("VCDEBUG: cKey = " + c.getKey() + "; cName = " 
			  + c.getName());
		if(rs.next()) {
		    log.debug("VCDEBUG: UPDATE");
		    ps = conn.prepareStatement(UPDATE_CALENDAR);
		    ps.setString(1, c.getName());
		    ps.setInt(2, c.getCalendarType());
		    ps.setInt(3, c.getDaysPerWeek());
		    ps.setInt(4, c.getHoursPerDay());

		    ps.setString(5, c.getKey().toString());
		} else {
		    log.debug("VCDEBUG: INSERT");
		    ps = conn.prepareStatement(INSERT_CALENDAR);
		    ps.setString(1, c.getKey().toString());
		    ps.setString(2, c.getName());
		    ps.setInt(3, c.getCalendarType());
		    ps.setInt(4, c.getDaysPerWeek());
		    ps.setInt(5, c.getHoursPerDay());
		    ps.setInt(6, 0);
		    
		}
		ps.execute();
		log.debug("Saved calendar in db: " + c.getName());
	    } catch(SQLException sqle) {
		log.error("Can't save calendar:" + c.getName() 
			  + " into DB: " + sqle.getMessage() 
			  + "; SQLState: " + sqle.getSQLState() + "; err = " 
			  + sqle.getErrorCode());
	    } finally {
		try {
		    if( ps != null) {
			ps.close();
		    }
		} catch(Exception e) {
		}
		
	    }
	}

	
	public void visitResource(ResourceInterface r){
		
		//CEC Refactored to DAO Class
	}

	/* (non-Javadoc)
	 * @see pm275.common.persistance.ProjectVisitorInterface#visitResource(pm275.core.resource.ResourceInterface)
	 */
	public void visitResourceOld(ResourceInterface r) {
		PreparedStatement ps = null;
		try {
		    ps = conn.prepareStatement(SELECT_RESOURCE_WITH_ID);
		    ps.setString(1, r.getKey().toString());
		    ResultSet rs = ps.executeQuery();

		    String rName = r.getName();
			String fName = rName;
			String lName = "";
			
			int index = rName.lastIndexOf(' ');
			if(index >= 0)
		    {
				fName = rName.substring(0, rName.lastIndexOf(' '));
				lName = rName.substring(rName.lastIndexOf(' '));
		    }
		    
		    
		    //check if resource is already there
		    if(rs.next()) {
			ps = conn.prepareStatement(UPDATE_RESOURCE);

			ps.setString(1, fName);
			ps.setString(2, lName);
			ps.setString(3, r.getKey().toString());
			log.debug("Update " + rName + " in the db...");
		    } else {
			
			ps = conn.prepareStatement(INSERT_RESOURCE);
			ps.setString(1, r.getKey().toString());

			ps.setString(2, fName);
			ps.setString(3, lName);
			log.debug("Insert " + rName + " into the db...");
		    }

		    // insert or update
		    ps.execute();
		} catch(SQLException sqle) {
		    log.error("Can't save resource:" + r.getName() 
			      + " into DB!");
/*
		} catch(PersistanceException pse) {
		    log.error("Can't save resource: " + pse.getMessage());
*/
		} finally {
		    try {
			if( ps != null) {
			    ps.close();
			}
		    } catch(Exception e) {
		    }
		}
	}

    /**
     * @see pm275.common.persistance.ProjectVisitorInterface#visitResource(pm275.core.resource.ResourceInterface)
     */
    public void visitAssignment(AssignmentInterface a) {
	PreparedStatement ps = null;
	try {
	    ps = conn.prepareStatement(SELECT_ASSIGNMENT_WITH_ID);
	    ps.setString(1, a.getAssignmentKey().toString());
	    ResultSet rs = ps.executeQuery();
	    
	    //check if assignment is already there
	    if(rs.next()) {
		ps = conn.prepareStatement(UPDATE_ASSIGNMENT);
		ps.setString(1, a.getResource().toString());
		ps.setString(2, a.getActivityKey().toString());
		ps.setDouble(3, a.getPercent());
		ps.setString(4, a.getAssignmentKey().toString());
		log.debug("Update " + a.getAssignmentKey() + " in the db...");
	    } else {
		ps = conn.prepareStatement(INSERT_ASSIGNMENT);
		ps.setString(1, a.getAssignmentKey().toString());
		ps.setString(2, a.getResource().toString());
		ps.setString(3, a.getActivityKey().toString());
		ps.setDouble(4, a.getPercent());
		log.debug("Insert " + a.getAssignmentKey() + " into the db...");
	    }
	    
	    // insert or update
	    ps.execute();
	} catch(SQLException sqle) {
		//sqle.printStackTrace();
	    log.error("Can't save assignment:" + a.getAssignmentKey() + " into DB!");
	} finally {
	    try {
		if( ps != null) {
		    ps.close();
		}
	    } catch(Exception e) {
	    }
	}
    }
    
    
	/* (non-Javadoc)
	 * @see pm275.common.persistance.ProjectVisitorInterface#visitActivity(pm275.core.project.ActivityInterface)
	 */
	public void visitActivity(ActivityInterface a) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see pm275.common.persistance.ProjectVisitorInterface#visitProject(pm275.core.project.ProjectInterface)
	 */
	public void visitProject(ProjectInterface p) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see pm275.common.persistance.ProjectVisitorInterface#writeProject()
	 */
	public void writeProject() {
		// TODO Auto-generated method stub

	}
	/* (non-Javadoc)
	 * @see pm275.common.persistance.ProjectVisitorInterface#readProject()
	 */
	public void readProject() {
		// TODO Auto-generated method stub
		
	}

    public void readAssignments(ActivityKey actKey, ProjectKey pKey) {
	ResourceService rService = ResourceService.getInstance();
	PreparedStatement ps = null;
	try {
	    ps = conn.prepareStatement(SELECT_ASSIGNMENT_BY_AKEY);
	    ps.setString(1, actKey.toString());
	    ResultSet rs = ps.executeQuery();
	    while(rs.next()) {
		ResourceKey rKey = new ResourceKey(rs.getString(ASSIGNMENT_RKEY));
		try {
		    rService.deassign(rKey, actKey, pKey);
		} catch(Exception e) {}

		try {
		    rService.assign(rKey, actKey, pKey, rs.getDouble(ASSIGNMENT_EFFORT));
		} catch(ResourceException rse) {
		    log.error("Can't (de)assign " + rKey + "/" + actKey + "/" + pKey + " :" + rse.getMessage());
		}

	    }
	} catch(SQLException sqle) {
	    log.error("Can't load assignments from DB for: " + actKey);
	} finally {
	    try {
		if( ps != null) {
		    ps.close();
		}
	    } catch(Exception e) {
	    }
	}
    }


    public Map readResources() {
	PreparedStatement ps = null;
	Map res = new TreeMap();
	try {
	    ps = conn.prepareStatement(SELECT_RESOURCES);
	    
	    ResultSet rs = ps.executeQuery();
	    while(rs.next()) {
		res.put(rs.getString(RESOURCE_ID),
			rs.getString(RESOURCE_FIRST) + " " + rs.getString(RESOURCE_LAST));
	    }
	    
	} catch(SQLException sqle) {
	    log.error("Can't load resources from DB:");
	} finally {
	    try {
		if( ps != null) {
		    ps.close();
		}
	    } catch(Exception e) {
	    }
	}
	
	return res;
    }

    /* (non-Javadoc)
     * @see pm275.common.persistance.ProjectVisitorInterface#readCalendar(pm275.core.project.ProjectKey)
     */
//    public void readCalendar(CalendarKey cKey) {
    public CalendarKey readCalendar(String cStr) {
	CalendarKey cKey = new CalendarKey(cStr);
	CalendarService cs = CalendarService.getInstance();
	PreparedStatement ps  = null;
	try {
	    ps = conn.prepareStatement(SELECT_CALENDAR_WITH_ID);
	    ps.setString(1, cKey.toString());
	    ResultSet rs = ps.executeQuery();
	    while(rs.next()) {
		try {
		    cs.removeCalendar(cKey);
		} catch(Exception e) {};

		CalendarInterface ci = cs.addCalendar(cKey);
		ci.setName(rs.getString(CALENDAR_NAME));
		ci.setCalendarType(rs.getInt(CALENDAR_TYPE));
		ci.setHoursPerDay(rs.getInt(CALENDAR_HOURS));
		ci.setDaysPerWeek(rs.getInt(CALENDAR_DWEEK));
		log.debug("Calendar " + ci.getName() + " loaded successfully!");
	    }
	} catch(SQLException sqle) {
	    log.error("Can't load calendar from DB!");
	} finally {
	    try {
		if( ps != null) {
		    ps.close();
		}
	    } catch(Exception e) {
	    }
	}

	return cKey;
    }

}

/*
 * $Log: ProjectVisitorDBMS.java,v $
 * Revision 1.16  2005/05/13 20:34:11  ccochran
 * refactored to use Resource DAO
 *
 * Revision 1.15  2005/05/09 18:32:47  vchumako
 * Passed created CalendarKey directly to project
 *
 * Revision 1.14  2005/05/09 15:00:53  vchumako
 * Changed the way to load calendar by constructing CalendarKey first
 *
 * Revision 1.13  2005/05/09 04:29:43  vchumako
 * Set calendar name if its null
 *
 * Revision 1.12  2005/05/09 03:47:02  vchumako
 * Updated calendar methods
 *
 * Revision 1.11  2005/05/09 02:42:18  vchumako
 * Stabbed out calendar db visitor
 *
 * Revision 1.10  2005/05/08 23:29:21  rcoutinh
 * check for existence of space before substring of lastname and first name for resource
 *
 * Revision 1.9  2005/05/08 06:02:26  vchumako
 * Added load/save DBMS Assignments
 *
 * Revision 1.8  2005/05/02 23:46:38  vchumako
 * Implemented save/load resources.
 *
 * Revision 1.7  2005/05/02 21:35:14  vchumako
 * Fixed saving resource
 *
 * Revision 1.6  2005/05/02 18:05:48  vchumako
 * Using jdbc connection by defalt.
 *
 * Revision 1.5  2005/05/01 21:37:22  tlong2
 * removed AssignmentVisitor code
 *
 * Revision 1.3  2005/05/01 05:11:09  vchumako
 * Fixed DAO exception name, added resource to dbms, still need to fix jndi ro get back to plain jdbc
 *
 * Revision 1.2  2005/04/30 17:12:18  vchumako
 * Initial stub out for DBMS functionality
 *
 * 
 */
