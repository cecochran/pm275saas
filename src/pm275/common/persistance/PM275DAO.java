/***************************************************
 * File:        $RCSfile: PM275DAO.java,v $
 * @author      $Author: vchumako $,
 * @version     $Id: PM275DAO.java,v 1.3 2005/05/01 05:11:09 vchumako Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose: The PM275DAO Interface represents a generic means to
 * write PM275 data to either XML or RDBMS
 * </p>
 */
package pm275.common.persistance;

import pm275.common.PM275BaseKey;
import pm275.common.exception.DAOException;

public interface PM275DAO {
			
	public Object get(PM275BaseKey key);  
	
	public void set(Object o) throws DAOException;
	
	public void delete(PM275BaseKey k) throws DAOException;
		
	public String toString();	
	

}

/*
 * $Log: PM275DAO.java,v $
 * Revision 1.3  2005/05/01 05:11:09  vchumako
 * Fixed DAO exception name, added resource to dbms, still need to fix jndi ro get back to plain jdbc
 *
 * Revision 1.2  2005/04/30 23:17:00  ccochran
 * *** empty log message ***
 *
 * Revision 1.1  2005/04/30 22:05:22  ccochran
 * stub
 *
 * Revision 1.2  2005/04/30 17:12:18  ccochran
 * Initial stub 
 *
 * 
 */
