/*
 * File: $RCSfile: DBConnection.java,v $
 * @author $Author: dnathan $,
 * @version $Id: DBConnection.java,v 1.9 2005/05/15 17:54:36 dnathan Exp $
 * Assignment: CSCI E-275 Team 1 Project
 * <p>
 * Contents: This should be used to get a connection with mysql database.
 * 			  First time getConnection() should be supplied with the url.
 * </p>
 * <p>
 * Purpose: Establish a connection with MySQL database.
 * </p>
 */
package pm275.common.persistance;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;

import pm275.common.exception.PersistanceException;

/**
 * @author vc
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class DBConnection
{
    private static Logger mLog = Logger.getLogger(DBConnection.class);

    private static String lookupName;

    Connection con = null;

    Statement stmt = null;

    public DBConnection()
    {
        super();

    }

    /** Sets the default name for the lookup */
    public static void setLookupName(String name)
    {
        lookupName = name;
        mLog.debug("Setting Datasource lookupName to: " + name);
    }

    /*
     * public static Connection getConnection() throws PersistanceException {
     * return getConnection(lookupName); }
     */

    public void connect()
    {
        try
        {
            con = getConnection();
        } catch (Exception e)
        {
            mLog.error("Can't connect to Datasource: " + e.getMessage());
        }
    }

    public static Connection getConnection() throws PersistanceException
    {

        // USE JDBC and Properties file
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = DriverManager.getConnection(
                    PM275DBProperties.DATABASE, PM275DBProperties.USER,
                    PM275DBProperties.PASSWORD);

            return con;
        } catch (ClassNotFoundException cnfe)
        {
            mLog.error("Can't connect to Datasource: " + cnfe.getMessage());
            throw new PersistanceException("Cannot connect to Datasource: "
                    + cnfe.getMessage());
        } catch (IllegalAccessException ie)
        {
            mLog.error("Can't connect to Datasource: " + ie.getMessage());
            throw new PersistanceException("Cannot connect to Datasource: "
                    + ie.getMessage());
        } catch (InstantiationException ine)
        {
            mLog.error("Can't connect to Datasource: " + ine.getMessage());
            throw new PersistanceException("Cannot connect to Datasource: "
                    + ine.getMessage());
        } catch (SQLException se)
        {
            mLog.error("Can't get SQL connection: " + se.getMessage()
                    + "; state = " + se.getSQLState() + "; error = "
                    + se.getErrorCode());
            throw new PersistanceException("Can't get SQL connection: "
                    + se.getMessage());
        }
    }

    public static Connection getConnection(String name)
            throws PersistanceException
    {
        InitialContext jndi = null;
        try
        {
            jndi = new InitialContext();
            DataSource dataSource = (DataSource) jndi.lookup(name);
            jndi.close();

            return dataSource.getConnection();
        } catch (NamingException ne)
        {
            mLog.error("Can't connect to Datasource: " + ne.getMessage());
            throw new PersistanceException("Cannot connect to Datasource: "
                    + ne.getMessage());
        } catch (SQLException se)
        {
            mLog.error("Can't get SQL connection: " + se.getMessage()
                    + "; state = " + se.getSQLState() + "; error = "
                    + se.getErrorCode());
            throw new PersistanceException("Can't get SQL connection: "
                    + se.getMessage());
        } finally
        {
            try
            {
                jndi.close();
            } catch (NamingException ne)
            {
                mLog.error(ne.getMessage());
            }
        }
    }

    public boolean isClosed()
    {
        try
        {
            return con.isClosed();
        } catch (Exception se)
        {
            mLog.error("Can't get execute SQL statment: " + se.getMessage());

        }
        return false;
    }

    public ResultSet executeQuery(String sqlCmd) throws PersistanceException
    {

        ResultSet rs = null;

        try
        {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sqlCmd);

        } catch (SQLException se)
        {
            mLog.error("Can't get execute SQL statment: " + se.getMessage()
                    + "; state = " + se.getSQLState() + "; error = "
                    + se.getErrorCode());
            throw new PersistanceException("Can't get execute SQL statment: "
                    + se.getMessage());
        }
        return rs;
    }

    public void execute(String sqlCmd) throws PersistanceException
    {

        ResultSet rs = null;

        try
        {
            con = getConnection();
            stmt = con.createStatement();
            stmt.execute(sqlCmd);

        } catch (SQLException se)
        {
            mLog.error("Can't get execute SQL statment: " + se.getMessage()
                    + "; state = " + se.getSQLState() + "; error = "
                    + se.getErrorCode());
            throw new PersistanceException("Can't get execute SQL statment: "
                    + se.getMessage());
        } finally
        {

            // must clear connection upon execution
            try
            {
                stmt.close();
                con.close();
            } catch (SQLException e)
            {
                mLog.error(e);
            }
            stmt = null;
            con = null;
        }

    }

    public PreparedStatement getPreparedPreparedStatement(String sql)
    {

        PreparedStatement pstmt = null;
        try
        {

            if (con == null)
            {
                con = getConnection();
            }

            pstmt = con.prepareStatement(sql);

        } catch (Exception se)
        {
            mLog.error("Can't set prepared statment: " + se.getMessage());
        }
        return pstmt;
    }

    public void execute(PreparedStatement pstmt) throws PersistanceException
    {

        ResultSet rs = null;

        try
        {

            pstmt.executeUpdate();

        } catch (SQLException se)
        {
            mLog.error("Can't get execute SQL statment: " + se.getMessage()
                    + "; state = " + se.getSQLState() + "; error = "
                    + se.getErrorCode());
            throw new PersistanceException("Can't get execute SQL statment: "
                    + se.getMessage());
        } finally
        {

            // must clear connection upon execution
            try
            {
                pstmt.close();
                con.close();
            } catch (SQLException e)
            {
                mLog.error(e);
            }
            pstmt = null;
            con = null;
        }

    }

    public void close()
    {

        // must clear connection upon execution
        try
        {
            if (stmt != null)
            {
                stmt.close();

            }

            if (con != null)
            {
                con.close();
            }
        } catch (SQLException e)
        {

            mLog.error(e);
            mLog.error("Can't close connection: " + e.getMessage());

        }
        stmt = null;
        con = null;

    }

}

/*
 * $Log: DBConnection.java,v $
 * Revision 1.9  2005/05/15 17:54:36  dnathan
 * *** empty log message ***
 *
 * Revision 1.8  2005/05/15 17:51:11  dnathan
 * Moved out System.err.println's
 * Revision 1.7 2005/05/03 09:44:17 ccochran changed
 * close to avoid double close
 * 
 * Revision 1.6 2005/05/02 12:28:50 ccochran added prepared statement
 * 
 * Revision 1.5 2005/05/02 09:46:53 ccochran added default constructor
 * 
 * Revision 1.4 2005/05/01 08:50:01 ccochran added execute functionalty
 * 
 * Revision 1.3 2005/05/01 05:11:08 vchumako Fixed DAO exception name, added
 * resource to dbms, still need to fix jndi ro get back to plain jdbc
 * 
 * Revision 1.2 2005/04/30 17:12:18 vchumako Initial stub out for DBMS
 * functionality
 * 
 * Revision 1.1 2005/04/30 16:51:29 vchumako Added Visitor class for Database,
 * also added DBConnection to setup a connection with a database
 * 
 */