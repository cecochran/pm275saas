/***************************************************
 * File: 		$RCSfile: PersistanceService.java,v $ 
 * @author 		$Author: vchumako $, 
 * @version 	$Id: PersistanceService.java,v 1.20 2005/05/09 18:31:58 vchumako Exp $
 * Assignment:	CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * This service will help with reading and writing
 * to the XML and DB layers
 * OUT OF SCOPE V1
 * 
 * From Architecture Document
 * The first function of the Plan Repository is to store versions of project plans
 * created and updated by project management staff. The Repository maintains a
 * catalog of available project definitions, calendars, and resource pools used by the
 * projects and stores the data for these in a database.
 * It also provides an API for importing and exporting the data from and to XML.
 * Since there might be multiple ways to represent project plans in XML over time,
 * this API should allow multiple readers and writers of XML data.
 * Time Reporting data will be stored in the database as well.
 * 
 * </p>
 */
package pm275.common.persistance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;

import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.PersistanceException;
import pm275.common.exception.ResourceException;
import pm275.core.calendar.CalendarKey;
import pm275.core.calendar.CalendarService;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ProjectKey;
import pm275.core.project.ActivityKey;
import pm275.core.resource.Assignment;
import pm275.core.resource.AssignmentInterface;
import pm275.core.resource.Resource;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.ResourceKey;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourceService;

	
/**
 * <p></p>
 */
public class PersistanceService {
	
	private static final Logger log = Logger.getLogger(PersistanceException.class );
	private boolean resourcePoolLoaded = false;
	
/**
 * path to properties file relative to classpath
 * 
 */
	private static final String PROP_FILE = "resourcepool.txt";

	
/**
 * <p>Represents ...</p>
 */
    private static PersistanceService persistanceService;
    
    private static ProjectVisitorInterface dbVisitor = new ProjectVisitorDBMS();

	private PersistanceService()
	{
		super();
	}

/**
 * <p>Singlton instance for the service</p>
 * This is need to make sure there is only one
 * Project Service instance per VM
 *
 *
 * @return ProjectService
 */
    public static final PersistanceService getInstance() {
        //standard instance code to ensure there is only one
    	if (persistanceService == null ){
			persistanceService= new PersistanceService( );
    	}
         return persistanceService;
    }
    
    public void saveResourcesToDBMS() {
    	
    	ResourceService rs = ResourceService.getInstance();
    	try {
	    Collection resources = rs.listResources();
	    for(Iterator it = resources.iterator(); it.hasNext(); ) {
		Resource r = (Resource) it.next();
		r.acceptVisitor(dbVisitor);
	    }
    	} catch(ResourceException re) {
	    log.error("Can't get resource list!");
    	}
	
    }
    
    public void saveCalendarToDBMS(CalendarKey cKey) {
    	CalendarService cs = CalendarService.getInstance();
    	try {
			cs.getCalendar(cKey).acceptVisitor(dbVisitor);
		} catch (KeyNotFoundException e) {
			log.error("Can't get calendar " + cKey 
				  + ": " + e.getMessage());
		}
    }
    
    /* save all assignments related to this particular activity to DB */
    public void saveAssignmentsToDBMS(ActivityKey aKey) {
	try {
	    Collection allAssignments = ResourceService.getInstance().listAllAssignments();
	    for(Iterator it = allAssignments.iterator(); it.hasNext(); ) {
		AssignmentInterface asgn = (Assignment) it.next();
		if(aKey.equals(asgn.getActivityKey())) {
		    asgn.acceptVisitor(dbVisitor);
		}
	    }
	} catch(ResourceException re) {
	    log.warn("Can't get assignment list: " + re.getMessage());
	}
	
    }

    /* 
     * Returns a Map<String key, String _full_name_>
     * have to call ResourceService.createResource(key, _full_name_)
     */
    public Map loadResourcesFromDBMS() {
	return dbVisitor.readResources();
    }

    /** Finds all assignment by Activity Key 
     */
    public void loadAssignmentsFromDBMS(ActivityKey aKey, ProjectKey pKey) {
	dbVisitor.readAssignments(aKey, pKey);
    }

//    public void loadCalendarFromDBMS(CalendarKey cKey) {
    public CalendarKey loadCalendarFromDBMS(String cKey) {
	return dbVisitor.readCalendar(cKey);
    }

    public ProjectInterface loadProject() throws PersistanceException
    {
    	// load project
    	
    	
    	// load ResourcePool:
    	// 1) requirment: only 1 resource pool exists
    	// 2) requirment: done when the project is loaded
    	loadResourcePool();
    	
    	return null;
    }
    
    private void loadResourcePool() throws PersistanceException
    {
		try 
		{
			if(!resourcePoolLoaded)
			{
				ResourceService rs = ResourceService.getInstance();
				ResourcePool rp = rs.createResourcePool();
				InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(PROP_FILE);
				if(is == null)
				{    
					log.error("ERROR Cannot load properties file: " + PROP_FILE);
					throw new PersistanceException("Cannot load ResourcePool properties file: " + PROP_FILE);
				}
				
				// each row in the text file represents a Resource.
				// for each line in the text file: create + add to pool
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				String line = new String();
				ResourceInterface resource = null;
				for(int i = 0; (line = reader.readLine()) != null; i++)
				{
				    ResourceKey rKey = new ResourceKey("Rdef_" + i);
//				    try {
					if((resource = rs.getResource(rKey)) == null) {
					    resource = rs.createResource("Rdef_" + i, line);
					    rp.addResource(resource);
					} else {
					    resource.setName(line);
					}
//				    } catch(ResourceException re) {
					// skip existing resource
//				    }
				    resource = null;
				    line = null;
				}

				Map dbRes = loadResourcesFromDBMS();
				Iterator ir = dbRes.keySet().iterator();
				while(ir.hasNext()) {
				    try {
					String rKey = (String) ir.next();
					resource = rs.createResource(rKey,
								(String) dbRes.get(rKey));
					rp.addResource(resource);
				    } catch(ResourceException re) {
					// skip existing resource
				    }
				    resource = null;
				}
				
				// add loaded resource pool to resource service.
				rs.addResourcePool(rp);
				resourcePoolLoaded = true;
			}
		}
		catch (ResourceException e)
		{
			throw new PersistanceException("Resource Exception while loading ResourcePool: " + e);	
		}
		catch(IOException e)
		{
			throw new PersistanceException("IO exception while loading ResourcePool: " + e);	
		}
		
    }
    
}
/* 
* $Log: PersistanceService.java,v $
* Revision 1.20  2005/05/09 18:31:58  vchumako
* Passed created CalendarKey directly to project
*
* Revision 1.19  2005/05/09 16:32:28  vchumako
* Assigned id's to default resources manually.
*
* Revision 1.18  2005/05/09 15:00:39  vchumako
* Changed the way to load calendar by constructing CalendarKey first
*
* Revision 1.17  2005/05/09 03:46:49  vchumako
* Added load/save calendar dbms methods
*
* Revision 1.16  2005/05/09 02:42:18  vchumako
* Stabbed out calendar db visitor
*
* Revision 1.15  2005/05/08 06:01:56  vchumako
* Added load/save DBMS Assignments
*
* Revision 1.14  2005/05/02 23:45:53  vchumako
* Added DBMS methods: saveResourcesToDBMS(), loadResourcesFromDBMS().
*
* Revision 1.13  2005/05/02 19:38:03  bbuchana
* Added VCT functionality: Iteration 3
*
* Revision 1.12  2005/05/01 13:49:54  tlong2
* Added CVS footer
*
* 
*/
