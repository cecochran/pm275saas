/**
 * CSCIE 275 - Software Architecture and Engineering
 *
 * File: $RCSfile: ProjectVisitorInterface.java,v $
 * @author $Author: vchumako $,
 * @version $Revision: 1.11 $
 *
 * <p>For output to XML file</p>
 *
 * $Id: ProjectVisitorInterface.java,v 1.11 2005/05/09 18:33:05 vchumako Exp $
 */


package pm275.common.persistance;

import java.util.Map;

import pm275.core.calendar.CalendarInterface;
import pm275.core.calendar.CalendarKey;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.AssignmentInterface;
import pm275.core.project.ActivityInterface;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ActivityKey;
import pm275.core.project.ProjectKey;



public interface ProjectVisitorInterface {
  
  

    public abstract void visitCalendar(CalendarInterface c);
    public abstract void visitResource(ResourceInterface r);
    public abstract void visitActivity(ActivityInterface a); 
    public abstract void visitProject(ProjectInterface p);
    public abstract void visitAssignment(AssignmentInterface a);
  
  
  public abstract void writeProject();
  
  public abstract void readProject();

    public abstract Map readResources();
    public abstract void readAssignments(ActivityKey aKey, ProjectKey pKey);
//    public abstract void readCalendar(CalendarKey cKey);
    public abstract CalendarKey readCalendar(String cKey);
  
}

/**
 * $Log: ProjectVisitorInterface.java,v $
 * Revision 1.11  2005/05/09 18:33:05  vchumako
 * Passed created CalendarKey directly to project
 *
 * Revision 1.10  2005/05/09 15:00:59  vchumako
 * Changed the way to load calendar by constructing CalendarKey first
 *
 * Revision 1.9  2005/05/09 03:47:14  vchumako
 * Updated calendar methods
 *
 * Revision 1.8  2005/05/09 02:42:18  vchumako
 * Stabbed out calendar db visitor
 *
 * Revision 1.7  2005/05/08 06:02:39  vchumako
 * Added load/save DBMS Assignments
 *
 * Revision 1.6  2005/05/02 23:47:51  vchumako
 * Added dummy method to read resources from persistent storage.
 *
 * Revision 1.5  2005/05/01 21:37:32  tlong2
 * removed AssignmentVisitor code
 *
 * Revision 1.2  2005/04/30 16:51:29  vchumako
 * Added Visitor class for Database, also added DBConnection to setup a connection with a database
 *
 * Revision 1.1  2005/04/28 03:06:51  tlong2
 * Experimenting with vistor pattern
 *
 * Revision 1.0  2005/04/27 12:45:25  tlong2
 * Initial Check in
 */
