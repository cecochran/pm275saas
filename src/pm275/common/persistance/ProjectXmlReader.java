/*
 *  File:        $RCSfile: ProjectXmlReader.java,v $
 *  @author      $Author: tlong2 $,
 *  @version     $Id: ProjectXmlReader.java,v 1.14 2005/05/14 22:18:46 tlong2 Exp $
 *  Assignment:  CSCI E-275 Team 1 Project
 *  <p>
 *  Contents:
 *  </p>
 *  <p>
 *  Purpose:
 *  </p>
 */

package pm275.common.persistance;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.lang.Double;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import pm275.core.calendar.CalendarInterface;
import pm275.core.calendar.CalendarKey;
import pm275.core.calendar.CalendarService;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourceService;
import pm275.core.resource.ResourceKey;
import pm275.core.project.ProjectService;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ActivityInterface;
import pm275.core.project.ActivityKey;
import pm275.common.PM275BaseKey;
import pm275.common.exception.PersistanceException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.DuplicateKeyException;
import pm275.common.exception.ResourceException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.CycleDependencyException;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.JDOMException;


/**
 *  This class reads and parses an XML document to generate a project and all
 *  its components (activities, resources, calendars).
 */
public class ProjectXmlReader
{
    //
    //  Data members.
    //

    private final File inputFile;

    // Logger.
    private static final Logger log = Logger.getLogger( ProjectXmlReader.class );

    // Date format.
    private static final DateFormat DATE_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

    // For internal use.
    private ExternalToInternalKeyMap activitiesKeyMap;
    private ExternalToInternalKeyMap resourcesKeyMap;
    private ExternalToInternalKeyMap calendarsKeyMap;


    //
    //  Methods.
    //

    /**
     *  Creates a reader that loads and parses the specified XML file.
     *  @param file File containing the XML document.
     */
    public ProjectXmlReader(File file)
    {
        this.inputFile = file;

    }

    /**
     *  Reads and parses the XML document contained in this reader's file.
     *  @return Project created from the XML document.
     *  @throws PersistanceException If the project cannot be read and created from the XML document.
     */
    public ProjectInterface readProject()
            throws  PersistanceException, InternalActivityException,
                    CycleDependencyException, ResourceException

    {
        prepareToRead();

        Document document = null;
        ProjectInterface project = null;
        try
        {
            // Read the XML file into DOM.
            document = new SAXBuilder().build(inputFile);
            Element documentRoot = document.getRootElement();

            // Handle <calendars>
            // Make a pass of <calendars> element to create the
            // calendars and add them to the project.

            CalendarService calendarService = CalendarService.getInstance();

            Element calendarsElement = documentRoot.getChild("calendars");
            if (calendarsElement != null)
            {
                // Handle <calendar>
                for (Iterator i = calendarsElement.getChildren("calendar").iterator(); i.hasNext(); )
                {
                    Element calendarElement = (Element) i.next();
                    CalendarInterface calendar = calendarFromElement(calendarElement);
                    calendarsKeyMap.mapKeys(
                            calendarElement.getAttributeValue("calendarKey"),
                            calendar.getKey());
                }
            }

            /////////////////

            // Parse the <project> element.
            project = projectFromElement(documentRoot.getChild("project"));

            // Assign project's calendar.
            String projectCalendarKeyStr =
                    documentRoot.getChild("project")
                            .getChild("projectCalendar")
                            .getChild("calendarRef")
                            .getAttributeValue("calendarKey");
            CalendarKey projectCalendarKey =
                    (CalendarKey) calendarsKeyMap.getKey(projectCalendarKeyStr);
            CalendarInterface projectCalendar =
                    calendarService.getCalendar(projectCalendarKey);
            project.setCalendarKey(projectCalendar.getKey());

            // TODO: Calendar names are not persisted in XML (due to limitation
            // of the schema), so provide a name from the project.
            projectCalendar.setName(project.getName() + " Calendar");

            ////////////////////////////////////////
            // Handle <resourcePool>
            // Make a pass of <resourcePool> element to create the
            // resources and add them to the project.

            // Create Resource Pool for project
            ResourceService resourceService = ResourceService.getInstance();
            ResourcePool resourcePool = resourceService.createResourcePool();

            Element poolElement = documentRoot.getChild("resourcePool");
            if (poolElement != null)
            {
                // Handle <resource>
                for (Iterator i = poolElement.getChildren("resource").iterator(); i.hasNext(); )
                {
                    Element resourceElement = (Element) i.next();
                    ResourceInterface resource =
                        resourceFromElement(resourceElement);
                    resourcePool.addResource(resource);
                    resourcesKeyMap.mapKeys(
                            resourceElement.getAttributeValue("resourceKey"),
                            resource.getKey());

                    // Handle <resourceCalendar>
                    Element resourceCalendarElement = resourceElement.getChild("resourceCalendar");
                    Element resourceCalendarRefElement = resourceCalendarElement.getChild("calendarRef");
                    CalendarKey refCalKey = new CalendarKey(resourceCalendarRefElement.getText());
                    resource.setCalendarKey(refCalKey);

                }
                resourceService.addResourcePool(resourcePool);
                project.setResourcePoolkey(resourcePool.getResourcePoolKey());
            }
            /////////////////


            // Make first pass of <activities> element to create the activities
            // and add them to the project.
            Element activitiesElement = documentRoot.getChild("activities");
            if (activitiesElement != null)
            {
                // Handle <activity>
                for (Iterator i = activitiesElement.getChildren("activity").iterator(); i.hasNext(); )
                {
                    Element activityElement = (Element) i.next();
                    ActivityInterface activity =
                            activityFromElement(activityElement, project);
                    activitiesKeyMap.mapKeys(
                            activityElement.getAttributeValue("activityKey"),
                            activity.getActivityKey());
                }
            }

            // Make second pass thru activities to establish dependencies
            // and resources
            activitiesElement = documentRoot.getChild("activities");
            if (activitiesElement != null)
            {
                for (Iterator i = activitiesElement.getChildren("activity").iterator(); i.hasNext(); )
                {
                    Element activityElement = (Element) i.next();
                    ActivityKey actSuccKey = (ActivityKey) activitiesKeyMap.getKey(activityElement.getAttributeValue("activityKey"));

                    // Handle <dependencies>
                    Element dependenciesElement = activityElement.getChild("dependencies");
                    Iterator it = dependenciesElement.getChildren("activityRef").iterator();
                    while (it.hasNext())
                    {
                        Element predecessorElement = (Element) it.next();
                        // Handle <activityRef>
                        ActivityKey actPredKey = (ActivityKey) activitiesKeyMap.getKey(predecessorElement.getAttributeValue("activityKey"));

                        project.associateActivities(actPredKey,actSuccKey);
                    }

                    // Handle <assignments>
                    Element assignmentsElement = activityElement.getChild("assignments");
                    it = assignmentsElement.getChildren("assignment").iterator();
                    while (it.hasNext())
                    {
                        Element assignmentElement = (Element) it.next();
                        // Handle <assignment>

                        ResourceKey resourceKey = (ResourceKey) resourcesKeyMap.getKey(assignmentElement.getAttributeValue("resourceKey"));

                        // Handle <effort>
                        Element effortElement = assignmentElement.getChild("effort");

                        // Assign the resource
                        resourceService.assign(resourceKey,actSuccKey,project.getProjectKey(),Double.parseDouble(effortElement.getText()));
                    }

                }
            }
            project.calculateCriticalPath();
        }
        catch (IOException ex)
        {
            String message = "Cannot read the input file " + inputFile.getAbsolutePath();
            log.error(message);
            throw new PersistanceException(message, ex);
        }
        catch (JDOMException ex)
        {
            String message;
            if (document == null)
            {
                message = "Cannot parse the file " + inputFile.getAbsolutePath();
            }
            else
            {
                message = "XML file contains error: " + ex.getMessage();
            }
            log.error(message);
            throw new PersistanceException(message, ex);
        }
        catch (InternalProjectException ex)
        {
            String message;
            if (project == null)
            {
                message = "Cannot create project from XML file.";
            }
            else
            {
                message = "Project has errors.";
            }
            log.error(message);
            throw new PersistanceException(message, ex);
        }
        catch (KeyNotFoundException ex)
        {
            String message = "Attempted to use invalid key.";
            log.error(message);
            throw new PersistanceException(message, ex);
        }
        catch (DuplicateKeyException ex)
        {
            String message = "Attempted to use duplicate key.";
            log.error(message);
            throw new PersistanceException(message, ex);
        }
        catch (ResourceException ex)
        {
            String message = "Error creating a resource or assigning one to an activity.";
            log.error(message);
            throw new PersistanceException(message, ex);
        }
        return project;
    }

    /**
     *  Creates and returns a resource from the specified <resource> element.
     *  @param element The <resource> element.
     *  @return Resource represented by the element.
     *  @throws ResourceException If the resource cannot be created.
     */
    private ResourceInterface resourceFromElement(Element element)
            throws ResourceException
    {
        // We must have a <resource> element.
        if ((element == null) || (! element.getName().equals("resource")))
        {
            String message = "ProjectXmlReader.resourceFromElement() requires <resource> element.";
            log.error(message);
            throw new IllegalArgumentException(message);
        }

        String first = element.getChildText("first");
        String last = element.getChildText("last");
        String resKey = element.getAttributeValue("resourceKey");
        return ResourceService.getInstance().createResource(resKey, first + " " + last);
    }

    /**
     *  Creates and returns a activity from the specified <activity> element.
     *  @param element The <activity> element.
     *  @return Activity represented by the element.
     *  @throws KeyNotFoundException If an attempt was made to use a invalid key.
     *  @throws InternalProjectException If the project has errors.
     *  @throws DuplicateKeyException If an attempt was made to use a duplicate key.
     */
    private ActivityInterface activityFromElement(Element element,ProjectInterface project)
            throws KeyNotFoundException, InternalProjectException, DuplicateKeyException
    {
        // We must have a <activity> element.
        if ((element == null) || (! element.getName().equals("activity")))
        {
            String message = "ProjectXmlReader.activityFromElement() requires <activity> element.";
            log.error(message);
            throw new IllegalArgumentException(message);
        }

        // Get the activity's name and create the new project.
        String name = element.getChild("name").getText();
        ActivityInterface activity =
                ProjectService.getInstance().addActivity(
                        project.getProjectKey(), name);

        // Activity's start date.
        String startDateStr = element.getChild("startDate").getText();
        Date startDate = null;
        try
        {
            startDate = DATE_FORMAT.parse(startDateStr);
        }
        catch (ParseException ex)
        {
            log.warn("Invalid date format: " + startDateStr +
                     ": " + ex.getMessage());
            startDate = project.getStartDate();
        }
        activity.setStartDate(startDate);

        // Is start date fixed?
        String startDateFixedStr =
                element.getChild("startDate").getAttributeValue("fixed");
        boolean startDateFixed = new Boolean(startDateFixedStr).booleanValue();
        activity.setIsFixedStartDate(startDateFixed);

        // Duration.
        String durationStr = element.getChild("duration").getText();
        int duration = Integer.parseInt(durationStr);
        activity.setDuration(duration);

        // Is duration fixed?
        String durationFixedStr =
                element.getChild("duration").getAttributeValue("fixed");
        boolean durationFixed = new Boolean(durationFixedStr).booleanValue();
        activity.setIsFixedDuration(durationFixed);

        return activity;
    }

    /**
     *  Creates and returns a calendar from the specified <calendar> element.
     *  @param element The <calendar> element.
     *  @return Calendar represented by the element.
     */
    private CalendarInterface calendarFromElement(Element element)
    {
        // We must have a <calendar> element.
        if ((element == null) || (! element.getName().equals("calendar")))
        {
            String message = "ProjectXmlReader.calendarFromElement() requires <calendar> element.";
            log.error(message);
            throw new IllegalArgumentException(message);
        }

        String calKeyStr = element.getAttributeValue("calendarKey");
        CalendarInterface calIfc = CalendarService.getInstance().addCalendar(new CalendarKey());
        String daysPerWeekStr = element.getChildText("daysInWeek");
        String hoursInDayStr = element.getChildText("hoursInDay");
        int daysPerWeek = 5;
        int hoursInDay = 8;
        try
        {
            daysPerWeek = Integer.parseInt(daysPerWeekStr);
            hoursInDay = Integer.parseInt(hoursInDayStr);
        }catch(NumberFormatException ne){
            daysPerWeek = 5;
            hoursInDay = 8;
        }
        calIfc.setDaysPerWeek(daysPerWeek);
        calIfc.setHoursPerDay(hoursInDay);
        return calIfc;
    }

    /**
     *  Creates and returns a project from the specified <project> element.
     *  @param element The <project> element.
     *  @return Project represented by the element.
     *  @throws InternalProjectException If the project has errors.
     */
    private ProjectInterface projectFromElement(Element element)
            throws InternalProjectException, ResourceException
    {
        // We must have a <project> element.
        if ((element == null) || (! element.getName().equals("project")))
        {
            String message = "ProjectXmlReader.projectFromElement() requires <project> element.";
            log.error(message);
            throw new IllegalArgumentException(message);
        }

        // Get the name and start date.
        String name = element.getChild("name").getText();
        String startDateStr = element.getChild("startDate").getText();
        Date startDate = null;
        try
        {
            startDate = DATE_FORMAT.parse(startDateStr);
        }
        catch (ParseException ex)
        {
            log.warn("Invalid date format: " + startDateStr +
                     ": " + ex.getMessage());
            startDate = new Date();
        }

        // Create project and set start date.
        ProjectInterface project = ProjectService.getInstance().addProject(name);
        project.setStartDate(startDate);

        return project;
    }

    /**
     *  Prepares to read the XML document by initializing variables for internal
     *  use.
     */
    private final void prepareToRead()
    {
        // Initializes the object that maps external activity keys to internal
        // keys.
        if (activitiesKeyMap == null)
        {
            activitiesKeyMap = new ExternalToInternalKeyMap();
        }
        else
        {
            activitiesKeyMap.clear();
        }

        // Initializes the object that maps external resource keys to internal
        // keys.
        if (resourcesKeyMap == null)
        {
            resourcesKeyMap = new ExternalToInternalKeyMap();
        }
        else
        {
            resourcesKeyMap.clear();
        }

        // Initializes the object that maps external calendar keys to internal
        // keys.
        if (calendarsKeyMap == null)
        {
            calendarsKeyMap = new ExternalToInternalKeyMap();
        }
        else
        {
            calendarsKeyMap.clear();
        }
    }


    ////////////////////////////////////////////////////////////////////////////
    //
    //  Inner classes.

    /**
     *  This helper class maps external keys from the XML document to the keys
     *  of internal objects.
     */
    private static final class ExternalToInternalKeyMap
    {
        private final Map keyMap;

        protected ExternalToInternalKeyMap()
        {
            keyMap = new HashMap();
        }

        protected void mapKeys(String externalKey, PM275BaseKey internalKey)
        {
            keyMap.put(externalKey, internalKey);
        }

        protected PM275BaseKey getKey(String externalKey)
        {
            return (PM275BaseKey) keyMap.get(externalKey);
        }

        protected void clear()
        {
            keyMap.clear();
        }
    }
}

/*
 * $Log: ProjectXmlReader.java,v $
 * Revision 1.14  2005/05/14 22:18:46  tlong2
 * removed unused imports
 *
 * Revision 1.13  2005/05/09 16:45:19  vchumako
 * Assigned proper resourceKey
 *
 * Revision 1.12  2005/05/09 02:50:18  achou
 * Removed erroneous ProjectVisitorToXML references.
 *
 * Revision 1.11  2005/05/09 00:02:07  achou
 * Removed commented debug code.
 *
 * Revision 1.10  2005/05/09 00:01:17  achou
 * Modified calendarFromElement() to use calendar service to create new calendar with randomly generated key.
 * Implemented external key to internal key map for calendars.
 * Moved readProject() to top of class.  Added name for calendar.
 *
 * Revision 1.9  2005/05/08 19:53:18  achou
 * Changed resource pool code to restrict its scope and to create it only once.
 * Modified resourceFromElement() to create resource using random key and to throw its exception.
 * Implemented external key to internal key map using inner class.
 *
 * Revision 1.8  2005/05/08 18:44:43  tlong2
 * removed //TODO comments
 *
 * Revision 1.7  2005/05/08 18:44:00  tlong2
 * Calendar read is done. I think it is all done
 *
 * Revision 1.6  2005/05/08 16:50:21  tlong2
 * added calcualateCriticalPath() to end of readProject
 *
 * Revision 1.5  2005/05/08 16:33:00  tlong2
 * Activity resources now loading
 *
 * Revision 1.4  2005/05/08 10:11:48  tlong2
 * Added Support for reading resource, but still not working see TODOs
 *
 * Revision 1.3  2005/05/08 08:48:42  tlong2
 * Got activity dependencies (successors & predecessors) working
 *
 * Revision 1.2  2005/05/07 05:17:01  rcoutinh
 * Added read methods for calendar and resource
 *
 * Revision 1.1  2005/05/06 23:54:14  achou
 * Initial check-in of the XML reader.
 *
 */
