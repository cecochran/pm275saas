/*
 * File: $RCSfile: PM275DBSchema.java,v $
 * @author $Author: rcoutinh $,
 * @version $Id: PM275DBSchema.java,v 1.10 2005/05/14 23:06:30 rcoutinh Exp $
 * Assignment: CSCI E-275 Team 1 Project
 * <p>
 * Contents: Keeps name of the tables and their fields
 * </p>
 * <p>
 * Purpose: Marshalling class should implement this schema
 * </p>
 */
package pm275.common.persistance;

/**
 * @author vc
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface PM275DBSchema {
	
    /** table names */
    public static final String PROJECT_TABLE = "project";
    public static final String ACTIVITY_TABLE = "activity";
    public static final String RESOURCE_TABLE = "resource";
    public static final String ASSIGNMENT_TABLE = "assignment";
    public static final String CALENDAR_TABLE = "calendar";
    public static final String ACTIVITY_REF_TABLE = "activityref";
    public static final String HOLIDAY_TABLE = "holidays";
    
    /** resource table columns */
    public static final String RESOURCE_ID = "ID";
    public static final String RESOURCE_FIRST = "first";
    public static final String RESOURCE_LAST = "last";

    /** assignment table columns */
    public static final String ASSIGNMENT_ID = "ID";
    public static final String ASSIGNMENT_RKEY = "resourceKey";
    public static final String ASSIGNMENT_AKEY = "activityKey";
    public static final String ASSIGNMENT_EFFORT = "effort";

    /** calendar table columns */
    public static final String CALENDAR_ID = "ID";
    public static final String CALENDAR_PKEY = "projectID";
    public static final String CALENDAR_NAME = "calendarName";
    public static final String CALENDAR_TYPE = "calendarType";
    public static final String CALENDAR_DWEEK = "daysInWeek";
    public static final String CALENDAR_HOURS = "hoursInDay";
    public static final String CALENDAR_WORKDAYS = "workWeek";
	
    /* prepared statements */
    /** calendar related statements */
    public static final String SELECT_CALENDAR_WITH_ID = 
    "SELECT * FROM " + CALENDAR_TABLE + " WHERE " + CALENDAR_ID + " = ?;";
	
	/** calendar related statements */
    public static final String SELECT_CALENDAR_WITH_PROJID = 
    "SELECT  calendarkey FROM " + PROJECT_TABLE + " WHERE  projectkey " + " = ?;";

    public static final String UPDATE_CALENDAR = 
    "UPDATE " + CALENDAR_TABLE + " SET "
    + CALENDAR_NAME + " =?, " + CALENDAR_TYPE + " =?, " 
    + CALENDAR_DWEEK + " =?, " + CALENDAR_HOURS + " =? "
    + " WHERE " + CALENDAR_ID + " = ?;";

    public static final String INSERT_CALENDAR = 
    "INSERT INTO " + CALENDAR_TABLE + " VALUES(?, ?, ?, ?, ?, ?);";

    /** resource related statements */
    public static final String INSERT_RESOURCE = 
    "INSERT INTO resource VALUES( ?, ?, ?);";

    public static final String UPDATE_RESOURCE = 
    "UPDATE resource SET " + RESOURCE_FIRST + " = ?, " + RESOURCE_LAST + " = ? "
    + "WHERE " + RESOURCE_ID + " = ?;";

    public static final String SELECT_RESOURCE_WITH_ID = 
    "SELECT * FROM resource WHERE " + RESOURCE_ID + " = ?;";

    public static final String SELECT_RESOURCES = "SELECT * FROM resource;";

    /** assignment related statements */
    public static final String SELECT_ASSIGNMENT_WITH_ID =
      "SELECT * FROM assignment WHERE " + ASSIGNMENT_ID + " = ?;";

     public static final String SELECT_ASSIGNMENT_BY_AKEY =
      "SELECT * FROM assignment WHERE " + ASSIGNMENT_AKEY + " = ?;";

   public static final String INSERT_ASSIGNMENT = 
    "INSERT INTO assignment VALUES( ?, ?, ?, ?);";

    public static final String UPDATE_ASSIGNMENT = 
    "UPDATE assignment SET " + ASSIGNMENT_RKEY + " = ?, " + ASSIGNMENT_RKEY + " = ?, "
    + ASSIGNMENT_EFFORT + " = ? WHERE " + ASSIGNMENT_ID + " = ?;";
    
    
    
}
/* 
 * $Log: PM275DBSchema.java,v $
 * Revision 1.10  2005/05/14 23:06:30  rcoutinh
 * Added query to look for calendar based on project id
 *
 * Revision 1.9  2005/05/09 03:46:21  vchumako
 * Updated calendar prepared stametements
 *
 * Revision 1.8  2005/05/08 06:00:58  vchumako
 * Added Assignment table column names and prepared statements
 *
 * Revision 1.7  2005/05/04 06:22:27  ccochran
 * lowercase r in activity ref
 *
 * Revision 1.6  2005/05/02 23:44:03  vchumako
 * Added SELECT and UPDATE statements.
 *
 * Revision 1.5  2005/05/02 21:34:25  vchumako
 * Added update statement
 *
 * Revision 1.4  2005/05/02 09:01:02  ccochran
 * removed connectinfo
 *
 * Revision 1.3  2005/05/02 08:53:35  ccochran
 * added dbinfo
 *
 * Revision 1.2  2005/05/01 05:11:09  vchumako
 * Fixed DAO exception name, added resource to dbms, still need to fix jndi ro get back to plain jdbc
 *
 * Revision 1.1  2005/04/30 17:12:18  vchumako
 * Initial stub out for DBMS functionality
 *
 *
 */
