/***************************************************
 * File:        $RCSfile: PM275DAOtoXML.java,v $
 * @author      $Author: tlong2 $,
 * @version     $Id: PM275DAOtoXML.java,v 1.2 2005/05/01 17:54:10 tlong2 Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose: The PM275DAOtoXML base class used to represents a generic means to
 * write PM275 data to XML 
 * </p>
 */
package pm275.common.persistance;

import pm275.common.PM275BaseKey;
import pm275.common.exception.DAOException;



public class PM275DAOtoXML implements PM275DAO {
 
    
    public PM275DAOtoXML(){
      super();
    }
    
    
	public Object get(PM275BaseKey key){
		return null;
	}
	
	public void set(Object o) throws DAOException{
		
	}
	
	public void delete(PM275BaseKey k) throws DAOException{
		
	}
		
	public String toString(){
		return null;
	}	
	
	
}
/* 
* $Log: PM275DAOtoXML.java,v $
* Revision 1.2  2005/05/01 17:54:10  tlong2
* Added CVS footer
*
*
*
*/
