/***************************************************
 * File:        $RCSfile: PM275DAOtoDB.java,v $
 * @author      $Author: tlong2 $,
 * @version     $Id: PM275DAOtoDB.java,v 1.4 2005/05/01 13:51:29 tlong2 Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose: The PM275DAO Interface represents a generic means to
 * write PM275 data to either XML or RDBMS
 * </p>
 */
package pm275.common.persistance;

import pm275.common.PM275BaseKey;
import pm275.common.exception.DAOException;
import pm275.common.persistance.DBConnection;

/**
 * @author Chris
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PM275DAOtoDB implements PM275DAO {
    protected DBConnection db;
    
    public PM275DAOtoDB(){
      super();
    }
    
    
	public Object get(PM275BaseKey key){
		return null;
	}
	
	public void set(Object o) throws DAOException {
		
	}
	
	public void delete(PM275BaseKey k) throws DAOException {
		
	}
		
	public String toString(){
		return null;
	}	
	
	
}


/* 
* $Log: PM275DAOtoDB.java,v $
* Revision 1.4  2005/05/01 13:51:29  tlong2
* Added CVS footer
*
*
*
*/