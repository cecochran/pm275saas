/*
 * File:        $RCSfile: PM275DBProperties.java,v $
 * @author      $Author: ccochran $,
 * @version     $Id: PM275DBProperties.java,v 1.7 2005/05/14 14:10:43 ccochran Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 *
 * Connection specific settings, makes sense to move them away into config file.
 */
package pm275.common.persistance;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * @author Chris
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public final class PM275DBProperties {
    private static final Logger log = Logger.getLogger(PM275DBProperties.class );
    
    /* mysql properties file name in the conf directory */
    private static final String MYDB_FILE = "mysql.properties";

    /* driver/db property name */
    private static final String MYDB_NAME = "mysql.db";

    /* user name property */
    private static final String MYDB_USER = "mysql.user";

    /* password property */
    private static final String MYDB_PASSWORD = "mysql.password";

    /* Database Conenction Info*/
    public static String DATABASE = "jdbc:mysql:///team1";
    public static String USER  = "root";
    
    // password is empty since it doesn't require to change from default
    public static String PASSWORD = "harvard";
    /*
    static {
	    try{
		Method method = Thread.class.getMethod( "getContextClassLoader" , (Class[]) null );
		ClassLoader loader = (ClassLoader) method.invoke( Thread.currentThread() , (Object[]) null );
		if( loader.getResource( MYDB_FILE ) != null )
		{
		    String url = loader.getResource( MYDB_FILE ).getFile();
		    Properties p = new Properties();
		    p.load( new FileInputStream( new File(url) ) );
		    DATABASE = p.getProperty(MYDB_NAME);
		    USER = p.getProperty(MYDB_USER);
		    PASSWORD = p.getProperty(MYDB_PASSWORD);
		    log.debug("MySQL settings: DB_NAME = " + DATABASE + "; User = " + USER + "; Password = " + PASSWORD);
		} else {
		    log.error("Can't read " + MYDB_FILE + "!");
		}
	    } catch(Exception e) {
		log.error("Can't load mysql settings: " + e.getMessage());
	    }
    }
    */
    
}

/*
 * $Log: PM275DBProperties.java,v $
 * Revision 1.7  2005/05/14 14:10:43  ccochran
 * reset to correct version
 *
 * Revision 1.5  2005/05/08 06:00:09  vchumako
 * Read MySQL settings from conf/mysql.properties file
 *
 * Revision 1.4  2005/05/02 23:43:04  vchumako
 * Made password empty for now, so people don't have to change mysql privileges
 * at this point
 *
 *
 */
