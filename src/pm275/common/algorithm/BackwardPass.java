/*
 * File:        $RCSfile: BackwardPass.java,v $
 * @author      $Author: achou $,
 * @version     $Id: BackwardPass.java,v 1.8 2005/05/09 16:56:39 achou Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * Backward Iterator for Critical Path algorithm
 *
 *
 *  <p>
 */

package pm275.common.algorithm;

import pm275.core.project.Activity;

import java.util.List;
import java.util.Iterator;
import java.util.Vector;

import org.apache.log4j.Logger;
import pm275.common.exception.CriticalPathException;

public class BackwardPass implements ProjectIterator
{

    private List todoActivities;
    private List doneActivities;
    private int todo = 0;
    private int done = 0;
    private static final Logger log = Logger.getLogger(ForwardPass.class);
    private static final String ATTR = "ForwardPass";

    public BackwardPass(List listOfActivities)
    {
        this.todoActivities = listOfActivities;
        this.reset();
    }

    public boolean hasNext() throws CriticalPathException
    {
        boolean returnValue = ( todo != 0 );
        if( this.todo == 1 )
        {
            Activity a = (Activity) this.todoActivities.get(0);
            returnValue = (! a.isEndMilestone());
        }
        return returnValue;
    }

     public synchronized Activity next() throws CriticalPathException
     {
         int position = 0;
         while( position < this.todo )
         {
             Activity current = (Activity) this.todoActivities.get(position);
             boolean condition1 = false;
             boolean condition2 = true;

             // make sure that current's flag
             condition1 = current.getLateStartFlag();

             // check preds of current
             Iterator succs = current.getSuccessors().iterator();
             while( succs.hasNext() )
             {
                 Activity succ = (Activity) succs.next();
                 condition2 &= succ.getLateStartFlag();
             }

             position += 1;

             if( condition1 || current.isEndMilestone() )
             {
                 this.doneActivities.add(current);
                 this.done++;
                 this.todoActivities.remove(current);
                 this.todo--;
                 condition2 = false;
                 // important!
                 position = 0;
             }

             if( condition2 )
             {
                 this.doneActivities.add(current);
                 this.done++;
                 this.todoActivities.remove(current);
                 this.todo--;
                 return current;
             }
         }

         throw new CriticalPathException( "Could not get next" );
     }

     public void reset()
     {
         if( doneActivities != null )
         {
             Iterator resetIterator = doneActivities.iterator();

             while( resetIterator.hasNext() )
             {
                 this.todoActivities.add( resetIterator.next() );
             }
         }

         todo = this.todoActivities.size();
         done = 0;
         doneActivities = new Vector();
     }
}

/*
 * $Log: BackwardPass.java,v $
 * Revision 1.8  2005/05/09 16:56:39  achou
 * Fixed forward/backward pass logic that sometimes threw exception depending on the order in which the activities were processed.
 *
 */
