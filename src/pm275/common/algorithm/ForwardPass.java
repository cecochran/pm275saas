/*
 * File:        $RCSfile: ForwardPass.java,v $
 * @author      $Author: achou $,
 * @version     $Id: ForwardPass.java,v 1.17 2005/05/09 16:56:39 achou Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * Forward Iterator for Critical Path algorithm
 *
 *
 *  <p>
 */

package pm275.common.algorithm;

import pm275.core.project.Activity;

import java.util.List;
import java.util.Iterator;
import java.util.Vector;

import org.apache.log4j.Logger;
import pm275.common.exception.CriticalPathException;

public class ForwardPass implements ProjectIterator
{

    private List todoActivities;
    private List doneActivities;
    private int todo = 0;
    private int done = 0;
    private static final Logger log = Logger.getLogger(ForwardPass.class);
    private static final String ATTR = "ForwardPass";

    public ForwardPass(List listOfActivities)
    {
        this.todoActivities = listOfActivities;
        this.reset();
    }

    public boolean hasNext() throws CriticalPathException
    {
        boolean returnValue = ( todo != 0 );
        if( this.todo == 1 )
        {
            Activity a = (Activity) this.todoActivities.get(0);
            returnValue = (! a.isStartMilestone());
        }
        return returnValue;
    }

     public synchronized Activity next() throws CriticalPathException
     {
         int position = 0;
         while( position < this.todo )
         {
             Activity current = (Activity) this.todoActivities.get(position);
             boolean condition1 = false;
             boolean condition2 = true;

             // make sure that current's flag
             condition1 = current.getEarlyFinishFlag();

             // check preds of current
             Iterator preds = current.getPredecessors().iterator();
             while( preds.hasNext() )
             {
                 Activity pred = (Activity) preds.next();
                 condition2 &= pred.getEarlyFinishFlag();
             }

             position += 1;

             if( condition1 || current.isStartMilestone())
             {
                 this.doneActivities.add(current);
                 this.done++;
                 this.todoActivities.remove(current);
                 this.todo--;
                 condition2 = false;
                 // important!
                 position = 0;
             }

             if( condition2 )
             {
                 this.doneActivities.add(current);
                 this.done++;
                 this.todoActivities.remove(current);
                 this.todo--;
                 return current;
             }
         }

         throw new CriticalPathException( "Could not get next" );
     }

     public void reset()
     {
         if( doneActivities != null )
         {
             Iterator resetIterator = doneActivities.iterator();

             while( resetIterator.hasNext() )
             {
                 this.todoActivities.add( resetIterator.next() );
             }
         }

         todo = this.todoActivities.size();
         done = 0;
         doneActivities = new Vector();
     }
}

/*
 * $Log: ForwardPass.java,v $
 * Revision 1.17  2005/05/09 16:56:39  achou
 * Fixed forward/backward pass logic that sometimes threw exception depending on the order in which the activities were processed.
 *
 * Revision 1.16  2005/04/27 00:55:50  tlong2
 * Readded the headers to the file
 *
 * Revision 1.15  2005/04/24 20:10:53  dnathan
 * Now working as specified.
 *
 * Revision 1.14  2005/04/19 20:07:32  achou
 * Fixed bug in next() and added more info to exception message.
 *
 * Revision 1.13  2005/04/19 18:50:34  ccochran
 * updated comments
 *
 * Revision 1.12  2005/04/19 18:45:01  ccochran
 * reset
 *
 * Revision 1.11  2005/04/19 18:41:58  ccochran
 * Imp Alex Idea
 *
 * Revision 1.10  2005/04/19 04:19:00  vchumako
 * Shouldn't reset to the beginning of the list in hasNext() method.
 *
 * Revision 1.9  2005/04/18 21:32:56  tlong2
 * Critical Path working!
 *
 * Revision 1.8  2005/04/16 19:21:10  achou
 * Converted enumerations for activity predecessors/successors to collections.
 *
 * Revision 1.7  2005/04/13 04:36:51  tlong2
 * Added comments
 *
 * Revision 1.6  2005/04/13 04:23:59  tlong2
 * Completed Main CriticalPath Algorithm code
 *
 * Revision 1.5  2005/04/13 00:08:36  tlong2
 * Critical Path algorithm updates
 *
 * Revision 1.4  2005/04/12 03:41:20  dnathan
 * Removed unneeded imports.
 *
 * Revision 1.3  2005/04/11 20:24:33  ccochran
 * added log 4j, made private
 *
 * Revision 1.2  2005/04/10 13:22:31  tlong2
 * Updates for Critical Path algorithm implementation
 *
 * Revision 1.1  2005/04/10 04:21:18  tlong2
 * adding initial Critical Path code
 *
 *
 */
