/***************************************************
 * File:        $RCSfile: CycleDependency.java,v $
 * @author      $Author: umangkus $,
 * @version     $Id: CycleDependency.java,v 1.24 2005/04/18 06:22:07 umangkus Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents: CycleDependency class
 * </p>
 * <p>
 * Purpose: Perform algorithm to check if given project has circular dependency(s) among activities. This algorithm only supports one project, does not support activities association from multiple projects.
 * </p>
 */

package pm275.common.algorithm;

import pm275.core.project.ActivityInterface;
import pm275.core.project.Activity;
import pm275.core.project.ActivityKey;
import pm275.core.project.ProjectInterface;
import pm275.common.exception.CycleDependencyException;
import pm275.common.exception.InternalCycleException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.InternalActivityException;

import java.util.*; // Vector, List, Enumeration

import org.apache.log4j.Logger;

public class CycleDependency
{

    // log this
    private static final Logger log =
    Logger.getLogger( CycleDependency.class );
    private static final String ATTR = "CycleDependency";
    /**
     * Constructor to examined circular dependency given a project. Will examined starting from every activity. Use this method if wanted to check all activities in the project.
     *
     *@param project Project to be examined.
     */
    public CycleDependency(ProjectInterface project)
    throws CycleDependencyException
    {
    // looping and run algorithm starting from
    // every activity in Project p

    List activities = project.getActivities(null); // no need for sort

    for (int i=0; i<activities.size(); i++) {

        Activity temp = (Activity)activities.get(i);

        try {
                runAlgorithm(project, temp, null);
            }
            catch (InternalCycleException e) {
                throw new CycleDependencyException("Found circular dependency from Activity " + temp.getActivityKey().toString());
            }


    } // close for loop
    }

    /**
     * Constructor to examined circular dependency given a project. Will examined starting from given activity.
     *
     * @param project Project to be examined.
     * @param successor Run algorithm starting from this node.
     */
    public CycleDependency(ProjectInterface project, Activity successor)
        throws CycleDependencyException
    {
    // run algorithm starting from successor

    try {
        runAlgorithm(project, successor, null);
    }
    catch (InternalCycleException e) {
        throw new CycleDependencyException("Found circular dependency from Activity " + successor.getActivityKey().toString());
    }
    }

    /**
     * Constructor to examined circular dependency given two activity. Will examined from given successor.
     *
     * @param project Project that store this activity.
     * @param predecessor Start finding given successor from this node.
     * @param successor Added dependancy to this node.
     */
    public CycleDependency(ProjectInterface project,
               Activity predecessor,
               Activity successor)
        throws CycleDependencyException,InternalActivityException
    {
    // run algorithm on predecessor

    predecessor.addSuccessor(successor.getActivityKey());
    successor.addPredecessor(predecessor.getActivityKey());

    try {
        runAlgorithm(project, successor, null);
    }
    catch (InternalCycleException e) {
        throw new CycleDependencyException("Found circular dependency from Activity " + successor.getActivityKey().toString());
    }
    }


    /**
     * Constructor to examined circular dependency given two activity. Will examined from given successor.
     *
     * @param project Project that hold these activities with given keys.
     * @param predecessor Start finding given successor from this node.
     * @param successor Added dependency to this node.
     */
    public CycleDependency(ProjectInterface project,
                           ActivityKey predecessor,
                           ActivityKey successor)
        throws CycleDependencyException,InternalActivityException
    {


    // run algorithm on predecessor

    try {
        Activity pred = project.getActivity(predecessor);
        Activity succ = project.getActivity(successor);

        // just to make sure they are connected
        succ.addPredecessor(predecessor);

        try {
        runAlgorithm(project, succ, null);
        }
        catch (InternalCycleException e) {
        throw new CycleDependencyException("Found circular dependency from Activity " + successor.toString());
        }
    }
    catch (KeyNotFoundException e) {
        // write to log
        log.error("ERROR: predecessor or successor is null");
    }
    catch (InternalProjectException e) {
        // write to log
            log.error("ERROR: cannot found activity from given project");
    }
    }



    /**
     * Method to run the algorithm (similar to BFS). Will run one way (use successor only in recursive manner.
     *
     * @param project Project that own given Activity and its predecessor.
     * @param activity Activity to start from.
     * @param list Visited activity (contains activityKey string value)
     * @return boolean true if found a circular dependency
     */
    private void runAlgorithm(ProjectInterface project,
                  ActivityInterface activity,
                  Vector list)
    throws InternalCycleException
    {

        if (list == null)
            list = new Vector();

        list.add(activity.getActivityKey().toString());

        // TODO: I think there are still problems with this algorithm!  In
        // VCT project 4, this does not detect a circular dependency, which
        // leads to a stack overflow.

	// NOPE: stack overflow was caused by badly placed setStartDate() and
	// compute start date 

        // Iterate thru the predecessors.
        for (Iterator i = activity.getPredecessors().iterator(); i.hasNext(); )
        {
            ActivityInterface tempActivity = (ActivityInterface) i.next();

            // check if predecessor has been visited
            if (list.contains(tempActivity.getActivityKey().toString()))
            {
                throw new InternalCycleException("");
            }

            // recurse
            runAlgorithm(project, tempActivity, (Vector) list.clone());
        }
    }
}

/**
 * $Log: CycleDependency.java,v $
 * Revision 1.24  2005/04/18 06:22:07  umangkus
 * Check algorithm and found no bug (bad report) that cause overflow.
 *
 * Revision 1.23  2005/04/17 04:41:26  achou
 * Changed iteration code since getPredecessors() now returns a list of activities instead of their keys.
 *
 * Revision 1.22  2005/04/16 21:06:06  dnathan
 * Warning hunting.
 *
 * Revision 1.21  2005/04/16 20:05:25  rcoutinh
 * Added InternalActivityException
 *
 * Revision 1.20  2005/04/16 19:21:10  achou
 * Converted enumerations for activity predecessors/successors to collections.
 *
 * Revision 1.19  2005/04/16 09:00:26  umangkus
 * Fixed minute bug.
 *
 * Revision 1.18  2005/04/15 22:29:37  ccochran
 * Fixed to use Project Interface
 *
 * Revision 1.17  2005/04/15 09:50:51  umangkus
 * Fix major bug.
 *
 * Revision 1.16  2005/04/14 22:06:35  umangkus
 * *** empty log message ***
 *
 * Revision 1.15  2005/04/14 22:05:46  umangkus
 * Fixed a bug
 *
 * Revision 1.14  2005/04/14 22:04:23  umangkus
 *
 *
 * Revision 1.13  2005/04/12 03:39:01  dnathan
 * Changed variable named "enum" to a 1.5 non-reserved word.
 *
 * Revision 1.12  2005/04/11 20:20:00  ccochran
 * fixed attribute
 *
 * Revision 1.11  2005/04/09 09:04:05  umangkus
 * Added more info into header comment.
 *
 * Revision 1.10  2005/04/09 09:01:44  umangkus
 * Added j4log.
 *
 * Revision 1.9  2005/04/09 07:54:39  umangkus
 * Added real algorithm. Seems ready for testing.
 *
 * Revision 1.8  2005/04/09 07:53:07  umangkus
 * *** empty log message ***
 *
 * Revision 1.7  2005/04/09 07:16:09  umangkus
 * Modified header with comment.
 *
 * Revision 1.6  2005/04/09 07:13:52  umangkus
 * Added header and footer for log.
 *
 *
 **/
