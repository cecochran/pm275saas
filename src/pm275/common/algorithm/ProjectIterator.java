package pm275.common.algorithm;

/***************************************************
 * File: 		$RCSfile: ProjectIterator.java,v $ 
 * @author 		$Author: tlong2 $, 
 * @version 	$Id: ProjectIterator.java,v 1.2 2005/04/13 00:40:15 tlong2 Exp $
 * Assignment:	CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * Inteface defines the methods for traversing to next project activity 
 * used in the forward and backward passes of the critical path algorigthm
 * 
 * 
 *  <p>
 */

import pm275.common.exception.CriticalPathException;
import pm275.core.project.Activity;


public interface ProjectIterator {

    /**
     * Check if Iterator has more activities on the list
     *  @return True if more activities exist on list. Otherwise, false
     */
	 public abstract boolean hasNext() 
	 							throws CriticalPathException ;

	 
    /**
     * Return the next activity on the list
     *  @return next Activity on the list
     */
	 public abstract Activity next()
	 							throws CriticalPathException;
	 
    /**
     * Return the first activity on the list
     */
	 public abstract void reset();

	
}


/**
 * $Log: ProjectIterator.java,v $
 * Revision 1.2  2005/04/13 00:40:15  tlong2
 * Added exceptions
 *
 * Revision 1.1  2005/04/10 04:21:17  tlong2
 * adding initial Critical Path code
 *
 *
 **/