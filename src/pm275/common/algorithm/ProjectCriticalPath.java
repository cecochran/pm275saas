
/***************************************************
 * File:        $RCSfile: ProjectCriticalPath.java,v $
 * @author      $Author: achou $,
 * @version     $Id: ProjectCriticalPath.java,v 1.17 2005/05/16 07:57:09 achou Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * Class for computing the critical path of a project. This could be contained
 * within the ProjectInterface and implemented in project or some other way. But
 * it seems kind of nice to have it declared separately.
 *
 *
 *  <p>
 */
package pm275.common.algorithm;


import java.util.Collection;
import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.ListIterator;

import org.apache.log4j.Logger;

import pm275.core.project.ActivityInterface;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ProjectService;
import pm275.core.project.ProjectKey;
import pm275.core.project.Activity;
import pm275.common.exception.*;
import pm275.common.algorithm.BackwardPass;
import pm275.common.algorithm.ForwardPass;


public class ProjectCriticalPath
{

    private ProjectInterface currProject;
    private ProjectService projectService;
    private static final Logger log = Logger.getLogger(ProjectCriticalPath.class);
    private static final String ATTR = "ProjectCriticalPath";
    private static int MAX_DURATION = 99999;

    public ProjectCriticalPath( ProjectInterface p )
    {
        this.currProject = p;
    }

     public ProjectCriticalPath (ProjectKey pKey) throws KeyNotFoundException
     {
         projectService = ProjectService.getInstance();

         try
         {
             currProject = projectService.getProject(pKey);
         }
         catch (Exception e)
         {
            log.error("Error: ProjectCriticalPath constructor. Project not found");
            throw new KeyNotFoundException("Error: ProjectCriticalPath constructor. Project not found");
         }
     }

     public LinkedList calculateCriticalPath()
     {
        List activitiesList = currProject.getActivities(null,true);
        ActivityInterface startMilestone = currProject.getStartMilestone();
        ActivityInterface endMilestone = currProject.getEndMilestone();
        resetCriticalPathValues(activitiesList);

        startMilestone.setEarlyFinishFlag(true);
        startMilestone.setEarlyStartHours(0);
        startMilestone.setEarlyFinishHours(0);
        makeForwardPass(activitiesList);

        activitiesList = currProject.getActivities(null,true);

        endMilestone.setLateStartFlag(true);
        endMilestone.setLateFinishHours( endMilestone.getEarlyFinishHours() );
        endMilestone.setLateStartHours( endMilestone.getEarlyStartHours() );
        makeBackwardPass(activitiesList);


        // Calculate critical path
        LinkedList criticalPath = new LinkedList();
        ListIterator activities = currProject.getActivities(null,true).listIterator();
        Activity temp;
        while (activities.hasNext()) {
           temp = (Activity)activities.next();
          if (temp.calculateSlackTime() == 0)
              criticalPath.add(temp);
        }

        return criticalPath;
      }

     private void makeForwardPass(List activitiesList)
     {
        // Forward Pass and set Flag values
        ForwardPass forwardPass = new ForwardPass(activitiesList);
        Activity a;

        try
        {
            while (forwardPass.hasNext())
            {
                a = forwardPass.next();

                if( a.getEarlyFinishFlag() )
                {
                    log.error("PROBLEM");
                }

                int maxEF = -1;
                Iterator preds = a.getPredecessors().iterator();

                while( preds.hasNext() )
                {
                    Activity pred = (Activity) preds.next();
                    if( pred.getEarlyFinishHours() > maxEF )
                    {
                        maxEF = pred.getEarlyFinishHours();
                    }
                }

                // Calculate the activity's duration.
                int duration = a.getDuration();
                if ((! a.getIsFixedDuration()) && (a.getResources().size() > 0))
                {
                    duration /= a.getResources().size();
                }

                a.setEarlyStartHours( maxEF );
                a.setEarlyFinishHours( maxEF + duration );
                a.setEarlyFinishFlag(true);
            }
        }
        catch (CriticalPathException cpe) {
            log.error("ERROR : Exception thrown in ProjectCritcalPath.makeForwardPass()");
        }
     }

     private void makeBackwardPass(List activitiesList)
     {
        // Forward Pass and set Flag values
        BackwardPass backwardPass = new BackwardPass(activitiesList);
        Activity a;

        try
        {
            while (backwardPass.hasNext())
            {
                a = backwardPass.next();

                if( a.getLateStartFlag() )
                {
                    log.error("PROBLEM");
                }

                int minLS = 999999;
                Iterator succs = a.getSuccessors().iterator();

                while( succs.hasNext() )
                {
                    Activity succ = (Activity) succs.next();
                    if( succ.getLateStartHours() < minLS )
                    {
                        minLS = succ.getLateStartHours();
                    }
                }

                // Calculate the activity's duration.
                int duration = a.getDuration();
                if ((! a.getIsFixedDuration()) && (a.getResources().size() > 0))
                {
                    duration /= a.getResources().size();
                }

                a.setLateFinishHours( minLS );
                a.setLateStartHours( minLS - duration );

                a.setLateStartFlag(true);
            }
        }
        catch (CriticalPathException cpe) {
            log.error("ERROR : Exception thrown in ProjectCritcalPath.makeForwardPass()");
        }
     }

     private void resetCriticalPathValues(List activitiesList)
     {
        Iterator activitiesIterator = activitiesList.iterator();

        while (activitiesIterator.hasNext())
        {
            Activity a = (Activity)activitiesIterator.next();
            a.setEarlyFinishHours(-1);
            a.setEarlyStartHours(-1);
            a.setEarlyFinishFlag(false);
            a.setLateFinishHours(-1);
            a.setLateStartHours(-1);
            a.setLateStartFlag(false);
        }
     }
}


/**
 * $Log: ProjectCriticalPath.java,v $
 * Revision 1.17  2005/05/16 07:57:09  achou
 * Divide activity duration by number of resources, if it's based on level of effort.
 *
 * Revision 1.16  2005/05/09 16:58:09  achou
 * Forward/backward pass were fixed so the extra start/end milestones are no longer needed.
 *
 * Revision 1.15  2005/05/05 05:11:54  tlong2
 * fixed calculateCriticalPath Method so it doesn't always return null
 *
 * Revision 1.14  2005/04/24 20:10:53  dnathan
 * Now working as specified.
 *
 * Revision 1.13  2005/04/18 21:32:55  tlong2
 * Critical Path working!
 *
 * Revision 1.12  2005/04/18 13:24:33  tlong2
 * minor changes
 *
 * Revision 1.11  2005/04/18 12:17:15  tlong2
 * bug fix
 *
 * Revision 1.10  2005/04/18 06:37:56  tlong2
 * Added x.setResources() which should probably be handled differently.
 *
 * Revision 1.9  2005/04/18 03:44:12  tlong2
 * bug fix
 *
 * Revision 1.8  2005/04/16 21:06:06  dnathan
 * Warning hunting.
 *
 * Revision 1.7  2005/04/16 19:21:10  achou
 * Converted enumerations for activity predecessors/successors to collections.
 *
 * Revision 1.6  2005/04/15 22:29:37  ccochran
 * Fixed to use Project Interface
 *
 * Revision 1.5  2005/04/13 04:36:51  tlong2
 * Added comments
 *
 * Revision 1.4  2005/04/13 04:23:59  tlong2
 * Completed Main CriticalPath Algorithm code
 *
 * Revision 1.3  2005/04/12 03:41:20  dnathan
 * Removed unneeded imports.
 *
 * Revision 1.2  2005/04/11 03:19:16  achou
 * Commented out unused code that caused compile errors.
 *
 * Revision 1.1  2005/04/10 04:21:17  tlong2
 * adding initial Critical Path code
 *
 *
 **/
