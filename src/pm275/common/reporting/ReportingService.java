/***************************************************
 * File: 		$RCSfile: ReportingService.java,v $ 
 * @author 		$Author: ccochran $, 
 * @version 	$Id: ReportingService.java,v 1.3 2005/03/29 15:43:32 ccochran Exp $
 * Assignment:	CSCI E-275 Team 1 Project

 * Purpose:
 *  This is the reporting service providing central point for all reports
 *  Out of scope V1: From Architecture Spec
 * 
 *
 * 
 *  
 */
package pm275.common.reporting;

/**
 * 
 */
public class ReportingService {

/**
 * 
 */
    private static ReportingService reportingService;

/**
 *  
 * 
 * 
 * @return 
 */
    public static final ReportingService getInstance() {        
        // your code here
        return null;
    } 
 }

/**
 * $Log: ReportingService.java,v $
 * Revision 1.3  2005/03/29 15:43:32  ccochran
 * Added defintion from spec
 *
 * Revision 1.2  2005/03/26 17:45:47  dnathan
 * Moved package name.
 *
 * Revision 1.1  2005/03/26 09:02:36  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.3  2005/03/26 04:26:50  ccochran
 * CEC Small comments
 *
 * Revision 1.2  2005/03/23 01:47:51  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
  * Revision 1.1  2005/03/22 04:30:30  dnathan
 * New file.
 *
 **/
