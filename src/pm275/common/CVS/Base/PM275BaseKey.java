/***************************************************
 * File:        $RCSfile: PM275BaseKey.java,v $
 * @author      $Author: ccochran $
 * @version     $Id: PM275BaseKey.java,v 1.20 2005/05/14 18:33:35 ccochran Exp $
 * Assignment:  CSCI E-275 Team 1 Project
* <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * PM275BaseKey The root key from which all other keys will derive
 * </p>
 */
package pm275.common;

import org.apache.log4j.Logger;

import java.util.*;

//import pm275.common.exception.*;

public class PM275BaseKey implements Comparable
{
    private static final Logger log = Logger.getLogger( PM275BaseKey.class );
    private static final String ATTR = "PM275BaseKey ";
    private final String keyId;
    
    
    //private final String keyId;
    /**
     * Only public constructor.
     * Uses calendar to set the time for key id
     */
    public PM275BaseKey()
    {
        // Set default key to string version of current time
        java.util.Calendar cal = java.util.Calendar.getInstance();

	
	// below generate random long
	// not very ellegant but works -urip-
	Random random = new Random();
	long knum = random.nextInt();
	random = new Random();
	while (knum == random.nextInt())
	    random = new Random();	
	knum = random.nextInt();

        keyId = Long.toString(knum) + " " +
	    Long.toString(cal.getTimeInMillis());
        

    }

    public PM275BaseKey(String str)
    {
        this.keyId = str.trim();
    
    }
    
    
   
    
    /**
     * <p>Does given key match.</p>
     *
     * @return boolean
     * @param key
     */
    public boolean equals(PM275BaseKey key) {
  
    	
    /*	if(this == key) return true;
    	if((key == null) || (key.getClass() != this.getClass())) return false;
    	//object must be PM275BaseKey at this point
    	PM275BaseKey test = (PM275BaseKey) key;
    		 return keyId == test.keyId || ((key != null) && keyId.equals(test.keyId ));
    	
    	*/
    	
    	 if (key !=null){
        if ( key instanceof PM275BaseKey ){
            return keyId.equals( ((PM275BaseKey)key).keyId );
        }
    }else{
        log.error("Error: Key.equals sent a null key" );
    }
            return  false;

    }

    /**
     *  Returns the key ID.  (We should use this rather than toString() to get
     *  the ID.)
     *  @return This key's ID.
     */
    public final String getKeyId()
    {
        return keyId.trim();
    }

    /**
     * <p>Method to return the string represnation of a key</p>
     *
     *
     *
     * @return String
     */
    public String toString() {
        return keyId.trim();
    }

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object o1) {
		return o1.toString().compareTo(this.toString());
	}

	/*
	 *  
	 * @see java.lang.Object#hashCode()
	 */
    public int hashCode() {
    	return this.keyId.hashCode();
    }
 }
/**

 *
 * Revision 1.1  2005/03/22 04:30:32  dnathan
 * New file.
 *
 **/
