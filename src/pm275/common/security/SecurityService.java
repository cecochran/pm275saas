/***************************************************
 * File: 		$RCSfile$ 
 * @author 		$Author$, 
 * @version 	$Id$
 * Assignment:	CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * The security service purpose it to authnticate users
 * that are currently listed in a usernames text file
 * If correct name and password is given then this 
 * will grant access. This could be later expanded
 * to connect to an LDAP repositry, but is out of
 * scope for this PM-275 at this time
 * </p>
 */
package pm275.common.security;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;

import java.util.StringTokenizer;
import java.util.Hashtable;

import org.apache.log4j.Logger;

//import pm275.core.project.Activity;
/**
 * SecurityService is responsible for validating a user Id 
 * and password to allow login into the PM-275 system.
 */
public class SecurityService {

	
	/**
	 * path to properties file relative to classpath
	 * 
	 */
	private static final String PROP_FILE = "usernames.txt";
	
	/**
	 * Default Separator for separating the username and password on a line
	 */
	private static final String SEPARATOR = ",";
	/**
	 * This service is a singleton and this variable represents
	 * the only instance of this service.
	 */
    private static SecurityService _instance ;
		
	/**
	 * a map of username and passwords
	 */
	private Hashtable _users = new Hashtable();
	/**
	 * Constructor 
	 *
	 */
	private static final Logger log = Logger.getLogger( SecurityService.class );
	private static final String ATTR = "SecurityService";
	
	private SecurityService() throws SecurityException
	{
		try {
			InputStream is = ClassLoader.getSystemClassLoader().getResourceAsStream(PROP_FILE);
			if(is == null)
			{    
				log.error("ERROR Cannot load properties file: " + PROP_FILE);
				throw new SecurityException("Cannot load properties file: " + PROP_FILE);
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = "";
			
			
			while( ( line = reader.readLine()) != null)
			{
				String [] list = new String [2];
				int index = 0;
				StringTokenizer st = new StringTokenizer(line,SEPARATOR);
				while(st.hasMoreTokens())
				{
					list[index] = st.nextToken();
					index++;
				}
				if(list[0] != null && list[1] != null)
				{
					_users.put(list[0],list[1]);
				}
			}
			
		} catch (IOException e) {
			log.error("Error creating Security service- " + 
					e.getMessage());
			throw new SecurityException("Error creating Security service- " + 
										e.getMessage());
		}
	}
 
/**
 * 
 * @return an instance of this service. 
 */
    public static final SecurityService getInstance()
		throws SecurityException
	{        
        if(_instance == null)
		{
			_instance = new SecurityService();
		}
        return _instance;
    } 

/**
 * Checks the given username password with the existing list
 * 
 * @return true if given username and password exist in list,
 * false otherwise 
 * @param username 
 * @param password 
 */
    public boolean login(String username, String password)
		throws SecurityException
	{
		if(username == null || username.length() == 0)
		{
			log.error("ERROR: Username cannot be null or blank");
			throw new SecurityException("Username cannot be null or blank");
		}
		if(password == null)
		{
			log.error("ERROR: Password cannot be null or blank");
			throw new SecurityException("Password cannot be null or blank");
		}
		if(_users.containsKey(username))
		{
			String passwordFromList = (String)_users.get(username);
			if(password.equals(passwordFromList))
			{
				return true;
			}
			return false;
		}
		return false;
    } 
 }

/**
 * $Log$
 * Revision 1.3  2005/03/30 06:05:41  ccochran
 * comment out unused import to remove warn
 *
 * Revision 1.2  2005/03/29 15:52:47  ccochran
 * Added Class description and Log messages
 *
 * Revision 1.1  2005/03/24 01:09:10  rcoutinh
 * Security Service
 *
 * Revision 1.2  2005/03/23 01:47:46  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.1  2005/03/22 04:30:30  dnathan
 * Revision 1.2  2005/03/23 05:30:30  rcoutinho
 *
 **/