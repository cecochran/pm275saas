/**
 *  File      :  TableColumnData.java
 *  @version   :  $Id: TableColumnData.java,v 1.3 2005/04/02 15:27:02 jkeiser Exp $
 *  @author    :  Jennifer Keiser
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Basic structure for the attributes of columns in a table of data
 *               about a project, to be displayed in the UI.
 *  Purpose   :  Encapsulates information about each TableColumn.
 */

package pm275.gui.panels;
import org.apache.log4j.Logger;

class TableColumnData {

	public String colTitle;
	public int colWidth;
	public int colAlignment;
    private static final Logger log = Logger.getLogger(TableColumnData.class);
    private static final String ATTR = "TableColumnData";

	public TableColumnData(String title, int width, int alignment) {
		colTitle = title;
		colWidth = width;
		colAlignment = alignment;
	}
	
	/**
     * Gets table column name 
     * @return String	The column's name
     */
	public String getTitle() {
		return colTitle;
	}
	
	/**
     * Gets table column width 
     * @return int	The column's width
     */
	public int getWidth() {
		return colWidth;
	}

	/**
     * Gets table column alignment 
     * @return int	The column's alignment setting
     */
	public int getAlign() {
		return colAlignment;
	}
	
}
/*
 *  $Log: TableColumnData.java,v $
 *  Revision 1.3  2005/04/02 15:27:02  jkeiser
 *  Cleaned up comments.  Added get methods.  Added CVS $Log.
 *
 */