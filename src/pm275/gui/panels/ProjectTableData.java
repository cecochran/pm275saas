/**
 *  File      :  ProjectTableData.java
 *  @version  :  $Id: ProjectTableData.java,v 1.33 2005/05/09 01:54:29 achou Exp $
 *  @author   :  Jennifer Keiser
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Defines table columns to be referenced by ProjectPanel and methods
 *               for accessing their contents.
 *  Purpose   :  Data model for project data.  Defines the structure of a project for display
 *               in the GUI.  In future iterations, additional columns will be added to hold
 *               resources, dependencies, etc.
 */
package pm275.gui.panels;

import javax.swing.table.*;
import javax.swing.JLabel;
import java.util.*;

import pm275.common.exception.InternalProjectException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.ResourceException;
import pm275.core.project.*;
import pm275.core.resource.AssignmentInterface;
import pm275.core.resource.AssignmentKey;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.ResourceKey;
import pm275.core.resource.ResourceService;
import pm275.core.calendar.CalendarService;
import pm275.core.calendar.CalendarInterface;
import pm275.gui.common.*;
import org.apache.log4j.Logger;

public class ProjectTableData extends AbstractTableModel {

    private static final long serialVersionUID = 3258407352633144883L;
    protected ProjectInterface   project;
    protected List      activities;
    private static final Logger log = Logger.getLogger(ProjectTableData.class);
    private static final String ATTR = "ProjectTableData";

    // Column indices.
    private static final int ROW_COLUMN_INDEX          = 0;
    private static final int NAME_COLUMN_INDEX         = 1;
    private static final int DURATION_COLUMN_INDEX     = 2;
    private static final int START_DATE_COLUMN_INDEX   = 3;
    private static final int END_DATE_COLUMN_INDEX     = 4;
    private static final int DEPENDENCIES_COLUMN_INDEX = 5;
    private static final int RESOURCES_COLUMN_INDEX    = 6;
    private static final int CRITICAL_PATH_COLUMN_INDEX = 7;
    private static final int SLACK_TIME_COLUMN_INDEX   = 8;

    // Column labels.
    // TODO: Externalize the strings.
    private static final String ROW_COLUMN_LABEL          = "#";
    private static final String NAME_COLUMN_LABEL         = "Activity";
    private static final String DURATION_COLUMN_LABEL     = "Duration";
    private static final String START_DATE_COLUMN_LABEL   = "Start Date";
    private static final String END_DATE_COLUMN_LABEL     = "End Date";
    private static final String DEPENDENCIES_COLUMN_LABEL = "Depends On";
    private static final String RESOURCES_COLUMN_LABEL    = "Resources";
    private static final String CRITICAL_PATH_COLUMN_LABEL= "On Critical Path?";
    private static final String SLACK_TIME_COLUMN_LABEL   = "Slack Time";

    public static final int HOURS = 0;
    public static final int DAYS = 1;
    private Collection filterResources = null;


    /**
     * Defines the table columns included in the display of a project in a JTable
     */
    public static final TableColumnData[] PROJECT_TABLE_COLUMNS =
            {
                new TableColumnData(ROW_COLUMN_LABEL, 10, JLabel.LEFT),
                new TableColumnData(NAME_COLUMN_LABEL, 200, JLabel.LEFT),
                new TableColumnData(DURATION_COLUMN_LABEL, 50, JLabel.LEFT),
                new TableColumnData(START_DATE_COLUMN_LABEL, 50, JLabel.LEFT),
                new TableColumnData(END_DATE_COLUMN_LABEL, 50, JLabel.LEFT),
                new TableColumnData(DEPENDENCIES_COLUMN_LABEL, 100, JLabel.LEFT),
                new TableColumnData(RESOURCES_COLUMN_LABEL, 100, JLabel.LEFT),
                new TableColumnData(CRITICAL_PATH_COLUMN_LABEL, 100, JLabel.LEFT),
                new TableColumnData(SLACK_TIME_COLUMN_LABEL, 50, JLabel.LEFT)
            };

    /**
     * Constructor for ProjectTableData; establishes vector to allow addressing activities by index
     * @param myProject The project for which the table data is being defined
     */
    public ProjectTableData(ProjectInterface myProject) {
        project = myProject;

        // Get the list of activities, sorted in ascending order by name.
        this.activities = project.getActivities(Activity.OrderComparator.ascending());
        this.activities.add(0, null);
    }

    /**
     * Gets count of columns
     * @return int  The number of columns available to be displayed
     */
    public int getColumnCount() {
        return PROJECT_TABLE_COLUMNS.length;
    }

    /**
     * Gets count of rows
     * @return int  The number of rows (activities) available to be displayed
     */
    public int getRowCount() {
        if (activities == null)
            return 0;
        else
            return activities.size();
    }

    /**
     * Gets column name (displayed in ProjectPanel's JTable)
     * @param col   The number of the desired column
     * @return String   The column's name
     */
    public String getColumnName(int col) {
        return PROJECT_TABLE_COLUMNS[col].colTitle;
    }

    /**
     * Gets column width (for creating ProjectPanel's JTable)
     * @param col   The number of the desired column
     * @return int  The column's width
     */
    public int getColumnWidth(int col) {
        return PROJECT_TABLE_COLUMNS[col].colWidth;
    }

    /**
     * Gets column alignment (for creating ProjectPanel's JTable)
     * @param col   The number of the desired column
     * @return int  The column's alignment value
     */
    public int getColumnAlign(int col) {
        return PROJECT_TABLE_COLUMNS[col].colAlignment;
    }

    /**
     * Gathers the value in a particular row and column of Project Table data model
     * @param nRow  The number of the row of the desired value
     * @param nCol  The number of the column of the desired value
     * @return  Value at specified row and column
     */
    public Object getValueAt(int nRow, int nCol) {
        if (nRow < 0 || nRow >= getRowCount()) {
            log.error("Error: Reference to non-existent table row: "+nRow);
            return "";
        }
        ActivityInterface row = getActivityAt(nRow);

        // Return the project or activity's value.
        Object value = null;
        if (nRow == 0)
        {
            // First row represents the project.
            value = getProjectValue(nCol);
        }
        else
        {
            // All other rows represent activities.
            value = getActivityValue(row, nCol);
        }
        return value;
    }

    /**
     *  Returns the activity's value for the specified column.
     *  @param activity The activity to query.
     *  @param col Column index.
     *  @return Activity's value for the specified column.
     */
    private Object getActivityValue(ActivityInterface activity, int col)
    {
        Object value = null;
        StringBuffer sb = null;
        switch (col)
        {
            case ROW_COLUMN_INDEX:
                value = new Integer(activity.getDisplayOrder());
                break;

            case NAME_COLUMN_INDEX:
                value = activity.getName();
                break;

            case DURATION_COLUMN_INDEX:
                if (activity.getDurationMeasure() == DAYS)
                {
                    int hoursPerDay = 1;
                    try
                    {
                        CalendarInterface calendar = CalendarService.getInstance().getCalendar(project.getCalendarKey());
                        hoursPerDay = calendar.getHoursPerDay();
                    }
                    catch (Exception e)
                    {
                        log.error("Error: Can't find project's calendar: " + e);
                    }
                    value = String.valueOf((activity.getDuration()/hoursPerDay) + " days");
                }
                else
                {
                    value = String.valueOf(activity.getDuration() + " hours");
                }

                break;

            case START_DATE_COLUMN_INDEX:
                value = DateFormatter.getStringForDate(activity.getStartDate());
                break;

            case END_DATE_COLUMN_INDEX:
                // Massage the end date so that if the activity finishes at the
                // start of a day, treat it as the end of the previous work day.
                Date endDate = activity.getEndDate();
                Date newDate = massageEndDate(endDate);
                if (newDate.after(activity.getStartDate()))
                {
                    endDate = newDate;
                }
                value = DateFormatter.getStringForDate(endDate);
                break;

            //TODO look for methods on Activities to list resources and dependencies here
            case DEPENDENCIES_COLUMN_INDEX:
                String separator = ", ";
                boolean startLast = false;

                sb = new StringBuffer();
                for (Iterator i = activity.getPredecessors().iterator(); i.hasNext(); )
                {
                    ActivityInterface act = (ActivityInterface) i.next();

                    if( ! act.isStartMilestone() )
                    {
                        startLast = false;
                        sb.append(act.getName());
                        if (i.hasNext())
                        {
                            sb.append( separator );
                        }
                    }
                    else
                    {
                        startLast = true;
                    }
                }

                // check for trailing comma because of isStartMilestone check
                String depList = sb.toString();

                if( startLast && depList.length() >= 2 )
                {
                    depList = depList.substring( 0 , depList.length() - 2 );
                }

                value = depList;
                break;

            case RESOURCES_COLUMN_INDEX:
                sb = new StringBuffer();
                for (Iterator i = activity.getResources().iterator(); i.hasNext(); )
                {
                    sb.append(((ResourceInterface) i.next()).getName());
                    if (i.hasNext())
                    {
                        sb.append(", ");
                    }
                }
                value = sb.toString();
                break;

            case CRITICAL_PATH_COLUMN_INDEX:
                if( activity.calculateSlackTime() > 1000 )
                {
                    value = "Not Calculated";
                }
                else if( activity.calculateSlackTime() == 0 )
                {
                    value = "Yes";
                }
                else
                {
                    value = "No";
                }
                break;

            case SLACK_TIME_COLUMN_INDEX:
                if( activity.calculateSlackTime() < 10000 )
                {
                    value = String.valueOf(activity.calculateSlackTime()) + " hours";
                }
                else
                {
                    value = "";
                }
                break;
            default:
                log.error("Error: Reference to non-existent table column: " + col);
        }
        return value;
    }

    /**
     *  Returns the project's value for the specified column.
     *  @param activity The activity to query.
     *  @param col Column index.
     *  @return Activity's value for the specified column.
     */
    private Object getProjectValue(int col)
    {
        Object value = null;
        switch (col)
        {
            case NAME_COLUMN_INDEX:
                value = "Project: " + project.getName();
                // TODO: Use a custom renderer to display a project, rather
                // than a different label.
                break;

            case DURATION_COLUMN_INDEX:
                value = "";
                break;

            case START_DATE_COLUMN_INDEX:
                value = DateFormatter.getStringForDate(project.getStartDate());
                break;

            case END_DATE_COLUMN_INDEX:
                try {
                    // Massage the end date so that if the project finishes at the
                    // start of a day, treat it as the end of the previous work day.
                    Date endDate = project.getEndDate();
                    Date newDate = massageEndDate(endDate);
                    if (newDate.after(project.getStartDate()))
                    {
                        endDate = newDate;
                    }
                    value = DateFormatter.getStringForDate(endDate);
                } catch (InternalProjectException e) {
                    e.printStackTrace();
                } catch (KeyNotFoundException e) {
                    e.printStackTrace();
                }
                break;

            case ROW_COLUMN_INDEX:
            case DEPENDENCIES_COLUMN_INDEX:
            case RESOURCES_COLUMN_INDEX:
            case CRITICAL_PATH_COLUMN_INDEX:
            case SLACK_TIME_COLUMN_INDEX:
                value = "";
                break;

            default:
                log.error("Error: Reference to non-existent table column: " + col);
        }
        return value;
    }

    /**
     *  Returns the activity represented by the specified row.
     *  @param row Row number.
     *  @return Activity at the specified row.
     */
    public ActivityInterface getActivityAt(int row)
    {
        return (ActivityInterface) activities.get(row);
    }

    /**
     * Adds an activity to the end of the project's activities list
     * @param newActivity   The Activity object to be added
     */
    public void addRow(ActivityInterface newActivity) {
        activities.add(newActivity);
    }

    /**
     * Adds an activity to the specified position in the project's activities list
     * @param position      The row position in which to add the newActivity
     * @param newActivity   The Activity object to be added
     */
    public void addRow(int position, ActivityInterface newActivity) {
        activities.add(position,newActivity);
    }

    /**
     *  Massages an activity's end date such that if it ends at the start of a
     *  day, converts it to the end of the previous work day.  This is purely
     *  for display and has no effect on the actual end date stored with the
     *  activity.
     *  @param endDate Activity's end date.
     *  @return Massaged version of the end date.
     */
    private final Date massageEndDate(Date endDate)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        if ((cal.get(Calendar.HOUR_OF_DAY) == 0) &&
            (cal.get(Calendar.MINUTE) == 0) &&
            (cal.get(Calendar.SECOND) == 0) &&
            (cal.get(Calendar.MILLISECOND) == 0))
        {
            CalendarInterface calendar =
                    CalendarService.getInstance().getCorporateCalendar();
            endDate = calendar.addDaysToDate(endDate, -1);
            cal.setTime(endDate);
            cal.add(Calendar.HOUR_OF_DAY, calendar.getHoursPerDay());
            endDate = cal.getTime();
        }
        return endDate;
    }

    public void applyResourceFilter( Collection resourceKeys )
    {
        this.clearResourceFilter();
        this.filterResources = resourceKeys;

        Iterator resourceIterator = this.filterResources.iterator();
        Map visibleActivities = new Hashtable();

        // get a list of the activities that we should display
        while( resourceIterator.hasNext() )
        {
            try
            {
                ResourceInterface res = ResourceService.getInstance().getResource( (ResourceKey) resourceIterator.next() );
                Iterator assignIterator = res.listAssignments();
                while( assignIterator.hasNext() )
                {
                    try
                    {
                        AssignmentKey assignKey = (AssignmentKey) assignIterator.next();
                        AssignmentInterface assign = ResourceService.getInstance().getAssignment( assignKey );
                        ActivityKey activityKey = assign.getActivityKey();
                        ActivityInterface activity = ProjectService.getInstance().getActivity( this.project.getProjectKey() , activityKey );
                        visibleActivities.put( activity.getActivityKey().toString() , activity );
                    }
                    catch (ResourceException e)
                    {
                    }
                    catch (KeyNotFoundException e)
                    {
                    }
                    catch (InternalProjectException e)
                    {
                    }
                }
            }
            catch (ResourceException e)
            {
            }
        }

        for( int x = this.activities.size()-1 ; x > 0  ; x-- )
        {
            ActivityInterface activity = (ActivityInterface) this.activities.get(x);
            if( visibleActivities.containsKey( activity.getActivityKey().toString() ) )
            {
                // leave it in the list
            }
            else
            {
                this.activities.remove( activity );
            }
        }
    }

    public void clearResourceFilter()
    {
        this.filterResources = null;
        this.activities = project.getActivities(Activity.OrderComparator.ascending());
        this.activities.add(0, null);
    }

    /**
     *  Returns the index of the name column in the table.
     *  @return Index of the name column.
     */
    public static final int getNameColumnIndex()
    {
        return NAME_COLUMN_INDEX;
    }
}
/*
 *  $Log: ProjectTableData.java,v $
 *  Revision 1.33  2005/05/09 01:54:29  achou
 *  Added method to return the index of the name column.
 *
 *  Revision 1.32  2005/05/09 00:06:05  achou
 *  Massaged display of project end date when it sits at the boundary of a date.
 *
 *  Revision 1.31  2005/05/08 22:36:51  jkeiser
 *  Changed display of activity to divide duration by calendar's Hours Per Day if activity is expressed in days.
 *
 *  Revision 1.30  2005/05/07 04:22:29  dnathan
 *  Added code to allow filtering of Activities by assigned Resource.
 *
 *  Revision 1.29  2005/04/29 05:15:40  achou
 *  Fixed bug where end date was before start date for activities with 0 duration.
 *
 *  Revision 1.28  2005/04/28 05:37:05  achou
 *  Massages display of end date when it sits at the boundary of a date.
 *
 *  Revision 1.27  2005/04/24 20:30:57  dnathan
 *  Fixed trailing comma bug when start milestone was the last activity in a dep list.
 *
 *  Revision 1.26  2005/04/24 20:16:56  dnathan
 *  Removed debug columns.
 *
 *  Revision 1.25  2005/04/23 13:09:02  jkeiser
 *  Updated project display to include Activity durations.
 *
 *  Revision 1.24  2005/04/21 21:02:36  dnathan
 *  Additional "stuff" to support CP testing.
 *
 *  Revision 1.23  2005/04/20 20:58:32  dnathan
 *  Added critical path display code.
 *
 *  Revision 1.22  2005/04/19 18:53:39  dnathan
 *  Fixed new minor bug where we'd get an occassional trailing comma in the dep list when the Start Milestone was the last Activity returned.
 *
 *  Revision 1.21  2005/04/19 17:54:26  dnathan
 *  Resolved bug where project end date was not moving.
 *
 *  Revision 1.20  2005/04/19 17:04:14  dnathan
 *  Updated table model to show project end date.
 *
 *  Revision 1.19  2005/04/18 04:51:40  achou
 *  Sort activities by their display order instead of by key.
 *
 *  Revision 1.18  2005/04/18 04:21:35  achou
 *  Added constants to represent column indices and labels.  Added row number to table display.
 *
 *  Revision 1.17  2005/04/17 17:09:45  dnathan
 *  Changed End Date column to display end date and not start date.
 *
 *  Revision 1.16  2005/04/17 11:33:46  achou
 *  Added code to display activity predecessors and resources in table.
 *
 *  Revision 1.15  2005/04/16 18:31:48  achou
 *  Changed Project and Activity references to use their interfaces.
 *
 *  Revision 1.14  2005/04/15 22:29:38  ccochran
 *  Fixed to use Project Interface
 *
 *  Revision 1.13  2005/04/15 04:47:00  achou
 *  Label the project differently.
 *
 *  Revision 1.12  2005/04/15 04:45:33  achou
 *  Display the project in the table's first row.  Added methods to return the project/activity's value for a specified column.
 *
 *  Revision 1.11  2005/04/11 03:20:23  achou
 *  Added method to return the activity represented in a specific row.
 *
 *  Revision 1.10  2005/04/07 02:06:45  jkeiser
 *  Added more table columns to support display of activities in iteration 2.
 *
 *  Revision 1.9  2005/04/02 15:27:42  jkeiser
 *  Cleaned up comments. Added CVS $Log.
 *
 */
