/*
 *  File      :  EditActivityAction.java
 *  Version   :  $Id: EditActivityAction.java,v 1.8 2005/04/18 15:21:23 achou Exp $
 *  Author    :  Alex Chou
 *  Assignment:  PM-275
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */

package pm275.gui.actions;

import java.awt.event.ActionEvent;
import org.apache.log4j.Logger;

import pm275.core.project.ProjectInterface;
import pm275.core.project.ActivityInterface;
import pm275.gui.common.Settings;
import pm275.gui.common.SwingUtilities;
import pm275.gui.dialogs.ActivityDialog;
import pm275.gui.dialogs.ActivityDialogResult;
import pm275.gui.panels.MainPanel;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.DuplicateKeyException;
import pm275.common.exception.CycleDependencyException;


/**
 *  Action for editing the activity currently selected in the project table.
 *  Launches the activity dialog, then updates both current project and main
 *  panel.
 */
public class EditActivityAction extends AbstractPmAction
{
    // This field auto-generated by Eclipse.  DN.
    private static final long serialVersionUID = 3832899944846932019L;
    private static final Logger log = Logger.getLogger(EditActivityAction.class);
    private static final String ATTR = "EditActivityAction";

    /**
     *  Creates the action for editing an activity.
     *  @param panel Main panel containing this action.
     */
    public EditActivityAction(MainPanel panel)
    {
        super(panel, "action.editactivity" );
    }

    /**
     *  Handles the action by showing the activity dialog, modifying the activity
     *  with new information, and updating the current project and main panel.
     *  @param e Event generated by this action.
     */
    public void actionPerformed(ActionEvent e)
    {
        // Create a result object from the current activity.
        MainPanel panel = getPanel();
        ActivityInterface activity = panel.getCurrentPanel().getCurrentActivity();
        if (activity == null)
        {
            return;
        }

        // Show the dialog and edit the activity if the user completed the
        // dialog.
        ActivityDialogResult result = ActivityDialogResult.fromActivity(activity);
        ActivityDialog dialog = new ActivityDialog(panel.getDriver(), Settings.getSetting( "action.editactivity.title" ), true, result);
        if (result.getIsCompleted())
        {
            try
            {
                result.setToActivity(activity);
                ProjectInterface project = panel.getCurrentProject();
                project.updateActivity(activity.getActivityKey(), activity);
                panel.updateCurrentProject(project);
            }
            catch (KeyNotFoundException ex)
            {
                log.error(ex.getMessage());
            }
            catch (InternalActivityException ex)
            {
                log.error(ex.getMessage());
            }
            catch (InternalProjectException ex)
            {
                log.error(ex.getMessage());
            }
            catch (DuplicateKeyException ex)
            {
                log.error(ex.getMessage());
            }
            catch (CycleDependencyException ex)
            {
                SwingUtilities.reportErrorMessage(Settings.getSetting("error.message.circular-dependency"));
            }
        }
    }
}

/*
 *  $Log: EditActivityAction.java,v $
 *  Revision 1.8  2005/04/18 15:21:23  achou
 *  Added call to display circular dependency error message in a dialog.
 *
 *  Revision 1.7  2005/04/18 14:38:45  achou
 *  Added catch block for CycleDependencyException (though it doesn't do anything yet.
 *
 *  Revision 1.6  2005/04/17 11:45:21  achou
 *  Moved code that sets activity fields using dialog result data to ActivityDialogResult.setToActivity().
 *
 *  Revision 1.5  2005/04/16 18:31:47  achou
 *  Changed Project and Activity references to use their interfaces.
 *
 *  Revision 1.4  2005/04/15 04:33:52  achou
 *  Make sure that activity is not null when performing the action.
 *
 *  Revision 1.3  2005/04/14 04:46:25  dnathan
 *  Externalized Strings.
 *
 *  Revision 1.2  2005/04/12 03:42:58  dnathan
 *  Added serialization ID to remove warning.
 *
 *  Revision 1.1  2005/04/11 03:19:03  achou
 *  Initial check-in.
 *
 */
