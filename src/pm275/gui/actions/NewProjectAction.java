/*
 *  File      :  NewProjectAction.java
 *  Version   :  $Id: NewProjectAction.java,v 1.9 2005/04/16 18:31:48 achou Exp $
 *  Author    :  Alex Chou
 *  Assignment:  PM-275
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */

package pm275.gui.actions;

import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.Date;
import org.apache.log4j.Logger;

import pm275.core.project.ProjectInterface;
import pm275.core.project.ProjectService;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourceService;
import pm275.gui.common.Settings;
import pm275.gui.dialogs.ParameterFormatException;
import pm275.gui.dialogs.ProjectDialog;
import pm275.gui.dialogs.ProjectDialogResult;
import pm275.gui.panels.MainPanel;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.PersistanceException;
import pm275.common.exception.ResourceException;
import pm275.common.persistance.PersistanceService;


/**
 *  Action for creating a new project.  Launches the project dialog to collect
 *  information for the project and adds it to the main panel.
 */
public class NewProjectAction extends AbstractPmAction
{
    /**
     * This field auto-generated by Eclipse.  DN.
     */
    private static final long serialVersionUID = 3257853190182287409L;
    private static final Logger log = Logger.getLogger(NewProjectAction.class);
    private static final String ATTR = "NewProjectAction";

    /**
     *  Creates the action for creating a new project.
     *  @param panel Main panel containing this action.
     */
    public NewProjectAction(MainPanel panel)
    {
        super(panel, "action.newproject" );
    }

    /**
     *  Handles the action by showing the project dialog, creating the project
     *  with the information entered, and adding it to the main panel.
     *  @param e Event generated by this action.
     */
    public void actionPerformed(ActionEvent e)
    {
        // Create result object with today's date.
        MainPanel panel = getPanel();
        ProjectDialogResult result = new ProjectDialogResult();
        Date today = new Date( System.currentTimeMillis() );
        try
        {
            result.setStartDate( today );
        }
        catch (ParameterFormatException e1)
        {
            // ignore?
        }

        // Show the dialog and create a new project if the user completed the
        // dialog.
        ProjectDialog dialog = new ProjectDialog(panel.getDriver(), Settings.getSetting( "action.newproject.title" ) , true, result);
        result = dialog.getResult();
        if (result.getIsCompleted())
        {
            try
            {
                ProjectInterface project =
                        ProjectService.getInstance().addProject(result.getName());
                project.setStartDate(result.getStartDate());
                panel.addProject(project);

                // This is where we would assign the ResourcePool
                // Since this is not needed now, we assign the static one
                // Code is ugly, yet temporary.
                try {
                    PersistanceService.getInstance().loadProject();
                    Collection c = ResourceService.getInstance().listResourcesPools();
                    Object[] pools = c.toArray();

                    if( pools.length > 0 )
                    {
                        ResourcePool pool = (ResourcePool) pools[0];
                        project.setResourcePoolkey( pool.getResourcePoolKey() );
                    }
                    else
                    {
                        // TODO
                        // no resource pool?
                        new Exception("No resource pool").printStackTrace();
                    }
                }
                catch (ResourceException e1)
                {
                    e1.printStackTrace();
                }
                catch (PersistanceException e1)
                {
                    e1.printStackTrace();
                }
            }
            catch (InternalProjectException ex)
            {
                log.error(ex.toString());
            }
        }
    }
}

/*
 *  $Log: NewProjectAction.java,v $
 *  Revision 1.9  2005/04/16 18:31:48  achou
 *  Changed Project and Activity references to use their interfaces.
 *
 *  Revision 1.8  2005/04/16 02:29:10  dnathan
 *  Minor clean up.
 *
 *  Revision 1.7  2005/04/14 04:46:24  dnathan
 *  Externalized Strings.
 *
 *  Revision 1.6  2005/03/30 16:14:46  dnathan
 *  Added serialization field to supress warnings.
 *
 *  Revision 1.5  2005/03/30 13:29:15  achou
 *  Added comments and log4j (if needed).
 *
 *  Revision 1.4  2005/03/30 02:36:22  dnathan
 *  Added current date as start date.
 *
 *  Revision 1.3  2005/03/29 09:10:51  achou
 *  Set start date when creating project.
 *
 *  Revision 1.2  2005/03/29 08:52:06  achou
 *  Removed unneeded imports.
 *
 *  Revision 1.1  2005/03/29 04:42:35  achou
 *  Actions for PM-275
 *
 */
