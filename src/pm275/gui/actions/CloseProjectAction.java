/*
 *  File      :  CloseProjectAction.java
 *  Version   :  $Id: CloseProjectAction.java,v 1.4 2005/04/14 04:46:24 dnathan Exp $
 *  Author    :  Alex Chou
 *  Assignment:  PM-275
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */

package pm275.gui.actions;

import java.awt.event.ActionEvent;

import pm275.gui.panels.MainPanel;


/**
 *  Action for closing the current project.
 */
public class CloseProjectAction extends AbstractPmAction
{
    /**
	 * This field auto-generated by Eclipse.  DN.
	 */
	private static final long serialVersionUID = 3257572788914237750L;

	/**
     *  Creates the action for closing the current project.
     *  @param panel Main panel containing this action.
     */
    public CloseProjectAction(MainPanel panel)
    {
        super(panel, "action.closeproject" );
    }

    /**
     *  Handles the action by closing the current project.
     */
    public void actionPerformed(ActionEvent e)
    {
        MainPanel panel = getPanel();
        panel.closeCurrentProject();
    }
}

/*
 *  $Log: CloseProjectAction.java,v $
 *  Revision 1.4  2005/04/14 04:46:24  dnathan
 *  Externalized Strings.
 *
 *  Revision 1.3  2005/03/30 16:14:46  dnathan
 *  Added serialization field to supress warnings.
 *
 *  Revision 1.2  2005/03/30 13:29:15  achou
 *  Added comments and log4j (if needed).
 *
 *  Revision 1.1  2005/03/29 08:51:11  achou
 *  New project actions.
 *
 */
