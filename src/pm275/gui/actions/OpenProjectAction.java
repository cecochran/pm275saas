/*
 *  File      :  OpenProjectAction.java
 *  Version   :  $Id: OpenProjectAction.java,v 1.4 2005/05/15 23:29:26 tlong2 Exp $
 *  Author    :  Jennifer Keiser
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */
package pm275.gui.actions;

import java.awt.event.ActionEvent;
import java.util.Iterator;
import org.apache.log4j.Logger;
import pm275.core.project.ProjectAbba;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ProjectService;
import pm275.gui.common.Settings;
import pm275.gui.common.SwingUtilities;
import pm275.gui.dialogs.OpenProjectDialog;
import pm275.gui.dialogs.OpenProjectDialogResult;
import pm275.gui.dialogs.ParameterFormatException;
import pm275.gui.panels.MainPanel;

/**
 *  Action for opening a project from the database.  
 */
public class OpenProjectAction extends AbstractPmAction{
	
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(OpenProjectAction.class);
    private static final String ATTR = "OpenProjectAction";

    /**
     *  Creates the action for opening a project.
     *  @param panel Main panel containing this action.
     */
    public OpenProjectAction(MainPanel panel)
    {
        super(panel, "action.openproject" );
    }
    
    /**
     *  Handles the action by ...
     *
     *  @param e Event generated by this action.
     */
    public void actionPerformed(ActionEvent e)
    {
        OpenProjectDialogResult result = new OpenProjectDialogResult();
        result.setIsCompleted(false);
        MainPanel panel = getPanel();
        Iterator projects = ProjectService.getInstance().getProjectAbba().iterator();
        
        while( projects.hasNext() )
        {
            try 
            {
                ProjectAbba project = (ProjectAbba) projects.next();
                result.addProjectNameChoice( project );
            }
            catch (ParameterFormatException e1) 
            {
            }
        }
                
        OpenProjectDialog dialog = new OpenProjectDialog(panel.getDriver(), Settings.getSetting( "action.openproject.title" ) , true, result);
        
        if( result.getIsCompleted() )
        {
            ProjectInterface project = null;
            ProjectAbba openProject = result.getSelectedProject();

            try 
            {
                ProjectService.getInstance().loadProjectDB( openProject.getKey() );
                project = ProjectService.getInstance().getProject( openProject.getKey() );
                
                if( project != null )
                {
                    panel.addProject( project );
                }
            }
            catch ( Exception e1 ) 
            {
                SwingUtilities.reportErrorMessage( "Could not open project" );
                return;
            }
        }
    }
}
/**
 * $Log: OpenProjectAction.java,v $
 * Revision 1.4  2005/05/15 23:29:26  tlong2
 * cleaned up compile warnings
 *
 * Revision 1.3  2005/05/09 01:56:43  dnathan
 * Added code to serialize from DB.
 *
 * Revision 1.2  2005/05/01 20:47:41  dnathan
 * Implementation.  Awaiting core changes.
 *
 * Revision 1.1  2005/04/24 12:42:15  jkeiser
 * Initial checkin.  Currently a shell; action still needs to be implemented.
 *
 *
**/