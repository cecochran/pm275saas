/*
 *  File      :  AbstractPmAction.java
 *  Version   :  $Id: AbstractPmAction.java,v 1.3 2005/04/14 04:46:24 dnathan Exp $
 *  Author    :  Alex Chou
 *  Assignment:  PM-275
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */

package pm275.gui.actions;

import java.lang.reflect.Method;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import pm275.gui.common.Settings;
import pm275.gui.panels.MainPanel;


/**
 *  Abstract base class for all the actions in PM-275.  Provides an attribute
 *  and get method for the application's main panel.
 */
public abstract class AbstractPmAction extends AbstractAction
{
    private final MainPanel panel;

    /**
     *  Creates an action with the specified label and remembers the main
     *  panel.
     *  @param panel Main panel to which the action belongs.
     *  @param label Text label for the action.
     */
    public AbstractPmAction(MainPanel panel, String label)
    {
        super( Settings.getSetting( label + ".title" ) , getIcon( Settings.getSetting( label + ".icon" ) ) );
        this.panel = panel;
    }

    /**
     *  Returns the main panel that holds this action.
     *  @return Main panel containing this action.
     */
    protected final MainPanel getPanel()
    {
        return panel;
    }
    
    private static Icon getIcon( String file )
	{
		try {
			Method method = Thread.class.getMethod( "getContextClassLoader" , (Class[]) null );
			ClassLoader loader = (ClassLoader) method.invoke( Thread.currentThread() , (Object[]) null );

			if( loader.getResource( file ) != null )
			{
				return new ImageIcon(loader.getResource( file ));
			}
			else
			{
				return null;
			}
		}
		catch (Exception e) 
		{
			return null;
		}
	}
}

/*
 *  $Log: AbstractPmAction.java,v $
 *  Revision 1.3  2005/04/14 04:46:24  dnathan
 *  Externalized Strings.
 *
 *  Revision 1.2  2005/03/30 13:29:14  achou
 *  Added comments and log4j (if needed).
 *
 *  Revision 1.1  2005/03/29 04:42:35  achou
 *  Actions for PM-275
 *
 */
