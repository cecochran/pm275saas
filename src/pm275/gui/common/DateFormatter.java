/***************************************************
 * File:        $RCSfile: DateFormatter.java,v $
 * @author      $Author: dnathan $,
 * @version     $Id: DateFormatter.java,v 1.8 2005/03/30 16:03:33 dnathan Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:    Functions for converting Dates to Strings and Strings to Dates.
 * </p>
 * <p>
 * Purpose:     This file contains functions needed to handle dates the same throughout
 *              the user interface.
 * </p>
 */
package pm275.gui.common;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;

public class DateFormatter
{
    // The PM-275 standard date format.
    private static final DateFormat DATE_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd");


    /**
     * Converts a Date into a PM-275 standard date string.
     * @param theDate The date to convert.
     * @return The date converted to a PM-275 standard date string.
     */
    public static String getStringForDate( Date theDate )
    {
        String dateStr = "";
        if( theDate != null )
        {
            dateStr = DATE_FORMAT.format(theDate);
        }
        return dateStr;
    }

    /**
     * Converts a PM-275 standard date string into a Date.
     * @param theString PM-275 standard date string to convert.
     * @return A Date set to the date defined in the String.
     * @throws ParseException If the date is not in the correct format.
     */
    public static Date getDateForString( String theString )
            throws ParseException
    {
        Date date = null;
        if ((theString != null) && (theString.trim().length() > 0))
        {
            date = DATE_FORMAT.parse(theString);
        }
        return date;
    }


    // I'm overwriting Dan's version 1.6 check-in.  His version is commented
    // out below, in case we need to go or refer back to it later.

    /*
    private static final String separator = " - ";
    */

    /**
     * Converts a Date into a PM-275 standard date string.
     * @param theDate The date to convert.
     * @return The date converted to a PM-275 standard date string.
     */
    /*
    public static String getStringForDate( Date theDate )
    {
        if( theDate == null )
        {
            return "";
        }

        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime( theDate );

        String year = Integer.toString( calendar.get( Calendar.YEAR ) );
        String month = Integer.toString( calendar.get( Calendar.MONTH ) );
        String day = Integer.toString( calendar.get( Calendar.DAY_OF_MONTH ) );

        StringBuffer buff = new StringBuffer();
        buff.append( year );
        buff.append( separator );
        buff.append( month );
        buff.append( separator );
        buff.append( day );

        return buff.toString();

    }
    */

    /**
     * Converts a PM-275 standard date string into a Date.
     * @param theString PM-275 standard date string to convert.
     * @return A Date set to the date defined in the String.
     */
    /*
    public static Date getDateForString( String theString )
    {
        if( theString == null )
        {
            return null;
        }

        if( theString.trim().equals("") )
        {
            return null;
        }

        String[] parts = theString.split( separator );

        if( parts.length == 3 )
        {
            String normalDateString = parts[1] + "/" + parts[2] + "/" + parts[0];

            try
            {
                return new Date( Date.parse( normalDateString ) );
            }
            catch( Exception e )
            {
                return null;
            }
        }
        else
        {
            try
            {
                return new Date( Date.parse( theString ) );
            }
            catch( Exception e )
            {
                return null;
            }
        }
    }
    */
}


/**
 * $Log: DateFormatter.java,v $
 * Revision 1.8  2005/03/30 16:03:33  dnathan
 * Removed unneeded imports.
 *
 * Revision 1.7  2005/03/29 04:38:16  achou
 * Changed implementation to use SimpleDateFormat instead of parsing/formatting date by hand.
 *
 * Revision 1.6  2005/03/29 03:17:11  dnathan
 * Moved to GregorianCalendar.
 *
 * Revision 1.5  2005/03/29 01:55:13  dnathan
 * Added JavaDoc comments.
 *
 * Revision 1.4  2005/03/29 01:34:18  dnathan
 * Added check for null/empty string.
 *
 * Revision 1.3  2005/03/29 01:30:52  dnathan
 * Added check for null Date.
 *
 * Revision 1.2  2005/03/29 01:17:05  dnathan
 * Made methods static.
 *
 * Revision 1.1  2005/03/29 01:16:40  dnathan
 * New file.
 *
 *
 **/
