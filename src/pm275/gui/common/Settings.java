package pm275.gui.common;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Properties;

public class Settings 
{
	private Settings()
	{}
	
	public static String getSetting( String name )
	{
		String setting = ""; 
		
		try {
			Method method = Thread.class.getMethod( "getContextClassLoader" , (Class[]) null );
			ClassLoader loader = (ClassLoader) method.invoke( Thread.currentThread() , (Object[]) null );
			if( loader.getResource( "gui.properties" ) != null )
			{
				String url = loader.getResource( "gui.properties" ).getFile();
				Properties p = new Properties();
				p.load( new FileInputStream( new File(url) ) );
				return p.getProperty( name );
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return setting;
	}
}
