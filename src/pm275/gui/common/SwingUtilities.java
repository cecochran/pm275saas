/*
 *  File      :  SwingUtilities.java
 *  Version   :  $Id: SwingUtilities.java,v 1.1 2005/04/18 15:19:44 achou Exp $
 *  Author    :  Alex Chou
 *  Assignment:  PM-275
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */

package pm275.gui.common;

import javax.swing.JOptionPane;


/**
 *  This class contains various Swing utilities and convenience methods.
 */
public final class SwingUtilities
{
    /**
     *  Reports an exception by displaying its message in a dialog.  Calls
     *  reportErrorMessage().
     *  @param ex The exception to report.
     */
    public static final void reportException(Exception ex)
    {
        reportErrorMessage(ex.getMessage());
    }

    /**
     *  Displays an error message in a dialog.
     *  @param message The message to display.
     */
    public static final void reportErrorMessage(String message)
    {
        JOptionPane.showMessageDialog(null, message,
                Settings.getSetting("error.dialog.title"),
                JOptionPane.ERROR_MESSAGE);
    }
}

/*
 *  $Log: SwingUtilities.java,v $
 *  Revision 1.1  2005/04/18 15:19:44  achou
 *  Initial check-in
 *
 */