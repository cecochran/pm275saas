/***************************************************
 * File:        $RCSfile: HolidayDialogResult.java,v $
 * @author      $Author: jkeiser $,
 * @version     $Id: HolidayDialogResult.java,v 1.2 2005/05/15 00:00:43 jkeiser Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:    A class for communicating between the main GUI and the holiday edit/create dialog.
 * </p>
 * <p>
 * Purpose:     This class is used to communicate Holiday details between the 
 *              holiday dialogs and the application.
 * </p>
 */
package pm275.gui.dialogs;

import org.apache.log4j.Logger;
import java.util.Date;
import pm275.core.calendar.Holiday;

public class HolidayDialogResult
{
    private static final Logger log = Logger.getLogger(HolidayDialogResult.class);
    private static final String ATTR = "HolidayDialogResult";
    private Holiday holiday = null;
    private String name = " ";
    private Date date = new Date( System.currentTimeMillis() );
    private boolean isCompleted = false;

    public HolidayDialogResult()
    {
    }

    /**
     * @return Returns the holiday object
     */
    public Holiday getHoliday()
    {
        return holiday;
    }

    /**
     * @param hol The holiday to set.
     * @throws ParameterFormatException Thrown if the holiday is null.
     */
    public void setHoliday(Holiday hol) throws ParameterFormatException
    {
        if( hol == null )
        {
            throw new ParameterFormatException();
        }

        this.holiday = hol;
        //may be redunant, but set these just in case
        setDate(holiday.getDate());
        setName(holiday.getName());
    }
    
    /**
     * @return Returns the name.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name The name to set.
     * @throws ParameterFormatException Thrown if the name is null or empty.
     */
    public void setName(String name) throws ParameterFormatException
    {
        if( name == null )
        {
            throw new ParameterFormatException();
        }

        if( name.length() == 0 )
        {
            throw new ParameterFormatException();
        }

        this.name = name;
    }

    /**
     * @return Returns the holiday date.
     */
    public Date getDate()
    {
        return date;
    }

    /**
     * @param startDate The startDate to set.
     * @throws ParameterFormatException Thrown if the date is null.
     */
    public void setDate(Date holidayDate) throws ParameterFormatException
    {
        if( holidayDate == null )
        {
            throw new ParameterFormatException();
        }

        this.date = holidayDate;
    }

    /**
     *  Returns true if the user completed the dialog (by hitting "OK"),
     *  false otherwise.
     *  @return If the user completed the dialog.
     */
    public boolean getIsCompleted()
    {
        return isCompleted;
    }

    /**
     *  Sets the flag indicating whether the user completed the login dialog.
     *  @param isCompleted True if the user completed the login dialog, false
     *                     otherwise.
     */
    public void setIsCompleted(boolean isCompleted)
    {
        this.isCompleted = isCompleted;
    }

    /**
     *  Convenience method to create a project dialog result from a project.
     *  @param project Project from which to create a dialog result instance.
     *  @return Dialog result object with the parameters set based on the project.
     */
    public final HolidayDialogResult fromHoliday(Holiday holiday)
    {
        // Set the name.
        try
        {
            this.setName(holiday.getName());
        }
        catch (ParameterFormatException ex)
        {
            log.error(ex.getMessage());        }

        // Set the date.
        try
        {
            this.setDate(holiday.getDate());
        }
        catch (ParameterFormatException ex)
        {
            log.error(ex.getMessage());
        }
       
        // Set the holiday.
        try
        {
            this.setHoliday(holiday);
        }
        catch (ParameterFormatException ex)
        {
            log.error(ex.getMessage());
        }        
        
        this.setIsCompleted(false);
        return this;
    }
}

/**
 * $Log: HolidayDialogResult.java,v $
 * Revision 1.2  2005/05/15 00:00:43  jkeiser
 * Fixed AWT error in fromHoliday.
 *
 * Revision 1.1  2005/05/07 11:35:24  jkeiser
 * Initial checkin
 *
 *
 */
