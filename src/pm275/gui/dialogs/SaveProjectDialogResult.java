/*
 *  File      :  SaveProjectDialogResult.java
 *  Version   :  $Id: SaveProjectDialogResult.java,v 1.2 2005/05/01 18:15:12 dnathan Exp $
 *  Author    :  Dan Nathan
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */
package pm275.gui.dialogs;

public class SaveProjectDialogResult 
{
    private boolean isCompleted = false;
    private String name = null;

    /**
     *  Returns true if the user completed the login dialog (by hitting "OK"),
     *  false otherwise.
     *  @return If the user completed the login dialog.
     */
    public boolean getIsCompleted()
    {
        return isCompleted;
    }

    /**
     *  Sets the flag indicating whether the user completed the login dialog.
     *  @param isCompleted True if the user completed the login dialog, false
     *                     otherwise.
     */
    public void setIsCompleted(boolean isCompleted)
    {
        this.isCompleted = isCompleted;
    }

    /**
     * @return Returns the name.
     */
    public String getName() 
    {
        return name;
    }
    

    /**
     * @param name The name to set.
     */
    public void setName(String name) throws ParameterFormatException
    {
        if( name == null )
        {
            throw new ParameterFormatException();
        }
        
        if( name.length() == 0 )
        {
            throw new ParameterFormatException();
        }
        
        this.name = name;
    }
    
}

/**
 * $Log: SaveProjectDialogResult.java,v $
 * Revision 1.2  2005/05/01 18:15:12  dnathan
 * Added additional check for empty string name.
 *
 * Revision 1.1  2005/04/30 15:52:22  dnathan
 * New file.
 *
 *
 **/