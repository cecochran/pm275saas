/***************************************************
 * File:        $RCSfile: CalendarDialog.java,v $
 * @author      $Author: dnathan $,
 * @version     $Id: CalendarDialog.java,v 1.10 2005/05/15 18:00:59 dnathan Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:    A JDialog for collecting information about a calendar.
 * </p>
 * <p>
 * Purpose:     To allow a user to enter or edit calendar details.
 * </p>
 */
package pm275.gui.dialogs;

import org.apache.log4j.Logger;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import java.lang.reflect.Method;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.border.TitledBorder;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import pm275.core.calendar.CalendarService;
import pm275.core.calendar.Holiday;
import pm275.gui.actions.NewHolidayAction;
import pm275.gui.actions.EditHolidayAction;
import pm275.gui.actions.RemoveHolidayAction;
import pm275.gui.common.SpringUtilities;
import pm275.gui.dialogs.HolidayModel;

public class CalendarDialog extends JDialog implements ActionListener, KeyListener, ListDataListener
{
    private static final Logger log = Logger.getLogger(CalendarDialog.class);
    private static final String ATTR = "CalendarDialog";
	protected CalendarService calendarService = CalendarService.getInstance();
	protected CalendarDialogResult dialogConfig = null;

    private static final int SUNDAY = java.util.Calendar.SUNDAY;
    private static final int MONDAY = java.util.Calendar.MONDAY;
    private static final int TUESDAY = java.util.Calendar.TUESDAY;
    private static final int WEDNESDAY = java.util.Calendar.WEDNESDAY;
    private static final int THURSDAY = java.util.Calendar.THURSDAY;
    private static final int FRIDAY = java.util.Calendar.FRIDAY;
    private static final int SATURDAY = java.util.Calendar.SATURDAY;

	protected JPanel namePanel = null;
	protected JPanel workWeekPanel = null;
	protected JPanel holidaysPanel = null;
	protected JPanel holidaysButtonPanel = null;
	protected JPanel buttonPanel = null;

	protected HolidayModel holidayModel = null;
	protected NewHolidayAction newHolidayAction = null;
	protected EditHolidayAction editHolidayAction = null;
	protected RemoveHolidayAction removeHolidayAction = null;

    protected JButton okButton = null;
    protected JButton cancelButton = null;
    protected JButton addButton = null;
    protected JButton editButton = null;
    protected JButton deleteButton = null;

    protected JLabel nameLabel = null;
	protected JTextField nameField = null;
    protected JCheckBox sundayCheckBox = null;
    protected JCheckBox mondayCheckBox = null;
    protected JCheckBox tuesdayCheckBox = null;
    protected JCheckBox wednesdayCheckBox = null;
    protected JCheckBox thursdayCheckBox = null;
    protected JCheckBox fridayCheckBox = null;
    protected JCheckBox saturdayCheckBox = null;
    protected JLabel hoursPerDayLabel = null;
    protected JTextField hoursPerDayField = null;
    protected JList holidayList = null;
    protected JScrollPane holidayListScroll = null;

    protected boolean inTesting = false;
    protected boolean hasError = false;

    public static void main( String[] args )
    {
        JFrame frame = new JFrame();
        String dialogTitle = "Calendar Details";
        CalendarDialog dialog = new CalendarDialog( frame , dialogTitle , false , null );
        dialog.inTesting = false;
        dialog.hasError = false;
        dialog.show();
    }

    /**
     * @param owner     The Frame to use as the parent.
     * @param title     The caption displayed in titlebar of the application.
     * @param modal     Whether the dialog should be modal.
     * @param config    If an edit, the settings to edit.  Else an empty result.
     */
    public CalendarDialog( Frame owner , String title , boolean modal , CalendarDialogResult config )
    {
        super( owner , title , modal );
        this.setUndecorated(true);
        this.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);

        if( config == null )
        {
            this.dialogConfig = new CalendarDialogResult(true);
            //TODO this shouldn't get called, but if it does, it's missing a
            //call to dialogConfig.setMainPanel (not available from here)
        }
        else
        {
            this.dialogConfig = config;
            this.dialogConfig.setIsCompleted(false);
        }

        initComponents();

        // center the dialog over the calling window
        this.setLocationRelativeTo( owner );
        this.show();
    }


    /**
     * Create the UI components.
     */
    private void initComponents()
    {
        this.nameField = new JTextField( this.dialogConfig.getName() , 10 );
        this.nameField.addKeyListener( this );
		SpringLayout sl = new SpringLayout();

        Container contentPane = getContentPane();
        contentPane.setLayout( sl );

        this.nameField = new JTextField( this.dialogConfig.getName(), 30 );
		this.nameField.addKeyListener(this);
        this.nameLabel = new JLabel( " Calendar Name:" );
		this.nameLabel.setLabelFor( this.nameField );

        this.hoursPerDayField = new JTextField( String.valueOf(this.dialogConfig.getHoursPerDay()) , 10 );
		this.hoursPerDayField.addKeyListener(this);
        this.hoursPerDayLabel = new JLabel( " Hours Per Day:" );
		this.hoursPerDayLabel.setLabelFor( this.hoursPerDayField );

		this.sundayCheckBox = new JCheckBox( "Sun", this.dialogConfig.getWorkDay(1));
		this.mondayCheckBox = new JCheckBox( "Mon", this.dialogConfig.getWorkDay(2));
		this.tuesdayCheckBox = new JCheckBox( "Tue", this.dialogConfig.getWorkDay(3));
		this.wednesdayCheckBox = new JCheckBox( "Wed", this.dialogConfig.getWorkDay(4));
		this.thursdayCheckBox = new JCheckBox( "Thu", this.dialogConfig.getWorkDay(5));
		this.fridayCheckBox = new JCheckBox( "Fri", this.dialogConfig.getWorkDay(6));
		this.saturdayCheckBox = new JCheckBox( "Sat", this.dialogConfig.getWorkDay(7));

        //this.holidayModel = new HolidayModel(this.dialogConfig.listHolidays());
		this.holidayModel = new HolidayModel(null);
		Collection existingHolidays = this.dialogConfig.listHolidays();
		for (Iterator i = existingHolidays.iterator(); i.hasNext();)
		{
			this.addHoliday((Holiday) i.next());
		}
        this.holidayList = new JList(this.holidayModel);
        this.holidayModel.addListDataListener(this);
        this.holidayList.setBorder( BorderFactory.createLineBorder( Color.BLACK , 1  ) );
        this.holidayListScroll = new JScrollPane(this.holidayList);
        this.holidayListScroll.setPreferredSize( new Dimension(250,100));

        //create the actions
        newHolidayAction = new NewHolidayAction(dialogConfig.getMainPanel(), this);
        editHolidayAction = new EditHolidayAction(dialogConfig.getMainPanel(), this);
        removeHolidayAction = new RemoveHolidayAction(dialogConfig.getMainPanel(), this);

        this.okButton = new JButton( "OK" );
        this.okButton.addActionListener( this );

        this.cancelButton = new JButton( "Cancel" );
        this.cancelButton.addActionListener( this );

		this.addButton = new JButton( "Add" );
		this.addButton.setAction( newHolidayAction );
        this.addButton.addActionListener( this );

		this.editButton = new JButton( "Edit" );
		this.editButton.setAction( editHolidayAction );
		this.editButton.addActionListener( this );

		this.deleteButton = new JButton( "Delete" );
		this.deleteButton.setAction( removeHolidayAction );
        this.deleteButton.addActionListener( this );

        //create the sub-panels
        this.namePanel = new JPanel();
        this.namePanel.setLayout( new SpringLayout() );
        this.namePanel.add( this.nameLabel );
        this.namePanel.add( this.nameField );
        this.namePanel.add( this.hoursPerDayLabel );
        this.namePanel.add( this.hoursPerDayField );
        SpringUtilities.makeCompactGrid( this.namePanel ,
                2, 2,   //rows, cols
                6, 6,   //initX, initY
                6, 6);  //xPad, yPad

        this.workWeekPanel = new JPanel();
        this.workWeekPanel.setLayout( new SpringLayout() );
        workWeekPanel.add( this.sundayCheckBox );
        workWeekPanel.add( this.mondayCheckBox );
        workWeekPanel.add( this.tuesdayCheckBox );
        workWeekPanel.add( this.wednesdayCheckBox );
        workWeekPanel.add( this.thursdayCheckBox );
        workWeekPanel.add( this.fridayCheckBox );
        workWeekPanel.add( this.saturdayCheckBox );
        TitledBorder workWeekTitle = BorderFactory.createTitledBorder("Work Week");
        workWeekPanel.setBorder(workWeekTitle);

        SpringUtilities.makeCompactGrid( this.workWeekPanel ,
                1, 7,   //rows, cols
                6, 6,   //initX, initY
                6, 6);  //xPad, yPad

        this.holidaysPanel = new JPanel();
        this.holidaysPanel.setLayout( new SpringLayout() );
        this.holidaysPanel.setName( "Holidays" );
        this.holidaysPanel.add( this.holidayListScroll );

        this.holidaysButtonPanel = new JPanel();
        this.holidaysButtonPanel.setLayout( new SpringLayout() );
        this.holidaysButtonPanel.add( this.addButton );
        this.holidaysButtonPanel.add( this.editButton );
        this.holidaysButtonPanel.add( this.deleteButton );
        SpringUtilities.makeCompactGrid( this.holidaysButtonPanel ,
                3, 1,   //rows, cols
                6, 6,   //initX, initY
                6, 6);  //xPad, yPad
        this.holidaysPanel.add( this.holidaysButtonPanel );
        TitledBorder holidayTitle = BorderFactory.createTitledBorder("Holidays");
        holidaysPanel.setBorder(holidayTitle);

        SpringUtilities.makeCompactGrid( this.holidaysPanel ,
                1, 2,   //rows, cols
                6, 6,   //initX, initY
                6, 6);  //xPad, yPad

        this.buttonPanel = new JPanel();
        this.buttonPanel.setLayout( new SpringLayout() );
        this.buttonPanel.add( this.cancelButton );
        this.buttonPanel.add( this.okButton );
        SpringUtilities.makeCompactGrid( this.buttonPanel ,
                1, 2,   //rows, cols
                6, 6,   //initX, initY
                6, 6);  //xPad, yPad

        contentPane.add( this.namePanel);
        contentPane.add( this.workWeekPanel);
        contentPane.add( this.holidaysPanel);
        contentPane.add( this.buttonPanel);
        SpringUtilities.makeCompactGrid( this.getContentPane() ,
                4, 1,   //rows, cols
                6, 6,   //initX, initY
                6, 6);  //xPad, yPad

        this.pack();
        this.setSize( this.getPreferredSize() );
        this.nameField.grabFocus();
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed( ActionEvent event )
    {
        if( event.getSource().equals( this.okButton ) )
        {
            ok();
        }

        if( event.getSource().equals( this.cancelButton ) )
        {
            cancel();
        }

        if( event.getSource().equals( this.addButton ) )
        {
            //action is tied to button
        	this.holidayList.setListData(new Vector(dialogConfig.listHolidays()));

        }
        if( event.getSource().equals( this.editButton ) )
        {
            //action is tied to button
        }

        if( event.getSource().equals( this.deleteButton ) )
        {
            //action is tied to button
        }
    }

    /**
     * Called when the user presses the OK button.
     */
    private void ok()
    {
        try
        {
            this.dialogConfig.setName( this.nameField.getText() );
        }
        catch (ParameterFormatException e)
        {
            if( this.inTesting )
            {
                this.hasError = true;
                return;
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Please enter a valid name" , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
                return;
            }
        }

        //set hours per day
        try
        {
            int enteredHours = Integer.parseInt( this.hoursPerDayField.getText() );
            this.dialogConfig.setHoursPerDay(enteredHours);

        }
        catch (ParameterFormatException e)
        {
            if( this.inTesting )
            {
                this.hasError = true;
                return;
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Please enter a number of hours between 0 and 24" , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
                return;
            }
        }

        try {
        	dialogConfig.setWorkDay(SUNDAY, sundayCheckBox.isSelected());
        	dialogConfig.setWorkDay(MONDAY, mondayCheckBox.isSelected());
        	dialogConfig.setWorkDay(TUESDAY, tuesdayCheckBox.isSelected());
        	dialogConfig.setWorkDay(WEDNESDAY, wednesdayCheckBox.isSelected());
        	dialogConfig.setWorkDay(THURSDAY, thursdayCheckBox.isSelected());
        	dialogConfig.setWorkDay(FRIDAY, fridayCheckBox.isSelected());
        	dialogConfig.setWorkDay(SATURDAY, saturdayCheckBox.isSelected());
        }
        catch (ParameterFormatException e)
        {
            if( this.inTesting )
            {
                this.hasError = true;
                return;
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Internal error: invalid valid day of week" , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
                return;
            }
        }

        this.dialogConfig.setIsCompleted(true);
        this.setVisible( false );
    }

    /**
     * Called when the user presses the Cancel button.
     */
    private void cancel()
    {
        this.dialogConfig.setIsCompleted(false);
        this.setVisible( false );
    }

    /**
     * @return The config being used.
     */
    public CalendarDialogResult getResult()
    {
        return this.dialogConfig;
    }

    /**
     * @return The holiday currently selected.
     */
    public Holiday getSelectedHoliday()
    {
    	if (this.holidayList.getSelectedIndex() < 0)
    		return null;
		else
		{
			return (Holiday) this.holidayList.getSelectedValue();
		}
    }
    /**
     * @param The holiday to add.
     */
    public void addHoliday(Holiday hol)
    {
        this.getResult().addHoliday(hol);
        holidayModel.addItem(hol);
    }

    /**
     * @param The holiday to edit.
     */
    public void editHoliday(Holiday hol)
    {
        //most editing is done in EditHolidayAction
        holidayModel.removeItem(holidayList.getSelectedIndex());
        holidayModel.addItem(hol);
    }

    /**
     * @param The holiday to remove.
     */
    public void removeHoliday(Holiday hol)
    {
    	this.getResult().removeHoliday(hol);
        holidayModel.removeItem(holidayList.getSelectedIndex());
    }

    public void contentsChanged(ListDataEvent event)
    {
    	this.holidayList.setListData(new Vector(dialogConfig.listHolidays()));
    }

    public void intervalAdded(ListDataEvent event)
    {
    	this.holidayList.setListData(new Vector(dialogConfig.listHolidays()));
    }

    public void intervalRemoved(ListDataEvent event)
    {
    	this.holidayList.setListData(new Vector(dialogConfig.listHolidays()));
    }

    /* (non-Javadoc)
     * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
     */
    public void keyPressed(KeyEvent e)
    {
        if( e.getKeyCode() == KeyEvent.VK_ENTER )
        {
            ok();
        }
        if( e.getKeyCode() == KeyEvent.VK_ESCAPE )
        {
            cancel();
        }
    }


    /* (non-Javadoc)
     * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
     */
    public void keyReleased(KeyEvent e)
    {
        // noop
    }


    /* (non-Javadoc)
     * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
     */
    public void keyTyped(KeyEvent e)
    {
        //noop
    }

    private static Icon getIcon( String file )
    {
        try {
            Method method = Thread.class.getMethod( "getContextClassLoader" , (Class[]) null );
            ClassLoader loader = (ClassLoader) method.invoke( Thread.currentThread() , (Object[]) null );

            if( loader.getResource( file ) != null )
            {
                return new ImageIcon(loader.getResource( file ));
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
/**
* $Log: CalendarDialog.java,v $
* Revision 1.10  2005/05/15 18:00:59  dnathan
* Moved out System.err.println's
*
* Revision 1.9  2005/05/15 00:04:13  jkeiser
* Fixed ClassCastException in getSelectedHoliday.
*
* Revision 1.8  2005/05/12 03:25:21  jkeiser
* Changed call to CalendarDialogResult to support updated constructor.
*
* Revision 1.7  2005/05/09 09:32:41  jkeiser
* Changed to support update of HolidayModel.  Removed debugging statements.
*
* Revision 1.6  2005/05/08 22:20:45  jkeiser
* Added support for edit and removal of holidays.
*
* Revision 1.5  2005/05/08 14:20:46  jkeiser
* Added debugging messages.
*
* Revision 1.4  2005/05/08 01:55:48  jkeiser
* Enhanced to support management of holdays
*
* Revision 1.3  2005/05/07 11:51:41  jkeiser
* Set this dialog to be a ListActionListener for the list of holidays.
*
* Revision 1.2  2005/05/01 16:02:09  jkeiser
* Enhanced ok() method to save work week.
*
* Revision 1.1  2005/04/30 22:23:42  jkeiser
* Initial checkin
*
*
**/
