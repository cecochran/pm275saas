/*
 * File      :  HolidayModel.java
 * @author   :  $Author: jkeiser $,
 * @version  :  $Id: HolidayModel.java,v 1.3 2005/05/15 00:05:35 jkeiser Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * Contents  :  Please see class comments below.
 * Purpose   :  Please see class comments below.
 */

package pm275.gui.dialogs;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import pm275.core.calendar.Holiday;

/*
 * Holds information to be used in a List of holidays in the CalendarDialog 
 */
class HolidayModel implements ListModel
{
    private List holidays;
    private List listeners;
    private int size;

    public HolidayModel( Collection holidays )
    {
        if (holidays != null)
        {
        	this.holidays = new LinkedList(holidays);
        	this.size = holidays.size();
        }
        else
        {
        	this.holidays = new LinkedList();
        	this.size = 0;
        }
        this.listeners = new Vector();
    }

    public int getSize()
    {
        return this.size;
    }

    public Object getElementAt(int index)
    {
        Holiday h = (Holiday) this.holidays.get( index );
        return h;
    }

    public void addListDataListener(ListDataListener l)
    {
        this.listeners.add(l);
    }

    public void removeListDataListener(ListDataListener l)
    {
        this.listeners.remove(l);
    }

    public Holiday getItem( int index )
    {
        return (Holiday) this.holidays.get( index );
    }

    public synchronized void removeItem( int index )
    {
        this.size -= 1;
        this.holidays.remove( index );
        ListDataEvent e = new ListDataEvent( this , ListDataEvent.INTERVAL_REMOVED , index, index );

        Iterator i = listeners.iterator();
        while(i.hasNext())
        {
            ListDataListener listener = (ListDataListener) i.next();
            listener.intervalRemoved(e);
        }
    }

    public synchronized void addItem( Holiday h )
    {
        this.holidays.add( h );
        ListDataEvent e = new ListDataEvent( this , ListDataEvent.INTERVAL_ADDED , this.size, this.size );
        this.size += 1;

        Iterator i = listeners.iterator();
        while(i.hasNext())
        {
            ListDataListener listener = (ListDataListener) i.next();
            listener.intervalAdded(e);
        }
    }
}
/**
* $Log: HolidayModel.java,v $
* Revision 1.3  2005/05/15 00:05:35  jkeiser
* Modified constructor to allow null list of holidays to be passed in.
*
* Revision 1.2  2005/05/08 14:20:31  jkeiser
* Additional cleanup.  (Had some incorrect references to Resource.)
*
* Revision 1.1  2005/05/07 11:34:14  jkeiser
* Initial checkin
*
*
**/