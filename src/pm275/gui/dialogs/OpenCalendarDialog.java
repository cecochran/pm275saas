/*
 *  File      :  OpenCalendarDialog.java
 * @author    :  $Author: tlong2 $,  
 * @version   :  $Id: OpenCalendarDialog.java,v 1.2 2005/05/15 23:29:42 tlong2 Exp $
 *  Author    :  Jennifer Keiser
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */
package pm275.gui.dialogs;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import pm275.core.calendar.CalendarService;
import pm275.core.calendar.CalendarInterface;
import pm275.gui.common.SpringUtilities;

public class OpenCalendarDialog  extends JDialog implements ActionListener, KeyListener
{
    protected OpenCalendarDialogResult dialogConfig = null;
    protected JPanel calendarPanel = null;
    protected JPanel buttonPanel = null;
    
    protected JButton okButton = null;
    protected JButton cancelButton = null;
    
    protected JLabel calendarLabel = null;
    protected JList calendarList = null;
    protected JScrollPane calendarListScroll = null;
    protected StringListModel calendarListModel = null;
    
    public OpenCalendarDialog( Frame owner , String title , boolean modal , OpenCalendarDialogResult config )
    {
        super( owner , title , modal );
        this.setUndecorated(true);
        this.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);

        if( config == null )
        {
            // push back a Result
            this.dialogConfig = new OpenCalendarDialogResult();
        }
        else
        {
            this.dialogConfig = config;
            this.dialogConfig.setIsCompleted(false);
        }

        initComponents();

        // center the dialog over the calling window
        this.setLocationRelativeTo( owner );
        this.setVisible(true);
    }


    /**
     * Create the UI components.
     */
    private void initComponents()
    {
        Container contentPane = getContentPane();
        contentPane.setLayout( new SpringLayout() );
        
        this.calendarPanel = new JPanel();
        this.calendarPanel.setLayout( new SpringLayout() );
        this.getContentPane().add( this.calendarPanel );
        
        this.buttonPanel = new JPanel();
        this.buttonPanel.setLayout( new SpringLayout() );
        this.getContentPane().add( this.buttonPanel );
        
        SpringUtilities.makeCompactGrid( this.getContentPane() ,
                2, 1, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        this.cancelButton = new JButton( "Cancel" );
        this.cancelButton.addActionListener( this );
        this.buttonPanel.add( this.cancelButton );

        this.okButton = new JButton( "OK" );
        this.okButton.addActionListener( this );
        this.buttonPanel.add( this.okButton );
        
        SpringUtilities.makeCompactGrid( this.buttonPanel ,
                1, 2, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad
        
        this.calendarLabel = new JLabel("Calendars in repository:");
        this.calendarPanel.add( this.calendarLabel );
        
        //this.calendarListModel = new StringListModel( this.dialogConfig.listCalendarNameChoices() );
        this.calendarListModel = new StringListModel( new Vector(CalendarService.getInstance().listCalendars()) );
		
        this.calendarList = new JList( this.calendarListModel );
        this.calendarList.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
        
        this.calendarListScroll = new JScrollPane( this.calendarList );
        this.calendarListScroll.setPreferredSize( new Dimension( 200 ,100 ) );
        this.calendarPanel.add( this.calendarListScroll );
        
        SpringUtilities.makeCompactGrid( this.calendarPanel ,
                2, 1, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        this.pack();
        this.setSize( this.getPreferredSize() );
    }

    /**
     * Called when the user presses the OK button.
     */
    private void ok()
    {
        if( this.calendarList.getSelectedIndex() > -1 )
        {
            CalendarInterface calendar = (CalendarInterface) this.calendarListModel.getElementAt(this.calendarList.getSelectedIndex());
            String name = calendar.getName();
            try 
            {
                this.dialogConfig.setSelectedCalendarName( name );
                this.dialogConfig.setSelectedCalendar( calendar );                
                this.dialogConfig.setIsCompleted(true);
                this.setVisible( false );
            }
            catch (ParameterFormatException e) 
            {
                JOptionPane.showMessageDialog(this, "The calendar name \"" + name + "\" is not valid" , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
                return;
            }
            
        }
        else
        {
            JOptionPane.showMessageDialog(this, "Please select a calendar to open" , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
        }
    }

    /**
     * Called when the user presses the Cancel button.
     */
    private void cancel()
    {
        this.dialogConfig.setIsCompleted(false);
        this.setVisible( false );
    }

    /**
     * @return The config being used.
     */
    public OpenCalendarDialogResult getResult()
    {
        return this.dialogConfig;
    }

    public void actionPerformed(ActionEvent event) 
    {
        if( event.getSource().equals( this.okButton ) )
        {
            ok();
        }

        if( event.getSource().equals( this.cancelButton ) )
        {
            cancel();
        }
    }

    public void keyTyped(KeyEvent arg0) 
    {
        // This space intentionally left blank        
    }

    public void keyPressed(KeyEvent e) 
    {
        if( e.getKeyCode() == KeyEvent.VK_ENTER )
        {
            ok();
        }
        if( e.getKeyCode() == KeyEvent.VK_ESCAPE )
        {
            cancel();
        }        
    }

    public void keyReleased(KeyEvent arg0) 
    {
        // This space intentionally left blank
    }
}

/**
 * $Log: OpenCalendarDialog.java,v $
 * Revision 1.2  2005/05/15 23:29:42  tlong2
 * cleaned up compile warnings
 *
 * Revision 1.1  2005/05/08 01:50:29  jkeiser
 * Initial checkin.
 *
 *
**/