/***************************************************
 * File:        $RCSfile: ActivityDialog.java,v $
 * @author      $Author: dnathan $,
 * @version     $Id: ActivityDialog.java,v 1.26 2005/05/01 18:23:51 dnathan Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:    A JDialog for collecting information about a project activity.
 * </p>
 * <p>
 * Purpose:     To allow a user to enter or edit project activity details.
 * </p>
 */
package pm275.gui.dialogs;

import java.util.Date;
import java.lang.reflect.Method;
import java.text.ParseException;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.Box;

import pm275.core.project.Activity;
import pm275.core.resource.Resource;
import pm275.gui.common.DateFormatter;
import pm275.gui.common.Settings;
import pm275.gui.common.SpringUtilities;

public class ActivityDialog extends JDialog implements ActionListener, KeyListener
{
    private static final long serialVersionUID = 3978142145192409393L;
    protected ActivityDialogResult dialogConfig = null;
    protected JButton okButton = null;
    protected JButton cancelButton = null;
    protected JTextField nameField = null;
    protected JTextField startDateField = null;
    protected JLabel nameLabel = null;
    protected JLabel startDateLabel = null;
    protected JPanel durationRadioPanel = null;

    protected ButtonGroup startDateTypeGroup;
    protected JRadioButton fixedStartDateRadio;
    protected JRadioButton floatingStartDateRadio;

    protected ButtonGroup durationTypeGroup = null;
    protected JRadioButton fixedDurationRadio = null;
    protected JRadioButton effortDurationRadio = null;
    protected JLabel durationLabel = null;
    protected JTextField durationField = null;
    protected JComboBox durationMeasurementDropDown = null;

    protected JPanel basicPanel = null;
    protected JPanel basicTabPanel = null;
    protected JPanel buttonPanel = null;
    protected JPanel durationPanel = null;
    protected JPanel durationTabPanel = null;
    protected JPanel durationTypePanel = null;
    protected JPanel durationLengthPanel = null;
    protected JPanel tabPanel = null;
    protected JPanel resourceButtonPanel = null;
    protected JPanel predecessorButtonPanel = null;
    protected JPanel resourcePanel = null;
    protected JPanel resourceTabPanel = null;
    protected JPanel predecessorPanel = null;
    protected JPanel predecessorTabPanel = null;

    protected JTabbedPane tabbedPane = null;

    protected JScrollPane availableResourceListScroll = null;
    protected JScrollPane selectedResourceListScroll = null;
    protected JList availableResourceList = null;
    protected JList selectedResourceList = null;
    protected JButton moveResourceRightButton = null;
    protected JButton moveResourceAllRightButton = null;
    protected JButton moveResourceLeftButton = null;
    protected JButton moveResourceAllLeftButton = null;

    protected JScrollPane availablePredecessorListScroll = null;
    protected JScrollPane selectedPredecessorListScroll = null;
    protected JList availablePredecessorList = null;
    protected JList selectedPredecessorList = null;
    protected JButton movePredecessorRightButton = null;
    protected JButton movePredecessorAllRightButton = null;
    protected JButton movePredecessorLeftButton = null;
    protected JButton movePredecessorAllLeftButton = null;

    private ResourceModel availableResourcesModel = null;
    private ResourceModel assignedResourcesModel = null;
    private ActivityModel availableActivitiesModel = null;
    private ActivityModel assignedActivitiesModel = null;

    protected boolean inTesting = false;
    protected boolean hasError = false;

    public static void main( String[] args )
    {
        JFrame frame = new JFrame();
        String dialogTitle = "Activity Details";
        ActivityDialog dialog = new ActivityDialog( frame , dialogTitle , false , null );
        dialog.inTesting = false;
        dialog.hasError = false;
        dialog.setVisible(true);
    }

    /**
     * @param owner     The Frame to use as the parent.
     * @param title     The caption displayed in titlebar of the application.
     * @param modal     Whether the dialog should be modal.
     * @param config    If an edit, the settings to edit.  Else an empty result.
     */
    public ActivityDialog( Frame owner , String title , boolean modal , ActivityDialogResult config )
    {
        super( owner , title , modal );
        this.setUndecorated(true);
        this.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);

        if( config == null )
        {
            this.dialogConfig = new ActivityDialogResult();
        }
        else
        {
            this.dialogConfig = config;
            this.dialogConfig.setIsCompleted(false);
        }

        initComponents();

        // center the dialog over the calling window
        this.setLocationRelativeTo( owner );
        this.setVisible(true);
    }


    /**
     * Create the UI components.
     */
    private void initComponents()
    {
        // basic fields
        this.nameField = new JTextField( this.dialogConfig.getName() , 10 );
        this.nameField.addKeyListener( this );

        this.startDateField = new JTextField( DateFormatter.getStringForDate( this.dialogConfig.getStartDate() ) , 10 );
        this.startDateField.addKeyListener( this );

        this.nameLabel = new JLabel( "Activity Name:" );
        this.nameLabel.setLabelFor( this.nameField );
        this.startDateLabel = new JLabel( "Activity Start Date:" );
        this.startDateLabel.setLabelFor( this.nameLabel );

        // Start date type radio buttons.
        startDateTypeGroup = new ButtonGroup();
        fixedStartDateRadio = new JRadioButton("Fixed start date:");
        fixedStartDateRadio.addKeyListener(this);
        fixedStartDateRadio.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        enableStartDate(fixedStartDateRadio.isSelected());
                        enableDependencies(! fixedStartDateRadio.isSelected());
                    }
                });
        floatingStartDateRadio = new JRadioButton("Start date depends on dependencies:");
        floatingStartDateRadio.addKeyListener(this);
        floatingStartDateRadio.addActionListener(
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        enableStartDate(! floatingStartDateRadio.isSelected());
                        enableDependencies(floatingStartDateRadio.isSelected());
                    }
                });
        startDateTypeGroup.add(fixedStartDateRadio);
        startDateTypeGroup.add(floatingStartDateRadio);

        // Duration type radio buttons.
        this.durationTypeGroup = new ButtonGroup();
        this.effortDurationRadio = new JRadioButton( "Level of Effort" );
        this.effortDurationRadio.addKeyListener(this);
        this.fixedDurationRadio = new JRadioButton( "Fixed Duration" );
        this.fixedDurationRadio.addKeyListener(this);
        this.durationTypeGroup.add( this.effortDurationRadio );
        this.durationTypeGroup.add( this.fixedDurationRadio );

        // duration length devices
        this.durationField = new JTextField( Integer.toString(this.dialogConfig.getDuration()) , 4 );
        this.durationField.addKeyListener(this);
        this.durationLabel = new JLabel( "Time:" );
        this.durationLabel.setLabelFor( this.durationField );
        String[] choices = {"Hours" , "Days" };
        this.durationMeasurementDropDown = new JComboBox(choices);
        this.durationMeasurementDropDown.addKeyListener(this);
        this.durationMeasurementDropDown.setSelectedIndex(
                this.dialogConfig.getDurationMeasure());

        if( this.dialogConfig.getDurationType() == ActivityDialogResult.EFFORT )
        {
            this.effortDurationRadio.setSelected(true);
        }
        else
        {
            this.fixedDurationRadio.setSelected(true);
        }

        // buttons
        this.okButton = new JButton( "OK" );
        this.okButton.addActionListener( this );

        this.cancelButton = new JButton( "Cancel" );
        this.cancelButton.addActionListener( this );

        // tabs
        this.tabPanel = new JPanel();
        this.tabbedPane = new JTabbedPane();
        this.tabPanel.add(tabbedPane);
        this.tabPanel.setSize(800,500);

        // resource & predecessor devices
        this.resourceButtonPanel = new JPanel();
        this.resourceButtonPanel.setLayout( new SpringLayout() );

        this.predecessorButtonPanel = new JPanel();
        this.predecessorButtonPanel.setLayout( new SpringLayout() );

        this.resourcePanel = new JPanel();
        this.resourcePanel.setLayout( new SpringLayout() );

        this.predecessorPanel = new JPanel();
        this.predecessorPanel.setLayout( new SpringLayout() );

        this.moveResourceRightButton = new JButton("" , getIcon(Settings.getSetting("button.right.icon")));
        this.moveResourceRightButton.addActionListener(this);
        this.moveResourceAllRightButton = new JButton("" , getIcon(Settings.getSetting("button.allright.icon")));
        this.moveResourceAllRightButton.addActionListener(this);
        this.moveResourceLeftButton = new JButton("" , getIcon(Settings.getSetting("button.left.icon")));
        this.moveResourceLeftButton.addActionListener(this);
        this.moveResourceAllLeftButton = new JButton("" , getIcon(Settings.getSetting("button.allleft.icon")));
        this.moveResourceAllLeftButton.addActionListener(this);

        this.resourceButtonPanel.add(this.moveResourceRightButton);
        this.resourceButtonPanel.add(this.moveResourceAllRightButton);
        this.resourceButtonPanel.add(this.moveResourceAllLeftButton);
        this.resourceButtonPanel.add(this.moveResourceLeftButton);

        // set the layout
        SpringUtilities.makeCompactGrid( this.resourceButtonPanel ,
                4, 1,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        this.movePredecessorRightButton = new JButton("" , getIcon(Settings.getSetting("button.right.icon")));
        this.movePredecessorRightButton.addActionListener(this);
        this.movePredecessorAllRightButton = new JButton("" , getIcon(Settings.getSetting("button.allright.icon")));
        this.movePredecessorAllRightButton.addActionListener(this);
        this.movePredecessorLeftButton = new JButton("" , getIcon(Settings.getSetting("button.left.icon")));
        this.movePredecessorLeftButton.addActionListener(this);
        this.movePredecessorAllLeftButton = new JButton("" , getIcon(Settings.getSetting("button.allleft.icon")));
        this.movePredecessorAllLeftButton.addActionListener(this);

        this.predecessorButtonPanel.add(this.movePredecessorRightButton);
        this.predecessorButtonPanel.add(this.movePredecessorAllRightButton);
        this.predecessorButtonPanel.add(this.movePredecessorAllLeftButton);
        this.predecessorButtonPanel.add(this.movePredecessorLeftButton);

        // set the layout
        SpringUtilities.makeCompactGrid( this.predecessorButtonPanel ,
                4, 1,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        this.availableResourcesModel = new ResourceModel(this.dialogConfig.listAvailableResources());
        this.availableResourceList = new JList(this.availableResourcesModel);
        this.availableResourceList.setBorder( BorderFactory.createLineBorder( Color.BLACK , 1  ) );
        this.availableResourceListScroll = new JScrollPane(this.availableResourceList);
        this.availableResourceListScroll.setPreferredSize( new Dimension(250,100));

        this.assignedResourcesModel = new ResourceModel(this.dialogConfig.listAssignedResources());
        this.selectedResourceList = new JList(this.assignedResourcesModel);
        this.selectedResourceList.setBorder( BorderFactory.createLineBorder( Color.BLACK , 1  ) );
        this.selectedResourceListScroll = new JScrollPane(this.selectedResourceList);
        this.selectedResourceListScroll.setPreferredSize( new Dimension(250,100));

        this.availableActivitiesModel = new ActivityModel(this.dialogConfig.listAvailablePredecessors());
        this.availablePredecessorList = new JList(this.availableActivitiesModel);
        this.availablePredecessorList.setBorder( BorderFactory.createLineBorder( Color.BLACK , 1  ) );
        this.availablePredecessorListScroll = new JScrollPane(this.availablePredecessorList);
        this.availablePredecessorListScroll.setPreferredSize( new Dimension(250,100));

        this.assignedActivitiesModel = new ActivityModel(this.dialogConfig.listAssignedPredecessors());
        this.selectedPredecessorList = new JList(this.assignedActivitiesModel);
        this.selectedPredecessorList.setBorder( BorderFactory.createLineBorder( Color.BLACK , 1  ) );
        this.selectedPredecessorListScroll = new JScrollPane(this.selectedPredecessorList);
        this.selectedPredecessorListScroll.setPreferredSize( new Dimension(250,100));

        this.resourcePanel.add(this.availableResourceListScroll);
        this.resourcePanel.add(this.resourceButtonPanel);
        this.resourcePanel.add(this.selectedResourceListScroll);

        // set the layout
        SpringUtilities.makeCompactGrid( this.resourcePanel ,
                1, 3,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad


        // Dependencies.
        this.predecessorPanel.add(this.availablePredecessorListScroll);
        this.predecessorPanel.add(this.predecessorButtonPanel);
        this.predecessorPanel.add(this.selectedPredecessorListScroll);

        // Fixed start date.
        JPanel fixedDatePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        fixedDatePanel.add(Box.createHorizontalStrut(20));
        fixedDatePanel.add(startDateLabel);
        fixedDatePanel.add(startDateField);

        // Main start date panel.
        JPanel startDatePanel = new JPanel(new SpringLayout());
        startDatePanel.add(fixedStartDateRadio);
        startDatePanel.add(fixedDatePanel);
        startDatePanel.add(floatingStartDateRadio);
        startDatePanel.add(predecessorPanel);
        SpringUtilities.makeCompactGrid(startDatePanel,
                4, 1,     // rows, cols
                6, 6,     // initX, initY
                6, 6);    // xPad, yPad

        // Starting states depend on config.
        boolean isFixedStartDate = dialogConfig.getIsFixedStartDate();
        fixedStartDateRadio.setSelected(isFixedStartDate);
        enableStartDate(isFixedStartDate);
        floatingStartDateRadio.setSelected(! isFixedStartDate);
        enableDependencies(! isFixedStartDate);

        // set the layout
        SpringUtilities.makeCompactGrid( this.predecessorPanel ,
                1, 3,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        // basic panel
        this.basicPanel = new JPanel();
        this.basicPanel.setLayout( new SpringLayout() );

        // add the objects to the basic panel
        this.basicPanel.add( this.nameLabel );
        this.basicPanel.add( this.nameField );
        // this.basicPanel.add( this.startDateLabel );
        // this.basicPanel.add( this.startDateField );

        // set the layout
        SpringUtilities.makeCompactGrid( this.basicPanel ,
                1, 2,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        this.buttonPanel = new JPanel();
        this.durationPanel = new JPanel();
        this.durationTypePanel = new JPanel();
        this.durationLengthPanel = new JPanel();

        // set the layout for the panels
        this.getContentPane().setLayout( new SpringLayout() );
        this.buttonPanel.setLayout( new SpringLayout() );
        this.durationPanel.setLayout( new SpringLayout() );
        this.durationTypePanel.setLayout( new SpringLayout() );
        this.durationLengthPanel.setLayout( new SpringLayout() );

        // achou - Commented out to let Java determine the size.
        /*
        this.tabbedPane.setPreferredSize(new Dimension(Integer.parseInt( Settings.getSetting("window.activity.width") ) ,
                Integer.parseInt( Settings.getSetting("window.activity.height") ) ) );
        */

        // add the panels to overall dialog
        this.getContentPane().add( this.tabPanel );
        this.getContentPane().add( this.buttonPanel );

        this.basicTabPanel = new JPanel();
        this.durationTabPanel = new JPanel();
        this.predecessorTabPanel = new JPanel();
        this.resourceTabPanel = new JPanel();

        this.basicTabPanel.setLayout( new BorderLayout() );
        this.durationTabPanel.setLayout( new BorderLayout() );
        this.predecessorTabPanel.setLayout( new BorderLayout() );
        this.resourceTabPanel.setLayout( new BorderLayout() );

        this.basicTabPanel.add( basicPanel , BorderLayout.PAGE_START );
        this.durationTabPanel.add( this.durationPanel , BorderLayout.PAGE_START );
        // this.predecessorTabPanel.add( this.predecessorPanel , BorderLayout.PAGE_START );
        this.predecessorTabPanel.add(startDatePanel, BorderLayout.PAGE_START);
        this.resourceTabPanel.add( this.resourcePanel , BorderLayout.PAGE_START );

        this.tabbedPane.addTab( "Basic" , this.basicTabPanel );
        this.tabbedPane.addTab( "Duration" , this.durationTabPanel );
        this.tabbedPane.addTab( "Dependencies" , this.predecessorTabPanel );
        this.tabbedPane.addTab( "Resources" , this.resourceTabPanel );

        // set the layout
        SpringUtilities.makeCompactGrid( this.getContentPane() ,
                2, 1,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        //  add the sub-panels to the duration panel
        this.durationPanel.add( this.durationTypePanel );
        this.durationPanel.add( this.durationLengthPanel );

        // set the layout
        SpringUtilities.makeCompactGrid( this.durationPanel ,
                2, 1,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad
        // add the objects to the duration type panel
        this.durationTypePanel.add( this.fixedDurationRadio );
        this.durationTypePanel.add( this.effortDurationRadio );

        // set the layout
        SpringUtilities.makeCompactGrid( this.durationTypePanel ,
                2, 1,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        // add the objects to the duration length type panel
        this.durationLengthPanel.add( this.durationLabel );
        this.durationLengthPanel.add( this.durationField );
        this.durationLengthPanel.add( this.durationMeasurementDropDown );

        // set the layout
        SpringUtilities.makeCompactGrid( this.durationLengthPanel ,
                1, 3,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        // add the objects to the button panel
        this.buttonPanel.add( this.cancelButton );
        this.buttonPanel.add( this.okButton );

        // set the layout
        SpringUtilities.makeCompactGrid( this.buttonPanel ,
                1, 2,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad.

        this.pack();
        this.setSize( this.getPreferredSize() );
        this.nameField.grabFocus();
    }

    /**
     *  Enables/disables the start date field.
     *  @param isEnabled True to enable the field, false to disable it.
     */
    private final void enableStartDate(boolean isEnabled)
    {
        startDateField.setEnabled(isEnabled);
    }

    /**
     *  Enables/disables the lists of available/assigned activities and the
     *  left/right buttons.
     *  @param isEnabled True to enable the lists and buttons, false to disable
     *                   them.
     */
    private final void enableDependencies(boolean isEnabled)
    {
        availablePredecessorList.setEnabled(isEnabled);
        selectedPredecessorList.setEnabled(isEnabled);
        movePredecessorRightButton.setEnabled(isEnabled);
        movePredecessorAllRightButton.setEnabled(isEnabled);
        movePredecessorLeftButton.setEnabled(isEnabled);
        movePredecessorAllLeftButton.setEnabled(isEnabled);
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed( ActionEvent event )
    {
        if( event.getSource().equals( this.okButton ) )
        {
            ok();
        }

        if( event.getSource().equals( this.cancelButton ) )
        {
            cancel();
        }

        if( event.getSource().equals( this.moveResourceRightButton ) )
        {
            int[] selected = this.availableResourceList.getSelectedIndices();
            for( int x = 0 ; x  < selected.length  ; x++ )
            {
                // The X offset is because the list (and indexes) are changing.
                // Going in reverse reverses the entries.  Thus, this.
                Resource resource = this.availableResourcesModel.getItem(selected[x]-x);
                this.assignedResourcesModel.addItem(resource);
                this.availableResourcesModel.removeItem(selected[x]-x);
            }
        }

        if( event.getSource().equals( this.moveResourceAllRightButton ) )
        {
            int selected = this.availableResourcesModel.getSize();
            for( int x = 0 ; x  < selected  ; x++ )
            {
                Resource resource = this.availableResourcesModel.getItem(0);
                this.assignedResourcesModel.addItem(resource);
                this.availableResourcesModel.removeItem(0);
            }
        }

        if( event.getSource().equals( this.moveResourceLeftButton ) )
        {
            int[] selected = this.selectedResourceList.getSelectedIndices();
            for( int x = 0 ; x < selected.length  ; x++ )
            {
                Resource resource = this.assignedResourcesModel.getItem(selected[x]-x);
                this.availableResourcesModel.addItem(resource);
                this.assignedResourcesModel.removeItem(selected[x]-x);
            }
        }

        if( event.getSource().equals( this.moveResourceAllLeftButton ) )
        {
            int selected = this.assignedResourcesModel.getSize();

            for( int x = 0 ; x < selected  ; x++ )
            {
                Resource resource = this.assignedResourcesModel.getItem(0);
                this.availableResourcesModel.addItem(resource);
                this.assignedResourcesModel.removeItem(0);
            }
        }

        if( event.getSource().equals( this.movePredecessorRightButton ) )
        {
            int[] selected = this.availablePredecessorList.getSelectedIndices();
            for( int x = 0 ; x  < selected.length  ; x++ )
            {
                // The X offset is because the list (and indexes) are changing.
                // Going in reverse reverses the entries.  Thus, this.
                Activity activity = this.availableActivitiesModel.getItem(selected[x]-x);
                this.assignedActivitiesModel.addItem(activity);
                this.availableActivitiesModel.removeItem(selected[x]-x);
            }
        }

        if( event.getSource().equals( this.movePredecessorAllRightButton ) )
        {
            int selected = this.availableActivitiesModel.getSize();
            for( int x = 0 ; x  < selected  ; x++ )
            {
                Activity activity = this.availableActivitiesModel.getItem(0);
                this.assignedActivitiesModel.addItem(activity);
                this.availableActivitiesModel.removeItem(0);
            }
        }

        if( event.getSource().equals( this.movePredecessorLeftButton ) )
        {
            int[] selected = this.selectedPredecessorList.getSelectedIndices();
            for( int x = 0 ; x < selected.length  ; x++ )
            {
                Activity activity = this.assignedActivitiesModel.getItem(selected[x]-x);
                this.availableActivitiesModel.addItem(activity);
                this.assignedActivitiesModel.removeItem(selected[x]-x);
            }
        }

        if( event.getSource().equals( this.movePredecessorAllLeftButton ) )
        {
            int selected = this.assignedActivitiesModel.getSize();

            for( int x = 0 ; x < selected  ; x++ )
            {
                Activity activity = this.assignedActivitiesModel.getItem(0);
                this.availableActivitiesModel.addItem(activity);
                this.assignedActivitiesModel.removeItem(0);
            }
        }
    }

    /**
     * Called when the user presses the OK button.
     */
    private void ok()
    {
        try
        {
            this.dialogConfig.setName( this.nameField.getText() );
        }
        catch (ParameterFormatException e)
        {
            if( this.inTesting )
            {
                this.hasError = true;
                return;
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Please enter a valid name" , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
                return;
            }
        }

        // Fixed start date?
        boolean isFixedStartDate = fixedStartDateRadio.isSelected();
        dialogConfig.setIsFixedStartDate(isFixedStartDate);

        // Start date.
        try
        {
            Date startDate = DateFormatter.getDateForString(
                                     this.startDateField.getText());
            this.dialogConfig.setStartDate(startDate);
        }
        catch (ParseException ex)
        {
            if( this.inTesting )
            {
                this.hasError = true;
                return;
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Please enter a valid start date." + System.getProperty( "line.separator" ) + "Dates should use the format YYYY-MM-DD." , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
                return;
            }
        }
        catch (ParameterFormatException e)
        {
            if( this.inTesting )
            {
                this.hasError = true;
                return;
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Please enter a valid start date." + System.getProperty( "line.separator" ) + "Dates should use the format YYYY-MM-DD."  , this.getTitle() , JOptionPane.INFORMATION_MESSAGE);
                return;
            }
        }

        try
        {
            int duration = Integer.parseInt( this.durationField.getText() );
            int durationMeasure = this.durationMeasurementDropDown.getSelectedIndex();
            this.dialogConfig.setDurationMeasure(durationMeasure);
            this.dialogConfig.setDuration(duration);

        }
        catch (ParameterFormatException e)
        {
            if( this.inTesting )
            {
                this.hasError = true;
                return;
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Please enter a valid duration" , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
                return;
            }
        }

        try
        {
            if( this.effortDurationRadio.isSelected() )
            {
                this.dialogConfig.setDurationType( ActivityDialogResult.EFFORT );
            }
            else
            {
                this.dialogConfig.setDurationType( ActivityDialogResult.FIXED_DURATION );
            }
        }
        catch (ParameterFormatException e)
        {
            if( this.inTesting )
            {
                this.hasError = true;
                return;
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Please enter a duration type" , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
                return;
            }
        }

        this.dialogConfig.clearAssignedPredecessors();
        this.dialogConfig.clearAssignedResources();
        this.dialogConfig.clearAvailablePredecessors();
        this.dialogConfig.clearAvailableResources();

        for( int act = 0 ; act < this.assignedActivitiesModel.getSize() ; act++ )
        {
            this.dialogConfig.addAssignedPredecessor( this.assignedActivitiesModel.getItem(act) );
        }

        for( int act = 0 ; act < this.availableActivitiesModel.getSize() ; act++ )
        {
            this.dialogConfig.addAvailablePredecessor( this.availableActivitiesModel.getItem(act) );
        }

        for( int res = 0 ; res < this.assignedResourcesModel.getSize() ; res++ )
        {
            this.dialogConfig.addAssignedResource( this.assignedResourcesModel.getItem(res) );
        }

        for( int res = 0 ; res < this.availableResourcesModel.getSize() ; res++ )
        {
            this.dialogConfig.addAvailableResource( this.availableResourcesModel.getItem(res) );
        }

        this.dialogConfig.setIsCompleted(true);
        this.setVisible( false );
    }

    /**
     * Called when the user presses the Cancel button.
     */
    private void cancel()
    {
        this.dialogConfig.setIsCompleted(false);
        this.setVisible( false );
    }

    /**
     * @return The config being used.
     */
    public ActivityDialogResult getResult()
    {
        return this.dialogConfig;
    }


    /* (non-Javadoc)
     * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
     */
    public void keyPressed(KeyEvent e)
    {
        if( e.getKeyCode() == KeyEvent.VK_ENTER )
        {
            ok();
        }
        if( e.getKeyCode() == KeyEvent.VK_ESCAPE )
        {
            cancel();
        }
    }


    /* (non-Javadoc)
     * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
     */
    public void keyReleased(KeyEvent e)
    {
        // noop
    }


    /* (non-Javadoc)
     * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
     */
    public void keyTyped(KeyEvent e)
    {
        //noop
    }

    private static Icon getIcon( String file )
    {
        try {
            Method method = Thread.class.getMethod( "getContextClassLoader" , (Class[]) null );
            ClassLoader loader = (ClassLoader) method.invoke( Thread.currentThread() , (Object[]) null );

            if( loader.getResource( file ) != null )
            {
                return new ImageIcon(loader.getResource( file ));
            }
            else
            {
                return null;
            }
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
/**
 * $Log: ActivityDialog.java,v $
 * Revision 1.26  2005/05/01 18:23:51  dnathan
 * Cleaned up warnings.
 *
 * Revision 1.25  2005/04/30 22:19:26  achou
 * Moved start date to dependencies tab and added radio button to select fixed vs. floating start date.
 *
 * Revision 1.24  2005/04/24 21:13:24  dnathan
 * Refactored list mpodels into external classes for reuse in other dialogs.
 *
 * Revision 1.23  2005/04/19 06:56:29  achou
 * Removed code that converts days <---> hours.  This is now done in ActivityDialogResult, which uses the hours-per-day property of the calendar.
 *
 * Revision 1.22  2005/04/17 09:19:27  achou
 * Converted Enumeration code to use Collection instead.  Cleaned up constructor and listeners code for the list models.
 *
 * Revision 1.21  2005/04/16 20:22:49  dnathan
 * Added logic to store assigned resources and predecessors.
 *
 * Revision 1.20  2005/04/16 18:59:54  dnathan
 * Added predecessor assignment UI code.
 *
 * Revision 1.19  2005/04/16 18:48:31  dnathan
 * Added resource assignment UI code.
 *
 * Revision 1.18  2005/04/16 02:46:50  dnathan
 * Interim build.
 *
 * Revision 1.17  2005/04/15 06:32:15  dnathan
 * Added beginnings of Lists for Resources and Dependencies.
 *
 * Revision 1.15  2005/04/12 14:13:54  dnathan
 * Added tabs, stubs for resources and predecessors.
 *
 * Revision 1.14  2005/04/03 20:56:57  dnathan
 * Added code to support different time calculations, the amount of time, and the measure of the time.
 *
 * Revision 1.13  2005/03/30 16:14:45  dnathan
 * Added serialization field to supress warnings.
 *
 * Revision 1.12  2005/03/30 06:59:55  dnathan
 * Added Keyboard listeners for Enter and ESC.  For real this time.
 *
 * Revision 1.11  2005/03/30 06:46:05  dnathan
 * Added Keyboard listeners for Enter and ESC.
 *
 * Revision 1.10  2005/03/30 06:30:40  dnathan
 * Moved to use Spring Layout.
 *
 * Revision 1.9  2005/03/30 05:53:45  dnathan
 * Moved to platform independent window decorations.
 *
 * Revision 1.8  2005/03/30 03:57:44  dnathan
 * Changed show() order to properly center on screen.
 *
 * Revision 1.7  2005/03/30 03:45:52  dnathan
 * Updated error message to show required Date format.
 *
 * Revision 1.6  2005/03/29 06:06:19  dnathan
 * Added code to support "silent" unit testing.  HasError is displayed in lieu of an error message if the dialog is in test mode.
 *
 * Revision 1.5  2005/03/29 04:37:25  achou
 * Added code to catch new ParseException from DateFormatter.
 *
 * Revision 1.4  2005/03/29 04:10:35  dnathan
 * Added JavaDoc comments.
 *
 * Revision 1.3  2005/03/29 01:41:08  dnathan
 * Added implementation code for Iteration 1.  All elements are protected to allow for JUnit tests to work.
 *
 * Revision 1.2  2005/03/23 01:55:23  dnathan
 * Updated headers and footers to reflect coding standards.
 *
 * Revision 1.1  2005/03/22 05:56:25  dnathan
 * New file.
 *
 **/
