/***************************************************
 * File: 		$RCSfile: ResourceDialogResult.java,v $ 
 * @author 		$Author: dnathan $, 
 * @version 	$Id: ResourceDialogResult.java,v 1.2 2005/04/03 04:41:52 dnathan Exp $
 * Assignment:	CSCI E-275 Team 1 Project
 * <p>
 * Contents:	A class for communicating between the main GUI and the resource edit/create dialog.
 * </p>
 * <p>
 * Purpose: 	This class is used to communicate Resource details between the "New Resource"
 * 				and "Resource Activity" dialogs and the application.
 * </p>
 */
package pm275.gui.dialogs;

public class ResourceDialogResult 
{
	private String _name = null;
	private boolean isCompleted = false;

	/**
	 * @return Returns the name of the resource.
	 */
	public String getName() 
	{
		return _name;
	}
	

	/**
	 * @param name The name of the resource to set.
	 */
	public void setName(String name) throws ParameterFormatException 
	{
		if( name == null )
		{
			throw new ParameterFormatException();
		}
		
		if( name.length() < 1 )
		{
			throw new ParameterFormatException();
		}
		
		this._name = name;
	}	

    /**
     *  Returns true if the user completed the login dialog (by hitting "OK"),
     *  false otherwise.
     *  @return If the user completed the login dialog.
     */
    public boolean getIsCompleted()
    {
        return isCompleted;
    }

    /**
     *  Sets the flag indicating whether the user completed the login dialog.
     *  @param isCompleted True if the user completed the login dialog, false
     *                     otherwise.
     */
    public void setIsCompleted(boolean isCompleted)
    {
		this.isCompleted = isCompleted;
    }
}
