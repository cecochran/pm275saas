/***************************************************
 * File: 		$RCSfile: ResourcePoolDialogResult.java,v $ 
 * @author 		$Author: dnathan $, 
 * @version 	$Id: ResourcePoolDialogResult.java,v 1.2 2005/04/03 04:41:52 dnathan Exp $
 * Assignment:	CSCI E-275 Team 1 Project
 * <p>
 * Contents:	A class for communicating between the main GUI and the activity edit/create dialog.
 * </p>
 * <p>
 * Purpose: 	This class is used to communicate Activity details between the "New Activity"
 * 				and "Edit Activity" dialogs and the application.
 * </p>
 */
package pm275.gui.dialogs;

import pm275.core.resource.ResourceKey;

public class ResourcePoolDialogResult 
{
	private String _poolName = null;
	private ResourceKey[] _resources = null;
	private boolean isCompleted = false;
	
	/**
	 * @return Returns the name of the pool.
	 */
	public String getName() 
	{
		return _poolName;
	}
	
	/**
	 * @param name What to set the name of the pool to.
	 */
	public void setName( String name ) throws ParameterFormatException
	{
		if( name == null )
		{
			throw new ParameterFormatException();
		}
		
		if( name.length() < 1 )
		{
			throw new ParameterFormatException();
		}
		
		_poolName = name;
	}
	
	/**
	 * @return Returns an array of the Resources in the pool.
	 */
	public ResourceKey[] getResources() 
	{
		return _resources;
	}
	
	/**
	 * @param _resources The _resources to set.
	 */
	public void setResources( ResourceKey[] resources ) throws ParameterFormatException
	{
		if( resources == null )
		{
			throw new ParameterFormatException();
		}
		
		this._resources = resources;
	}

    /**
     *  Returns true if the user completed the login dialog (by hitting "OK"),
     *  false otherwise.
     *  @return If the user completed the login dialog.
     */
    public boolean getIsCompleted()
    {
        return isCompleted;
    }

    /**
     *  Sets the flag indicating whether the user completed the login dialog.
     *  @param isCompleted True if the user completed the login dialog, false
     *                     otherwise.
     */
    public void setIsCompleted(boolean isCompleted)
    {
		this.isCompleted = isCompleted;
    }
}
/**
 * $Log: ResourcePoolDialogResult.java,v $
 * Revision 1.2  2005/04/03 04:41:52  dnathan
 * Added IsComplete property.
 *
 * Revision 1.1  2005/04/03 04:38:54  dnathan
 * New file.
 *
 *
 */