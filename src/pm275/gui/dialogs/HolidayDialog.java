/***************************************************
 * File:        $RCSfile: HolidayDialog.java,v $
 * @author      $Author: tlong2 $,
 * @version     $Id: HolidayDialog.java,v 1.3 2005/05/15 23:29:42 tlong2 Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:    A JDialog for collecting information about a holiday.
 * </p>
 * <p>
 * Purpose:     To allow a user to enter or edit holiday details within a calendar.
 * </p>
 */
package pm275.gui.dialogs;

import org.apache.log4j.Logger;
import java.util.Date;
import java.text.ParseException;

import java.awt.Container;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import pm275.gui.common.DateFormatter;
import pm275.gui.common.SpringUtilities;

public class HolidayDialog extends JDialog implements ActionListener, KeyListener
{
    private static final Logger log = Logger.getLogger(HolidayDialog.class);
    private static final String ATTR = "HolidayDialog";
	protected HolidayDialogResult dialogConfig = null;
    protected JButton okButton = null;
    protected JButton cancelButton = null;
    protected JTextField nameField = null;
    protected JTextField dateField = null;
    protected JLabel nameLabel = null;
    protected JLabel dateLabel = null;
    protected boolean inTesting = false;
	protected boolean hasError = false;

    /**
     * @param owner     The Frame to use as the parent.
     * @param title     The caption displayed in titlebar of the application.
     * @param modal     Whether the dialog should be modal.
     * @param config    If an edit, the settings to edit.  Else an empty result.
     */
    public HolidayDialog( Frame owner , String title , boolean modal , HolidayDialogResult config )
    {
        super( owner , title , modal );
		this.setUndecorated(true);
	    this.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);

        if( config == null )
        {
            // push back a Result
            this.dialogConfig = new HolidayDialogResult();
        }
        else
        {
            this.dialogConfig = config;
            this.dialogConfig.setIsCompleted(false);
        }

        initComponents();

        // center the dialog over the calling window
        this.setLocationRelativeTo( owner );
		this.show();
    }


    /**
     * Create the UI components.
     */
    private void initComponents()
    {
		SpringLayout sl = new SpringLayout();
		
        Container contentPane = getContentPane();
        contentPane.setLayout( sl );
        
        //create the buttons
        this.okButton = new JButton( "OK" );
        this.okButton.addActionListener( this );

        this.cancelButton = new JButton( "Cancel" );
        this.cancelButton.addActionListener( this );

        this.nameField = new JTextField( this.dialogConfig.getName() , 10 );      
		this.nameField.addKeyListener( this );

        this.dateField = new JTextField(
                DateFormatter.getStringForDate( this.dialogConfig.getDate() ) , 10 );
		this.dateField.addKeyListener( this );
		
        this.nameLabel = new JLabel( " Holiday Name:" );
        this.dateLabel = new JLabel( " Holiday Date:" );

        contentPane.add( this.nameLabel );
        contentPane.add( this.nameField );

        contentPane.add( this.dateLabel );
        contentPane.add( this.dateField );
        
        contentPane.add( this.cancelButton );
        contentPane.add( this.okButton );
		
		SpringUtilities.makeCompactGrid( this.getContentPane() ,
                3, 2, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        this.pack();
        this.setSize( this.getPreferredSize() );
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed( ActionEvent event )
    {
        if( event.getSource().equals( this.okButton ) )
        {
            ok();
        }

        if( event.getSource().equals( this.cancelButton ) )
        {
            cancel();
        }
    }

    /**
     * Called when the user presses the OK button.
     */
    private void ok()
    {
        try
        {
            this.dialogConfig.setName( this.nameField.getText() );
        }
        catch (ParameterFormatException e)
        {
			if( this.inTesting )
			{
				this.hasError = true;
				return;
			}
			else
			{
	            JOptionPane.showMessageDialog(this, "Please enter a valid name for the holiday." , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
				return;
			}
        }

        try
        {
            Date holidayDate = DateFormatter.getDateForString(
                                     this.dateField.getText());
            this.dialogConfig.setDate(holidayDate);
        }
        catch (ParseException ex)
        {
			if( this.inTesting )
			{
				this.hasError = true;
				return;
			}
			else
			{
	            JOptionPane.showMessageDialog(this, "Please enter a valid date." + System.getProperty( "line.separator" ) + "Dates should use the format YYYY-MM-DD." , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
				return;
			}
        }
        catch (ParameterFormatException e)
        {
			if( this.inTesting )
			{
				this.hasError = true;
				return;
			}
			else
			{
	            JOptionPane.showMessageDialog(this, "Please enter a valid date." + System.getProperty( "line.separator" ) + "Dates should use the format YYYY-MM-DD." , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
				return;
			}
        }
      
        this.dialogConfig.setIsCompleted(true);
        this.setVisible( false );
    }

    /**
     * Called when the user presses the Cancel button.
     */
    private void cancel()
    {
        this.dialogConfig.setIsCompleted(false);
        this.setVisible( false );
    }

    /**
     * @return The config being used.
     */
    public HolidayDialogResult getResult()
    {
        return this.dialogConfig;
    }


	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	public void keyPressed(KeyEvent e) 
	{
		if( e.getKeyCode() == KeyEvent.VK_ENTER )
		{
			ok();
		}
		if( e.getKeyCode() == KeyEvent.VK_ESCAPE )
		{
			cancel();
		}
	}


	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	public void keyReleased(KeyEvent e) 
	{
		// noop
	}


	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	public void keyTyped(KeyEvent e) 
	{
		//noop
	}
}

/**
 * $Log: HolidayDialog.java,v $
 * Revision 1.3  2005/05/15 23:29:42  tlong2
 * cleaned up compile warnings
 *
 * Revision 1.2  2005/05/09 09:31:15  jkeiser
 * Debugging
 *
 * Revision 1.1  2005/05/07 11:35:24  jkeiser
 * Initial checkin
 *
 *
 **/
