/*
 *  File      :  SaveProjectDialog.java
 *  Version   :  $Id: SaveProjectDialog.java,v 1.3 2005/05/01 18:21:14 dnathan Exp $
 *  Author    :  Dan Nathan
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */
package pm275.gui.dialogs;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import pm275.gui.common.SpringUtilities;

public class SaveProjectDialog extends JDialog implements ActionListener, KeyListener
{
    private static final long serialVersionUID = 3833188021138174005L;
    private SaveProjectDialogResult dialogConfig = null;
    protected boolean inTesting = false;
    protected boolean hasError = false;
    
    protected JLabel nameLabel = null;
    protected JTextField nameField = null;
    protected JButton okButton = null;
    protected JButton cancelButton = null;
    
    protected JPanel namePanel = null;
    protected JPanel buttonPanel = null;
    
    public SaveProjectDialog( Frame owner , String title , boolean modal , SaveProjectDialogResult config )
    {
        super( owner , title , modal );
        this.addKeyListener( this );
        this.setUndecorated(true);
        this.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);

        if( config == null )
        {
            // push back a Result
            this.dialogConfig = new SaveProjectDialogResult();
        }
        else
        {
            this.dialogConfig = config;
            this.dialogConfig.setIsCompleted(false);
        }

        initComponents();

        // center the dialog over the calling window
        this.setLocationRelativeTo( owner );
        this.setVisible(true);
        this.dialogConfig = config;
    }
    
    private void initComponents()
    {
        this.nameLabel = new JLabel( "Save as:" );
        this.nameField = new JTextField( this.dialogConfig.getName() , 10 );
        this.nameField.addKeyListener( this );
        
        this.okButton = new JButton( "OK" );
        this.okButton.addActionListener(this);
        this.cancelButton = new JButton( "Cancel" );
        this.cancelButton.addActionListener(this);
        
        this.namePanel = new JPanel();
        this.buttonPanel = new JPanel();
        
        this.getContentPane().setLayout( new SpringLayout() );
        this.namePanel.setLayout( new SpringLayout() );
        this.buttonPanel.setLayout( new SpringLayout() );
        
        this.namePanel.add( this.nameLabel );
        this.namePanel.add( this.nameField );
        
        this.buttonPanel.add( this.cancelButton );
        this.buttonPanel.add( this.okButton );
        
        this.getContentPane().add( this.namePanel );
        this.getContentPane().add( this.buttonPanel );
        
        SpringUtilities.makeCompactGrid( this.getContentPane() ,
                2, 1,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad
        
        SpringUtilities.makeCompactGrid( this.namePanel ,
                1, 2,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad
        
        SpringUtilities.makeCompactGrid( this.buttonPanel ,
                1, 2,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad
        
        this.nameField.selectAll();
        
        this.pack();
        this.setSize( this.getPreferredSize() );
    }

    /**
     * Called when the user presses the OK button.
     */
    private void ok()
    {
        try
        {
            this.dialogConfig.setName( this.nameField.getText() );
        }
        catch (ParameterFormatException e)
        {
            if( this.inTesting )
            {
                this.hasError = true;
                return;
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Please enter a valid project name." , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
                return;
            }
        }     
        
        this.dialogConfig.setIsCompleted(true);
        this.setVisible( false );
    }

    /**
     * Called when the user presses the Cancel button.
     */
    private void cancel()
    {
        this.dialogConfig.setIsCompleted(false);
        this.setVisible( false );
    }

    /**
     * @return The config being used.
     */
    public SaveProjectDialogResult getResult()
    {
        return this.dialogConfig;
    }

    public void actionPerformed(ActionEvent event) 
    {
        if( event.getSource().equals( this.okButton ) )
        {
            ok();
        }

        if( event.getSource().equals( this.cancelButton ) )
        {
            cancel();
        }        
    }

    public void keyTyped(KeyEvent arg0) 
    {
        // this space intentionally left blank.
    }

    public void keyPressed(KeyEvent e) 
    {
        if( e.getKeyCode() == KeyEvent.VK_ENTER )
        {
            ok();
        }
        if( e.getKeyCode() == KeyEvent.VK_ESCAPE )
        {
            cancel();
        }
    }

    public void keyReleased(KeyEvent arg0) 
    {
        // this space intentionally left blank.
    }
}

/**
 * $Log: SaveProjectDialog.java,v $
 * Revision 1.3  2005/05/01 18:21:14  dnathan
 * Cleaned up warnings.
 *
 * Revision 1.2  2005/05/01 18:14:54  dnathan
 * Implemented OK/Cancel logic.
 *
 * Revision 1.1  2005/04/30 17:47:43  dnathan
 * New file.
 *
 *
 **/