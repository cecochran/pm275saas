/*
 *  File      :  OpenProjectDialog.java
 *  Version   :  $Id: OpenProjectDialog.java,v 1.1 2005/05/01 20:47:21 dnathan Exp $
 *  Author    :  Dan Nathan
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */
package pm275.gui.dialogs;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;

import pm275.gui.common.SpringUtilities;

public class OpenProjectDialog  extends JDialog implements ActionListener, KeyListener
{
    protected OpenProjectDialogResult dialogConfig = null;
    protected JPanel projectPanel = null;
    protected JPanel buttonPanel = null;
    
    protected JButton okButton = null;
    protected JButton cancelButton = null;
    
    protected JLabel projectLabel = null;
    protected JList projectList = null;
    protected JScrollPane projectListScroll = null;
    protected StringListModel projectListModel = null;
    
    public OpenProjectDialog( Frame owner , String title , boolean modal , OpenProjectDialogResult config )
    {
        super( owner , title , modal );
        this.setUndecorated(true);
        this.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);

        if( config == null )
        {
            // push back a Result
            this.dialogConfig = new OpenProjectDialogResult();
        }
        else
        {
            this.dialogConfig = config;
            this.dialogConfig.setIsCompleted(false);
        }

        initComponents();

        // center the dialog over the calling window
        this.setLocationRelativeTo( owner );
        this.setVisible(true);
    }


    /**
     * Create the UI components.
     */
    private void initComponents()
    {
        Container contentPane = getContentPane();
        contentPane.setLayout( new SpringLayout() );
        
        this.projectPanel = new JPanel();
        this.projectPanel.setLayout( new SpringLayout() );
        this.getContentPane().add( this.projectPanel );
        
        this.buttonPanel = new JPanel();
        this.buttonPanel.setLayout( new SpringLayout() );
        this.getContentPane().add( this.buttonPanel );
        
        SpringUtilities.makeCompactGrid( this.getContentPane() ,
                2, 1, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        this.cancelButton = new JButton( "Cancel" );
        this.cancelButton.addActionListener( this );
        this.buttonPanel.add( this.cancelButton );

        this.okButton = new JButton( "OK" );
        this.okButton.addActionListener( this );
        this.buttonPanel.add( this.okButton );
        
        SpringUtilities.makeCompactGrid( this.buttonPanel ,
                1, 2, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad
        
        this.projectLabel = new JLabel("Projects in repository:");
        this.projectPanel.add( this.projectLabel );
        
        this.projectListModel = new StringListModel( this.dialogConfig.listProjectNameChoices() );
        
        this.projectList = new JList( this.projectListModel );
        this.projectList.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
        
        this.projectListScroll = new JScrollPane( this.projectList );
        this.projectListScroll.setPreferredSize( new Dimension( 200 ,100 ) );
        this.projectPanel.add( this.projectListScroll );
        
        SpringUtilities.makeCompactGrid( this.projectPanel ,
                2, 1, //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad

        this.pack();
        this.setSize( this.getPreferredSize() );
    }

    /**
     * Called when the user presses the OK button.
     */
    private void ok()
    {
        if( this.projectList.getSelectedIndex() > -1 )
        {
            String name = (String) this.projectListModel.getElementAt(this.projectList.getSelectedIndex());
            try 
            {
                this.dialogConfig.setSelectedProjectName( name );
                this.dialogConfig.setIsCompleted(true);
                this.setVisible( false );
            }
            catch (ParameterFormatException e) 
            {
                JOptionPane.showMessageDialog(this, "The project name \"" + name + "\" is not valid" , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
                return;
            }
        }
        else
        {
            JOptionPane.showMessageDialog(this, "Please select a project to open" , this.getTitle() , JOptionPane.INFORMATION_MESSAGE );
        }
    }

    /**
     * Called when the user presses the Cancel button.
     */
    private void cancel()
    {
        this.dialogConfig.setIsCompleted(false);
        this.setVisible( false );
    }

    /**
     * @return The config being used.
     */
    public OpenProjectDialogResult getResult()
    {
        return this.dialogConfig;
    }

    public void actionPerformed(ActionEvent event) 
    {
        if( event.getSource().equals( this.okButton ) )
        {
            ok();
        }

        if( event.getSource().equals( this.cancelButton ) )
        {
            cancel();
        }
    }

    public void keyTyped(KeyEvent arg0) 
    {
        // This space intentionally left blank        
    }

    public void keyPressed(KeyEvent e) 
    {
        if( e.getKeyCode() == KeyEvent.VK_ENTER )
        {
            ok();
        }
        if( e.getKeyCode() == KeyEvent.VK_ESCAPE )
        {
            cancel();
        }        
    }

    public void keyReleased(KeyEvent arg0) 
    {
        // This space intentionally left blank
    }
}

/**
 * $Log: OpenProjectDialog.java,v $
 * Revision 1.1  2005/05/01 20:47:21  dnathan
 * New file.
 *
 *
 *
**/