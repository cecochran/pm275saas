/*
* File:        $RCSfile: ResourceActivityReportDialog.java,v $
* @author      $Author: dnathan $,
* @version     $Id: ResourceActivityReportDialog.java,v 1.6 2005/05/07 04:22:30 dnathan Exp $
* Assignment:  CSCI E-275 Team 1 Project
* <p>
* Contents:    A JDialog for collecting information about a project.
* </p>
* <p>
* Purpose:     To allow a user to enter or edit project details.
* </p>
*/
package pm275.gui.dialogs;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;

import pm275.common.exception.ResourceException;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourcePoolKey;
import pm275.core.resource.ResourceService;
import pm275.gui.common.Settings;
import pm275.gui.common.SpringUtilities;
import pm275.gui.common.SwingUtilities;

public class ResourceActivityReportDialog extends JDialog implements ActionListener
{
    private static final long serialVersionUID = 3256446914777069876L;
    protected ResourceActivityReportDialogConfig config = null;
    private ResourceModel resModel = null;
    protected boolean inTesting = false;
    protected boolean hasError = false;

    protected JButton okButton = null;
    protected JButton cancelButton = null;
    protected JLabel resourceLabel = null;
    protected JList resourceList = null;
    protected JPanel buttonPanel = null;
    protected JPanel resourcePanel = null;
    protected JScrollPane resourceListScroll = null;

    /**
     * @param owner     The Frame to use as the parent.
     * @param title     The caption displayed in titlebar of the application.
     * @param modal     Whether the dialog should be modal.
     */
    public ResourceActivityReportDialog( Frame owner , String title , boolean modal , ResourceActivityReportDialogConfig config  )
    {
        super( owner , title , modal );
        this.config = config;
        this.setUndecorated(true);
        this.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);

        try
		{
			initComponents();
		}
		catch( Exception e )
		{
			this.config.setCompleted(false);
			return;
		}

        // center the dialog over the calling window
        this.setLocationRelativeTo( owner );
        this.show();
    }


    /**
     * Create the UI components.
     */
    private void initComponents() throws Exception
    {
         SpringLayout sl = new SpringLayout();
         
        Container contentPane = getContentPane();
        contentPane.setLayout( sl );
        
        ResourcePool pool = null;        
        ResourcePoolKey poolKey = this.config.getProject().getResourcePoolKey();
        
        try 
        {
            pool = ResourceService.getInstance().getResourcePool( poolKey );
        } 
        catch (ResourceException e) 
        {
			SwingUtilities.reportErrorMessage( "This project does not have a resource pool!" );
			throw new Exception( "This project does not have a resource pool!" );
        }
		
        try
		{
			this.resModel = new ResourceModel( pool.listResources() );        
		}
		catch( Exception e )
		{
			SwingUtilities.reportErrorMessage( "This project has no resources assigned to its Resource Pool." );
			throw new Exception( "No resources in resource pool" );
		}

        this.cancelButton = new JButton( "Cancel" );
        this.cancelButton.addActionListener( this );
		
        this.okButton = new JButton( "OK" );
        this.okButton.addActionListener( this );
        
        this.resourceLabel = new JLabel( Settings.getSetting("window.resourceactivityreport.resourcelabel") );
        
        this.resourceList = new JList( resModel );
        this.resourceList.setSelectionMode( ListSelectionModel.MULTIPLE_INTERVAL_SELECTION );
        
        this.resourceListScroll = new JScrollPane( this.resourceList );
        this.resourceListScroll.setPreferredSize( new Dimension(450,200));
        
        this.buttonPanel = new JPanel();
        this.buttonPanel.setLayout( new SpringLayout() );
        this.buttonPanel.add( this.cancelButton );
        this.buttonPanel.add( this.okButton );

        // set the layout
        SpringUtilities.makeCompactGrid( this.buttonPanel ,
                1, 2,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad    
        
        this.resourcePanel = new JPanel();
        this.resourcePanel.setLayout( new BorderLayout() );
        this.resourcePanel.add( this.resourceLabel , BorderLayout.NORTH );
        this.resourcePanel.add( this.resourceListScroll , BorderLayout.SOUTH );
        
        contentPane.add( this.resourcePanel );
        contentPane.add( this.buttonPanel );

        // set the layout
        SpringUtilities.makeCompactGrid( contentPane ,
                2, 1,        //rows, cols
                6, 6,        //initX, initY
                6, 6);       //xPad, yPad        

        this.pack();
        this.setSize( this.getPreferredSize() );
    }
	
	protected void ok()
	{
		this.config.clearResources();
		int[] selected = this.resourceList.getSelectedIndices();
		
		for( int x = 0 ; x < selected.length ; x++ )
		{
			this.config.addResource( this.resModel.getItem( selected[x] ).getKey() );
		}
		
		this.config.setCompleted(true);
		this.setVisible( false );
	}
	
	protected void cancel()
	{
		this.config.setCompleted(false);
		this.setVisible( false );
	}

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed( ActionEvent event )
    {
        if( event.getSource().equals( this.cancelButton ) )
        {
            cancel();
			return;
        }
        if( event.getSource().equals( this.okButton ) )
        {
            ok();
			return;
        }
    }
}
/**
* $Log: ResourceActivityReportDialog.java,v $
* Revision 1.6  2005/05/07 04:22:30  dnathan
* Added code to allow filtering of Activities by assigned Resource.
*
* Revision 1.5  2005/05/04 02:49:03  dnathan
* Fixed bugs that mis-cast AssignmentKeys as ActivityKeys.  Oops.
*
**/
