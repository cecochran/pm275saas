package pm275.gui.dialogs;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import pm275.core.resource.Resource;

class ResourceModel implements ListModel
{
    private List resources;
    private List listeners;
    private int size;

    public ResourceModel( Collection resources )
    {
        this.resources = new LinkedList(resources);
        this.size = resources.size();
        this.listeners = new Vector();
    }

    public int getSize()
    {
        return this.size;
    }

    public Object getElementAt(int index)
    {
        Resource a = (Resource) this.resources.get( index );
        return a.getName();
    }

    public void addListDataListener(ListDataListener l)
    {
        this.listeners.add(l);
    }

    public void removeListDataListener(ListDataListener l)
    {
        this.listeners.remove(l);
    }

    public Resource getItem( int index )
    {
        return (Resource) this.resources.get( index );
    }

    public synchronized void removeItem( int index )
    {
        this.size -= 1;
        this.resources.remove( index );
        ListDataEvent e = new ListDataEvent( this , ListDataEvent.INTERVAL_REMOVED , index, index );

        Iterator i = listeners.iterator();
        while(i.hasNext())
        {
            ListDataListener listener = (ListDataListener) i.next();
            listener.intervalRemoved(e);
        }
    }

    public synchronized void addItem( Resource r )
    {
        this.resources.add( r );
        ListDataEvent e = new ListDataEvent( this , ListDataEvent.INTERVAL_ADDED , this.size, this.size );
        this.size += 1;

        Iterator i = listeners.iterator();
        while(i.hasNext())
        {
            ListDataListener listener = (ListDataListener) i.next();
            listener.intervalAdded(e);
        }
    }
}
