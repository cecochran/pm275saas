/***************************************************
 * File:        $RCSfile: ActivityDialogResult.java,v $
 * @author      $Author: achou $,
 * @version     $Id: ActivityDialogResult.java,v 1.24 2005/05/15 18:03:49 achou Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:    A class for communicating between the main GUI and the activity edit/create dialog.
 * </p>
 * <p>
 * Purpose:     This class is used to communicate Activity details between the "New Activity"
 *              and "Edit Activity" dialogs and the application.
 * </p>
 */
package pm275.gui.dialogs;

import java.util.Date;
import java.util.Collection;
import java.util.Vector;
import java.util.Iterator;
import java.util.Hashtable;
import java.util.Map;

import pm275.core.project.ProjectInterface;
import pm275.core.project.ActivityInterface;
import pm275.core.project.ProjectService;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.ResourcePoolKey;
import pm275.core.resource.ResourcePool;
import pm275.core.resource.ResourceKey;
import pm275.core.resource.ResourceService;
import pm275.core.calendar.CalendarService;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.DuplicateKeyException;
import pm275.common.exception.CycleDependencyException;
import pm275.common.exception.ResourceException;

public class ActivityDialogResult
{
    public static final int EFFORT = 0;
    public static final int FIXED_DURATION = 1;

    public static final int HOURS = 0;
    public static final int DAYS = 1;

    private static final ResourceService resourceService = ResourceService.getInstance();

    private String name = null;
    private Date startDate = null;
    private boolean isFixedStartDate = true;
    private boolean isCompleted = false;
    private int durationType = ActivityDialogResult.FIXED_DURATION;
    private int duration = 8;
    private int durationMeasure = ActivityDialogResult.HOURS;

    private Map assignedResources = null;
    private Map availableResources = null;
    private Map assignedPreds = null;
    private Map availablePreds = null;

    public ActivityDialogResult()
    {
        assignedResources = new Hashtable();
        availableResources = new Hashtable();
        assignedPreds = new Hashtable();
        availablePreds = new Hashtable();
    }

    /**
     * @return Returns the name of the activity.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name The displayed name of the activity.
     * @throws ParameterFormatException Thrown if the name is null or empty.
     */
    public void setName(String name) throws ParameterFormatException
    {
        if( name == null )
        {
            throw new ParameterFormatException();
        }

        if( name.length() == 0 )
        {
            throw new ParameterFormatException();
        }

        this.name = name;
    }

    /**
     * @return Returns the startDate.
     */
    public Date getStartDate()
    {
        return startDate;
    }

    /**
     * @param startDate The start date of the activity.
     * @throws ParameterFormatException Thrown if the startDate is null.
     */
    public void setStartDate(Date startDate) throws ParameterFormatException
    {
        if( startDate == null )
        {
            throw new ParameterFormatException();
        }

        this.startDate = startDate;
    }

    /**
     *  Returns true if user selected fixed start date in the dialog.
     *  @return True if user selected fixed start date in the dialog, false otherwise.
     */
    public boolean getIsFixedStartDate()
    {
        return isFixedStartDate;
    }

    /**
     *  Sets the default start date mode in the dialog.
     *  @return True to select fixed start date in the dialog, false to have the
     *          start date calculated from dependencies.
     */
    public void setIsFixedStartDate(boolean isFixed)
    {
        isFixedStartDate = isFixed;
    }

    /**
     *  Returns true if the user completed the login dialog (by hitting "OK"),
     *  false otherwise.
     *  @return If the user completed the login dialog.
     */
    public boolean getIsCompleted()
    {
        return isCompleted;
    }

    /**
     *  Sets the flag indicating whether the user completed the login dialog.
     *  @param isCompleted True if the user completed the login dialog, false
     *                     otherwise.
     */
    public void setIsCompleted(boolean isCompleted)
    {
        this.isCompleted = isCompleted;
    }

    /**
     * @return Returns the duration in hours.
     */
    public int getDuration()
    {
        return duration;
    }


    /**
     * @param duration The duration to set in hours.
     */
    public void setDuration(int duration) throws ParameterFormatException
    {
        if( duration < 0 )
        {
            throw new ParameterFormatException();
        }

        this.duration = duration;
    }


    /**
     * @return Returns the durationType.
     */
    public int getDurationType()
    {
        return durationType;
    }


    /**
     * @param durationType The durationType to set.
     */
    public void setDurationType(int durationType) throws ParameterFormatException
    {
        if( durationType == EFFORT || durationType == FIXED_DURATION )
        {
            this.durationType = durationType;
        }
        else
        {
            throw new ParameterFormatException();
        }
    }

    /**
     * @return Returns the durationMeasure.
     */
    public int getDurationMeasure() {
        return durationMeasure;
    }


    /**
     * @param durationMeasure The durationMeasure to set.
     */
    public void setDurationMeasure(int durationMeasure) throws ParameterFormatException
    {
        if( durationMeasure == ActivityDialogResult.HOURS || durationMeasure == ActivityDialogResult.DAYS )
        {
            this.durationMeasure = durationMeasure;
        }
        else
        {
            throw new ParameterFormatException();
        }
    }


    public void addAvailablePredecessor( ActivityInterface activity )
    {
        this.availablePreds.put( activity.getActivityKey() , activity );
    }


    public void removeAvailablePredecessor( ActivityInterface activity )
    {
        this.availablePreds.remove( activity.getActivityKey() );
    }


    public Collection listAvailablePredecessors()
    {
        return this.availablePreds.values();
    }


    public void clearAvailablePredecessors()
    {
        this.availablePreds.clear();
    }


    public void addAssignedPredecessor( ActivityInterface activity )
    {
        this.assignedPreds.put( activity.getActivityKey() , activity );
    }


    public void removeAssignedPredecessor( ActivityInterface activity )
    {
        this.assignedPreds.remove( activity.getActivityKey() );
    }


    public Collection listAssignedPredecessors()
    {
        return this.assignedPreds.values();
    }


    public void clearAssignedPredecessors()
    {
        this.assignedPreds.clear();
    }


    public void addAvailableResource( ResourceInterface resource )
    {
        this.availableResources.put( resource.getKey() , resource );
    }


    public void removeAvailableResource( ResourceInterface resource )
    {
        this.availableResources.remove( resource.getKey() );
    }


    public Collection listAvailableResources()
    {
        return this.availableResources.values();
    }


    public void clearAvailableResources()
    {
        this.availableResources.clear();
    }


    public void addAssignedResource( ResourceInterface resource )
    {
        this.assignedResources.put( resource.getKey() , resource );
    }


    public void removeAssignedResource( ResourceInterface resource )
    {
        this.assignedResources.remove( resource.getKey() );
    }


    public Collection listAssignedResources()
    {
        return this.assignedResources.values();
    }


    public void clearAssignedResources()
    {
        this.assignedResources.clear();
    }

    /**
     *  Convenience method to create an activity dialog result from an activity.
     *  @param activity ActivityInterface from which to create a dialog result instance.
     *  @return Dialog result object with the parameters set based on the activity.
     */
    public static final ActivityDialogResult fromActivity(ActivityInterface activity)
    {
        ActivityDialogResult result = new ActivityDialogResult();

        // Set the name.
        try
        {
            result.setName(activity.getName());
        }
        catch (ParameterFormatException ex)
        {
            // TODO: What to do here?
            ex.printStackTrace();
        }

        // Set the start date.
        try
        {
            result.setStartDate(activity.getStartDate());
        }
        catch (ParameterFormatException ex)
        {
            // TODO: What to do here?
            ex.printStackTrace();
        }

        // Fixed start date?
        result.setIsFixedStartDate(activity.getIsFixedStartDate());

        // Set the duration.
        int duration = activity.getDuration();
        if (activity.getDurationMeasure() == DAYS)
        {
            // TODO: Replace the current block with the two commented lines below
            // when we get project calendar working correctly.
            // ProjectInterface project = ProjectService.getInstance().getProject(activity.getProjectKey());
            // duration /= CalendarService.getInstance().getCalendar(project.getCalendarKey()).getHoursPerDay();
            duration /= CalendarService.getInstance().getCorporateCalendar().getHoursPerDay();
        }
        try
        {
            result.setDuration(duration);
        }
        catch (ParameterFormatException ex)
        {
            // TODO: What to do here?
            ex.printStackTrace();
        }

        // Duration is hours.
        // TODO: Should we calculate this in a smarter way?
        try
        {
            result.setDurationMeasure(activity.getDurationMeasure());
        }
        catch (ParameterFormatException ex)
        {
            // TODO: What to do here?
            ex.printStackTrace();
        }

        // Duration type.
        try
        {
            result.setDurationType(activity.getIsFixedDuration() ?
                                   FIXED_DURATION : EFFORT);
        }
        catch (ParameterFormatException ex)
        {
            // TODO: What to do here?
            ex.printStackTrace();
        }

        try
        {
            ProjectInterface project =
                    ProjectService.getInstance().getProject(activity.getProjectKey());

            // Add activities to the assigned or available list.
            Collection predecessors = activity.getPredecessors();
            for (Iterator i = project.getActivities(null).iterator(); i.hasNext(); )
            {
                ActivityInterface a = (ActivityInterface) i.next();
                if (predecessors.contains(a))
                {
                    result.addAssignedPredecessor(a);
                }
                else if (! activity.getActivityKey().equals(a.getActivityKey()))
                {
                    result.addAvailablePredecessor(a);
                }
            }

            // Add resources to the assigned or available list.
            Collection resources = activity.getResources();
            ResourcePoolKey rpKey = project.getResourcePoolKey();
            ResourcePool pool = ResourceService.getInstance().getResourcePool(rpKey);
            if (pool != null)
            {
                for (Iterator i = pool.listResourceKeys().iterator(); i.hasNext(); )
                {
                    ResourceKey key = (ResourceKey) i.next();
                    ResourceInterface resource =
                            ResourceService.getInstance().getResource(key);
                    if (resources.contains(resource))
                    {
                        result.addAssignedResource(resource);
                    }
                    else
                    {
                        result.addAvailableResource(resource);
                    }
                }
            }
        }
        catch (KeyNotFoundException ex)
        {
            ex.printStackTrace();
        }
        catch (InternalProjectException ex)
        {
            ex.printStackTrace();
        }
        catch (ResourceException e1)
        {
            e1.printStackTrace();
        }

        result.setIsCompleted(false);
        return result;
    }

    /**
     *  Updates an activity with user data from this result object.
     *  @param activity The activity to update.  Must not be null.
     *  @return Updated activity.  Note that the activity is updated in place,
     *          so the caller might not care about the return value.
     *  @throws KeyNotFoundException If there's an invalid activity, project, or
     *                               resource key.
     *  @throws InternalActivityException If the activity's internal state is not
     *                                    valid.
     *  @throws InternalProjectException If the parent project's internal state is
     *                                   not valid.
     */
    public ActivityInterface setToActivity(ActivityInterface activity)
            throws KeyNotFoundException, InternalProjectException,
                   InternalActivityException, DuplicateKeyException,
                   CycleDependencyException
    {
        // Activity must be specified.
        if (activity == null)
        {
            throw new IllegalArgumentException("Activity cannot be null.");
        }

        // First, get the project.
        ProjectInterface project =
                ProjectService.getInstance().getProject(activity.getProjectKey());

        // Set the name and start date.
        activity.setName(getName());
        activity.setStartDate(getStartDate());

        // Fixed start date?
        activity.setIsFixedStartDate(getIsFixedStartDate());

        // Duration.
        int duration = getDuration();
        if (getDurationMeasure() == DAYS)
        {
            // TODO: Replace the current block with the two commented lines below
            // when we get project calendar working correctly.
            // ProjectInterface project = ProjectService.getInstance().getProject(activity.getProjectKey());
            // duration *= CalendarService.getInstance().getCalendar(project.getCalendarKey()).getHoursPerDay();
            duration *= CalendarService.getInstance().getCorporateCalendar().getHoursPerDay();
        }
        activity.setDuration(duration);
        activity.setDurationMeasure(getDurationMeasure());

        // store the type of duration (fixed,level of effort)
        if (getDurationType() == FIXED_DURATION)
        {
            activity.setIsFixedDuration(true);
        }
        else
        {
            activity.setIsFixedDuration(false);
        }

        // set whether we should display in days, hours, etc.
        activity.setDurationMeasure(getDurationMeasure());

        // add the resources assigned to the Activity
        //activity.clearResources();
        try {
            resourceService.deassignAll( activity.getActivityKey() , project.getProjectKey() );
        } catch (ResourceException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        Collection resources = listAssignedResources();
        for (Iterator i = resources.iterator(); i.hasNext(); )
        {
            ResourceInterface res = (ResourceInterface) i.next();
            //activity.addResource(res.getKey());
            //resourceService.assign(res.getKey(), activity.getActivityKey(),activity.getProjectKey(), 100);


            try
            {
                resourceService.assign(res.getKey(),activity.getActivityKey(), project.getProjectKey() ,100);
            }
            catch (ResourceException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        //activity.setResources();

        // Deal with the dependencies (predecessors).

        // First, get a list of the old and new dependencies.
        Collection oldDependencies = activity.getPredecessors();
        Collection newDependencies =
                (isFixedStartDate ? new Vector()
                                  : listAssignedPredecessors());

        // Determine the old dependencies that are no longer selected.
        Collection toRemove = new Vector();
        for (Iterator i = oldDependencies.iterator(); i.hasNext(); )
        {
            ActivityInterface predecessor = (ActivityInterface) i.next();
            if (! newDependencies.contains(predecessor))
            {
                toRemove.add(predecessor);
            }
        }

        // Now perform the removals.  This needs to be done separately from
        // creating the toRemove list.  Otherwise, if we try to do this in
        // the above loop, we will get a ConcurrentModificationException.
        for (Iterator i = toRemove.iterator(); i.hasNext(); )
        {
            ActivityInterface predecessor = (ActivityInterface) i.next();

            if( ! predecessor.isStartMilestone() )
            {
                predecessor.removeSuccessor(activity.getActivityKey());
                activity.removePredecessor(predecessor.getActivityKey());
            }
        }

        // Add new dependencies.
        for (Iterator i = newDependencies.iterator(); i.hasNext(); )
        {
            ActivityInterface dependency = (ActivityInterface) i.next();
            if (! oldDependencies.contains(dependency))
            {
                project.associateActivities(dependency.getActivityKey(),
                                            activity.getActivityKey());
            }
        }

        // Always re-calculate the start and end date to make sure that they
        // are in sync and correct.
        activity.computeStartDate();
        activity.computeEndDate(activity.getStartDate(), activity.getDuration());

        return activity;
    }
}


/**
 * $Log: ActivityDialogResult.java,v $
 * Revision 1.24  2005/05/15 18:03:49  achou
 * Prevent crash in fromActivity() when resource pool is null.
 *
 * Revision 1.23  2005/05/09 03:17:54  dnathan
 * Moved activity.ClearREesources() to the ResourceService and nuked activity.setResources()...
 *
 * Revision 1.22  2005/04/30 22:24:20  achou
 * Added flag and getter/setter for fixed start date.
 * When updating activity, remove all dependencies if start date is fixed, and always re-calculate both start and end dates.
 *
 * Revision 1.21  2005/04/27 02:02:11  rcoutinh
 * ResourceService.assign now accepts ProjectKey
 *
 * Revision 1.20  2005/04/26 09:49:10  dnathan
 * Added future code awaiting core changes.
 *
 * Revision 1.19  2005/04/23 13:07:30  jkeiser
 * Changed to call resourceService.assign instead of activity.addResource.
 *
 * Revision 1.18  2005/04/19 17:54:26  dnathan
 * Resolved bug where project end date was not moving.
 *
 * Revision 1.17  2005/04/18 14:35:31  achou
 * Re-wrote code in setToActivity() to correctly remove dependencies from activity that are no longer needed and associate new dependencies.
 *
 * Revision 1.16  2005/04/18 07:02:28  achou
 * Uncommented call to Activity.addResource().  Fixed code that populates the available/assigned resources lists in fromActivity().
 *
 * Revision 1.15  2005/04/17 23:25:26  vchumako
 * Implemented comparable for PM275BaseKey.
 * Now it's not required to use getKeyId() in order to put or remove keys from the Map.
 *
 * Revision 1.14  2005/04/17 18:08:18  dnathan
 * Fixed case where activities were singly liked, not in both directions.
 *
 * Revision 1.13  2005/04/17 16:36:05  dnathan
 * Warning hunting.
 *
 * Revision 1.12  2005/04/17 11:42:16  achou
 * New method setToActivity() to update an activity with user data from the result object.  Added code that sets up list of assigned/available activities and resources when creating result object.
 *
 * Revision 1.11  2005/04/17 09:18:52  achou
 * Converted Enumeration code to use Collection instead.
 *
 * Revision 1.10  2005/04/15 06:32:15  dnathan
 * Added beginnings of Lists for Resources and Dependencies.
 *
 * Revision 1.9  2005/04/11 03:19:46  achou
 * Added static method for creating a result object from an activity.
 *
 * Revision 1.8  2005/04/03 20:37:13  dnathan
 * Added code to support measurement types of hours and days.
 *
 * Revision 1.7  2005/04/03 05:20:25  dnathan
 * Added new properties for duration, duration type and predecessors.
 *
 * Revision 1.6  2005/03/29 04:24:45  dnathan
 * Added JavaDoc comments.
 *
 * Revision 1.5  2005/03/29 01:41:08  dnathan
 * Added implementation code for Iteration 1.  All elements are protected to allow for JUnit tests to work.
 *
 * Revision 1.4  2005/03/29 01:00:15  dnathan
 * Added attribute and get/set methods indicating whether the user completed the login dialog.
 *
 * Revision 1.3  2005/03/23 01:55:23  dnathan
 * Updated headers and footers to reflect coding standards.
 *
 * Revision 1.2  2005/03/22 07:19:41  dnathan
 * Added tests to check for null or empty names and start dates.
 *
 */
