/***************************************************
 * File:        $RCSfile: LoginDialogResult.java,v $
 * @author      $Author: dnathan $,
 * @version     $Id: LoginDialogResult.java,v 1.6 2005/03/29 04:24:45 dnathan Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:	A class for communicating between the main GUI and the login dialog.
 * </p>
 * <p>
 * Purpose: 	This class is used to manage passing usernames and passwords between the
 * 				application and the login dialog.
 * </p>
 */
package pm275.gui.dialogs;

public class LoginDialogResult
{
    private String username = null;
    private String password = null;
    private boolean isCompleted = false;

    /**
     * @return Returns the password.
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * @param password
     *            The password to set.
     */
    public void setPassword(String password) throws ParameterFormatException
    {
        if( password == null )
        {
            throw new ParameterFormatException();
        }

        if( password.length() == 0 )
        {
            throw new ParameterFormatException();
        }

        this.password = password;
    }

    /**
     * @return Returns the username.
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * @param username The username to set.
     * @throws ParameterFormatException Thrown if the username is null or empty.
     */
    public void setUsername(String username) throws ParameterFormatException
    {
        if( username == null )
        {
            throw new ParameterFormatException();
        }

        if( username.length() == 0 )
        {
            throw new ParameterFormatException();
        }

        this.username = username;
    }

    /**
     *  Returns true if the user completed the login dialog (by hitting "OK"),
     *  false otherwise.
     *  @return If the user completed the login dialog.
     */
    public boolean getIsCompleted()
    {
        return isCompleted;
    }

    /**
     *  Sets the flag indicating whether the user completed the login dialog.
     *  @param isCompleted True if the user completed the login dialog, false
     *                     otherwise.
     */
    public void setIsCompleted(boolean isCompleted)
    {
        this.isCompleted = isCompleted;
    }
}

/**
 * $Log: LoginDialogResult.java,v $
 * Revision 1.6  2005/03/29 04:24:45  dnathan
 * Added JavaDoc comments.
 *
 * Revision 1.5  2005/03/28 12:12:41  achou
 * Added attribute and get/set methods indicating whether the user completed the login dialog.
 *
 * Revision 1.4  2005/03/23 01:55:22  dnathan
 * Updated headers and footers to reflect coding standards.
 *
 * Revision 1.2  2005/03/22 07:19:41  dnathan
 * Added tests to check for null or empty names and start dates.
 *
 */
