package pm275.gui.dialogs;

import java.util.Collection;
import java.util.Collections;
import java.util.Vector;

import pm275.core.project.ProjectInterface;
import pm275.core.resource.ResourceKey;

public class ResourceActivityReportDialogConfig 
{
    private ProjectInterface project;
	private boolean completed = false;
	private Vector resources;
	
	public ResourceActivityReportDialogConfig()
	{
		this.resources = new Vector();
	}

    public ProjectInterface getProject() 
    {
        return project;
    }
    
    public void setProject(ProjectInterface project) 
    {
        this.project = project;
    }
	
	public Collection listResources()
	{
		return Collections.unmodifiableCollection( this.resources );
	}

	public void clearResources()
	{
		this.resources.clear();
	}
	
	public void addResource( ResourceKey resource )
	{
		this.resources.add( resource );
	}
	
	public void removeResource( ResourceKey resource )
	{
		this.resources.remove( resource );
	}

	public boolean isCompleted() {
		return completed;
	}
	

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}
	
}
