package pm275.gui.dialogs;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import pm275.core.project.Activity;


public class ActivityModel implements ListModel
{
    private List activities;
    private List listeners;
    private int size;

    public ActivityModel( Collection activities )
    {
        this.activities = new LinkedList(activities);
        this.size = activities.size();
        this.listeners = new Vector();
    }

    public int getSize()
    {
        return this.size;
    }

    public Object getElementAt(int index)
    {
        Activity a = (Activity) this.activities.get( index );
        return a.getName();
    }

    public void addListDataListener(ListDataListener l)
    {
        this.listeners.add(l);
    }

    public void removeListDataListener(ListDataListener l)
    {
        this.listeners.remove(l);
    }

    public Activity getItem( int index )
    {
        return (Activity) this.activities.get( index );
    }

    public synchronized void removeItem( int index )
    {
        this.size -= 1;
        this.activities.remove( index );
        ListDataEvent e = new ListDataEvent( this , ListDataEvent.INTERVAL_REMOVED , index, index );

        Iterator i = listeners.iterator();
        while(i.hasNext())
        {
            ListDataListener listener = (ListDataListener) i.next();
            listener.intervalRemoved(e);
        }
    }

    public synchronized void addItem( Activity r )
    {
        this.activities.add( r );
        ListDataEvent e = new ListDataEvent( this , ListDataEvent.INTERVAL_ADDED , this.size, this.size );
        this.size += 1;

        Iterator i = listeners.iterator();
        while(i.hasNext())
        {
            ListDataListener listener = (ListDataListener) i.next();
            listener.intervalAdded(e);
        }
    }
}