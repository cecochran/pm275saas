/*
 *  File      :  OpenProjectDialog.java
 *  Version   :  $Id: StringListModel.java,v 1.1 2005/05/01 20:47:20 dnathan Exp $
 *  Author    :  Dan Nathan
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */
package pm275.gui.dialogs;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

public class StringListModel implements ListModel
{
    private List listeners;
    private List items;
    
    public StringListModel( Collection elements )
    {
        this.items = new LinkedList(elements);
        this.listeners = new Vector();
    }
    
    public int getSize() 
    {
        return this.items.size();
    }

    public Object getElementAt(int arg0) 
    {
        return this.items.get(arg0);
    }

    public void addListDataListener(ListDataListener l)
    {
        this.listeners.add(l);
    }

    public void removeListDataListener(ListDataListener l)
    {
        this.listeners.remove(l);
    }
}
/**
 * $Log: StringListModel.java,v $
 * Revision 1.1  2005/05/01 20:47:20  dnathan
 * New file.
 *
 * 
 **/
