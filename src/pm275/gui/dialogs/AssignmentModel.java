package pm275.gui.dialogs;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import pm275.common.exception.InternalProjectException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.ResourceException;
import pm275.core.project.Activity;
import pm275.core.project.ActivityInterface;
import pm275.core.project.ActivityKey;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ProjectService;
import pm275.core.resource.AssignmentInterface;
import pm275.core.resource.AssignmentKey;
import pm275.core.resource.ResourceService;


public class AssignmentModel implements ListModel
{
    private List assignments;
    private List listeners;
	private ProjectInterface project;
    private int size;

    public AssignmentModel( Collection assignments , ProjectInterface project )
    {
        this.assignments = new LinkedList(assignments);
        this.size = assignments.size();
        this.listeners = new Vector();
		this.project = project;
    }

    public int getSize()
    {
        return this.size;
    }

    public Object getElementAt(int index)
    {
        AssignmentKey a = (AssignmentKey) this.assignments.get( index );
		AssignmentInterface ai = null;
		
		try 
		{
			ai = ResourceService.getInstance().getAssignment(a);
		} 
		catch (ResourceException e) 
		{
			return "";
		}
		
		ActivityKey ak = ai.getActivityKey();
		ActivityInterface acti = null;
		
		try 
		{
			acti = ProjectService.getInstance().getActivity( this.project.getProjectKey() , ak );
		} 
		catch (KeyNotFoundException e) 
		{
			return "";
		} 
		catch (InternalProjectException e) 
		{
			return "";
		}
		
        return acti.getName();
    }

    public void addListDataListener(ListDataListener l)
    {
        this.listeners.add(l);
    }

    public void removeListDataListener(ListDataListener l)
    {
        this.listeners.remove(l);
    }

    public AssignmentKey getItem( int index )
    {
        return (AssignmentKey) this.assignments.get( index );
    }

    public synchronized void removeItem( int index )
    {
        this.size -= 1;
        this.assignments.remove( index );
        ListDataEvent e = new ListDataEvent( this , ListDataEvent.INTERVAL_REMOVED , index, index );

        Iterator i = listeners.iterator();
        while(i.hasNext())
        {
            ListDataListener listener = (ListDataListener) i.next();
            listener.intervalRemoved(e);
        }
    }

    public synchronized void addItem( Activity r )
    {
        this.assignments.add( r );
        ListDataEvent e = new ListDataEvent( this , ListDataEvent.INTERVAL_ADDED , this.size, this.size );
        this.size += 1;

        Iterator i = listeners.iterator();
        while(i.hasNext())
        {
            ListDataListener listener = (ListDataListener) i.next();
            listener.intervalAdded(e);
        }
    }
}