/*
 *  File      :  OpenCalendarDialogResult.java
 * @author    :  $Author: jkeiser $,  
 * @version   :  $Id: OpenCalendarDialogResult.java,v 1.1 2005/05/08 01:50:29 jkeiser Exp $
 *  Author    :  Jennifer Keiser
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */
package pm275.gui.dialogs;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Hashtable;
import pm275.core.calendar.CalendarInterface;

public class OpenCalendarDialogResult 
{
    private Map calendarNames = null;
    private boolean isCompleted = false;
    private String selectedName = null;
    private CalendarInterface selectedCalendar = null;
    
    public OpenCalendarDialogResult()
    {
        this.calendarNames = new Hashtable();
    }
    
    public void addCalendarNameChoice( String name ) throws ParameterFormatException
    {
        if( name == null || name.length() == 0 )
        {
            throw new ParameterFormatException();
        }
        
        this.calendarNames.put( name , name );
    }
    
    public void removeCalendarNameChoice( String name )
    {
        this.calendarNames.remove( name );
    }
    
    public void clearCalendarNameChoices()
    {
        this.calendarNames.clear();
    }
    
    public Collection listCalendarNameChoices()
    {
        return Collections.unmodifiableCollection( this.calendarNames.values() );
    }
    
    public String getSelectedCalendarName()
    {
        return this.selectedName;
    }
    
    public void setSelectedCalendarName( String name ) throws ParameterFormatException
    {
        if( name == null || name.length() == 0 )
        {
            throw new ParameterFormatException();
        }
        this.selectedName = name;
    }

    public CalendarInterface getSelectedCalendar()
    {
        return this.selectedCalendar;
    }
    
    public void setSelectedCalendar( CalendarInterface cal ) throws ParameterFormatException
    {
        if( cal == null )
        {
            throw new ParameterFormatException();
        }
        this.selectedCalendar = cal;
    }

    /**
     * @return Returns the isCompleted.
     */
    public boolean getIsCompleted() 
    {
        return isCompleted;
    }
    

    /**
     * @param isCompleted The isCompleted to set.
     */
    public void setIsCompleted(boolean isCompleted) 
    {
        this.isCompleted = isCompleted;
    }
    
}

/**
 * $Log: OpenCalendarDialogResult.java,v $
 * Revision 1.1  2005/05/08 01:50:29  jkeiser
 * Initial checkin.
 *
 *
**/