/*
 *  File      :  OpenProjectDialogResult.java
 *  Version   :  $Id: OpenProjectDialogResult.java,v 1.3 2005/05/15 23:29:42 tlong2 Exp $
 *  Author    :  Dan Nathan
 *  Assignment:  CSCI E-275 Team 1 Project
 *  Contents  :  Please see class comments below.
 *  Purpose   :  Please see class comments below.
 */
package pm275.gui.dialogs;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Hashtable;
import java.util.Vector;

import pm275.core.project.ProjectAbba;

public class OpenProjectDialogResult 
{
    private Map projectNames = null;
    private boolean isCompleted = false;
    private ProjectAbba selectedProject = null;
    
    public OpenProjectDialogResult()
    {
        this.projectNames = new Hashtable();
    }
    
    public void addProjectNameChoice( ProjectAbba project ) throws ParameterFormatException
    {
        if( project == null )
        {
            throw new ParameterFormatException();
        }
        
        this.projectNames.put( project.getName() , project );
    }
    
    public void removeProjectNameChoice( String name )
    {
        this.projectNames.remove( name );
    }
    
    public void clearProjectNameChoices()
    {
        this.projectNames.clear();
    }
    
    public Collection listProjectNameChoices()
    {
        //convert to Strings
        Vector v = new Vector();
        Iterator projectABBAs = this.projectNames.values().iterator();
        
        while( projectABBAs.hasNext() )
        {
            ProjectAbba ABBA = (ProjectAbba) projectABBAs.next();
            v.add( ABBA.getName() );
        }
        
        return v;
    }
    
    public ProjectAbba getSelectedProject()
    {
        return this.selectedProject;
    }
    
    public void setSelectedProjectName( String name ) throws ParameterFormatException
    {
        if( name == null || name.length() == 0 )
        {
            throw new ParameterFormatException();
        }
        
        this.selectedProject = (ProjectAbba) this.projectNames.get( name );
    }

    /**
     * @return Returns the isCompleted.
     */
    public boolean getIsCompleted() 
    {
        return isCompleted;
    }
    

    /**
     * @param isCompleted The isCompleted to set.
     */
    public void setIsCompleted(boolean isCompleted) 
    {
        this.isCompleted = isCompleted;
    }
    
}

/**
 * $Log: OpenProjectDialogResult.java,v $
 * Revision 1.3  2005/05/15 23:29:42  tlong2
 * cleaned up compile warnings
 *
 * Revision 1.2  2005/05/09 01:56:43  dnathan
 * Added code to serialize from DB.
 *
 * Revision 1.1  2005/05/01 20:47:20  dnathan
 * New file.
 *
 *
 *
**/