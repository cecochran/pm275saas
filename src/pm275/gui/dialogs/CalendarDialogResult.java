/***************************************************
 * File:        $RCSfile: CalendarDialogResult.java,v $
 * @author      $Author: tlong2 $,
 * @version     $Id: CalendarDialogResult.java,v 1.10 2005/05/15 23:29:42 tlong2 Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:    A class for communicating between the main GUI and the calendar edit/create dialog.
 * </p>
 * <p>
 * Purpose:     This class is used to communicate Calendar details between the 
 *              project calendar dialog and the application.
 * </p>
 */
package pm275.gui.dialogs;

import org.apache.log4j.Logger;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Hashtable;
import java.util.Map;
import pm275.core.calendar.CalendarService;
import pm275.core.calendar.CalendarInterface;
import pm275.core.calendar.Holiday;
import pm275.common.exception.KeyNotFoundException;
import pm275.gui.panels.MainPanel;

public class CalendarDialogResult
{
    private static final Logger log = Logger.getLogger(CalendarDialogResult.class);
    private static final String ATTR = "CalendarDialogResult";
	protected CalendarService calendarService = CalendarService.getInstance();
	private CalendarDialog myDialog = null;

	private String name = null;
    private int hoursPerDay = 8;
    private boolean workWeek[] = null;
    private Map holidays = new Hashtable();
    
    private static final int SUNDAY = java.util.Calendar.SUNDAY;
    private static final int MONDAY = java.util.Calendar.MONDAY;
    private static final int TUESDAY = java.util.Calendar.TUESDAY;
    private static final int WEDNESDAY = java.util.Calendar.WEDNESDAY;
    private static final int THURSDAY = java.util.Calendar.THURSDAY;
    private static final int FRIDAY = java.util.Calendar.FRIDAY;
    private static final int SATURDAY = java.util.Calendar.SATURDAY;

    private MainPanel mainPanel = null;
    private boolean isCompleted = false;
    
    public CalendarDialogResult(boolean setDefaultWorkDays)
    {
    	workWeek = new boolean[8];
    	if (setDefaultWorkDays)
    	{
    		workWeek[SUNDAY] = false;
    		workWeek[MONDAY] = true;
    		workWeek[TUESDAY] = true;
    		workWeek[WEDNESDAY] = true;
    		workWeek[THURSDAY] = true;
    		workWeek[FRIDAY] = true;
    		workWeek[SATURDAY] = false;
    	}
    }

    /**
     * @return Returns the name of the calendar.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name The displayed name of the calendar.
     * @throws ParameterFormatException Thrown if the name is null or empty.
     */
    public void setName(String name) throws ParameterFormatException
    {
        if( name == null )
        {
            throw new ParameterFormatException();
        }

        if( name.length() == 0 )
        {
            throw new ParameterFormatException();
        }

        this.name = name;
    }
    
    /**
     * @return Returns ...
     */
    public boolean getWorkDay(int day)
    {
        return workWeek[day];
    }   
    
    /**
     * @param 
     * @throws ParameterFormatException Thrown if ...
     */
    public void setWorkDay(int day, boolean enabled) throws ParameterFormatException
    {
        if(( day < 1 )|( day > 7 ))
        {
            throw new ParameterFormatException();
        }
        this.workWeek[day] = enabled;
    }   

    /**
     * @return Returns the work hours per day.
     */
    public int getHoursPerDay()
    {
        return hoursPerDay;
    }

    /**
     * @param hours The hours of work per day.
     */
    public void setHoursPerDay(int hours) throws ParameterFormatException
    {
        if(( hours < 0 )|( hours > 24 ))
        {
            throw new ParameterFormatException();
        }

        this.hoursPerDay = hours;       
    }

    public void addHoliday( Holiday holiday )
    {
        this.holidays.put( holiday.getDate() , holiday );               
    }

    public void removeHoliday( Holiday holiday )
    {
        this.holidays.remove( holiday.getDate() );
    }

    /**
     * @return Returns the hashtable of holidays
     */
    public Map getHolidays()
    {
        return this.holidays;
    }

    public Collection listHolidays()
    {
        return this.holidays.values();
    }

    public void clearHolidays()
    {
        this.holidays.clear();
    }

    /**
     *  Returns true if the user completed the calendar dialog (by hitting "OK"),
     *  false otherwise.
     *  @return If the user completed the calendardialog.
     */
    public boolean getIsCompleted()
    {
        return isCompleted;
    }

    /**
     *  Sets the flag indicating whether the user completed the calendar dialog.
     *  @param isCompleted True if the user completed the calendar dialog, false
     *                     otherwise.
     */
    public void setIsCompleted(boolean isCompleted)
    {
        this.isCompleted = isCompleted;
    }
    
    /**
     * @return Returns the main panel to which the calendar belongs.
     */
    public MainPanel getMainPanel()
    {
        return mainPanel;
    }
    /**
     * @param name The displayed name of the calendar.
     * @throws ParameterFormatException Thrown if the name is null or empty.
     */
    public void setMainPanel(MainPanel panel) throws ParameterFormatException
    {
        if( panel == null )
        {
            throw new ParameterFormatException();
        }

        this.mainPanel = panel;
    }

    /**
     * @param dialog The dialog associated with this DialogResult.
     */
    public void setDialog(CalendarDialog dialog) 
    {
        this.myDialog = dialog;
    }
    
    /**
     *  Convenience method to create a calendar dialog result from a calendar.
     *  @param calendar CalendarInterface from which to create a dialog result instance.
     *  @return Dialog result object with the parameters set based on the calendar.
     */
    public final CalendarDialogResult fromCalendar(CalendarInterface calendar)
    {
        // Set the name
        try
        {
            this.setName(calendar.getName());            
        }
        catch (ParameterFormatException ex)
        {
            log.error(ex.getMessage());
        }

        // Set the hours per day
        try
        {
            this.setHoursPerDay(calendar.getHoursPerDay());
        }
        catch (ParameterFormatException ex)
        {
            log.error(ex.getMessage());
        }
 
        try
		{
        	Hashtable existingHolidays = calendar.getRestrictedDays();
        	for (Enumeration i = existingHolidays.elements(); i.hasMoreElements(); )
        	{
        		Holiday h = (Holiday) i.nextElement();
                this.addHoliday(h);
        	}
      
        	//Set work days
            Collection existingWorkWeek = calendar.getWorkWeek();
            for (Iterator j = existingWorkWeek.iterator(); j.hasNext(); )
            {
            	Integer dayOfWeek = (Integer)j.next(); 
            	//store only workdays that are valid
            	if ((dayOfWeek.intValue() > 0) && (dayOfWeek.intValue() < 8))
            	{
            		this.setWorkDay(dayOfWeek.intValue(), true);
            	}
            }	

        }
        catch (ParameterFormatException pfe)
		{
        	log.error("Internal error storing days of week: "+pfe);
		}
        catch (Exception e)
        {
        	log.error(e.getMessage());
        }

        // Store the main panel for use by child dialogs (e.g. addHoliday) 
        try
        {
            this.setMainPanel(this.mainPanel);
        }
        catch (ParameterFormatException ex)
        {
            log.error(ex.getMessage());
        }
        
        this.setIsCompleted(false);
        return this;
    }

    /**
     *  Updates a calendar with user data from this result object.
     *  @param calendar The calendar to update.  Must not be null.
     *  @return Updated calendar.  Note that the calendar is updated in place,
     *          so the caller might not care about the return value.
     *  @throws KeyNotFoundException If there's an invalid calendar key
     */
    public CalendarInterface setToCalendar(CalendarInterface calendar)
            throws KeyNotFoundException, IllegalArgumentException
    {
        // Calendar must be specified.
        if (calendar == null)
        {
            throw new IllegalArgumentException("Calendar cannot be null.");
        }

        // Calendar must be valid
        try 
        {
        	calendarService.getCalendar(calendar.getKey());
        }
        catch (Exception e)
		{
            throw new IllegalArgumentException("Calendar does not exist.");
        }

        // Set the attributes
        calendar.setName(getName());
        calendar.setHoursPerDay(getHoursPerDay());
        
        int storedWorkWeek[] = new int[7];
        int workDays = 0;
        if (workWeek[SUNDAY]) {
		   storedWorkWeek[workDays] = java.util.Calendar.SUNDAY;
		   workDays++;   
        }
        if (workWeek[MONDAY]) {
 		   storedWorkWeek[workDays] = java.util.Calendar.MONDAY;
 		   workDays++;	   
         }
        if (workWeek[TUESDAY]) {
 		   storedWorkWeek[workDays] = java.util.Calendar.TUESDAY;
 		   workDays++; 		   
        }
        if (workWeek[WEDNESDAY]) {
 		   storedWorkWeek[workDays] = java.util.Calendar.WEDNESDAY;
 		   workDays++; 		   
         }
        if (workWeek[THURSDAY]) {
 		   storedWorkWeek[workDays] = java.util.Calendar.THURSDAY;
 		   workDays++;	   
         }
        if (workWeek[FRIDAY]) {
 		   storedWorkWeek[workDays] = java.util.Calendar.FRIDAY;
 		   workDays++;		   
         }
        if (workWeek[SATURDAY]) {
 		   storedWorkWeek[workDays] = java.util.Calendar.SATURDAY;
 		   workDays++;
         }
					
		calendar.setWorkWeek(storedWorkWeek);
		calendar.setDaysPerWeek(workDays);        
        
        // add the holidays defined for the Calendar
        Collection holidays = listHolidays();
        calendar.clearRestrictedDays();       
        for (Iterator i = holidays.iterator(); i.hasNext(); )
        {
            Holiday hol = (Holiday) i.next();
             
            try 
            {
                calendar.addRestrictedDay(hol);            
            } 
            catch (Exception e) {
                log.error(e.getMessage());
            }
        }

        return calendar;
    }
}

/**
* $Log: CalendarDialogResult.java,v $
* Revision 1.10  2005/05/15 23:29:42  tlong2
* cleaned up compile warnings
*
* Revision 1.9  2005/05/15 00:02:11  jkeiser
* Minor changes while debugging editing holidays.
*
* Revision 1.8  2005/05/12 03:24:24  jkeiser
* Fixed AWT error in fromCalendar.  Changed constructor to support initialization of week or not.
*
* Revision 1.7  2005/05/09 09:37:47  jkeiser
* Added debugging messages
*
* Revision 1.6  2005/05/08 22:20:44  jkeiser
* Added support for edit and removal of holidays.
*
* Revision 1.5  2005/05/08 14:20:46  jkeiser
* Added debugging messages.
*
* Revision 1.4  2005/05/08 01:54:18  jkeiser
* Enhanced to support storage of holdays
*
* Revision 1.3  2005/05/07 11:52:57  jkeiser
* Changed to keep track of the MainPanel from which it came (HolidayDialog needs it).  Updated to take advantage of new Holiday class.
*
* Revision 1.2  2005/05/01 16:03:51  jkeiser
* Worked on fromCalendar and setToCalendar methods.  (More still to do.)
*
* Revision 1.1  2005/04/30 22:23:54  jkeiser
* Initial checkin
*
*
**/