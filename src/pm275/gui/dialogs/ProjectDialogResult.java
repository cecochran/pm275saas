/***************************************************
 * File:        $RCSfile: ProjectDialogResult.java,v $
 * @author      $Author: jkeiser $,
 * @version     $Id: ProjectDialogResult.java,v 1.10 2005/05/01 16:00:51 jkeiser Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:    A class for communicating between the main GUI and the project edit/create dialog.
 * </p>
 * <p>
 * Purpose:     This class is used to communicate Project details between the "New Project"
 *              and "Edit Project" dialogs and the application.
 * </p>
 */
package pm275.gui.dialogs;

import java.util.Date;

import pm275.core.project.ProjectInterface;
import pm275.core.calendar.CalendarService;
import pm275.core.calendar.CalendarInterface;
import pm275.common.exception.KeyNotFoundException;

public class ProjectDialogResult
{
    private String name = null;
    private Date startDate = null;
    private CalendarInterface calendar = null;
    private boolean isCompleted = false;

    /**
     * @return Returns the name.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name The name to set.
     * @throws ParameterFormatException Thrown if the name is null or empty.
     */
    public void setName(String name) throws ParameterFormatException
    {
        if( name == null )
        {
            throw new ParameterFormatException();
        }

        if( name.length() == 0 )
        {
            throw new ParameterFormatException();
        }

        this.name = name;
    }

    /**
     * @return Returns the startDate.
     */
    public Date getStartDate()
    {
        return startDate;
    }

    /**
     * @param startDate The startDate to set.
     * @throws ParameterFormatException Thrown if the date is null.
     */
    public void setStartDate(Date startDate) throws ParameterFormatException
    {
        if( startDate == null )
        {
            throw new ParameterFormatException();
        }

        this.startDate = startDate;
    }

    /**
     * @return Returns the project calendar.
     */
    public CalendarInterface getCalendar()
    {
        return calendar;
    }

    /**
     * @param calendar The calendar to set.
     * @throws ParameterFormatException Thrown if the calendar is null.
     */
    public void setCalendar(CalendarInterface cal) throws ParameterFormatException
    {
        if( cal == null )
        {
            throw new ParameterFormatException();
        }

        this.calendar = cal;
    }
    
    /**
     *  Returns true if the user completed the login dialog (by hitting "OK"),
     *  false otherwise.
     *  @return If the user completed the login dialog.
     */
    public boolean getIsCompleted()
    {
        return isCompleted;
    }

    /**
     *  Sets the flag indicating whether the user completed the login dialog.
     *  @param isCompleted True if the user completed the login dialog, false
     *                     otherwise.
     */
    public void setIsCompleted(boolean isCompleted)
    {
        this.isCompleted = isCompleted;
    }

    /**
     *  Convenience method to create a project dialog result from a project.
     *  @param project Project from which to create a dialog result instance.
     *  @return Dialog result object with the parameters set based on the project.
     */
    public static final ProjectDialogResult fromProject(ProjectInterface project)
    {
        ProjectDialogResult result = new ProjectDialogResult();

        // Set the name.
        try
        {
            result.setName(project.getName());
        }
        catch (ParameterFormatException ex)
        {
            // TODO: What to do here?
            ex.printStackTrace();
        }

        // Set the start date.
        try
        {
            result.setStartDate(project.getStartDate());
        }
        catch (ParameterFormatException ex)
        {
            // TODO: What to do here?
            ex.printStackTrace();
        }

        // Set the calendar
        try
        {
            result.setCalendar(CalendarService.getInstance().getCalendar(project.getCalendarKey()));
        }
        catch (ParameterFormatException ex)
        {
            // TODO: What to do here?
            ex.printStackTrace();
        }
        catch (KeyNotFoundException knfe)
        {
            // TODO: What to do here?
            knfe.printStackTrace();
        }
        
        result.setIsCompleted(false);
        return result;
    }
}

/**
 * $Log: ProjectDialogResult.java,v $
 * Revision 1.10  2005/05/01 16:00:51  jkeiser
 * Added support for specifying project calendar.
 *
 * Revision 1.9  2005/04/16 02:28:49  dnathan
 * Code clean up.
 *
 * Revision 1.8  2005/04/15 22:29:39  ccochran
 * Fixed to use Project Interface
 *
 * Revision 1.7  2005/03/30 16:02:22  dnathan
 * Removed unneeded warnings.
 *
 * Revision 1.6  2005/03/29 08:53:26  achou
 * Added convenience method to create a dialog result from a project.
 *
 * Revision 1.5  2005/03/29 04:24:44  dnathan
 * Added JavaDoc comments.
 *
 * Revision 1.4  2005/03/29 01:00:14  dnathan
 * Added attribute and get/set methods indicating whether the user completed the login dialog.
 *
 * Revision 1.3  2005/03/23 01:55:23  dnathan
 * Updated headers and footers to reflect coding standards.
 *
 * Revision 1.2  2005/03/22 07:19:41  dnathan
 * Added tests to check for null or empty names and start dates.
 *
 */
