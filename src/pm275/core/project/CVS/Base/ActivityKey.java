/***************************************************
 * File:        $RCSfile: ActivityKey.java,v $
 * @author      $Author: tlong2 $,
 * @version     $Id: ActivityKey.java,v 1.6 2005/03/28 03:50:07 tlong2 Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose: The ActivityKey represents the HashKey value of the activity
 * that uniquely identifies an Activity among all other activities.
 * The key is the first activity name given to an activity,
 * which is assumed to be unique.
 * </p>
 */
package pm275.core.project;
import org.apache.log4j.Logger;
import pm275.common.PM275BaseKey;
/**
 * <p>ActivityKey class</p>
 */
public class ActivityKey extends PM275BaseKey {

    private static final Logger log = Logger.getLogger(ActivityKey.class);
    private static final String ATTR = "ActivityKey";

	/**
	 * Only public constructor. Produced new id from java.util.Calendar object.
	 *
	 */
	public ActivityKey()
	{
		super();
	}
	
    public ActivityKey(String k) {
        super(k);
     }
/**
 * <p>Compare A key value to current value of this key ...</p>
 *
 * @param key ActivityKey value
 * @return True of Key matches, False if not

 */
    public boolean equals(ActivityKey k) {
        return super.equals(k);
    }

/**
 * <p>Return the value of the key as a string.</p>
 *
 * @return key  string value (activity name)
 */
    public String toString() {
        return super.toString();
    }
 }


/**
 * $Log: ActivityKey.java,v $
 * Revision 1.6  2005/03/28 03:50:07  tlong2
 * Completed all Stories for Iteration I
 *
 * Revision 1.5  2005/03/26 14:24:16  ccochran
 * fixed constructor
 *
 * Revision 1.4  2005/03/26 14:00:44  ccochran
 * Trying to extend Keys
 *
 * Revision 1.3  2005/03/26 12:01:00  ccochran
 * Log4J import
 *
 * Revision 1.2  2005/03/26 10:19:29  achou
 * Fixed package designation.
 *
 * Revision 1.1  2005/03/26 09:02:36  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.5  2005/03/23 04:44:48  tlong2
 * Minor updates..
 *
 * Revision 1.4  2005/03/23 01:47:50  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.3  2005/03/22 04:31:00  dnathan
 * Updated header to include CVS ID.
 *
 * Updated footer to include CVS log.
 *
 * Revision 1.2  2005/03/22 04:12:02  dnathan
 * Refactored to pm275.core package.
 *
 * Updated header to include CVS ID.
 *
 * Updated footer to include CVS log.
 *
 **/
