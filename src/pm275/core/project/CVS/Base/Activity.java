/***************************************************
 * File:        $RCSfile: Activity.java,v $
 * @author      $Author: ccochran $,
 * @version     $Id: Activity.java,v 1.77 2005/05/14 00:11:51 ccochran Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:     The Activity is the element of work to be done in a Project.
 * An Activity has a unique key identifier, name, duration,
 * oneor more Resources, time report.
 * Predecessors and Sucessors are Activity objects that come before
 * and after the current Activity and which the current activity is
 * dependent on.
 * </p>
 */
package pm275.core.project;
import org.apache.log4j.Logger;

//import java.net.*;

import java.util.Map;
import java.util.Hashtable;
import java.util.Date;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.ArrayList;


import pm275.common.exception.InternalActivityException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.ResourceException;
import pm275.core.resource.Assignment;
import pm275.core.resource.ResourceInterface;
import pm275.core.resource.ResourceKey;
import pm275.core.resource.ResourcePoolKey;
import pm275.core.calendar.CalendarService;
import pm275.core.calendar.CalendarInterface;
import pm275.core.resource.ResourceService;
import pm275.common.persistance.ProjectVisitorInterface;
/**
 * <p>Activity Class</p>
 */
public class Activity implements ActivityInterface {
    /**
     * <p>Activity Attributes</p>
     */


    private String name;        // Activity Name
    protected Date startDate;
    protected Date endDate;
    private int duration;       // In hours
    private int slackTime;      // In hours
    private int status; //Enumeration
    private static final Logger log = Logger.getLogger( Activity.class );
    private static final String ATTR = "Activity";
    private static int MAX_DURATION = 99999;
    private boolean bIsFixedDuration;
    private boolean isFixedStartDate;
    // THIS IS ONLY USED BY THE UI!!!  THIS CLASS STORES _EVEYTHING_ IN HOURS!
    private int durationMeasure;

    // THIS IS ONLY USED BY THE UI CODE.
    private int displayOrder;

/**
 * <p>Activity Data members ...</p>
 */
    protected ActivityKey myKey;
    
    protected ArrayList succKeys = new ArrayList(); //avoid NullPointers
    protected ArrayList predKeys = new ArrayList(); //avoid NullPointers;
    
    protected Map successors = new Hashtable(); //avoid NullPointers
    protected Map predecessors = new Hashtable(); //avoid NullPointers;
    protected Map resources = new Hashtable(); //avoid NullPointers;
    protected ResourcePoolKey resourcePoolKey;
    protected ProjectKey myProjectKey;
    // May go away based on predecssors and successors above
   // public activity = new java.util.TreeSet();


   /**
     * critical path variables
     */
    private int earlyStartHours;    // Forward pass earliest start time
    private int earlyFinishHours;   // Forward pass earliest finish time
    private int lateStartHours;     // Backward pass latest start time
    private int lateFinishHours;    // Backward pass latest finish time

    // lateFinishHours - earlyFinishHours = difference between late & early finish (slack time)
    // lateStartHours - earlyStartHours = difference between late & early finish (slack time)

    private boolean earlyFinishFlag;    // Forward Activity completely processed?
    private boolean lateStartFlag;     // Backward Activity completely processed

    public Activity(){
    	super();
    }
    /**
     * <p> Activity constructor. </p>
     * @param Date start. Start date of activity.
     *
     */
    public Activity(Date start,ProjectKey pKey) {
        boolean bIsFixed = true;
        // set default duration to 0
        int duration  = 8;
        initialize(start, duration, bIsFixed,pKey);
    }

    /*
     *  Activity Constructor
     * @param Date Start start date of activity
     * @param Duration of activity in hours
     * @param bIsFixed. Does it have fixed start\end dates
     * @param pKey. Project Key that activity belongs to.
     */
    public Activity(Date start, int duration, boolean bIsFixed,ProjectKey pKey)
    {

        initialize(start, duration, bIsFixed,pKey);
    }
/*
 * Called by ActivityDAOtoDB.java
 */
    public Activity( String inactivitykey,String inprojectkey,String inname,Date instartdate,Date inenddate,
    int induration,int inslacktime,int instatus,String inresourcepoolkey,int infixedduration,int infixedstartdate){

    	    super();
    	
            this.myKey = new ActivityKey(inactivitykey);
            
            
            this.myProjectKey= new ProjectKey(inprojectkey);
            this.name = inname;        // Activity Name
            this.startDate = instartdate;
            this.endDate = inenddate;
            this.duration = induration;
            this.slackTime = inslacktime;
            this.status = instatus;
            this.resourcePoolKey = new ResourcePoolKey(inresourcepoolkey);

            if(infixedduration==1){
              bIsFixedDuration = true;
            }else{
              bIsFixedDuration = false;
            }

            if(infixedstartdate==1){
              bIsFixedDuration = true;
            }else{
              bIsFixedDuration = false;
            }

            initialize(instartdate, duration, bIsFixedDuration,myProjectKey);
    }


    private void initialize(Date start, int duration, boolean bIsFixed,ProjectKey pKey)
    {
        status = ActivityEnumeration.NOT_STARTED;
        myProjectKey = pKey;
        myKey = new ActivityKey();

        /**
         * Set initial critical path values
         */
        resetCriticalPathValues();

       /**
        * create initial HashTables
        */
        successors   = new Hashtable();
        predecessors = new Hashtable();
        resources    = new Hashtable();

        setName("Activity name not set");
        setStartDate(start);
        setDuration(duration);
        setIsFixedDuration(bIsFixed);

        // Default is fixed start date.
        isFixedStartDate = true;

        if (duration == 0)
            endDate = startDate;
        else{
            computeEndDate(startDate,duration);
        }

        resourcePoolKey = null;
    }

    /**
     *  Computes and updates this activity's start date from its dependencies.
     *  Note that this method doesn't do anything for activities with fixed
     *  start dates.
     */
    public void computeStartDate()
    {
        if (! isFixedStartDate)
        {
            startDate = null;
            for (Iterator i = predecessors.values().iterator(); i.hasNext(); )
            {
                ActivityInterface a = (ActivityInterface) i.next();
                Date newDate = a.getEndDate();
                if ((startDate == null) || startDate.before(newDate))
                {
                    startDate = newDate;
                }
            }
        }
    }

    /**
     * computeEndDate() corrects the endDate field based on the current
     * values of startDate and duration.  It uses the CalendarService
     * class to account for restricted days (weekends and holidays)
     * When an activity end date is updated,all the dependant activities
     * start dates are updated as well.
     * @param startDate
     * @param dur
     */
      public void computeEndDate(Date startDate, int dur)
      {
        long elapsedTime = 0;

        ProjectService pService = ProjectService.getInstance();
        CalendarInterface calIfc = pService.getProjectCalendar(myProjectKey);
        if(bIsFixedDuration)
        {
            endDate = calIfc.addHoursToDate(startDate, dur);
        }
        else
        {

            int noOfResources = resources.size();
            if(noOfResources == 0)
            {
                endDate = calIfc.addHoursToDate(startDate, dur);

            }
            else//int only for now.. slightly inaccurate..
            {
                // Everyone will work equal number of hours
                // Always round up.
                int newDuration = (int) Math.ceil((double) duration / noOfResources);

                //Now, let's find out what is the actual date
                // each resource needs for the same duration.
                Collection resourceKeys = resources.keySet();
                Iterator iterator = resourceKeys.iterator();
                CalendarService calendarService = CalendarService.getInstance();
                while(iterator.hasNext())
                {
                    CalendarInterface resourceCal =
                            calendarService.getCalendar((ResourceKey)iterator.next());
                    // If resourceCalendar not found, use default calendar
                    if(resourceCal == null)
                    {
                        resourceCal = calIfc;
                    }
                    Date resourceEndDate = resourceCal.addHoursToDate(startDate, newDuration);
                    if(! endDate.equals(resourceEndDate))
                    {
                        endDate = resourceEndDate;
                    }
                }
            }
        }
        updateSuccessors();
    }

/**
 * <p>Get Activity Name ...</p>
 *
 * @return Activity name
 */
    public String getName() {
        return name;
    }

/**
 * <p>Set Activity Name ...</p>
 *
 * @param s Activity name
 */
    public void setName(String s) {
        name = s;
    }

/**
 * <p>Gets Activity start date</p>
 *
 * @return Date startDate of Activity
 */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Set the start date of the activity. Whenever the start date of an
     * activity changes, the end date is recomputed
     * @param date Date that activity starts
     */
    public void setStartDate(Date date)
    {
        // TODO: Temporarily commenting out project calendar code and using
        // corporate calendar.  The project calendar code was causing null
        // pointer exception when creating new project. [achou]
        // ProjectService pService = ProjectService.getInstance();
        // CalendarInterface calIfc = pService.getProjectCalendar(myProjectKey);
        CalendarInterface calIfc = CalendarService.getInstance().getCorporateCalendar();

        // Set the start date.  If it's not a work day, use the next available
        // work day.
        startDate = calIfc.getNextWorkDay(date);

        computeEndDate(startDate,duration);
    }

/**
 * <p>Gets Activity end date</p>
 *
 * @return Date endDate of Activity
 */
    public Date getEndDate() {

        return endDate;
    }

    /**
     *  Returns true if this activity has a fixed start date.
     *  @return True if activity has fixed start date, false otherwise (i.e. the
     *               start date is calculated from the dependencies).
     */
    public boolean getIsFixedStartDate()
    {
        return isFixedStartDate;
    }

    /**
     *  Specifies whether this activity's start date is fixed.
     *  @param isFixed True if the start date should be fixed, false otherwise
     *                 (i.e. it should be calculated from the dependencies).
     */
    public void setIsFixedStartDate(boolean isFixed)
    {
        isFixedStartDate = isFixed;
    }

    /**
     * <p>Gets Activity isFixedDuration</p>
     *
     * @return whether the activity is of fixed duration or now
     */
        public boolean getIsFixedDuration() {
            return bIsFixedDuration;
        }

    /**
     * <p>Set the activity bIsFixedDuration</p>
     *
     * @param True if activity is of fixed duration. False if not.
     */
        public void setIsFixedDuration(boolean b) {
            bIsFixedDuration = b;
            computeEndDate(this.startDate, this.duration);
        }
/**
 * <p>Gets Activity duration(in hours)</p>
 *
 * @return duration of Activity
 */
    public int getDuration() {
        return duration;
    }

/**
 * <p>Set the activity duration</p>
 *
 * @param dur the duration in hours
 */
    public void setDuration(int dur) {
        duration = dur;
        computeEndDate(this.startDate, this.duration);
    }


    /**
     * <p>Add a Resource</p>
     *
     * @param ResourceKey resourceKey of resource to add.
     */
        public void addResource(ResourceKey resourceKey) {
            ResourceInterface r = null;
            ProjectService ps = ProjectService.getInstance();

            try {

                if(resourceKey != null){

                    // avoid adding duplicate
                    if (!resources.containsKey(resourceKey)) {
                        r = ps.getResource(this.getProjectKey(),resourceKey);
                        resources.put(resourceKey, r);
                    }
                    computeEndDate(startDate,duration);
                }else {
                   log.error("ERROR: Activity.addResource() resourceKey passed is null ");
                }

            }
            catch (KeyNotFoundException knfe) {
                log.error("ERROR: Activity.addResource() KeyNotFoundException ");
            }
            catch (InternalProjectException ipe) {
                log.error("ERROR: Activity.addResource() InternalProjectException ");
            }
            catch (ResourceException ex)
            {
                log.error("ERROR: Activity.addResource() ResourceException: " + ex.getMessage());
            }
        }
        /**
         *<p>Get the values of this activity resource hashtable</p>
         *
         * @return Collection to iterate the values.
         */
        public Collection getResources()
        {

            return resources.values();
        }

        public ResourcePoolKey resourcePoolKey(){
            return this.resourcePoolKey;
        }

        /**
         *<p>set the values of this activity resource hashtable</p>
         *
         *
         */
        public void setResources()
        {
            try {
                    ResourceService rs = ResourceService.getInstance();
                    Iterator assignedResourcesIterator = rs.listAllAssignments().iterator();

                    while (assignedResourcesIterator.hasNext()) {
                        Assignment assn = (Assignment) assignedResourcesIterator.next();
                        if (assn.getActivityKey() == this.myKey) {
                            addResource(assn.getResource());

                        }
                    }


            }
            catch (ResourceException re) {
                log.error("ERROR : Activity.setResources. ResourceException");
            }


        }

        
        //Needed for DB Load
        public void setResources(Map inResources)
        {
              
        	this.resources = inResources;
        	
        }
        
        /**
         * <p>Removes a Resource from the actvity</p>
         *
         *
         *
         * @param resource
         */

        public void removeResource(ResourceInterface resource)
        {

                ResourceKey rKey = resource.getKey();
                if(resources.containsKey(rKey))
                {
                    resources.remove(rKey);
                }
                computeEndDate(startDate,duration);

        }

    /**
     *  Removes all the resources from the list.
         }
*/
    public void clearResources()
    {
        if(!resources.isEmpty())
        {
            resources = new Hashtable();
        }
    }



   

        
        
    

/**
 * <p>Add a preceding activity</p>
 *
 * @param predecessor ActivityKey object to add.
 */
    public void addPredecessor(ActivityKey predecessor)
        throws InternalActivityException
    {
        if(predecessor != null){

            // avoid adding duplicate
            if (!predecessors.containsKey(predecessor.toString()))
            {

                ActivityInterface  predecessorActivity =
            getActivity(predecessor);

        // commented line below cause stack error -um-
                // setStartDate(predecessorActivity.getEndDate());

                predecessors.put(predecessor.toString(),predecessorActivity);
            }

        }else{
            String msg = "ERROR: predecessor passed is null";
            log.error(msg);
            throw new InternalActivityException(msg);
        }

    // commented line below caused stack error -um-
    //computeEndDate(this.startDate, this.duration);
     }


    /**
     *<p>Get the value of this activity predecessor hashtable</p>
     *
     * @return Enumator to iterate the values.
     */
    public Collection getPredecessors()
    {
        return predecessors.values();
    }

    /**
     * <p>Removes a predeccessor from the predecessors list</p>
     *
     *
     *
     * @param predecessor
     */
    public void removePredecessor(ActivityKey predecessor) {
        predecessors.remove(predecessor.toString());
        computeEndDate(this.startDate, this.duration);
    }

    /**
     * <p>Add a successor activity</p>
     *
     *
     *
     * @param successor ActivityKey object to add.
     */
    public void addSuccessor(ActivityKey successor)
        throws InternalActivityException
    {


        if (successor != null) {

            // avoid adding duplicate
            if (!successors.containsKey(successor.toString()))
            {
                ActivityInterface successorActivity = getActivity(successor);
                // commented out -happens in Project -associateActivites
                //successorActivity.setStartDate(endDate);
                
               
                successors.put(successor.toString(),successorActivity);
            }
        }
        else {
            String msg = "ERROR: successor passed is null ";
            log.error(msg);
            throw new InternalActivityException(msg);
        }
    }


    /**
     *<p>Get the values of this activity accessor hashtable</p>
     *
     * @return Enumator to iterate the values.
     */
    public Collection getSuccessors()
    {
        return successors.values();
    }


    /**
     * <p>Removes a succesive activity link to another activity</p>
     *
     *
     *
     * @param successor
     */
    public void removeSuccessor(ActivityKey successor) {
        successors.remove(successor.toString());
    }

    /**
     * <p>Calculates slack time for a given activity</p>
     *
     *
     *
     * @return
     */
    public int calculateSlackTime() {
        slackTime = lateStartHours - earlyStartHours;
        return slackTime;

    }
    /**
     * Called from Project.removeActivity() and used to delete all connections
     * to this activity from its successors and predecessors
     *
     * @throws KeyNotFoundException
     */
    public void disassociateActivity()
                throws InternalActivityException {

        try {
            // Go throw Predecessors and remove current activity as a successor of it
            for (Iterator i = getPredecessors().iterator(); i.hasNext(); )
            {
                ActivityInterface a = (ActivityInterface) i.next();
                a.removeSuccessor(this.myKey);

            }

            // Go throw Successor and remove current activity as a Predecessor of it
            for (Iterator i = getSuccessors().iterator(); i.hasNext(); )
            {
                ActivityInterface a = (ActivityInterface) i.next();
                a.removeSuccessor(this.myKey);
            }

            // Update the successors.
            updateSuccessors();

        }
        catch (Exception e) {
            String sMsg = "ERROR : Exception thrown in Activity.disassociateActivity().";
            log.error(sMsg);
            throw new InternalActivityException(sMsg);

        }
    }

    /**
     * Method to return ResourcePoolkey key
     * @return Returns the myKey.
     */
    public ResourcePoolKey getResourcePoolKey() {
        //CEC: Defensive coding to avoid null keys
        if(resourcePoolKey == null){
            resourcePoolKey = new ResourcePoolKey("Default");
        }

        return this.resourcePoolKey;
    }

    /**
     * Method to return activity key
     * @return Returns the myKey.
     */
    public void getResourcePoolKey(ResourcePoolKey in) {
        this.resourcePoolKey = in;
    }
    /**
     * Method to return activity key
     * @return Returns the myKey.
     */
    public ProjectKey getProjectKey() {

        //CEC: Defensive coding to avoid null keys
        if(myProjectKey == null){
            myProjectKey = new ProjectKey("Default");
        }

        return myProjectKey;
    }


    /** Method to set the activty
     * @param myKey The myKey to set.
     */
    public void setProjectKey(ProjectKey myProjectKey) {
        this.myProjectKey = myProjectKey;
    }

    public void setResourcePoolKey(ResourcePoolKey myProjectKey) {
        this.resourcePoolKey = myProjectKey;
    }
/**
 * Method to return activity key
 * @return Returns the myKey.
 */
public ActivityKey getActivityKey() {

    //CEC: Defensive coding to avoid null keys
    if(myKey == null){
        myKey = new ActivityKey("Default");
    }

    return myKey;
}
/** Method to set the activty
 * @param myKey The myKey to set.
 */
public void setActivityKey(ActivityKey myKey) {
    this.myKey = myKey;
}
    /**
     * @return Returns the status.
     */
    public int getStatus() {
        return status;
    }
    /** Sets the activities stats per spec
     *
        NOT_STARTED = 0;
        IN_PROGRESS = 1;
        COMPLETE = 2;
        CANCELLED = 3;
     * @param status The status to set.
     */
    public void setStatus(int status) {
        if(status >= 0 && status <= ActivityEnumeration.MAX ){
          this.status = status;
        }else{
            log.error("ERROR: Invalid Activity Status");
        }
    }

    /**
     * Check if current activity is an StartMilestone (no Predecessors)
     * @return true is current activity is StartMilestone
     */
    public boolean isStartMilestone() {
        return predecessors.isEmpty();
    }

    /**
     * Check if current activity is an EndMilestone (no Successors)
     * @return true is current activity is EndMilestone
     */
    public boolean isEndMilestone() {
        return successors.isEmpty();
    }
// ****** BEGIN Critcal Path Methods
//  **

    /**
     * Method to initially set (or reset after a critical path calculation has been performed)
     * all the critical path variables for the activity.
     */
    public void resetCriticalPathValues() {
    log.debug("Activity " + this.getName() + ": reset CP values");
        earlyStartHours = -1;
        earlyFinishHours = -1;

        // Minor hack. make sufficiently larger than any activity duration. If -1 then first compare
        // for min will always return wrong value.
        lateStartHours = -1;
        lateFinishHours = -1;

        earlyFinishFlag = false;
        lateStartFlag = false;

      }
    /**
     * Critical Path method
     * @param
     */
    public void setEarlyStartHours(int duration) {
        earlyStartHours = duration;
      }
    /**
     * Critical Path method
     * @return
     */
      public int getEarlyStartHours() {
        return earlyStartHours;
      }
      /**
       * Critical Path method
       * @param duration
       */
      public void setEarlyFinishHours(int duration) {
        earlyFinishHours = duration;
      }
      /**
       * Critical Path method
       * @return
       */
      public int getEarlyFinishHours() {
        return earlyFinishHours;
      }
      /**
       * Critical Path method
       * @param duration
       */
      public void setLateStartHours(int duration) {
        lateStartHours = duration;
      }
      /**
       * Critical Path method
       * @return
       */
      public int getLateStartHours() {
        return lateStartHours;
      }
      /**
       * Critical Path method
       * @param duration
       */
      public void setLateFinishHours(int duration) {
        lateFinishHours = duration;
      }
      /**
       * Critical Path method
       * @return
       */
      public int getLateFinishHours() {
        return lateFinishHours;
      }
      /**
       * Critical Path method
       * @param value
       */
      public void setEarlyFinishFlag(boolean value) {
        earlyFinishFlag = value;
      }
      /**
       * Critical Path method
       * @return
       */
      public boolean getEarlyFinishFlag() {
        return earlyFinishFlag;
      }
      /**
       * Critical Path method
       * @param value
       */
      public void setLateStartFlag(boolean value) {
        lateStartFlag = value;
      }
      /**
       * Critical Path method
       * @return
       */
      public boolean getLateStartFlag() {
        return lateStartFlag;
      }

      /**
         * Private method to update the start date of all
         * successor activities whenever the end date is
         * updated.
         */
        private void updateSuccessors()
        {
            Collection successors = getSuccessors();
            Iterator iterator = successors.iterator();
            while(iterator.hasNext())
            {
                 ActivityInterface successor = (ActivityInterface)iterator.next();
                 successor.computeStartDate();
                 successor.computeEndDate(successor.getStartDate(), successor.getDuration());
            }
        }

        /**
         * Given an activity key get the Activity object from the project service
         * @param key
         * @return - Activity
         * @throws InternalActivityException - if the Activity object not found
         */
        private ActivityInterface getActivity(ActivityKey key) throws InternalActivityException
        {
            try {
                ActivityInterface activity = ProjectService.getInstance()
                                            .getActivity(myProjectKey,key);
                return activity;
            } catch (KeyNotFoundException e) {

                log.error("ERROR: Activity.getActivity() for " + key.toString(),e);
                // let's move on to the next activity
                throw new InternalActivityException("Cannot find activity: "+ key);
            } catch (InternalProjectException e) {
                log.error("ERROR: Activity.getActivity() for " + key.toString(),e);
                //let's move on to the next activity
                throw new InternalActivityException("Cannot find activity: "+ key);
            }
        }

    /**
     * @return Returns the durationMeasure.
     * THIS IS ONLY USED BY THE UI!!!  THIS CLASS STORES _EVEYTHING_ IN HOURS!
     */
    public int getDurationMeasure() {
        return durationMeasure;
    }


    /**
     * @param durationMeasure The durationMeasure to set.
     * THIS IS ONLY USED BY THE UI!!!  THIS CLASS STORES _EVEYTHING_ IN HOURS!
     */
    public void setDurationMeasure(int durationMeasure) {
        this.durationMeasure = durationMeasure;
    }

    
    
    public void addPredKey(ActivityKey key){
    	
    	this.predKeys.add(key);
    	
    }
    
    
    public void addSuccKey(ActivityKey key){
    	this.succKeys.add(key);
    }
    
    
    /**
     *  Returns a string representation of this activity, consisting of the
     *  name, start/end dates, duration, resources, and dependencies.
     *  @return String representation of this activity.
     */
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("Activity:").append("\n");
        sb.append("name: ").append(getName()).append("\n");
        sb.append("start date: ").append(getStartDate());
        if (getIsFixedStartDate())
        {
            sb.append(" (fixed)");
        }
        sb.append("\n");
        sb.append("end date: ").append(getEndDate()).append("\n");
        sb.append("duration: ").append(getDuration()).append(" hours (")
                .append(getIsFixedDuration() ? "fixed" : "effort").append(")\n");

        // Resources.
        sb.append("resources: ");
        for (Iterator i = getResources().iterator(); i.hasNext(); )
        {
            sb.append(i.next().toString());
            if (i.hasNext())
            {
                sb.append(", ");
            }
        }
        sb.append("\n");

        // Dependencies.
        sb.append("depends on: ");
        for (Iterator i = getPredecessors().iterator(); i.hasNext(); )
        {
            sb.append(((ActivityInterface) i.next()).getName());
            if (i.hasNext())
            {
                sb.append(", ");
            }
        }
        sb.append("\n");

        //Successors.
        sb.append("successors on: ");
        for (Iterator i = getSuccessors().iterator(); i.hasNext(); )
        {
            sb.append(((ActivityInterface) i.next()).getName());
            if (i.hasNext())
            {
                sb.append(", ");
            }
        }
        sb.append("\n");


        
        return sb.toString();
    }

    public int getSlackTime() {
        return slackTime;
    }


    /**
     *  Returns the ordinal in which this activity should be displayed in the
     *  project, with 1 signifying the first activity.  THIS IS ONLY USED BY
     *  THE UI CODE.
     *  @return Display order of this activity.
     */
    public int getDisplayOrder()
    {
        return displayOrder;
    }

    /**
     *  Sets the ordinal in which this activity should be displayed in the
     *  project, with 1 signifying the first activity.  THIS IS ONLY USED BY
     *  THE UI CODE.
     *  @param order Display order for this activity.
     */
    public void setDisplayOrder(int ordinal)
    {
        this.displayOrder = ordinal;
    }

    /*
     *
     * @author TLong
     *
     * Trying out the visitor pattern for writing to XML
     * @param ProjectVisitorInterface visitor instance of visitor
     *  to gather informatoin about this class
     */
    public void acceptVisitor(ProjectVisitorInterface visitor) {
        visitor.visitActivity(this);
    }

//  **
//  ****** END Critcal Path Methods

    //
    //  Inner classes.
    //

    // These comparators allow us to sort a list of activities.  For the first
    // iteration, we only need to perform an ascending sort by name.  In the
    // future, we should add more comparators and allow the user to choose how
    // the activites will be sorted.

    /**
     *  Compares two activities by their names.
     */
    public static final class NameComparator implements Comparator
    {
        private static NameComparator ASCENDING = null;
        private static NameComparator DESCENDING = null;

        private final int direction;

        /**
         *  Creates a name comparator for the specified direction.
         *  @param direction Sorting order for this comparator.  Positive integer
         *                   means ascending order, negative means descending, and
         *                   zero means arbitrary.
         */
        private NameComparator(int direction)
        {
            this.direction = direction;
        }

        /**
         *  Returns a comparator for ascending order sort by name.
         */
        public static NameComparator ascending()
        {
            if (ASCENDING == null)
            {
                ASCENDING = new NameComparator(1);
            }
            return ASCENDING;
        }

        /**
         *  Returns a comparator for descending order sort by name.
         */
        public static NameComparator descending()
        {
            if (DESCENDING == null)
            {
                DESCENDING = new NameComparator(-1);
            }
            return DESCENDING;
        }

        /**
         *  Compares two activities by their names.
         *  @param o1 First activity.
         *  @param o2 Second activity.
         *  @return Positive integer if the first activity should come before
         *          the second, negative integer if the second activity should
         *          come first, and zero if their names are the same.
         *  @throws ClassCastException If either object cannot be cast as an
         *                             activity.
         */
        public int compare(Object o1, Object o2)
                // throws ClassCastException
        {
            ActivityInterface a1 = (ActivityInterface) o1;
            ActivityInterface a2 = (ActivityInterface) o2;

            // Multiply the string comparison by the direction to get the
            // right value.
            int compare = a1.getName().compareTo(a2.getName());
            return compare * direction;
        }

        // No need to override equals()...since these comparators are
        // singletons, the default equals() behaviour works perfectly.
    }

    /**
     *  Compares two activities by their keys, which is normally the timestamp
     *  of when the activity was created.
     */
    public static final class KeyComparator implements Comparator
    {
        private static KeyComparator ASCENDING = null;
        private static KeyComparator DESCENDING = null;

        private final int direction;

        /**
         *  Creates a key comparator for the specified direction.
         *  @param direction Sorting order for this comparator.  Positive integer
         *                   means ascending order, negative means descending, and
         *                   zero means arbitrary.
         */
        private KeyComparator(int direction)
        {
            this.direction = direction;
        }

        /**
         *  Returns a comparator for ascending order sort by key.
         */
        public static KeyComparator ascending()
        {
            if (ASCENDING == null)
            {
                ASCENDING = new KeyComparator(1);
            }
            return ASCENDING;
        }

        /**
         *  Returns a comparator for descending order sort by key.
         */
        public static KeyComparator descending()
        {
            if (DESCENDING == null)
            {
                DESCENDING = new KeyComparator(-1);
            }
            return DESCENDING;
        }

        /**
         *  Compares two activities by their keys.
         *  @param o1 First activity.
         *  @param o2 Second activity.
         *  @return Positive integer if the first activity should come before
         *          the second, negative integer if the second activity should
         *          come first, and zero if their names are the same.
         *  @throws ClassCastException If either object cannot be cast as an
         *                             activity.
         */
        public int compare(Object o1, Object o2)
                // throws ClassCastException
        {
            ActivityInterface a1 = (ActivityInterface) o1;
            ActivityInterface a2 = (ActivityInterface) o2;
            String key1 = a1.getActivityKey().getKeyId();
            String key2 = a2.getActivityKey().getKeyId();

            int compare = 0;
            try
            {
                // Try to do a numeric comparison of the keys.
                long id1 = (new Long(key1)).longValue();
                long id2 = (new Long(key2)).longValue();
                compare = ((id1 == id2) ? 0 : ((id1 > id2) ? 1 : -1));
            }
            catch (NumberFormatException ex)
            {
                // One or both keys cannot be converted to numbers, so instead
                // do a string compare.
                compare = key1.compareTo(key2);
            }

            // Multiply the string comparison by the direction to get the
            // right value.
            return compare * direction;
        }

        // No need to override equals()...since these comparators are
        // singletons, the default equals() behaviour works perfectly.
    }

    /**
     *  Compares two activities by their keys, which is normally the timestamp
     *  of when the activity was created.
     */
    public static final class OrderComparator implements Comparator
    {
        private static OrderComparator ASCENDING = null;
        private static OrderComparator DESCENDING = null;

        private final int direction;

        /**
         *  Creates a order comparator for the specified direction.
         *  @param direction Sorting order for this comparator.  Positive integer
         *                   means ascending order, negative means descending, and
         *                   zero means arbitrary.
         */
        private OrderComparator(int direction)
        {
            this.direction = direction;
        }

        /**
         *  Returns a comparator for ascending order sort by user-specified
         *  display order.
         */
        public static OrderComparator ascending()
        {
            if (ASCENDING == null)
            {
                ASCENDING = new OrderComparator(1);
            }
            return ASCENDING;
        }

        /**
         *  Returns a comparator for descending order sort by user-specified
         *  display order.
         */
        public static OrderComparator descending()
        {
            if (DESCENDING == null)
            {
                DESCENDING = new OrderComparator(-1);
            }
            return DESCENDING;
        }

        /**
         *  Compares two activities by their display order.
         *  @param o1 First activity.
         *  @param o2 Second activity.
         *  @return Positive integer if the first activity should come before
         *          the second, negative integer if the second activity should
         *          come first, and zero if their names are the same.
         *  @throws ClassCastException If either object cannot be cast as an
         *                             activity.
         */
        public int compare(Object o1, Object o2)
                // throws ClassCastException
        {
            ActivityInterface a1 = (ActivityInterface) o1;
            ActivityInterface a2 = (ActivityInterface) o2;
            int order1 = a1.getDisplayOrder();
            int order2 = a2.getDisplayOrder();
            return (order1 - order2) * direction;
        }

        // No need to override equals()...since these comparators are
        // singletons, the default equals() behaviour works perfectly.
    }

}

/**
 * $Log: Activity.java,v $
 * Revision 1.77  2005/05/14 00:11:51  ccochran
 * added set resources map
 *
 * Revision 1.76  2005/05/10 20:20:52  ccochran
 * add successors in  to string
 *
 * Revision 1.75  2005/05/10 20:08:59  ccochran
 * fixes for load activities preds
 *
 * Revision 1.74  2005/05/06 23:37:10  achou
 * Updated toString() to output more activity info.
 *
 * Revision 1.73  2005/05/06 17:46:27  ccochran
 * defensive coding for null keys
 *
 * Revision 1.72  2005/05/05 05:15:25  tlong2
 * minor edits
 *
 * Revision 1.71  2005/05/05 00:04:15  tlong2
 * A little Clean up and doc
 *
 * Revision 1.70  2005/05/04 12:07:44  ccochran
 * fixed s
 *
 * Revision 1.69  2005/05/04 10:43:26  ccochran
 * added resource pool set getter
 *
 * Revision 1.68  2005/05/04 09:42:04  ccochran
 * added constructor
 *
 * Revision 1.67  2005/04/30 22:12:04  achou
 * Added flag and getter/setter to indicate if an activity has a fixed start date.Added method to compute activity's start date from dependencies.When disassociating an activity, call updateSuccessors(), which now also recalculates the start date.
 *
 * Revision 1.66  2005/04/30 15:44:07  rcoutinh
 * Removed circular dependancy between ResourceService and activity methods
 *
 * Revision 1.65  2005/04/29 01:10:30  tlong2
 * Added visitor pattern updates acceptVisitor methods, etc
 *
 * Revision 1.64  2005/04/27 12:44:34  tlong2
 * changed RemoveResource to call new rs.deAssign()
 *
 * Revision 1.63  2005/04/24 15:54:43  rcoutinh
 * Activity now uses Project calendar
 *
 * Revision 1.62  2005/04/19 09:24:51  achou
 * Used new Calendar method to get next work day.
 *
 * Revision 1.61  2005/04/19 08:48:23  achou
 * Replace calls to Calendar.computeFutureWorkDate() with Calendar.AddHoursToDate().
 *
 * Revision 1.60  2005/04/19 07:52:07  umangkus
 * Set computeEndDate() as public method.
 *
 * Revision 1.59  2005/04/19 07:24:23  achou
 * In computeEndDate(), fixed small error in calculation of newDuration.
 *
 * Revision 1.58  2005/04/19 05:47:08  vchumako
 * Debug statement in the resetCriticalPathValues() prints null instead of
 * activity name, whats wrong?
 *
 * Revision 1.57  2005/04/18 21:32:55  tlong2
 * Critical Path working!
 *
 * Revision 1.56  2005/04/18 16:04:59  rcoutinh
 * Whenever resources are added or deleted - end date should get updated. -Fixed
 *
 * Revision 1.55  2005/04/18 14:33:56  achou
 * Removed clearPredecessors() and clearSuccessors() methods.
 *
 * Revision 1.54  2005/04/18 13:58:12  rcoutinh
 * update of dependency dates happens in Project.
 *
 * Revision 1.53  2005/04/18 13:22:49  tlong2
 * minor changes
 *
 * Revision 1.52  2005/04/18 10:28:14  tlong2
 * CP debugging fixes
 *
 * Revision 1.51  2005/04/18 06:50:58  umangkus
 * Fixed type.
 *
 * Revision 1.50  2005/04/18 06:36:54  tlong2
 * Added setResources() which isn't very effecient and should be optimized later.
 *
 * Revision 1.49  2005/04/18 04:50:47  achou
 * Converted Activity references to ActivityInterface.  Added OrderComparator to sort activities by their display order.  Moved all Comparator inner classes to end of the Activity class.
 *
 * Revision 1.48  2005/04/18 04:14:24  achou
 * Implemented display order field and getter/setter.
 *
 * Revision 1.47  2005/04/18 03:56:51  rcoutinh
 * setStartDate - startDate should not be a weekend or restricted date
 *
 * Revision 1.46  2005/04/18 03:46:37  tlong2
 * bug fixes
 *
 * Revision 1.45  2005/04/17 17:10:30  dnathan
 * Added CalculateEndDate to all applicate setters.
 *
 * Revision 1.44  2005/04/17 16:24:31  dnathan
 * Added getter for slack time.
 *
 * Revision 1.43  2005/04/17 11:30:12  achou
 * Added methods to remove all resources, predecessors, and successors.  Changed resource map key to use the resource key.
 *
 * Revision 1.42  2005/04/17 08:39:02  achou
 * Commented out project calendar code and use corporate calendar instead.  Overrode toString() method.
 *
 * Revision 1.41  2005/04/16 21:59:44  rcoutinh
 * Update end date based on number of resources
 *
 * Revision 1.40  2005/04/16 21:20:47  rcoutinh
 * updated to use project's calendar rather than CalendarService
 *
 * Revision 1.39  2005/04/16 20:43:40  dnathan
 * Added DurationMeasure property.  Only used by the UI.
 *
 * Revision 1.38  2005/04/16 20:37:48  dnathan
 * Fixed null pointer problems.
 *
 * Revision 1.37  2005/04/16 20:26:58  rcoutinh
 * initialize empty Maps
 *
 * Revision 1.36  2005/04/16 19:57:01  rcoutinh
 * Update dependency calculation
 *
 * Revision 1.35  2005/04/16 19:24:01  achou
 * Changed names and return types for the methods that returns activity's resources, predecessors, and successors.  Changed Hashtable references to Map.
 *
 * Revision 1.34  2005/04/16 18:00:24  tlong2
 * finished support for calculateSlackTime()
 *
 * Revision 1.33  2005/04/16 17:58:42  tlong2
 * *** empty log message ***
 *
 * Revision 1.32  2005/04/16 17:58:07  tlong2
 * completed calculateStackTime()
 *
 * Revision 1.31  2005/04/16 15:33:20  tlong2
 * added comments and slacktime var
 *
 * Revision 1.30  2005/04/16 14:52:34  tlong2
 * Fixed bugs in new constructor added isFixedDuration getter and setter methods
 *
 * Revision 1.29  2005/04/16 14:41:35  tlong2
 * Added new constructor with params (Date start, int duration, boolean bIsFixed) {
 *
 * Revision 1.28  2005/04/16 13:34:55  tlong2
 * Added addResource(..) and removeResource(..) methods
 *
 * Revision 1.27  2005/04/16 08:32:53  umangkus
 * Edit addSuccessor() method.
 *
 * Revision 1.26  2005/04/15 02:25:39  dnathan
 * Added that this class is an implementation of the ActivityInterface interface.
 *
 * Revision 1.25  2005/04/14 01:16:48  tlong2
 * Added support for disassociating sucessors and predecessor from an activity that is being removed from a project
 *
 * Revision 1.24  2005/04/13 00:08:36  tlong2
 * Critical Path algorithm updates
 *
 * Revision 1.23  2005/04/12 03:02:23  jkeiser
 * Added getResource() method to support VCT classes.
 *
 * Revision 1.22  2005/04/10 13:22:31  tlong2
 * Updates for Critical Path algorithm implementation
 *
 * Revision 1.21  2005/04/10 12:12:04  tlong2
 * Lost my changes trying to merge :-(
 *
 * Revision 1.20  2005/04/09 02:52:47  umangkus
 * Modified addPredecessor() and addSuccessor(); avoid adding duplicates.
 *
 * Revision 1.19  2005/04/08 19:40:21  umangkus
 * Added getSuccessor method and import line.
 *
 * Revision 1.18  2005/04/08 19:28:13  umangkus
 * Added getPredecessor method. getSuccessor method is currently not needed.
 *
 * Revision 1.17  2005/04/08 19:16:05  umangkus
 * Swap key value pair for successor and predecessor hashtables.
 *
 * Revision 1.16  2005/04/04 01:24:16  tlong2
 * Beginnings of Critical Path & Detect Cycles code
 *
 * Revision 1.15  2005/03/30 11:18:00  achou
 * Added key comparator class.
 *
 * Revision 1.14  2005/03/30 10:09:31  ccochran
 * back out last minute interface change
 *
 * Revision 1.13  2005/03/30 08:27:16  ccochran
 * Updated for interface support
 *
 * Revision 1.12  2005/03/30 07:08:57  achou
 * Added NameComparator inner class for sorting a list of activities.
 *
 * Revision 1.11  2005/03/29 17:02:32  ccochran
 * Added Status to Activity Per Spec
 *
 * Revision 1.10  2005/03/29 09:09:03  ccochran
 * Added get-set for ActivityKey
 *
 * Revision 1.9  2005/03/28 14:58:41  ccochran
 * log 4j
 *
 * Revision 1.8  2005/03/28 08:57:36  achou
 * Added static keyword and initializer for calendarService.
 *
 * Revision 1.7  2005/03/28 05:59:56  tlong2
 * Minor update
 *
 * Revision 1.6  2005/03/28 04:07:59  tlong2
 * Put the ActivityKey back in the constructor
 *
 * Revision 1.5  2005/03/28 03:50:06  tlong2
 * Completed all Stories for Iteration I
 *
 * Revision 1.4  2005/03/27 09:05:15  ccochran
 * Comments
 *
 * Revision 1.3  2005/03/26 12:01:01  ccochran
 * Log4J import
 *
 * Revision 1.2  2005/03/26 11:16:26  achou
 * Added import statements.
 *
 * Revision 1.1  2005/03/26 09:02:35  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.9  2005/03/25 13:34:27  ccochran
 * CEC:Fixed some compile errors
 *
 * Revision 1.8  2005/03/25 06:47:11  tlong2
 * Minor bug fixes introduced in XP session
 *
 * Revision 1.7  2005/03/25 06:28:00  tlong2
 * Minor bug fixes introduced in XP session
 *
 * Revision 1.6  2005/03/24 02:34:28  tlong2
 * TL and CC XP Weds session updates
 *
 * Revision 1.5  2005/03/23 04:44:48  tlong2
 * Minor updates..
 *
 * Revision 1.4  2005/03/23 01:47:50  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.3  2005/03/22 04:31:00  dnathan
 * Updated header to include CVS ID.
 *
 * Updated footer to include CVS log.
 *
 * Revision 1.2  2005/03/22 04:10:14  dnathan
 * Refactored to pm275.core package.
 *
 * Updated header to include CVS ID.
 *
 * Updated footer to include CVS log.
 *
 **/

