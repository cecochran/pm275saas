/*
 * Created on Apr 30, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pm275.core.project;

import org.apache.log4j.Logger;

import pm275.common.PM275BaseKey;
import pm275.common.exception.DAOException;
import pm275.common.exception.ProjectCreationException;
import pm275.common.persistance.*;
import pm275.core.calendar.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import pm275.gui.common.DateFormatter;


/***************************************************
 * File:        $RCSfile: ProjectDAOtoDB.java,v $
 * @author      $Author: ccochran $,
 * @version     $Id: ProjectDAOtoDB.java,v 1.29 2005/05/15 20:33:34 ccochran Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * In the spirt of the layering pattern, this class provides a self contained 
 * mechanism for all Project based presistance to DBMS. Assumes a predefined
 * database structure that this class must mirror.
 * * */
public class ProjectDAOtoDB 
//extends  PM275DAOtoDB 

{
    private static final Logger log = Logger.getLogger(ProjectDAOtoDB.class);
    private static final String ATTR = "ProjectDAOtoDB";
    String sql = null;
    
    
    /**
     * <p>Default constructor</p>
     * 
     * 
     */
    public ProjectDAOtoDB(){
	super();
    }
	   
	  
    /**
     * <p> Loads a project from the database based on project key
     *     If the key doesnt exsist then it will return a null object
     * @param ProjectInterface
     */  
    public ProjectInterface get(PM275BaseKey pKey){
	Project  project1 = null;
	        
	PreparedStatement pstmt= null;
	DBConnection db = null;
	DBConnection db2 = null;
	int resultCnt = -1;
			
    System.out.println("Debug: Start Get");
			
	try{
	    db = new DBConnection();
	    db.connect();
	    int count = 0;
		  
	    log.debug("Debug: Get Count");
	    StringBuffer sb= new StringBuffer();
	    String sql = new String("select projectkey,name,startMilestonekey,endMilestonekey,resourcepoolkey,calendarkey,startdate  from project where projectkey = ?");
	    pstmt = db.getPreparedPreparedStatement(sql);
		pstmt.setString(1, pKey.getKeyId());
	    db.getPreparedPreparedStatement(sb.toString());
    
	    
	    String projKey  = new String();
	    String name = new String();
	    String startmstoneKey = new String();
	    String endmstoneKey = new String();
	    String rsrcpoolKey = new String();
	    String calendarKey = new String();
	    java.util.Date startdate = DateFormatter.getDateForString("2005-04-18");
	    
	    System.out.println("Debug: Get Count");
	    //Execute statement
	    ResultSet rs = pstmt.executeQuery();
	    
	    
	    while (rs.next()) {
	   
		    System.out.println("Debug: Get Count");
		    
		    /*projKey = rs.getString("projectkey");
		     name =  rs.getString("name");
		     startmstoneKey =  rs.getString("startMilestonekey");
			endmstoneKey  =  rs.getString("endMilestonekey");
			rsrcpoolKey =  rs.getString("resourcepoolkey");
			calendarKey =  rs.getString("calendarkey");
			*/
		
	    	projKey = rs.getString(1);
			name =  rs.getString(2);
			startmstoneKey =  rs.getString(3);
			endmstoneKey  =  rs.getString(4);
			rsrcpoolKey =  rs.getString(5);
			calendarKey =  rs.getString(6);
			startdate =  rs.getDate(7);
			
			ProjectKey projectKey =new ProjectKey(projKey );
			//load the calendar from DB
		    CalendarKey cKey = CalendarService.getInstance().loadDB(projectKey);		    
			//now build the project 
		    project1 = new Project(projKey,name,startmstoneKey,endmstoneKey, rsrcpoolKey,cKey,startdate);

	    }  

	    db.close();
	    
	}catch (Exception ex) {
				
	    String message = "Error loading project: " + pKey.toString()+ " " + ex.getMessage();
	    log.error(message);
	}finally {
				  
	    //must clear connection upon execution
	    if(db!= null){
		db.close();
	    }
	    if(db2!=null){
		db2.close();
	    }
	    db = null;
	    db2 = null;
						  
	}

	return project1;
			
    }
		
    /**
     * <p> Loads a project from the database based on project key
     *     If the key doesnt exsist then it will return a null object
     * @param ProjectInterface
     */  
    public ArrayList getAbbaList(){
	ArrayList list = new ArrayList();
	PreparedStatement pstmt= null;
	DBConnection db = null;
	DBConnection db2 = null;
	int resultCnt = -1;
	db = new DBConnection();
	db.connect();
			    
		    
	try{
	    String projKey ;
	    String name ;
	    StringBuffer sb= new StringBuffer();
	    sb.append("select projectkey, name from project");
				
	    //Execute statement
	    ResultSet rs = db.executeQuery(sb.toString());
				 
				 
	    while (rs.next()) {
		projKey = rs.getString(1);
		name =  rs.getString(2);
		ProjectKey projectKey = new ProjectKey(projKey);
		ProjectAbba projAbba = new ProjectAbba(projectKey,name);
		list.add(projAbba);
	    }
	         	
	}catch (Exception ex) {
	    String message = "Error set data: " + ex.getMessage();
	    log.error(message);
	    log.debug(message);
	} finally {
//				must clear connection upon execution
	    if(db!= null){
		db.close();
	    }
				
	    db = null;
				
	}
				
	return list;
    }
		
    /**
     * <p> Writes the project to the database  </p>
     *  If the project currently exsists then it will be updated
     *  otherwise it will be inserted
     *  Returns -1 if failed otherwise 1 for success
     * @param int  
     */
    public int set(ProjectInterface pr) throws DAOException{
	PreparedStatement pstmt= null;
	DBConnection db = null;
	DBConnection db2 = null;
	int resultCnt = -1;
			
	//Base Case 
	//make sure the project is not null before the write 
	if(pr != null){
	    log.debug("Debug: Start Set");
	    
	  
	    try{
		db = new DBConnection();
		db.connect();
		    
		log.debug("Debug: Get Count");
		StringBuffer sb= new StringBuffer();
		sb.append("select count(*) from project where projectkey = '");
		sb.append(pr.getProjectKey().getKeyId());
		sb.append("'");
		int count = 0;
		log.debug("Debug: Query "+ sb.toString());				 
	    	
		//Execute statement
		ResultSet rs = db.executeQuery(sb.toString());
		while (rs.next()) {
		    count = rs.getInt(1);
		    log.debug("Debug: result: "+ count);
		}  
	    db.close();			
			
		//now set up for second connection
		db2 = new DBConnection();
		db2.connect();
		    			 
		if(count == 0){
		    log.debug("Debug: Starting Insert"); 	
			 	
		    //Key does not exsist so do insert to project table
		    sql = new String("INSERT INTO project (projectkey,name,startMilestonekey,endMilestonekey,resourcepoolkey,calendarkey,startdate) VALUES(?,?,?,?,?,?,?)");	  
		    pstmt = db2.getPreparedPreparedStatement(sql);
		    if(pstmt != null){
			log.debug("Prepare: Not null");
		    }else{
			log.debug("Prepare:  null");
		    }
			 	
		    log.debug("Debug: Starting Update"); 	
			 	
		    String projectKey = new String();
		    String name = new String();
	    	    String startmstoneKey = new String();
	    	    String endmstoneKey  = new String();
	    	    String rsrcpoolKey = new String();
	    	    String calendarKey = new String();
			 	
	    	    
	    	    java.util.Date startdate = DateFormatter.getDateForString("2005-04-18");
	    	    
	    	    
	    	    
	    	        	    
		    try{
			projectKey = pr.getProjectKey().getKeyId();
			name = pr.getName();
			startmstoneKey = pr.getStartMilestone().getActivityKey().getKeyId();
			endmstoneKey  = pr.getEndMilestone().getActivityKey().getKeyId();
			rsrcpoolKey = pr.getResourcePoolKey().getKeyId();
			calendarKey = pr.getCalendarKey().getKeyId();
			java.sql.Date start_dt = new java.sql.Date(pr.getStartDate().getTime());
    	    			
			
			
		    // Prepare a statement to insert a record
		    pstmt.setString(1, projectKey);
		    pstmt.setString(2, name);
		    pstmt.setString(3, startmstoneKey);
		    pstmt.setString(4, endmstoneKey);
		    pstmt.setString(5, rsrcpoolKey);
		    pstmt.setString(6, calendarKey );
		    pstmt.setDate(7, start_dt); //start date  
		    
		    }catch (Exception ex) {
			String message = "Error geting data: " + ex.getMessage();
			log.error(message);
		    }
	  
			    
			    
		    log.debug("Prepare: " + pstmt.toString());
			        
			   
		    resultCnt = pstmt.executeUpdate();
		    log.debug("Debug: Insert result " + resultCnt);
			 	
			    
		    //Write the Actitivies  from map
			//CEC: Assumes that this list contains all activities
			//    if there are other activites out side this set
			//    they need to be augmented as well.
			Map map = pr.getActivities();
				   
			int cnt = 1;
			int res = 0;
			//get iterator
			java.util.Iterator it =  map.values().iterator();
			//cycle thru all the Activities
			while (it.hasNext()) {
			            
			    Activity act = (Activity) it.next();
			    //get project and call recalcualte method
			    ActivityDAOtoDB actDB = new ActivityDAOtoDB();  
			    res = actDB.set(act);
			    System.out.println("Insert Activity Count: "+cnt +" Set result: " + res);
			    cnt++;
			}    

		        
		}else{
			
		    log.debug("Debug: Starting Update"); 	
			 	
			 	
		    //Key exsists so do an update
		    sql = new String("Update project " +
				     "set projectkey = ?, " +
				     "name = ?, " +
				     "startMilestonekey= ?," +
				     "endMilestonekey=?," +
				     "resourcepoolkey =?, " +
				     "calendarkey =?, " +
					 "startdate = ? " +
				     "WHERE  projectkey= ?");    
			 				  
		    pstmt = db2.getPreparedPreparedStatement(sql);
			 	
		    
		    
		    
			java.sql.Date start_dt = new java.sql.Date(pr.getStartDate().getTime());
			
		    // Prepare a statement to update a record
		    pstmt.setString(1, pr.getProjectKey().getKeyId());
		    pstmt.setString(2, pr.getName());
		    pstmt.setString(3, pr.getStartMilestone().getActivityKey().getKeyId());
		    pstmt.setString(4, pr.getEndMilestone().getActivityKey().getKeyId());
		    pstmt.setString(5, pr.getResourcePoolKey().getKeyId());
		    pstmt.setString(6, pr.getCalendarKey().getKeyId());
		    pstmt.setDate(7, start_dt);
		    pstmt.setString(8, pr.getProjectKey().getKeyId());
		    //check result count
		    resultCnt = pstmt.executeUpdate();
			   		   
		}
			 
		//Write the Actitivies  from map
		//CEC: Assumes that this list contains all activities
		//    if there are other activites out side this set
		//    they need to be augmented as well.
		Map map = pr.getActivities();
			   
		int cnt = 1;
		int res = 0;
		//get iterator
		java.util.Iterator it =  map.values().iterator();
		//cycle thru all the Activities
		while (it.hasNext()) {
		            
		    Activity act = (Activity) it.next();
		    //get project and call recalcualte method
		    ActivityDAOtoDB actDB = new ActivityDAOtoDB();  
		    res = actDB.set(act);
		    log.debug("Update Activity Count: "+cnt +" Set result: " + res);
		    cnt++;
		}
		 
		
		        
	    }catch (Exception ex) {
				
		String message = "Error set data: " + ex.getMessage();
		log.error(message);
	    }finally {
				  
		//must clear connection upon execution
		if(db!= null){
		    db.close();
		}
		if(db2!=null){
		    db2.close();
		}
		db = null;
		db2 = null;
						  
	    }
	}
			
	return resultCnt;
    }
		
    /**
     * <p>Deletes project from database based on key</p>
     * Returns -1 if failed otherwise 1 for success
     * @param PM275BaseKey pKey
     */
    public int delete (PM275BaseKey pKey) throws DAOException{
	PreparedStatement pstmt= null;
	DBConnection db = null;
	int resultCnt = -1;
	int count;
			
	//Base case: make sure key is not null
	if(pKey != null){
	    log.debug("Debug: Start Delete");
			
	    try{
		db = new DBConnection();
		db.connect();
		   			 
		//Prepare a statement to de a record
		sql = new String("delete from project where projectkey = ?");	  
		pstmt = db.getPreparedPreparedStatement(sql);
		pstmt.setString(1, pKey.getKeyId());
			 			 	
		//check result count
		resultCnt = pstmt.executeUpdate();
			 
	    }catch (Exception ex) {
				
		String message = "Error set data: " + ex.getMessage();
		log.error(message);
	    }finally {
				  
		//must clear connection upon execution
		if(db!= null){
		    db.close();
		}
			 
		db = null;
			 
						  
	    }
	}
			
	return resultCnt;
    }
			
		
		
    /**
     * <p>Returns string</p>
     * 
     * @param string
     */
    public String toString(){
	return sql.toString();
    }	
		
		
}

/**
 * $Log: ProjectDAOtoDB.java,v $
 * Revision 1.29  2005/05/15 20:33:34  ccochran
 * fix start date
 *
 * Revision 1.28  2005/05/14 22:58:40  rcoutinh
 * Loads calendar from dB before creating project
 *
 * Revision 1.27  2005/05/14 20:15:59  ccochran
 * update to use this project service
 *
 * Revision 1.26  2005/05/13 18:46:04  ccochran
 * cleaned up dangling integration points to other services
 *
 * Revision 1.25  2005/05/13 18:02:54  ccochran
 * Refactored DB interaction to be owned by individual services invokeing presistance layer
 *
 * Revision 1.24  2005/05/12 20:59:11  ccochran
 * update for getting DB pred succ working
 *
 * Revision 1.23  2005/05/10 22:27:25  ccochran
 * update marshaling
 *
 * Revision 1.22  2005/05/10 20:08:15  ccochran
 * removed system outs
 *
 * Revision 1.21  2005/05/10 19:20:01  ccochran
 * update set activites
 *
 * Revision 1.20  2005/05/09 20:11:29  ccochran
 * cleaned up system outs
 *
 * Revision 1.19  2005/05/09 18:34:31  vchumako
 * Passed created CalendarKey directly to project
 *
 * Revision 1.18  2005/05/09 16:38:36  vchumako
 * Removed old statement
 *
 * Revision 1.17  2005/05/09 15:01:31  vchumako
 * Calendar has to be loaded before making up the project
 *
 * Revision 1.16  2005/05/09 13:31:40  ccochran
 * Fixed Activity Save
 *
 * Revision 1.15  2005/05/09 03:47:51  vchumako
 * Added save/load calendar
 *
 * Revision 1.14  2005/05/08 22:15:11  vchumako
 * Make sure that all resources in the db now
 *
 * Revision 1.13  2005/05/08 06:11:53  vchumako
 * Update activityRef for insert or update
 *
 * Revision 1.12  2005/05/07 19:13:31  ccochran
 * cleaned up code for load abba
 *
 * Revision 1.11  2005/05/07 18:34:42  ccochran
 * merge ProjectAbba UI
 *
 * Revision 1.10  2005/05/06 23:35:56  ccochran
 * cleaneup to string, fixed bug in load activities
 *
 * Revision 1.9  2005/05/06 23:18:23  ccochran
 * debuging for load activities
 *
 * Revision 1.8  2005/05/04 05:54:30  ccochran
 * added footers
 *
 *
 * Updated footer to include CVS log.
 *
 **/
