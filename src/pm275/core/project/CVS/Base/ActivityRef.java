/***************************************************
 * File:        $RCSfile: ActivityRef.java,v $
 * @author      $Author: ccochran $,
 * @version     $Id: ActivityRef.java,v 1.2 2005/05/07 06:22:20 ccochran Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:  The ActivityRef is a data helper class for working with predisessors
 *           It maps from a project and activity.
 * </p>
 */
package pm275.core.project;


/**
 * @author Chris
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ActivityRef {
	
    private ActivityKey actKey = new ActivityKey("default");
    private ProjectKey projectKey = new ProjectKey("default");
    private int predFlag=0;
	private String value = new String("");
	
	
	public ActivityRef ( ActivityKey inactKey, ProjectKey inprojectKey,int inflag, String invalue){
		super();
		
		System.out.println("new activity ref");
		actKey = inactKey; 
		projectKey = inprojectKey;
		predFlag = inflag;
		value = invalue;
	}
	
	
	/**
	 * @return Returns the actKey.
	 */
	public ActivityKey getActKey() {
		return actKey;
	}
	/**
	 * @param actKey The actKey to set.
	 */
	public void setActKey(ActivityKey actKey) {
		this.actKey = actKey;
	}
	/**
	 * @return Returns the predFlag.
	 */
	public int getPredFlag() {
		return predFlag;
	}
	/**
	 * @param predFlag The predFlag to set.
	 */
	public void setPredFlag(int predFlag) {
		this.predFlag = predFlag;
	}
	/**
	 * @return Returns the projectKey.
	 */
	public ProjectKey getProjectKey() {
		return projectKey;
	}
	/**
	 * @param projectKey The projectKey to set.
	 */
	public void setProjectKey(ProjectKey projectKey) {
		this.projectKey = projectKey;
	}
	/**
	 * @return Returns the value.
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value The value to set.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	
	public String toString(){
	  StringBuffer sb = new StringBuffer("AcitvityRef");
	  sb.append("ActivityKey: ").append(this.actKey.toString()).append("\n");
	  sb.append("ProjectKey: ").append(this.projectKey.toString()).append("\n");
	  sb.append("Pred Flag: ").append(this.predFlag).append("\n");
	  sb.append("Value: ").append(this.value).append("\n");
	  
	  return sb.toString();
	 
	}
	
}

/* 
* $Log: ActivityRef.java,v $
* Revision 1.2  2005/05/07 06:22:20  ccochran
* got insert working on activity ref
*
* Revision 1.1  2005/05/07 02:13:51  ccochran
* fixed spelling error
*
* Revision 1.6  2005/04/17 04:43:05  ccochran
* Added constructor that take both String and Throwable argumnets.
*/
