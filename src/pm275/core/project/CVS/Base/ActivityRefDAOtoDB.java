/*
 * Created on May 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pm275.core.project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import org.apache.log4j.Logger;

import pm275.common.PM275BaseKey;
import pm275.common.exception.DAOException;
import pm275.common.persistance.DBConnection;
import pm275.gui.common.DateFormatter;

/***************************************************
 * File:        $RCSfile: ActivityRefDAOtoDB.java,v $
 * @author      $Author: ccochran $,
 * @version     $Id: ActivityRefDAOtoDB.java,v 1.14 2005/05/13 18:46:04 ccochran Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * In the spirt of the layering pattern, this class provides a self contained 
 * mechanism for all ActivityRef based presistance to DBMS. Assumes a predefined
 * database structure that this class must mirror.
 * 
 * CREATE TABLE `activityref` (
  `activitykey` varchar(255) NOT NULL default '',
  `projectKey` varchar(255) default NULL,
  `predflag` int(11) default NULL,
  `value` varchar(255) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1
 * * */
public class ActivityRefDAOtoDB {

	private static final Logger log = Logger.getLogger(ActivityRefDAOtoDB.class);
    private static final String ATTR = "ActitivityRefDAOtoDB";
    String sql = null;
    
	
    
    /**
     * <p>Default constructor</p>
     * 
     * 
     */
	  public ActivityRefDAOtoDB(){
	      super();
	    }
	   
	  
	  /**
	   * <p> Loads a project from the database based on project key
	   *     If the key doesnt exsist then it will return a null object
	   * @param ProjectInterface
	   */  
		public ArrayList get(ActivityKey actKey, ProjectKey projKey, int predFlag){
	        ArrayList resultList = new ArrayList();
			PreparedStatement pstmt= null;
			DBConnection db = null;
			DBConnection db2 = null;
			int resultCnt = -1;
			
			log.debug("Debug: Start Set");
			
			try{
			 db = new DBConnection();
			 db.connect();
		    
			 log.debug("Debug: Get Count");
	    	 StringBuffer sb= new StringBuffer();
	    	 //Prepare a statement to de a record
			 sql = new String("select activitykey, projectKey, predflag,value from activityref where activitykey = ? and projectkey = ? and predflag = ?");	  
  		 	 pstmt = db.getPreparedPreparedStatement(sql);
			 pstmt.setString(1, actKey.getKeyId());
			 pstmt.setString(2, projKey.getKeyId());
			 pstmt.setInt(3, predFlag);
			 
			  //log.debug(" ActivityRef Get:  "+ pstmt.toString());
	    	   String activitykey;
			   String projectkey;
			   int predflag;
			   String value;
			   			   
			   
			   
			 //Execute statement
			 ResultSet rs = pstmt.executeQuery();
         	 while (rs.next()) {
	     	 	
         	 	/*activitykey = rs.getString("activitykey");
				projectkey = rs.getString("projectKey");
				predflag = rs.getInt("predflag");
				value = rs.getString("value");
				*/
         	 	
				activitykey = rs.getString(1);
				projectkey = rs.getString(2);
				predflag = rs.getInt(3);
				value = rs.getString(4);
				
				//build activity ref
				ActivityKey activityKey = new ActivityKey(activitykey.trim());
				ProjectKey  projectKey = new ProjectKey(projectkey.trim());
				ActivityKey  valueKey = new ActivityKey(value.trim());
				ActivityRef activityRef1 = new ActivityRef(activityKey,projectKey,predflag,valueKey);

				//Add activity Ref to result set
				resultList.add(activityRef1);	                    
				
          	 }  
			
			}catch (Exception ex) {
			  	
			   String message = "Error building ActivityRef for Activitykey : " + actKey.toString() + ex.getMessage();
		       log.error(message);
		       log.debug(message); 
			}finally {
				  
			 //must clear connection upon execution
			  if(db!= null){
			    db.close();
			  }
			  if(db2!=null){
			    db2.close();
			  }
			  db = null;
			  db2 = null;
						  
			}
			
			return resultList;
			
		}
		
		
		
		  /**
		   * <p> Loads a project from the database based on project key
		   *     If the key doesnt exsist then it will return a null object
		   * @param ProjectInterface
		   */  
			public ArrayList getByProject(ProjectKey projKey, int predFlag){
		        ArrayList resultList = new ArrayList();
				PreparedStatement pstmt= null;
				DBConnection db = null;
				DBConnection db2 = null;
				int resultCnt = -1;
				
				log.debug("Debug: Start Set");
				
				try{
				 db = new DBConnection();
				 db.connect();
			    
				 log.debug("Debug: Get Count");
		    	 StringBuffer sb= new StringBuffer();
		    	 //Prepare a statement to de a record
				 sql = new String("select activitykey, projectKey, predflag,value from activityref where  projectkey = ? and predflag = ?");	  
	  		 	 pstmt = db.getPreparedPreparedStatement(sql);
				 pstmt.setString(1, projKey.getKeyId());
				 pstmt.setInt(2, predFlag);
				 
				  //log.debug(" ActivityRef Get:  "+ pstmt.toString());
		    	   String activitykey;
				   String projectkey;
				   int predflag;
				   String value;
				   			   
				   
				   
				 //Execute statement
				 ResultSet rs = pstmt.executeQuery();
	         	 while (rs.next()) {
		     	 	activitykey = rs.getString("activitykey");
					projectkey = rs.getString("projectKey");
					predflag = rs.getInt("predflag");
					value = rs.getString("value");
					
					
					//build activity ref
					ActivityKey activityKey = new ActivityKey(activitykey.trim());
					ProjectKey  projectKey = new ProjectKey(projectkey.trim());
					ActivityKey  valueKey = new ActivityKey(value.trim());
					ActivityRef activityRef1 = new ActivityRef(activityKey,projectKey,predflag,valueKey);
		    	   	
					resultList.add(activityRef1);	                    
					
	          	 }  
							
				
				}catch (Exception ex) {
				  	
				   String message = "Error get data ActivityRef : " + ex.getMessage();
			       log.error(message);
			       log.debug(message); 
				}finally {
					  
				 //must clear connection upon execution
				  if(db!= null){
				    db.close();
				  }
				  if(db2!=null){
				    db2.close();
				  }
				  db = null;
				  db2 = null;
							  
				}
				
				return resultList;
				
			}
		
		public int setList(ActivityKey actKey, ProjectKey projKey, int predFlag,Collection inList ) throws DAOException{
		   int cnt = 1;
			   int res = 0;
			   //get iterator
		        java.util.Iterator it =  inList.iterator();
		        
		        
		       //cycle thru all the Activities
		        while (it.hasNext()) {
		        	ActivityInterface aIfc = (ActivityInterface) it.next();
			    	ActivityRef actRef = new ActivityRef (actKey, projKey, predFlag, aIfc.getActivityKey());
		        	res = set(actRef);
		            cnt++;
		        }
		return cnt;
		}
		
		
		/**
		 * <p> Writes the project to the database  </p>
		 *  If the project currently exsists then it will be updated
		 *  otherwise it will be inserted
		 *  Returns -1 if failed otherwise 1 for success
		 * @param int  
		 */
		public int set(ActivityRef actRef) throws DAOException{
			PreparedStatement pstmt= null;
			DBConnection db = null;
			DBConnection db2 = null;
			int resultCnt = -1;
			
			//Base Case 
			//make sure the project is not null before the write 
			if(actRef != null){
			log.debug("Debug: Start Set");
			
						
			
			try{
			 db = new DBConnection();
			 db.connect();
		    
			 log.debug("Debug: Get Count");
	    	 StringBuffer sb= new StringBuffer();
			 
			/* sql = new String("select count(*) from activity activityref where activitykey = ? and projectkey = ? and predfag = ?");	  
  		 	 pstmt = db.getPreparedPreparedStatement(sql);
			 pstmt.setString(1, actRef.getActKey().getKeyId());
			 pstmt.setString(2, actRef.getProjectKey().getKeyId());
			 pstmt.setInt(3, actRef.getPredFlag());
			 */	 
			 sb.append("select count(*) from activityref where activitykey = '").append(actRef.getActKey().getKeyId()+ "'");
			 sb.append(" and projectkey = '").append(actRef.getProjectKey().getKeyId() + "'");
			 sb.append(" and predflag = ").append(actRef.getPredFlag());
			 
			 int count = 0;
			 log.debug("Debug: Query "+ sb.toString());				 
	    	
			 //Execute statement
			 ResultSet rs = db.executeQuery(sb.toString());
         	 while (rs.next()) {
	    	   count = rs.getInt(1);
 	    	   log.debug("Debug: result: "+ count);
          	 }  
						
			
			 //now set up for second connection
			 db2 = new DBConnection();
		     db2.connect();
		    	
		  // count =1; //force update case for debugging
		     
			 if(count == 0){
			 	log.debug("Debug: Starting Insert"); 	
			 	
			 	//Key does not exsist so do insert to project table
			 	sql = new String("INSERT INTO activityref (activitykey,projectKey,predflag,value) VALUES(?,?,?,?)");	  
			 	pstmt = db2.getPreparedPreparedStatement(sql);
			 	
			 	if(pstmt != null){
			 		log.debug("Prepare: Not null");
			 	}else{
			 		log.debug("Prepare:  null");
			 	}
			 	
			 	log.debug("Debug: Starting Insert: "); 	
			 	
			    
			     
			 	//defensive coding technique to deal with nulls
			 	   String activitykey = new String();
				   String projectkey = new String();
				   int predFlag = 0;
				   String value = new String();
				
				   
				   try{
			 	 
			 		
			     //INSERT CASE
			     activitykey = actRef.getActKey().getKeyId();
			     projectkey = actRef.getProjectKey().getKeyId();
				 predFlag = actRef.getPredFlag();
			     value = actRef.getValue().getKeyId();
				 
				 
				   log.debug("Insert State: "+ pstmt.toString());
				    
				    pstmt.setString(1, activitykey);
				    pstmt.setString(2, projectkey);
				    pstmt.setInt(3, predFlag);
				    pstmt.setString(4, value); //start date
				  
				 
				 log.debug("Insert State: "+ pstmt.toString());					    
				 
				log.debug("Debug: Starting Insert ActRef: ");					    
			    resultCnt = pstmt.executeUpdate();
			    log.debug("Debug: Insert result " + resultCnt);
			 		
			    
			 	}catch (Exception ex) {
				   String message = "Error trying to do insert: " + ex.getMessage();
			       log.error(message);
			       log.debug(message);
				}
				
			    
			    
			    
			 }else{
			
			 	//UPDATE CASE 
			 	log.debug("Debug: Starting Update"); 	
			 	
			 	
			 	//Key exsists so do an update
		 	 	sql = new String("Update activityref " +
		 	 					 "set activitykey = ?,"+
		 	 					 "projectkey = ?, " +
		 	 					"predflag=?,"+
								 "value = ? " +
								 "WHERE  activitykey= ?  and  projectkey = ?  and predflag=?");    
			 				  
			    pstmt = db2.getPreparedPreparedStatement(sql);
			 	
			    
			 	//defensive coding technique to deal with nulls
			 	   String activitykey = new String();
				   String projectkey = new String();
				   int predflg =0;
				   String val = new String();
				   
				  				   
				   //INSERT CASE
				     activitykey = actRef.getActKey().getKeyId();
				     projectkey = actRef.getProjectKey().getKeyId();
					 predflg = actRef.getPredFlag();
				     val = actRef.getValue().getKeyId();
					 
					 
					   log.debug("Update State: "+ pstmt.toString());
						// Prepare a statement to update a record
					    pstmt.setString(1, activitykey);
					    pstmt.setString(2, projectkey);
					    pstmt.setInt(3, predflg);
					    pstmt.setString(4, val); //start date
					    pstmt.setString(5, activitykey);
					    pstmt.setString(6, projectkey);
					    pstmt.setInt(7, predflg);
					
			     log.debug("Update State: "+ pstmt.toString());					  
					
				//Execture sql check result count
			    resultCnt = pstmt.executeUpdate();
			   		   
			 } 
			}catch (Exception ex) {
			   
			   String message = "Error trying to do update: " + ex.getMessage();
			   
		       log.error(message);
			}finally {
				  
			 //must clear connection upon execution
			  if(db!= null){
			    db.close();
			  }
			  if(db2!=null){
			    db2.close();
			  }
			  db = null;
			  db2 = null;
						  
			}
			
			}
			return resultCnt;
		}
		
		/**
		 * <p>Deletes project from database based on key</p>
		 * Returns -1 if failed otherwise 1 for success
		 * @param PM275BaseKey pKey
		 */
		public int delete (ActivityKey actKey, ProjectKey projectKey, int predFlag) throws DAOException{
			PreparedStatement pstmt= null;
			DBConnection db = null;
		    int resultCnt = -1;
			int count;
			
			//Base case: make sure key is not null
			if(actKey != null &&  projectKey != null){
			log.debug("Debug: Start Delete");
			
			try{
			 db = new DBConnection();
			 db.connect();
		   			 
			 //Prepare a statement to de a record
			 sql = new String("delete from activityref where activitykey = ? and projectkey = ? and predfag = ?");	  
  		 	 pstmt = db.getPreparedPreparedStatement(sql);
			 pstmt.setString(1, actKey.getKeyId());
			 pstmt.setString(2, projectKey.getKeyId());
			 pstmt.setInt(3, predFlag);
			 
			 //check result count
			 resultCnt = pstmt.executeUpdate();
			 
			}catch (Exception ex) {
				
			   String message = "Error trying to delete: " + ex.getMessage();
		       log.debug(message);
			   log.error(message);
			}finally {
				  
			 //must clear connection upon execution
			  if(db!= null){
			    db.close();
			  }
			 
			  db = null;
			 
						  
			}
			}
			
			return resultCnt;
		}
			
		
		
		/**
		 * <p>Returns string</p>
		 * 
		 * @param string
		 */
		public String toString(){
			return "ActivityRefDAOtoDB";
		}	
		
		
		
}

/**
* $Log: ActivityRefDAOtoDB.java,v $
* Revision 1.14  2005/05/13 18:46:04  ccochran
* cleaned up dangling integration points to other services
*
* Revision 1.13  2005/05/10 21:30:54  ccochran
* cleaned up comments
*
* Revision 1.12  2005/05/10 20:08:59  ccochran
* fixes for load activities preds
*
* Revision 1.11  2005/05/10 19:19:24  ccochran
* changed activity ref
*
* Revision 1.10  2005/05/09 20:10:37  ccochran
* cleaned up system outs
*
* Revision 1.9  2005/05/09 19:50:18  ccochran
* fixed bug on get
*
* Revision 1.7  2005/05/09 03:58:30  vchumako
* Fixed activityref table name
*
* Revision 1.6  2005/05/08 23:12:47  rcoutinh
* fixed sql syntax in set method
*
* Revision 1.5  2005/05/08 19:03:57  rcoutinh
* Fixed ClassCastException in ActivityRef Constructor  and some queries
*
* Revision 1.4  2005/05/07 03:41:26  ccochran
* changed collection
*
* Revision 1.3  2005/05/07 03:36:51  ccochran
* code add list
*
* Revision 1.2  2005/05/04 12:40:34  ccochran
* updated dao
*
* Revision 1.1  2005/05/04 06:42:06  ccochran
* added to repository
*
* Revision 1.1  2005/05/04 05:54:29  ccochran
* added footers
*
*
* Updated footer to include CVS log.
*
**/

