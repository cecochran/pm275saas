/***************************************************
 * File:        $RCSfile: Project.java,v $
 * @author      $Author: tlong2 $,
 * @version     $Id: Project.java,v 1.101 2005/05/16 00:58:56 tlong2 Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * This is the primary project class which purpose is provide data
 * and methods for managing projects.Every project has a unique key
 * and a none blank name. Projects own unique activities.Project start dates are set but end
 * dates must be calculated by sum of the subsquent activities.
 */
package pm275.core.project;

import java.io.File;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Hashtable;
import java.util.Date;
import java.util.Collection;
import java.util.List;
import java.util.Vector;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import org.apache.log4j.Logger;
import pm275.core.resource.Resource;
import pm275.core.resource.ResourcePoolKey;
import pm275.core.resource.ResourceService;
import pm275.core.calendar.CalendarService;
import pm275.core.calendar.CalendarKey;
import pm275.core.calendar.CalendarInterface;
import pm275.common.exception.CycleDependencyException;
import pm275.common.exception.DuplicateKeyException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.PersistanceException;
import pm275.common.exception.ProjectCreationException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.ResourceException;
import pm275.common.persistance.ProjectVisitorInterface;
import pm275.common.persistance.ProjectXmlWriter;
import pm275.common.algorithm.CycleDependency;
import pm275.common.algorithm.ProjectCriticalPath;
import pm275.gui.common.DateFormatter;


/**
 * <p></p>
 */
public class Project implements ProjectInterface {
    /**
     * <p>Project Attributes</p>
     */
    //Project metadata
    private ProjectKey projectKey;
    private String name;

    //Activities
    private Map activities;
    protected ActivityKey startMilestoneKey;
    protected ActivityKey endMilestoneKey;
    protected Activity startMilestone;
    protected Activity endMilestone;

    //Resource Support
    private ResourcePoolKey resourcePoolKey;
    protected static ResourceService resourceService;

    //Calendar Support
    private CalendarKey calendarKey;
    protected static CalendarService calendarService;
    static
    {
        calendarService = CalendarService.getInstance();
    }
    private static final Logger log = Logger.getLogger(Project.class);
    private static final String ATTR = "Project";

/**
 * <p>Project Constructor ...</p>
 * String Name The name of the project
 * ProjectKey  The unique key for the project (project name)
 */
    protected Project(ProjectKey pKey,CalendarKey cKey, String name )
            throws ProjectCreationException
    {

        this.name = name;
        this.projectKey = pKey;
        this.activities = new Hashtable();

        // Default project start date is today.
        Date today = new Date();
        Date startDate;

        //
        // this.resourcePoolKey = rpKey;

        try {

            // default calendar
            calendarKey = cKey;
            

            if (calendarService != null){
            	CalendarInterface calendarIfc = calendarService.getCalendar(calendarKey);	
                // Set the start date.  If it's not a work day, use the next
                // available work day.
                startDate = calendarIfc.getNextWorkDay(today);
                // initialize Start\End Milestone for Critical Path
                startMilestone = new Activity(startDate,pKey);
                endMilestone = new Activity(startDate,pKey);

            }

          
            initializeMilestones();
        }
        catch (KeyNotFoundException ex)
        {
            String message = "Cannot find calendar key: " + ex.getMessage();
            log.error(message);
            throw new ProjectCreationException(message, ex);
        }

    }





  public Project(String pKey,String name,String startmstoneKey,String endmstoneKey, String rpKey, CalendarKey cKey,Date startdate )
  {

      try{
      	
      
        this.name = name;
        this.projectKey = new ProjectKey(pKey);
        this.activities = new Hashtable();

        // Default project start date is today.
        Date today = new Date();
        Date startDate = startdate;
  
       
        this.resourcePoolKey = new ResourcePoolKey(rpKey);

        // default calendar
        this.calendarKey = cKey; //new CalendarKey(cKey);
        if (calendarService != null){
        	CalendarInterface calendarIfc = calendarService.getCalendar(calendarKey);	
            // Set the start date.  If it's not a work day, use the next
            // available work day.
        	
            startDate = calendarIfc.getNextWorkDay(startdate);
            // initialize Start\End Milestone for Critical Path
            startMilestone = new Activity( startmstoneKey , projectKey.getKeyId() , "Start1 Project Milestone" , startDate , startDate , 0 , 0 , 0 , this.resourcePoolKey.getKeyId() , 1 , 1 , 0 );
            endMilestone = new Activity( endmstoneKey , projectKey.getKeyId() , "End1 Project Milestone" , startDate , startDate , 0 , 0 , 0 , this.resourcePoolKey.getKeyId() , 1 , 1 , 0 );
            //endMilestone = new Activity(startDate,projectKey);

            initializeMilestones();
        }
        
        }catch (KeyNotFoundException ex){

            String message = "Cannot find calendar key: " + ex.getMessage();
            log.error(message);
            //throw new ProjectCreationException(message, ex);
        }
 }

    private void initializeMilestones() {

        String field = null;

        startMilestone.setName("Start Project Milestone");
        startMilestone.setProjectKey(this.getProjectKey());

        endMilestone.setName("End Project Milestone");
        endMilestone.setProjectKey(this.getProjectKey());

        // default start and end milestones
        startMilestoneKey = startMilestone.getActivityKey();
        endMilestoneKey = endMilestone.getActivityKey();

        startMilestone.setDuration(0);
        startMilestone.setEarlyStartHours(0);
        startMilestone.setEarlyFinishHours(0);
        startMilestone.setEarlyFinishFlag(false);

        endMilestone.setLateStartFlag(true);
        endMilestone.setDuration(0);

        field = "start milestone";
        activities.put(startMilestone.getActivityKey(),startMilestone);

        field = "end milestone";
        activities.put(endMilestone.getActivityKey(),endMilestone);

    }
/**
 * <p>getName()</p>
 * This function returns the name of the project.Every project
 *  have a non blank, preferably unique name set by user
  * @return String
 */
    public String getName() {
        return name;
    }

/**
 * <p>setName()</p>
 * Sets the name of for a project
 * @param _name
 */
    public void setName(String s) {
        name = s;
    }

    /**
     *  Returns this project's unique key.
     *  @return Unique key for this project.
     */
    public void setProjectKey (ProjectKey key)
    {
        this.projectKey = key;
    }


    /**
     *  Returns this project's unique key.
     *  @return Unique key for this project.
     */
    public final ProjectKey getProjectKey()
    {
        //avoid null key possibility due to bad data
        if (projectKey == null){
            projectKey = new ProjectKey();
        }

        return projectKey;
    }

/**
 * <p>getStartDate.</p>
 * Start date for a project.
 *
 * @return Date s
 */
    public Date getStartDate()
            // throws InternalProjectException, KeyNotFoundException
    {
        //      Do we need to do traversal of all activities ?
        // TLong : no everytime we insert or delete an activity we need to
        //          force a recalculation of all the downstream activity durations.

        // Why was this in the code as of version 1.17?  Doesn't seem to make
        // any sense. [achou]
        // startMilestone = removeActivity(startMilestoneKey);

     
    	
    	 // Deal with null case
         if(startMilestone == null){
           	 // Default project start date is today.
           Date today = new Date();
           Date startDate;
           startMilestone = new Activity(today,this.projectKey);
          
         }
         
         return startMilestone.getStartDate(); 
    }

/**
 * <p>set StartDate(Date date)</p>
 *  Sets start date for a poject
 * @param _startDate
 */
    public void setStartDate(Date date)
            throws InternalProjectException //, KeyNotFoundException
    {
        Date startDate = new Date();
        try
        {
            CalendarInterface calendarIfc = calendarService.getCalendar(calendarKey);

            // Set the start date.  If it's not a work day, use the next
            // available work day.
            startDate = calendarIfc.getNextWorkDay(date);
        }
        catch(KeyNotFoundException kfe)
        {
            log.error("Project-setStartDate: Could not find defualt calendar", kfe);
            throw new InternalProjectException("Could not find defualt calendar");
        }



        // Why was this in the code as of version 1.17?  Doesn't seem to make
        // any sense. [achou]
        // startMilestone = removeActivity(startMilestoneKey);

        startMilestone.setStartDate(startDate);
        updateActivity(startMilestoneKey,startMilestone);

    }

/**
 * <p>Represents end date for entire project</p>
   @return date
 */
    public Date getEndDate() {
        //      Do we need to do traversal of all activities ?
        Collection activities = (this.getActivities()).values();
        Iterator it = activities.iterator();

        Activity a = (Activity)it.next();
        Date currEnd = a.getEndDate();
        try {

            while (it.hasNext()) {
                a = (Activity)it.next();

                if (currEnd.before(a.getEndDate()))
                    currEnd = a.getEndDate();

            }
        } catch (Exception e) {
            log.error("Project.getEndDate()" + e.getMessage());
        }
        return currEnd;
    }


/**
 * <p>ResourcePoolKey is the key for the group of resource assinged to project</p>
 *
 *  * @return ResourcePoolKey
 */
    public ResourcePoolKey getResourcePoolKey() {

        //avoid null key possibility due to bad data
        if (resourcePoolKey == null){
            resourcePoolKey = new ResourcePoolKey();
        }


        return resourcePoolKey;
    }

/**
 * <p>Sets the resource pool key for a given project</p>
 * The resource pool key will be used in conjunction
 * with resource service to retrive a pool of resources
 * to assign to a project
 * @param _resourcePool
 */
    public void setResourcePoolkey(ResourcePoolKey resPoolKey) {
        resourcePoolKey = resPoolKey;
    }



/**
 * <p>Returns the project calendar Key</p>
 * Every project has an associated calaneder unique to the project
 * @return CalendarKey
 */
    public CalendarKey getCalendarKey() {

        //Avoid null key data
        if (calendarKey == null){
            calendarKey = new CalendarKey();
        }


        return calendarKey;
    }

/**
 * <p>Sets the calendarKey for the project</p>
*
 * @param _calendar
 */
    public void setCalendarKey(CalendarKey key) {
        calendarKey = key;
    }

/**
 * <p>Iterates over the activites determing critical path</p>
 *  The LinkedList is that of series of activities
 *  @return LinkedList
 */
    public LinkedList calculateCriticalPath() {

        LinkedList criticalPath = null;
        try {

            ProjectCriticalPath pcp = new ProjectCriticalPath(this.projectKey);
            criticalPath = pcp.calculateCriticalPath();
        }
        catch (KeyNotFoundException knfe) {

            log.error("ERROR : Project.calculateCriticalPath() KeyNotFoundException");

        }

        return criticalPath;
    }

/**
 * <p>Detect Cycles</p>
 * A cycle is an unwanted event where one activity A depends on
 * another activity B. Then activity B depends on A.
 * This creates a cyclic dependancy. IF true a cycle exsists
 * @param Predecessor Activity Key
 * @param Successor Activity Key
 * @return boolean
 */
    public boolean detectCycles(ActivityKey p, ActivityKey s) {
        // your code here

    try {
        new CycleDependency(this, p, s);
    }
    catch (InternalActivityException e){
        return false;
    }
    catch (CycleDependencyException e){
        return true;
    }

        return false;
    }



    /**
     * <p>Associate Activities</p>
     * Associate 2 activitites
     * @param Predecessor Activity Key
     * @param Successor Activity Key
     * @return boolean
     */
    public void associateActivities(ActivityKey p, ActivityKey s)
                throws KeyNotFoundException,
                       DuplicateKeyException,
                       CycleDependencyException,
                       InternalActivityException{

        Activity actPred;
        Activity actSucc;

        try {
            actPred = this.getActivity(p);
            actSucc = this.getActivity(s);

        // associate them
        actPred.addSuccessor(s);
        actSucc.addPredecessor(p);

            /**
             * Test for a cycle.  If one is found, roll back the dependency inserted
             */
        if (detectCycles(p,s)) {
            actPred.removeSuccessor(s);
            actSucc.removePredecessor(p);
            throw new CycleDependencyException("Cycle Detected in Project.associateActivities()");
        }

        // Compute and update the successor's start and end dates.
        actSucc.computeStartDate();

        // TODO Review this change - Pred will call Succ's computerEndDate
        //      Trying to debug Bug in getEndDate for project.
        actPred.computeEndDate(actPred.getStartDate(),actPred.getDuration());
        //      actSucc.computeEndDate(actSucc.getStartDate(),actSucc.getDuration());

    } catch (InternalProjectException ipe) {
        log.error("Error: Project.associateActivities - Failed to associate activities.");
    }


        // TODO TLong Question. Does Project need to be recalculated here?


    }

    /**
     *  Returns the number of activities associated with this project.
     *  @return Number of activities in this project.
     */
    public int getActivitiesCount()
    {
        int count = activities.size();

        // If start and end milestones are in the activities list, decrease the
        // count accordingly.
        if (activities.containsValue(startMilestone))
        {
            count--;
        }
        if (activities.containsValue(endMilestone))
        {
            count--;
        }

        // Count cannot be less than 0.
        if (count < 0)
        {
            count = 0;
        }

        return count;
    }

    /**
     *  Returns a list of activities in this project with the start and end
     *  milestones removed.  The order of the activities is determined by the
     *  specified comparator.
     *  @param comparator Comparator that determines the sorted order of the list.
     *                    If null, the activities are arbitrarily sorted.
     *  @return Sorted list of activities in this project.
     */

    public List getActivities(Comparator comparator)
    {
        boolean bReturnMilestones = false;

        // TLong 4/17/05 : Put all of this into getActivities(Comparator comparator, boolean bReturnMilestones)
        // in order to make critical Path algorithm work
        return getActivities(comparator,bReturnMilestones);

    }

    /**
    *  Returns a list of activities in this project;
    */

   public Map getActivities()
   {
       return activities;

   }


 
 
 


  public void addActivityDB(ActivityKey key, ActivityInterface activity)
  {
  	 if(key != null && activity != null){	
     
            activities.put(key, activity);
  	 }
  	 
  }
  
    /**
     *  Adds an activity to the end of the project's activities list.
     *  @param key Unique key for the new activity.
     *  @param activity Object representing the activity.
     */
    public void addActivity(ActivityKey key, ActivityInterface activity )
                throws DuplicateKeyException, InternalProjectException {
        try {
      	
        if(key != null && activity != null){	
            if (activities.containsKey(key)){
                log.error("Duplicate Activity " + key.toString());
                throw new DuplicateKeyException("Duplicate Activity " + key.toString());
            }else{
                activities.put(key, activity);

        // replaced
        // associateActivities(startMilestone.getActivityKey(),key);
        startMilestone.addSuccessor(key);
        activity.addPredecessor(startMilestone.getActivityKey());
        if (activity.getStartDate().before(startMilestone.getEndDate()))
            {
            activity.setStartDate(startMilestone.getEndDate());
            }
        activity.computeEndDate(activity.getStartDate(),
                       activity.getDuration());

        // replaced
                //associateActivities(key,endMilestone.getActivityKey());
        activity.addSuccessor(endMilestone.getActivityKey());
        endMilestone.addPredecessor(key);
                if (endMilestone.getStartDate().before(activity.getEndDate()))
                    {
                        endMilestone.setStartDate(activity.getEndDate());
                    }
                endMilestone.computeEndDate(endMilestone.getStartDate(),
                       endMilestone.getDuration());

            }
        }
        
        } catch (Exception e) {
            log.error("Error: Project.addActivity - Failed to add activity." + e.getMessage());
            throw new InternalProjectException("Error: Project.addActivity - Failed to add activity.");

        }
       
        
        
        
    }

   /**
   *  Returns a list of activities in this project;
   */

  public Collection getResources()
  {
    try
    {
      return ResourceService.getInstance().getResourcePool(this.resourcePoolKey).listResources();
    }
    catch(ResourceException e)
    {
        log.error("Error: ResourceService: ResourcePool:  Could not get resource pool for this project:  ResourcePoolKey = " +
        resourcePoolKey + " Exception message " + e.getMessage());
        return null;
    }

  }

    /**
     *  Returns a list of activities in this project with the start and end
     *  milestones removed.  The order of the activities is determined by the
     *  specified comparator.
     *  @param comparator Comparator that determines the sorted order of the list.
     *                    If null, the activities are arbitrarily sorted.
     *  @param bReturnMilestones True of want to return Start\End milestones. Pass
     *                    true for critical path algorithm. False if using list in UI.
     *  @return Sorted list of activities in this project.
     */

    public List getActivities(Comparator comparator, boolean bReturnMilestones)
    {
        // Get the list of activities.
        Collection activitiesList = activities.values();
        List sortedList;
        if (activitiesList == null)
        {
            sortedList = new Vector();
        }
        else
        {
            sortedList = new Vector(activitiesList);

            if (!bReturnMilestones) {
            // Pull out the start/end milestones.
            sortedList.remove(startMilestone);
            sortedList.remove(endMilestone);
            }

            // If comparator is specified, sort the list.
            if (comparator != null)
            {
                Collections.sort(sortedList, comparator);
            }
        }

        // Return the list.
        return sortedList;
    }

    /**
     * <p>Get Activity</p>
     * Gets an activity from a project given the key
     *
     * @param key The Activity Key for a particular activity project
     *
     */
    public Activity getActivity(ActivityKey key)
                throws KeyNotFoundException, InternalProjectException {

        Activity act = null;
        try {
            if (!activities.containsKey(key)){

                log.error("Error: Project.getActivity() - Key not found " + key.toString());
                throw new KeyNotFoundException("Error: Project.getActivity() - Key not found " + key.toString());
            }else{
                act = (Activity)activities.get(key);
            }

        } catch (Exception e) {
            log.error("Error: Project.getActivity() - Failed to get activity " + key.toString());
            throw new InternalProjectException("Error: Project.getActivity() - Failed to locate activity " + key.toString());
        }

        return act;
    }


/**
 * <p>Remove Activity</p>
 * Removes an activity from a project given the key
 *
 *
 * @param key The Activity Key for a particular activity project
 *
 */
    public Activity removeActivity(ActivityKey key)
                throws KeyNotFoundException, InternalProjectException{

        Activity act = null;
        try {
            if (activities.containsKey(key)){

                // remove the activity
                act = (Activity) activities.remove(key);

                // remove connections from successors and predeccessors
                act.disassociateActivity();

                return act;
            }else{

                log.error("Error: Project.removeActivity() - Key not found " + key.toString());
                throw new KeyNotFoundException("Error: Project.removeActivity() - Key not found " + key.toString());
            }
        } catch (Exception e) {
            log.error("Error: Project.removeActivity() - Failed to remove key " + key.toString());
            throw new InternalProjectException("Error: Project.removeActivity() - Failed to remove key " + key.toString());
        }

       }



    /**
     * <p>Update Activity</p>
     * Removes an activity from a project given the key
     *
     * @param key The Activity Key for a particular activity project
     *
     */
    public void updateActivity(ActivityKey key, ActivityInterface activity)
                throws InternalProjectException {
        try {
            activities.put(key, activity);
        } catch (Exception e) {
            log.error("Error: Project.updateActivity - Failed to update activity.");
            throw new InternalProjectException("Error: Project.updateActivity - Failed to update activity.");

        }
}

    public ActivityInterface getStartMilestone() {

         if(startMilestone == null){

            try
            {
            Date startDate = DateFormatter.getDateForString("2005-04-18");
            startMilestone = new Activity(startDate,projectKey);
             } catch (Exception e) {
             log.error("Error: Project.getStartMilestone- Failed to get activity.");
          }

         }

        return startMilestone;
    }

    public ActivityInterface getEndMilestone() {

     if(endMilestone == null){
        try{
        Date startDate = DateFormatter.getDateForString("2005-04-18");
        endMilestone = new Activity(startDate,this.getProjectKey());
        }catch(Exception e){
             log.error("Error: Project.getEndMilestone- Failed to update activity.");
        }
     }

        return endMilestone;
    }

    // Visitor Methods
    public void acceptVisitor(ProjectVisitorInterface visitor) {
        visitor.visitProject(this);
    }

    public void writeToFile(ProjectVisitorInterface visitor) {
        visitor.writeProject();
    }


    /**
     * <p>Update Activity</p>
     *
     *
     * @param key The Activity Key for a particular activity project
     *
     */
    public Collection listTodaysActivities(Calendar today) throws InternalProjectException
    {
        try
        {
            Collection todaysActivities = new Vector();
            Collection activities = (this.getActivities()).values();
            Iterator activityIterator = activities.iterator();
            while (activityIterator.hasNext())
            {
                ActivityInterface activity = (ActivityInterface)activityIterator.next();

                Date start = activity.getStartDate();
                Calendar actStartDate = Calendar.getInstance();
                actStartDate.setTime(start);

                Date end = activity.getEndDate();
                Calendar actEndDate = Calendar.getInstance();
                actEndDate.setTime(end);

                if ( (actStartDate.equals(today) || actEndDate.equals(today)) || (actStartDate.after(today) && actEndDate.before(today) ))
                {
//                  Writer.write("Today's activities " + activity.getName());
                    todaysActivities.add(activity);
                }
            }
            return todaysActivities;
        }
        catch (Exception e)
        {
            log.error("Error: Project.listTodaysActivities - Failed to list todays activities.");
            throw new InternalProjectException("Error: Project.listTodaysActivities - Failed to calculate today's activities.");
        }
    }

    public Collection listTodaysResources(Calendar today) throws InternalProjectException
    {
        try
        {
            Collection todaysResources = new Vector();
            Collection activities = (this.getActivities()).values();
            Iterator activityIterator = activities.iterator();
            while (activityIterator.hasNext())
            {
                ActivityInterface activity = (ActivityInterface)activityIterator.next();

                Date start = activity.getStartDate();
                Calendar actStartDate = Calendar.getInstance();
                actStartDate.setTime(start);

                Date end = activity.getEndDate();
                Calendar actEndDate = Calendar.getInstance();
                actEndDate.setTime(end);

                if ( (actStartDate.equals(today) || actEndDate.equals(today)) || (actStartDate.after(today) || actEndDate.before(today) ))
                {
                    Collection assignedResources = activity.getResources();
                    Iterator resourceIterator = assignedResources.iterator();
                    while(resourceIterator.hasNext())
                    {
                        Resource resource = (Resource)resourceIterator.next();
                        todaysResources.add(resource);
                        //AssignmentInterface assignment = ResourceService.getInstance().getAssignment(resource.getKey(), activity.getActivityKey());

                    }
                }
            }
            return todaysResources;
        }
        catch (Exception e)
        {
            log.error("Error: Project.listTodaysResources - Failed to list todays resources.");
            throw new InternalProjectException("Error: Project.listTodaysResources - Failed to calculate today's resources.");
        }

    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer("Project").append("\n");
        sb.append("Key:").append(this.getProjectKey().toString()).append("\n");
        sb.append("Name:").append(this.getName()).append("\n");
        sb.append("StartMilestone:").append(this.getStartMilestone()).append("\n");
        sb.append("EndMilestone:").append(this.getEndMilestone()).append("\n");
        sb.append("ResourcePoolKey:").append(this.getResourcePoolKey()).append("\n");
        sb.append("CalendarKey:").append(this.getCalendarKey()).append("\n");


           Map actMap = this.getActivities();
           java.util.HashMap map = new java.util.HashMap(actMap);

           int cnt = 1;
           int res = 0;
           //get iterator
            java.util.Iterator it =  map.values().iterator();
           //cycle thru all the Activities
            while (it.hasNext()) {

                Activity act = (Activity) it.next();
                sb.append("Act#: "+cnt +" Activity  " + act.toString() +"\n");
                cnt++;
            }

        return sb.toString();
   }


    
    /**
     * writeProjectXML() runs through the list of Activity objects
     * that have been added to the Project and writes them out.
     * @param sToFile path and name of file to write to
     * @return iReturnValue 0 = success, 1 = failure, etc if needed
     */
      public int writeProjectXML(File fileTo) throws PersistanceException {
      	int iReturnValue = 0;
      	
      	try {
	      	ProjectXmlWriter writer = new ProjectXmlWriter(fileTo);
	      	
	      	writer.writeProject(this);

      	} catch (PersistanceException pe) {
            log.error("Error: Project.writeProjectXML - Error writing project to XML file.");
            iReturnValue = 1;
      	}
		
      	return iReturnValue;
      }
}


/**
 * $Log: Project.java,v $
 * Revision 1.101  2005/05/16 00:58:56  tlong2
 * cleaned up compile warnings
 *
 * Revision 1.100  2005/05/16 00:11:05  dnathan
 * Got a little less ambitious with commenting.
 *
 * Revision 1.99  2005/05/16 00:03:47  dnathan
 * Fixed problem with Start and End Milestones in project view.
 *
 * Revision 1.98  2005/05/15 22:57:12  ccochran
 * work with date
 *
 * Revision 1.97  2005/05/15 20:22:03  ccochran
 * added start date to constructor
 *
 * Revision 1.96  2005/05/15 16:55:01  rcoutinh
 * Corrected some error messages
 *
 * Revision 1.95  2005/05/14 20:15:58  ccochran
 * update to use this project service
 *
 * Revision 1.94  2005/05/13 18:46:04  ccochran
 * cleaned up dangling integration points to other services
 *
 * Revision 1.93  2005/05/13 18:02:53  ccochran
 * Refactored DB interaction to be owned by individual services invokeing presistance layer
 *
 * Revision 1.92  2005/05/12 20:59:11  ccochran
 * update for getting DB pred succ working
 *
 * Revision 1.91  2005/05/10 21:57:53  ccochran
 * updated marshaling for project
 *
 * Revision 1.90  2005/05/10 20:08:59  ccochran
 * fixes for load activities preds
 *
 * Revision 1.89  2005/05/10 19:19:44  ccochran
 * updated set activities
 *
 * Revision 1.88  2005/05/09 18:34:15  vchumako
 * Passed created CalendarKey directly to project
 *
 * Revision 1.87  2005/05/09 16:28:53  ccochran
 * patch for ui continued
 *
 * Revision 1.86  2005/05/09 15:43:50  ccochran
 * patch
 *
 * Revision 1.85  2005/05/07 18:34:18  ccochran
 * merge
 *
 * Revision 1.84  2005/05/07 18:01:28  tlong2
 * completed writeProjectXML
 *
 * Revision 1.83  2005/05/07 17:11:27  tlong2
 * Added writeProjectXML
 *
 * Revision 1.82  2005/05/07 15:00:53  achou
 * Removed XML write/load methods.
 *
 * Revision 1.81  2005/05/07 14:45:45  ccochran
 * added db xml support
 *
 * Revision 1.80  2005/05/06 23:15:59  ccochran
 * added support for loading activities from DB
 *
 * Revision 1.79  2005/05/06 22:42:10  ccochran
 * grew toString to list contents of activities
 *
 * Revision 1.78  2005/05/05 06:02:46  tlong2
 * Fixed getEndDate()
 *
 * Revision 1.77  2005/05/05 05:14:00  tlong2
 * Removed SetEndDate() as a fixed end date setter
 *
 * Revision 1.76  2005/05/04 04:59:48  ccochran
 * added code to deal with null milestone
 *
 * Revision 1.75  2005/05/03 13:49:06  ccochran
 * added setter for project key
 *
 * Revision 1.74  2005/05/03 13:18:35  ccochran
 * update constructor
 *
 * Revision 1.73  2005/05/03 13:14:16  ccochran
 * added constructor
 *
 * Revision 1.72  2005/05/03 09:13:47  ccochran
 * updated to string
 *
 * Revision 1.71  2005/05/02 23:55:51  vchumako
 * Removed references to pm275.vct.Writer class. Looks like they were for debug purposes.
 *
 * Revision 1.70  2005/05/02 19:40:03  bbuchana
 * Added VCT functionality: Iteration 3
 *
 * Revision 1.69  2005/04/30 22:13:28  achou
 * Re-calculate activity's start date when adding a dependency.
 *
 * Revision 1.68  2005/04/29 01:10:30  tlong2
 * Added visitor pattern updates acceptVisitor methods, etc
 *
 * Revision 1.67  2005/04/24 20:13:32  dnathan
 * Added start and end milesone accessors to support a cleaner critical path algo.
 *
 * Revision 1.66  2005/04/24 15:54:44  rcoutinh
 * Activity now uses Project calendar
 *
 * Revision 1.65  2005/04/19 09:24:50  achou
 * Used new Calendar method to get next work day.
 *
 * Revision 1.64  2005/04/19 08:59:18  achou
 * Replace calls to Calendar.computeFutureWorkDate() with Calendar.AddDaysToDate().
 *
 * Revision 1.63  2005/04/19 08:01:57  umangkus
 * Modified addActivity() to mirror some functionality of
 * associateActivities().
 *
 * Revision 1.62  2005/04/19 06:22:35  achou
 * In associateActivities(), update successor's start date only if it's before the predecessor's end date.
 *
 * Revision 1.61  2005/04/18 21:32:55  tlong2
 * Critical Path working!
 *
 * Revision 1.60  2005/04/18 13:56:17  rcoutinh
 * associate -method updates succ and pred activity dates
 *
 * Revision 1.59  2005/04/18 13:23:04  tlong2
 * minor changes
 *
 * Revision 1.58  2005/04/18 10:58:35  tlong2
 * one more time
 *
 * Revision 1.57  2005/04/18 10:57:19  tlong2
 * Another CP bug fix
 *
 * Revision 1.56  2005/04/18 10:48:57  tlong2
 * merge conflicts resolved
 *
 * Revision 1.55  2005/04/18 09:15:57  umangkus
 * Remove stack debug try catch block.
 *
 * Revision 1.54  2005/04/18 08:42:13  tlong2
 * Fix addActivity bug
 *
 * Revision 1.53  2005/04/18 08:13:17  tlong2
 * fixed logging error
 *
 * Revision 1.52  2005/04/18 07:59:49  tlong2
 * Updated code to fix CP bug (could impact DetectCycles()?)
 *     public void addActivity(ActivityKey key, ActivityInterface activity )
 *                 // All Activities  MUST be associated to start/end milestones for CP algo to work
 *                 associateActivities(startMilestoneKey,key);
 *                 associateActivities(key,endMilestoneKey);
 *
 * Revision 1.51  2005/04/18 06:50:59  umangkus
 * Fixed type.
 *
 * Revision 1.50  2005/04/18 04:37:27  achou
 * Fixed bug where getActivities(Comparator) was always discarding the comparator when calling getActivities(Comparator, boolean).
 *
 * Revision 1.49  2005/04/18 04:30:20  achou
 * When calculating the number of activities, look for milestones using the actual objects instead of the keys.
 *
 * Revision 1.48  2005/04/18 04:17:13  achou
 * Fixed typo in last check-in.
 *
 * Revision 1.47  2005/04/18 04:16:47  achou
 * Implemented method to return the number of activities.
 *
 * Revision 1.46  2005/04/18 04:01:53  rcoutinh
 * setStartDate - computeFutureWorkDate call needs to be with duration in hours
 *
 * Revision 1.45  2005/04/18 03:08:34  tlong2
 * updated getActivities( ) so it takes a boolean value indicating whether or not to return the start\end milestones. Start\end milestones are needed for Critical path calculations
 *
 * Revision 1.44  2005/04/17 11:32:23  achou
 * Commented code that removes end milestone in getEndDate().
 *
 * Revision 1.43  2005/04/17 08:49:12  achou
 * Cleaner version of last commit.
 *
 * Revision 1.42  2005/04/17 08:46:53  achou
 * In Project constructor, replaced catch block for Exception with catch blocks for specific exceptions.
 *
 * Revision 1.41  2005/04/16 21:16:52  rcoutinh
 * use default calendar rather than calendarservice itself
 *
 * Revision 1.40  2005/04/16 21:03:49  dnathan
 * Warning hunting.
 *
 * Revision 1.39  2005/04/16 20:06:46  rcoutinh
 * Updated detectCycles to handle InternalActivityException
 *
 * Revision 1.38  2005/04/16 19:58:58  rcoutinh
 * Added InternalActivityException to associateActivities
 *
 * Revision 1.37  2005/04/16 18:29:49  achou
 * Changed updateActivity() signature to take ActivityInterface param.
 *
 * Revision 1.36  2005/04/15 22:29:39  ccochran
 * Fixed to use Project Interface
 *
 * Revision 1.35  2005/04/14 03:43:16  tlong2
 * Added incomplete support for Add/Get/Remove Resource
 *
 * Revision 1.34  2005/04/14 01:16:48  tlong2
 * Added support for disassociating sucessors and predecessor from an activity that is being removed from a project
 *
 * Revision 1.33  2005/04/13 04:24:50  tlong2
 * Removed unneeded critical path code
 *
 * Revision 1.32  2005/04/13 00:08:36  tlong2
 * Critical Path algorithm updates
 *
 * Revision 1.31  2005/04/12 03:41:20  dnathan
 * Removed unneeded imports.
 *
 * Revision 1.30  2005/04/11 03:19:28  achou
 * Re-organized code in getActivities() and remove start/end milestones.
 *
 * Revision 1.29  2005/04/10 13:22:31  tlong2
 * Updates for Critical Path algorithm implementation
 *
 * Revision 1.28  2005/04/10 12:10:29  tlong2
 * Updates for Critical Path algorithm implementation
 *
 * Revision 1.27  2005/04/09 03:39:42  umangkus
 * Modified cycle detection method and cycle exception name.
 *
 * Revision 1.26  2005/04/08 19:59:26  umangkus
 * Modified cycleDetect() method.
 *
 * Revision 1.25  2005/04/08 19:51:17  umangkus
 * Modify associateActivities() and detectCycle() methods. Added import lines
 * on header.
 *
 * Revision 1.24  2005/04/05 06:34:12  tlong2
 * added associateActivities method
 *
 * Revision 1.23  2005/03/30 10:51:58  ccochran
 * back out interface
 *
 * Revision 1.21  2005/03/30 07:09:20  achou
 * Changed getActivities() to require a Comparator and return a List.
 *
 * Revision 1.20  2005/03/30 03:19:08  tlong2
 * Added default names to start and end milestones
 *
 * Revision 1.19  2005/03/29 18:56:04  ccochran
 * fixed bug in remove
 *
 * Revision 1.18  2005/03/29 08:41:48  achou
 * Commented out code that was removing the start milestone when we get/set start date.  Fixed activities.put() calls in addActivity() and updateActivity().
 *
 * Revision 1.17  2005/03/29 00:07:06  tlong2
 * Minor updates and additional logging added
 *
 * Revision 1.16  2005/03/28 18:24:36  ccochran
 * add resource service support
 *
 * Revision 1.15  2005/03/28 18:11:38  ccochran
 * removed non used imports
 *
 * Revision 1.14  2005/03/28 14:49:27  ccochran
 * Comments
 *
 * Revision 1.13  2005/03/28 14:47:09  ccochran
 * Added Logging
 *
 * Revision 1.12  2005/03/28 10:26:16  achou
 * Added getActivities() method.
 *
 * Revision 1.11  2005/03/28 08:58:33  achou
 * Added static keyword and initializer for calendarService.  Refer to activities by Map interface and initialize it in constructor.  Changed projectKey to private and added getProjectKey() method.
 *
 * Revision 1.10  2005/03/28 06:30:49  tlong2
 * added a few comments
 *
 * Revision 1.9  2005/03/28 03:50:07  tlong2
 * Completed all Stories for Iteration I
 *
 * Revision 1.8  2005/03/26 21:50:35  tlong2
 * Fixed contructor param error
 *
 * Revision 1.7  2005/03/26 21:44:11  tlong2
 * Improved Exception Handling fixed log errors
 *
 * Revision 1.6  2005/03/26 17:32:24  ccochran
 * Fixed some minor typos
 *
 * Revision 1.5  2005/03/26 16:32:36  tlong2
 * Exception Handling updates
 *
 * Revision 1.4  2005/03/26 12:01:00  ccochran
 * Log4J import
 *
 * Revision 1.3  2005/03/26 11:18:38  achou
 * Fixed typos.
 *
 * Revision 1.2  2005/03/26 10:19:29  achou
 * Fixed package designation.
 *
 * Revision 1.1  2005/03/26 09:02:35  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.7  2005/03/25 17:20:19  ccochran
 * CEC Updated Instance code
 *
 * Revision 1.6  2005/03/25 16:50:35  ccochran
 * CEC added some comment code
 *
 * Revision 1.6  2005/03/25 11:36:00  CEC
 * Added some comments
 *
 * Revision 1.5  2005/03/25 06:28:00  tlong2
 * Minor bug fixes introduced in XP session
 *
 * Revision 1.4  2005/03/24 02:34:51  tlong2
 * TL and CC XP Weds session updates
 *
 * Revision 1.3  2005/03/23 01:47:52  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.2  2005/03/22 04:31:00  dnathan
 * Updated header to include CVS ID.
 *
 * Updated footer to include CVS log.
 *
 **/
