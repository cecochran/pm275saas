/***************************************************
 * File:        $RCSfile: ActivityInterface.java,v $
 * @author      $Author: ccochran $,
 * @version     $Id: ActivityInterface.java,v 1.17 2005/05/04 12:07:18 ccochran Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:     The Activity Interface defines an activity is the element of work to be done in a Project.
 * An Activity has a unique key identifier, name, duration,
 * one or more Resources, time report.
 * Predecessors and Sucessors are Activity objects that come before
 * and after the current Activity and which the current activity is
 * dependent on.
 * </p>
 */
package pm275.core.project;

import java.util.Date;
import java.util.Collection;

import pm275.core.resource.ResourceInterface;
import pm275.core.resource.ResourceKey;
import pm275.core.resource.*;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.KeyNotFoundException;

/**
 * @author Chris
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ActivityInterface {

    /**
     *  Computes and updates this activity's start date from its dependencies.
     */
    public void computeStartDate();

    /**
     * <p>compute end date of this activity</p>
     * @param startdate
     * @param dur
     */
    public abstract void computeEndDate(Date startDate, int dur);

    /**
     * <p>Get Activity Name ...</p>
     *
     * @return Activity name
     */
    public abstract String getName();

    /**
     * <p>Set Activity Name ...</p>
     *
     * @param s Activity name
     */
    public abstract void setName(String s);

    /**
     * <p>Gets Activity start date</p>
     *
     * @return Date startDate of Activity
     */
    public abstract Date getStartDate();

    /**
     * <p>Represents ...</p>
     *
     * @param date Date that activity starts
     */
    public abstract void setStartDate(Date date);

    /**
     * <p>Gets Activity end date</p>
     *
     * @return Date endDate of Activity
     */
    public abstract Date getEndDate();

    /**
     *  Returns true if this activity has a fixed start date.
     *  @return True if activity has fixed start date, false otherwise (i.e. the
     *               start date is calculated from the dependencies).
     */
    public boolean getIsFixedStartDate();

    /**
     *  Specifies whether this activity's start date is fixed.
     *  @param isFixed True if the start date should be fixed, false otherwise
     *                 (i.e. it should be calculated from the dependencies).
     */
    public void setIsFixedStartDate(boolean isFixed);

    /**
     * <p>Gets Activity isFixedDuration</p>
     *
     * @return whether the activity is of fixed duration or now
     */
    public boolean getIsFixedDuration();

    /**
     * <p>Set the activity bIsFixedDuration</p>
     *
     * @param True if activity is of fixed duration. False if not.
     */
    public void setIsFixedDuration(boolean b);

    /**
     * <p>Gets Activity duration(in hours)</p>
     *
     * @return duration of Activity
     */
    public abstract int getDuration();

    /**
     * <p>Set the activity duration</p>
     *
     * @param dur the duration in hours
     */
    public abstract void setDuration(int dur);

    /**
     * <p>Add a Resource</p>
     *
     * @param ResourceKey resourceKey of resource to add.
     */
    public void addResource(ResourceKey resourceKey);

    /**
     *<p>Get the values of this activity resource hashtable</p>
     *
     * @return Enumator to iterate the values.
     */
    public Collection getResources();
    
    /**
     *<p>set the values of this activity resource hashtable</p>
     *
     *
     */
    public void setResources();

    /**
     *<p>Get the resource pool key
     *
     * @return ResourcePoolKey
     */
    public ResourcePoolKey  getResourcePoolKey();
    
    /**
     *<p>Get the values of this activity resource hashtable</p>
     *
     * @return Enumator to iterate the values.
     */
    public void setResourcePoolKey(ResourcePoolKey  rp);
    
    
    /**
     * <p>Removes a predeccessor from the predecessors list</p>
     *
     * @param predecessor
     */
    public void removeResource(ResourceInterface resource);

    /**
     *  Removes all the resources from the list.
     */
    public void clearResources();

    /**
     * <p>Add a preceding activity</p>
     *
     * @param predecessor ActivityKey object to add.
     */
    public abstract void addPredecessor(ActivityKey predecessor) throws InternalActivityException;

    /**
     *<p>Get the value of this activity predecessor hashtable</p>
     *
     * @return Enumator to iterate the values.
     */
    public Collection getPredecessors();

    /**
     * <p>Removes a predeccessor from the predecessors list</p>
     *
     * @param predecessor
     */
    public abstract void removePredecessor(ActivityKey predecessor);

    /**
     * <p>Add a successor activity</p>
     *
     * @param successor ActivityKey object to add.
     */
    public abstract void addSuccessor(ActivityKey successor) throws InternalActivityException;

    /**
     *<p>Get the values of this activity accessor hashtable</p>
     *
     * @return Enumator to iterate the values.
     */
    public Collection getSuccessors();

    /**
     * <p>Removes a succesive activity link to another activity</p>
     *
     *
     *
     * @param successor
     */
    public abstract void removeSuccessor(ActivityKey successor);

    /**
     * <p>Calculates slack time for a given activity</p>
     *
     *
     *
     * @return
     */
    public abstract int calculateSlackTime();


    /**
     * Delete all connections
     * to this activity from its successors and predecessors
     *
     * @throws KeyNotFoundException
     */
    public void disassociateActivity()
                throws InternalActivityException;

    public abstract ActivityKey getActivityKey();

    /** Method to set the activty
     * @param myKey The myKey to set.
     */
    public abstract void setActivityKey(ActivityKey myKey);

    /**
     * @return Returns the status.
     */
    public abstract int getStatus();

    /** Sets the activities stats per spec
     *
     NOT_STARTED = 0;
     IN_PROGRESS = 1;
     COMPLETE = 2;
     CANCELLED = 3;
     * @param status The status to set.
     */
    public abstract void setStatus(int status);
    /**
     * @return Returns the durationMeasure.
     * THIS IS ONLY USED BY THE UI!!!  THIS CLASS STORES _EVEYTHING_ IN HOURS!
     */
    public int getDurationMeasure();


    /**
     * @param durationMeasure The durationMeasure to set.
     * THIS IS ONLY USED BY THE UI!!!  THIS CLASS STORES _EVEYTHING_ IN HOURS!
     */
    public void setDurationMeasure(int durationMeasure);

    /**
     *  Method to return project key
     *  @return Returns the project key.
     */
    public ProjectKey getProjectKey();

    /**
     *  Method to set the project key.
     *  @param projectKey Project key.
     */
    public void setProjectKey(ProjectKey projectKey);

    /**
     * Check if current activity is an StartMilestone (no Predecessors)
     * @return true is current activity is StartMilestone
     */
    public boolean isStartMilestone();

    /**
     * Check if current activity is an EndMilestone (no Successors)
     * @return true is current activity is EndMilestone
     */
    public boolean isEndMilestone();

    //****** BEGIN Critcal Path Methods
    //**

    /**
     * Method to initially set (or reset after a critical path calculation has been performed)
     * all the critical path variables for the activity.
     */
    public void resetCriticalPathValues();

    /**
     * Critical Path method
     * @param
     */
    public void setEarlyStartHours(int duration);
    /**
     * Critical Path method
     * @return
     */
      public int getEarlyStartHours() ;
      /**
       * Critical Path method
       * @param duration
       */
      public void setEarlyFinishHours(int duration);
      /**
       * Critical Path method
       * @return
       */
      public int getEarlyFinishHours();
      /**
       * Critical Path method
       * @param duration
       */
      public void setLateStartHours(int duration);
      /**
       * Critical Path method
       * @return
       */
      public int getLateStartHours();
      /**
       * Critical Path method
       * @param duration
       */
      public void setLateFinishHours(int duration);
      /**
       * Critical Path method
       * @return
       */
      public int getLateFinishHours();
      /**
       * Critical Path method
       * @param value
       */
      public void setEarlyFinishFlag(boolean value);
      /**
       * Critical Path method
       * @return
       */
      public boolean getEarlyFinishFlag();
      /**
       * Critical Path method
       * @param value
       */
      public void setLateStartFlag(boolean value);
      /**
       * Critical Path method
       * @return
       */
      public boolean getLateStartFlag();

      /**
       *
       * @return slack time  in hours of current activity
       */
      public int getSlackTime();


    /**
     *  Returns the ordinal in which this activity should be displayed in the
     *  project, with 1 signifying the first activity.  THIS IS ONLY USED BY
     *  THE UI CODE.
     *  @return Display order of this activity.
     */
    public int getDisplayOrder();

    /**
     *  Sets the ordinal in which this activity should be displayed in the
     *  project, with 1 signifying the first activity.  THIS IS ONLY USED BY
     *  THE UI CODE.
     *  @param ordinal Display order for this activity.
     */
    public void setDisplayOrder(int ordinal);
}
