/*
 * Created on Apr 15, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pm275.core.project;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.io.File;
import pm275.common.exception.CycleDependencyException;
import pm275.common.exception.DuplicateKeyException;
import pm275.common.exception.InternalProjectException;
import pm275.common.exception.InternalActivityException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.PersistanceException;
import pm275.core.calendar.CalendarKey;
import pm275.core.resource.ResourcePoolKey;

/**
 * @author Chris
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ProjectInterface {
    /**
     * <p>getName()</p>
     * This function returns the name of the project.Every project
     *  have a non blank, preferably unique name set by user
     * @return String
     */
    public abstract String getName();

    /**
     * <p>setName()</p>
     * Sets the name of for a project
     * @param _name
     */
    public abstract void setName(String s);

    /**
     *  Returns this project's unique key.
     *  @return Unique key for this project.
     */
    public abstract ProjectKey getProjectKey();



    /**
     *  Returns this project's unique key.
     *  @return Unique key for this project.
     */
    public void setProjectKey(ProjectKey key);
    /**
     * <p>getStartDate.</p>
     * Start date for a project.
     *
     * @return Date s
     */
    public abstract Date getStartDate()
    // throws InternalProjectException, KeyNotFoundException
    ;

    /**
     * <p>set StartDate(Date date)</p>
     *  Sets start date for a poject
     * @param _startDate
     */
    public abstract void setStartDate(Date date)
            throws InternalProjectException //, KeyNotFoundException
    ;

    /**
     * <p>Represents end date for entire project</p>
     @return date
     */
    public abstract Date getEndDate() throws InternalProjectException,
            KeyNotFoundException;


    /**
     * <p>ResourcePoolKey is the key for the group of resource assinged to project</p>
     *
     *  * @return ResourcePoolKey
     */
    public abstract ResourcePoolKey getResourcePoolKey();

    /**
     * <p>Sets the resource pool key for a given project</p>
     * The resource pool key will be used in conjunction
     * with resource service to retrive a pool of resources
     * to assign to a project
     * @param _resourcePool
     */
    public abstract void setResourcePoolkey(ResourcePoolKey resPoolKey);

    /**
     * <p>Returns the project calendar Key</p>
     * Every project has an associated calaneder unique to the project
     * @return CalendarKey
     */
    public abstract CalendarKey getCalendarKey();

    /**
     * <p>Sets the calendarKey for the project</p>
     *
     * @param _calendar
     */
    public abstract void setCalendarKey(CalendarKey key);

    /**
     * <p>Iterates over the activites determing critical path</p>
     *  The LinkedList is that of series of activities
     *  @return LinkedList
     */
    public abstract LinkedList calculateCriticalPath();

    /**
     * <p>Detect Cycles</p>
     * A cycle is an unwanted event where one activity A depends on
     * another activity B. Then activity B depends on A.
     * This creates a cyclic dependancy. IF true a cycle exsists
     * @param Predecessor Activity Key
     * @param Successor Activity Key
     * @return boolean
     */
    public abstract boolean detectCycles(ActivityKey p, ActivityKey s);

    /**
     * <p>Associate Activities</p>
     * Associate 2 activitites
     * @param Predecessor Activity Key
     * @param Successor Activity Key
     * @return boolean
     */
    public abstract void associateActivities(ActivityKey p, ActivityKey s)
            throws KeyNotFoundException, DuplicateKeyException,
            CycleDependencyException,InternalActivityException;

    /**
     *  Adds an activity to the end of the project's activities list.
     *  @param key Unique key for the new activity.
     *  @param activity Object representing the activity.
     */
    public void addActivity(ActivityKey key, ActivityInterface activity)
            throws DuplicateKeyException, InternalProjectException;

    /**
     *  Returns the number of activities associated with this project.
     *  @return Number of activities in this project.
     */
    public int getActivitiesCount();

    /**
     *  Returns a list of activities in this project with the start and end
     *  milestones removed.  The order of the activities is determined by the
     *  specified comparator.
     *  @param comparator Comparator that determines the sorted order of the list.
     *                    If null, the activities are arbitrarily sorted.
     *  @return Sorted list of activities in this project.
     */
    public abstract List getActivities(Comparator comparator);

    /**
     *  Returns a list of activities in this project with the start and end
     *  milestones.  The order of the activities is determined by the
     *  specified comparator.
     *  @param comparator Comparator that determines the sorted order of the list.
     *                    If null, the activities are arbitrarily sorted.
     *  @param bReturnMilestones True of want to return Start\End milestones. Pass
     *                    true for critical path algorithm. False if using list in UI.
     *  @return Sorted list of activities in this project.
     */
    public abstract List getActivities(Comparator comparator, boolean bReturnMilestones);

    /**
     * <p>Get Activity</p>
     * Gets an activity from a project given the key
     *
     * @param key The Activity Key for a particular activity project
     *
     */
    public abstract Activity getActivity(ActivityKey key)
            throws KeyNotFoundException, InternalProjectException;

    /**
     * <p>Remove Activity</p>
     * Removes an activity from a project given the key
     *
     *
     * @param key The Activity Key for a particular activity project
     *
     */
    public abstract Activity removeActivity(ActivityKey key)
            throws KeyNotFoundException, InternalProjectException;

    /**
     * <p>Update Activity</p>
     * Removes an activity from a project given the key
     *
     * @param key The Activity Key for a particular activity project
     *
     */
    public abstract void updateActivity(ActivityKey key, ActivityInterface activity)
            throws InternalProjectException;

    public ActivityInterface getStartMilestone();

    public ActivityInterface getEndMilestone();

    public Map getActivities();

    public Collection getResources();

    public Collection listTodaysActivities(java.util.Calendar today) throws InternalProjectException;

    public Collection listTodaysResources(java.util.Calendar today) throws InternalProjectException;

        
  /**
   * writeProjectXML() runs through the list of Activity objects
   * that have been added to the Project and writes them out.
   * @param sToFile path and name of file to write to
   * @return iReturnValue 0 = success, 1 = failure, etc if needed
   */
    public int writeProjectXML(File fileTo) throws PersistanceException;

}

/*
 * $Log: ProjectInterface.java,v $
 * Revision 1.23  2005/05/15 23:29:10  tlong2
 * cleaned up compile warnings
 *
 * Revision 1.22  2005/05/12 20:59:10  ccochran
 * update for getting DB pred succ working
 *
 * Revision 1.21  2005/05/07 18:34:42  ccochran
 * merge ProjectAbba UI
 *
 * Revision 1.20  2005/05/07 18:02:52  tlong2
 * fixed bug from previous check in
 *
 * Revision 1.19  2005/05/07 17:11:42  tlong2
 * Added writeProjectXML
 *
 * Revision 1.18  2005/05/07 14:51:20  achou
 * Removing write/loadProjectXML() methods.  Not ready for them, method signatures need to change, and they will probably end up in PersistanceService instead.
 *
 * Revision 1.17  2005/05/07 14:23:54  ccochran
 * stubb out write DB XML
 *
 * Revision 1.16  2005/05/05 05:13:48  tlong2
 * Removed SetEndDate() as a fixed end date setter
 *
 * Revision 1.15  2005/05/03 13:48:40  ccochran
 * added setter for project key
 *
 * Revision 1.14  2005/05/02 19:40:43  bbuchana
 * Added VCT functionality: Iteration 3
 *
 * Revision 1.13  2005/04/24 20:18:13  dnathan
 * Added start and end milestone accessors...
 *
 * Revision 1.12  2005/04/18 04:18:03  achou
 * Added method to return the number of activities in the project.
 *
 * Revision 1.11  2005/04/18 03:12:05  tlong2
 * fixed addActivity error
 *
 * Revision 1.10  2005/04/18 03:06:19  tlong2
 * updated getActivities(Comparator comparator, boolean bReturnMilestones);
 *
 * Revision 1.9  2005/04/16 20:00:59  rcoutinh
 * Added InternalActivityException to associateActivities
 *
 * Revision 1.8  2005/04/16 18:29:49  achou
 * Changed updateActivity() signature to take ActivityInterface param.
 *
 */
