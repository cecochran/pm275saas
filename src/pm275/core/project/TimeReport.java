/***************************************************
 * File:        $RCSfile: TimeReport.java,v $
 * @author      $Author: tlong2 $,
 * @version     $Id: TimeReport.java,v 1.7 2005/03/30 03:36:50 tlong2 Exp $
 * Assignment:  CSCI E-275 Team 1 Project

 * Contents:
 * Purpose:
 *  This class is to help with time reporting
 *  Not in scope for V1 was expected to help 
 *  in V2
 */
package pm275.core.project;

import pm275.core.resource.ResourceKey;
import pm275.core.resource.AssignmentKey;
import org.apache.log4j.Logger;
/**
 * <p></p>
 */
public class TimeReport {
/**
 * <p></p>
 */
    private ResourceKey resourceKey;
/**
 * <p></p>
 */
    public AssignmentKey assignmentKey;
    private static final Logger log = Logger.getLogger(TimeReport.class);
    private static final String ATTR = "TimeReport";
/**
 * <p> TimeReport constructor. </p>
 *  *
 */    
public TimeReport()  {

	// Do something 
}

/**
 * <p> TimeReport constructor with ResourceKey </p>
 *  *
 */    
public TimeReport(ResourceKey rKey)  {

	setResourceKey(rKey);
}

/**
 * @return Returns the assignmentKey.
 */
public AssignmentKey getAssignmentKey() {
	return assignmentKey;
}
/**
 * @param assignmentKey The assignmentKey to set.
 */
public void setAssignmentKey(AssignmentKey assignmentKey) {
	this.assignmentKey = assignmentKey;
}
/**
 * @return Returns the resourceKey.
 */
public ResourceKey getResourceKey() {
	return resourceKey;
}
/**
 * @param resourceKey The resourceKey to set.
 */
public void setResourceKey(ResourceKey resourceKey) {
	this.resourceKey = resourceKey;
}
 }

/**
 * $Log: TimeReport.java,v $
 * Revision 1.7  2005/03/30 03:36:50  tlong2
 * Final TeamReport updates and testcases
 *
 * Revision 1.6  2005/03/29 15:53:21  ccochran
 * Added header info
 *
 * Revision 1.5  2005/03/28 17:56:38  ccochran
 * added get and setters
 *
 * Revision 1.4  2005/03/26 12:01:00  ccochran
 * Log4J import
 *
 * Revision 1.3  2005/03/26 11:19:01  achou
 * Added import statements and fixed typos.
 *
 * Revision 1.2  2005/03/26 10:19:34  achou
 * Fixed package designation.
 *
 * Revision 1.1  2005/03/26 09:02:36  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.2  2005/03/23 01:47:50  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.1  2005/03/22 04:30:32  dnathan
 * New file.
 *
 **/
