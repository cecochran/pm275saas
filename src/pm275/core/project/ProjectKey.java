/***************************************************
 * File:        $RCSfile: ProjectKey.java,v $
 * @author      $Author: rcoutinh $,
 * @version     $Id: ProjectKey.java,v 1.8 2005/05/15 20:21:34 rcoutinh Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * The Project Key is a unique representation of a project
 * Using keys allows for lazy instatation and allowing services to
 * store only the keys.The Project Key represents the HashKey value of the project
 * that uniquely identifies an Project among all other projects.
 *
 * </p>
 */
package pm275.core.project;
import org.apache.log4j.Logger;
import pm275.common.PM275BaseKey;
/**
 * <p></p>
 */
public class ProjectKey extends PM275BaseKey{
    private static final Logger log = Logger.getLogger(ProjectKey.class);
    private static final String ATTR = "ProjectKey";

	
	public ProjectKey(){
	 super();	
	}
	
    /**
     * <p> ProjectKey constructor. Set default key to Project name.
     * </p>
     * @param k Project Name
     *
     */
        public ProjectKey(String k) {
            super(k);
         }
/**
 * <p>Compare A key value to current value of this key ...</p>
 *
 * @param key ProjectKey value
 * @return True if Key matches, False if not

 */
    public boolean equals(Object k) {
        return super.equals(k);
    }

/**
 * <p>Return the value of the key as a string.</p>
 *
 * @return key  string value (Project name)
 */
    public String toString() {
    /*    StringBuffer sb = new StringBuffer("Project Key");
    	sb.append(super.getKeyId());
    	return sb.toString();
    	*/ 
      return super.toString();
    }
 }

/**
 * $Log: ProjectKey.java,v $
 * Revision 1.8  2005/05/15 20:21:34  rcoutinh
 * Corrected equals method signature
 *
 * Revision 1.7  2005/05/04 12:40:34  ccochran
 * updated dao
 *
 * Revision 1.6  2005/03/28 03:50:07  tlong2
 * Completed all Stories for Iteration I
 *
 * Revision 1.5  2005/03/26 14:27:52  ccochran
 * update key
 *
 * Revision 1.4  2005/03/26 14:24:16  ccochran
 * fixed constructor
 *
 * Revision 1.3  2005/03/26 12:01:00  ccochran
 * Log4J import
 *
 * Revision 1.2  2005/03/26 10:19:29  achou
 * Fixed package designation.
 *
 * Revision 1.1  2005/03/26 09:02:34  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.6  2005/03/26 06:35:00  ccochran
 * CEC Add comments
 *
 * Revision 1.5  2005/03/26 03:37:13  ccochran
 * CEC Comments
 *
 * Revision 1.4  2005/03/24 02:35:03  tlong2
 * TL and CC XP Weds session updates
 *
 * Revision 1.3  2005/03/23 01:47:46  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.2  2005/03/22 04:30:59  dnathan
 * Updated header to include CVS ID.
 *
 * Updated footer to include CVS log.
 *
 **/
