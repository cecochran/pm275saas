/*
 * Created on May 7, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pm275.core.project;

/***************************************************
* File:        $RCSfile: ProjectAbba.java,v $
* @author      $Author: ccochran $,
* @version     $Id: ProjectAbba.java,v 1.3 2005/05/09 12:45:16 ccochran Exp $
* Assignment:  CSCI E-275 Team 1 Project
* <p>
* Contents:
* </p>
* <p>
* Purpose:
*Data helper class for UI Dialog  that pairs key an name for a project
* * */


public class ProjectAbba {
//Short for abbreviation 
//or was it dan's love of all swedish music and 70's fashion
	 ProjectKey key = new ProjectKey();
	 String name = new String();

	public ProjectAbba(ProjectKey inkey, String inname){
		key = inkey;
		name = inname;
	}
	/**
 * @return Returns the key.
 */
public ProjectKey getKey() {
	return key;
}
/**
 * @param key The key to set.
 */
public void setKey(ProjectKey key) {
	this.key = key;
}
/**
 * @return Returns the name.
 */
public String getName() {
	return name;
}
/**
 * @param name The name to set.
 */
public void setName(String inname) {
	name = inname;
}

public String toString(){
	StringBuffer sb = new StringBuffer();
	sb.append(this.key);
	sb.append(" ");
	sb.append(name);
	sb.append("\n");
	
	return sb.toString();
	
}

}
/**
* $Log: ProjectAbba.java,v $
* Revision 1.3  2005/05/09 12:45:16  ccochran
* updated log
*
* Revision 1.6  2005/05/8 03:50:07  ccochran
* created
*
*/
