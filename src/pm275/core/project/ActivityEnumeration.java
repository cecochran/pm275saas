/*
 * Created on Mar 29, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pm275.core.project;

/***************************************************
 * File: 		$RCSfile: ActivityEnumeration.java,v $ 
 * @author 		$Author: ccochran  
 * @version 	$Id: ActivityEnumeration.java,v 1.2 2005/03/29 17:02:32 ccochran Exp $
 * Assignment:	CSCI E-275 Team 1 Project

 * Contents:
 *  Enumeration for Different activity states per spec
 * In general, transitions between states of an Activity are taken on explicit user
 * action, not on an indirect basis (e.g., just because the calculated start date of an
 * 
 */

public interface ActivityEnumeration {
	
	    // type of calendar
	    public final int NOT_STARTED = 0;
	    public final int IN_PROGRESS = 1;
	    public final int COMPLETE = 2;
	    public final int CANCELLED = 3;
	    public final int MAX= 3;
	   	
}
