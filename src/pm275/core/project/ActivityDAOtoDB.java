/*
 * Created on May 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pm275.core.project;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import org.apache.log4j.Logger;
import pm275.common.exception.DAOException;
import pm275.common.persistance.DBConnection;
import pm275.common.persistance.PersistanceService;
import pm275.gui.common.DateFormatter;

/***************************************************
 * File:        $RCSfile: ActivityDAOtoDB.java,v $
 * @author      $Author: tlong2 $,
 * @version     $Id: ActivityDAOtoDB.java,v 1.25 2005/05/16 01:01:44 tlong2 Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * In the spirt of the layering pattern, this class provides a self contained 
 * mechanism for all Activity based presistance to DBMS. Assumes a predefined
 * database structure that this class must mirror.
 * CREATE TABLE `activity` (
  `activitykey` varchar(255) NOT NULL default '',
  `projectkey` varchar(255) NOT NULL default '',
  `name` varchar(255) default NULL,
  `startdate` datetime default NULL,
  `enddate` datetime default NULL,
  `duration` int(11) default NULL,
  `slacktime` int(11) default NULL,
  `status` tinyint(4) default '0',
  `resourcepoolkey` varchar(255) default NULL,
  `fixedduration` tinyint(4) default '0',
  `fixedstartdate` tinyint(4) default '0',
  PRIMARY KEY  (`activitykey`),
  * * */
public class ActivityDAOtoDB {

//	extends  PM275DAOtoDB 


    private static final Logger log = Logger.getLogger(ActivityDAOtoDB.class);
    private static final String ATTR = "ActivityDAOtoDB";
    String sql = null;
	    
	    
    /**
     * <p>Default constructor</p>
     * 
     * 
     */
    public ActivityDAOtoDB(){
	super();
    }
		   
		  
    /**
     * <p> Loads a Activity from the database based on project key
     *     If the key doesnt exsist then it will return a null object
     * @param ProjectInterface
     */  
    public ActivityInterface get(ActivityKey aKey){
	Activity activity1 = null;
	//PersistanceService persistanceService = PersistanceService.getInstance();
	PreparedStatement pstmt= null;
	DBConnection db = null;
	DBConnection db2 = null;
	int resultCnt = -1;
	log.debug("Debug: Start Set");
				
	try{
	    db = new DBConnection();
	    db.connect();
			    
	    log.debug("Debug: Get Count");
	    StringBuffer sb= new StringBuffer();
	    sb.append("select activitykey,projectkey,name,startdate,enddate,duration,slacktime,status,resourcepoolkey,fixedduration,fixedstartdate, displayOrder from activity where activitykey = '");
	    sb.append(aKey.getKeyId());
	    sb.append("'");
	    int count = 0;
	    log.debug("Debug: Query "+ sb.toString());				 
		    	
				   
	    String activitykey;
	    String projectkey;
	    String name;
	    Date startdate;
	    Date enddate;
	    int duration;
	    int slacktime;
	    int status;
	    String resourcepoolkey;
	    int fixedduration;
	    int fixedstartdate;
	    String name_str;	   
			
	    log.debug("IN GET Activity"); 
	    		
	    //Execute statement
	    ResultSet rs = db.executeQuery(sb.toString());
	   	   
		
	    String str = null;
	    while (rs.next()) {
			   
		         	 	
	    name_str = rs.getString("name");
	    //log.debug("LOAD NAME : "+ name_str);
	    
	    activitykey = rs.getString("activitykey");
	    //log.debug("LOAD ActivityKey : "+ activitykey);
	    
		projectkey = rs.getString("projectkey");
		//log.debug("LOAD ProjectKey : "+ projectkey);
	    //projectkey = aProjectKey.getKeyId();
		
		
		startdate = rs.getDate("startdate");
		enddate = rs.getDate("enddate");
		duration = rs.getInt("duration");
		slacktime = rs.getInt("slacktime");
		status = rs.getInt("status");
		resourcepoolkey= rs.getString("resourcepoolkey");
		fixedduration = rs.getInt("fixedduration");
		fixedstartdate = rs.getInt("fixedstartdate");
						
		int displayOrder =	rs.getInt("displayorder");	
						
		activity1 = new Activity(activitykey,projectkey,name_str,
					 startdate,enddate,duration,slacktime,status,
					 resourcepoolkey,fixedduration,fixedstartdate,displayOrder);

		
		
		ProjectKey aProjectKey = new ProjectKey(projectkey);
		
	  }  
							
				
	  				
	}catch (Exception ex) {
					
	    String message = "Error set data: " + ex.getMessage();
	    log.error(message);
	}finally {
					  
	    //must clear connection upon execution
	    if(db!= null){
		db.close();
	    }
	    if(db2!=null){
		db2.close();
	    }
	    db = null;
	    db2 = null;
							  
	}
				
	return activity1;
				
    }
			
			
    /**
     * <p> Loads a project from the database based on project key
     *     If the key doesnt exsist then it will return a null object
     * @param ProjectInterface
     */  
    public HashMap getActivitiesByProject(ProjectKey aProjectKey){
	java.util.HashMap  resultMap = new java.util.HashMap();
	PersistanceService persistanceService = PersistanceService.getInstance();
	Activity activity1 = null;
	PreparedStatement pstmt= null;
	DBConnection db = null;
	DBConnection db2 = null;
	int resultCnt = -1;
					
	log.debug("ACTIVITY DAOTO IN getActivitiesByProject");
					
	try{
	    db = new DBConnection();
	    db.connect();
				    
	    log.debug("Debug: Get Count");
	    StringBuffer sb= new StringBuffer();
	    sb.append("select activitykey,projectkey,name,startdate,enddate,duration,slacktime,status,resourcepoolkey,fixedduration,fixedstartdate, displayorder from activity where projectkey = '");
	    sb.append(aProjectKey.getKeyId());
	    sb.append("'");
	    int count = 0;
	    //log.debug("Debug: Query \n"+ sb.toString());				 
			    	
					   
	    String activitykey;
	    String projectkey;
	    String name_str;
	    Date startdate;
	    Date enddate;
	    int duration;
	    int slacktime;
	    int status;
	    String resourcepoolkey;
	    int fixedduration;
	    int fixedstartdate;
		int displayorder;			                
		
	    String str = null;
	    //Execute statement
	    ResultSet rs = db.executeQuery(sb.toString());
	    
	    
	    while (rs.next()) {
	    name_str = rs.getString("name");
	    activitykey = rs.getString("activitykey");
	    projectkey = aProjectKey.getKeyId();
	    startdate = rs.getDate("startdate");
		enddate = rs.getDate("enddate");
		duration = rs.getInt("duration");
		slacktime = rs.getInt("slacktime");
		status = rs.getInt("status");
		resourcepoolkey= rs.getString("resourcepoolkey");
		fixedduration = rs.getInt("fixedduration");
		fixedstartdate = rs.getInt("fixedstartdate");
		displayorder = rs.getInt("displayorder");
	
		activity1 = new Activity(activitykey,projectkey,name_str,
					 startdate,enddate,duration,slacktime,status,
					 resourcepoolkey,fixedduration,fixedstartdate, displayorder);
		
		activity1.setActivityKey(new ActivityKey(activitykey));
		activity1.setProjectKey(new ProjectKey(projectkey));
		activity1.setName(name_str);
     	
		//add new activity to map
		resultMap.put(activitykey, activity1);
	
	    }  
	   
	    if(db!= null){
			db.close();
		    }
		    if(db2!=null){
			db2.close();
		    }
					
	}catch (Exception ex) {
						
	    String message = "Error get Activities by project: " + ex.getMessage();
	    log.error(message);
	   // log.debug(message);
	}finally {
						  
	    //must clear connection upon execution
	    if(db!= null){
		db.close();
	    }
	    if(db2!=null){
		db2.close();
	    }
	    db = null;
	    db2 = null;
								  
	}
					
	return resultMap;
					
    }
				
    /**
     * <p> Writes the project to the database  </p>
     *  If the project currently exsists then it will be updated
     *  otherwise it will be inserted
     *  Returns -1 if failed otherwise 1 for success
     * @param int  
     */
    public int set(ActivityInterface act) throws DAOException{
	PreparedStatement pstmt= null;
	DBConnection db = null;
	DBConnection db2 = null;
	int resultCnt = -1;
	
	//Base Case 
	//make sure the project is not null before the write 
	if(act != null){
	log.debug("Debug: Start Set");
	
				
	
	try{
	 db = new DBConnection();
	 db.connect();
    
	 log.debug("Debug: Get Count");
	 StringBuffer sb= new StringBuffer();
	 sb.append("select count(*) from activity where activitykey = '").append(act.getActivityKey().getKeyId()+ "'");
	 
	 int count = 0;
	 log.debug("Debug: Query "+ sb.toString());				 
	
	 //Execute statement
	 ResultSet rs = db.executeQuery(sb.toString());
 	 while (rs.next()) {
	   count = rs.getInt(1);
 	   log.debug("Debug: result: "+ count);
  	 }  
	 db.close();			
	
	 //now set up for second connection
	 db2 = new DBConnection();
     db2.connect();
     
	 if(count == 0){
	 	log.debug("Debug: Starting Insert"); 	
	 	
	 	//Key does not exsist so do insert to project table
	 	sql = new String("INSERT INTO activity (activitykey,projectkey,name,startdate,enddate,duration,slacktime,status,resourcepoolkey,fixedduration,fixedstartdate, displayorder) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");	  	  
	 	pstmt = db2.getPreparedPreparedStatement(sql);
	 	
	 	
	    //defensive coding technique to deal with nulls
	    String activitykey = new String();
	    String projectkey = new String();
	    String name = new String();
	    Date startdate = DateFormatter.getDateForString("2005-04-18");
	    Date enddate = DateFormatter.getDateForString("2005-04-18");
	    int duration = 0;
	    int slacktime =0;
	    int status = 0;
	    String resourcepoolkey = new String();
	    int fixedduration = 0;
	    int fixedstartdate =0;
	    int displayOrder = 0;
	    
		   try{
			//INSERT CASE
			activitykey = act.getActivityKey().getKeyId();
			
			if(act.getProjectKey()!=null){
			  projectkey = act.getProjectKey().getKeyId();
			}
			
			if( act.getResourcePoolKey()!=null){
				resourcepoolkey = act.getResourcePoolKey().getKeyId();
		    }
			
			name = act.getName();
			startdate = act.getStartDate();
			enddate = act.getEndDate();
					 
			duration = act.getDuration();
			slacktime = act.getSlackTime();
			status = act.getStatus();
					 
			
			if( act.getIsFixedDuration()){
			    fixedduration = 1;
			}
			if( act.getIsFixedStartDate()){
			    fixedstartdate = 1;
			}
					 
		   displayOrder = act.getDisplayOrder();
			
		   //log.debug("Insert State: "+ pstmt.toString());
		    java.sql.Date start_dt = new java.sql.Date(startdate.getTime());
			java.sql.Date end_dt = new java.sql.Date(enddate.getTime());
					    
			pstmt.setString(1, activitykey);
			pstmt.setString(2, projectkey);
			pstmt.setString(3, name);
			pstmt.setDate(4, start_dt); //start date
			pstmt.setDate(5, end_dt); //end date
			pstmt.setInt(6, duration); //duration
			pstmt.setInt(7, slacktime); //slacktim
			pstmt.setInt(8, status); //slacktim
			pstmt.setString(9, resourcepoolkey );
			pstmt.setInt(10, fixedduration);
			pstmt.setInt(11, fixedstartdate);
			pstmt.setInt(12, displayOrder);
					 
			//log.debug("CHRIS Insert State: "+ pstmt.toString());
		 
			//Execute			    
			resultCnt = pstmt.executeUpdate();
			
			pstmt.close();
	   		  
		 	//Deal with predisesscors
		 	ActivityRefDAOtoDB actrefDB = new ActivityRefDAOtoDB();
		 	actrefDB.setList(act.getActivityKey(),act.getProjectKey(),1,act.getPredecessors());
			    
		 	//Deal with successors
			//ActivityRefDAOtoDB actrefDB2 = new ActivityRefDAOtoDB();
		 	//actrefDB2.setList(act.getActivityKey(),act.getProjectKey(),0,act.getSuccessors());
		
		 	
	 	}catch (Exception ex) {
		   String message = "Error trying to do insert: " + ex.getMessage();
	       log.error(message);
	       log.debug(message);
		}finally{
		
	      db2.close();
		}
	    
	 }else{
		
	 	//UPDATE CASE 
	 	log.debug("Debug: Starting Update"); 	
		 	
		 	
	 	//Key exsists so do an update
	 	sql = new String("Update activity " +
		     "set activitykey = ?,"+
		     "projectkey = ?, " +
		     "name = ?, " +
		     "startdate =?,"+
		     "enddate= ?,"+
		     "duration= ?,"+
		     "slacktime= ?,"+
		     "status= ?,"+
		     "resourcepoolkey= ?,"+
		     "fixedduration= ?,"+
		     "fixedstartdate= ?,"+
			 "displayorder= ?"+
		     " WHERE  activitykey= ? ");    
		 				  
	 	pstmt = db2.getPreparedPreparedStatement(sql);
		 	
		    
	 	//defensive coding technique to deal with nulls
	 	String activitykey = new String();
	 	String projectkey = new String();
	 	String name = new String();
	 	Date startdate = DateFormatter.getDateForString("2005-04-18");
	 	Date enddate = DateFormatter.getDateForString("2005-04-18");
	 	int duration = 0;
	 	int slacktime =0;
	 	int status = 0;
	 	String resourcepoolkey = new String();
	 	int fixedduration = 0;
	 	int fixedstartdate =0;
	 	int displayOrder =0;	   
			   
		
	 	// 	force key 
	 	// activitykey = "FORCE";
	 	activitykey = act.getActivityKey().getKeyId();
	 	projectkey = act.getProjectKey().getKeyId();
	 	name = act.getName();
	 	startdate = act.getStartDate();
	 	enddate = act.getEndDate();
	 	java.sql.Date start_dt = new java.sql.Date(startdate.getTime());
	 	java.sql.Date end_dt = new java.sql.Date(enddate.getTime());
			
			 
	 	duration = act.getDuration();
	 	slacktime = act.getSlackTime();
	 	status = act.getStatus();
	 	resourcepoolkey = act.getResourcePoolKey().getKeyId();
	 	if( act.getIsFixedDuration()){
	 		fixedduration = 1;
	 	}
	 	if( act.getIsFixedStartDate()){
	 		fixedstartdate = 1;
	 	}
    	    
	 	displayOrder = act.getDisplayOrder();
			  
	 	// Prepare a statement to update a record
	 	pstmt.setString(1, activitykey);
	 	pstmt.setString(2, projectkey);
	 	pstmt.setString(3, name);
	 	pstmt.setDate(4, start_dt); //start date
	 	pstmt.setDate(5, end_dt); //end date
	 	pstmt.setInt(6, duration); //duration
	 	pstmt.setInt(7, slacktime); //slacktim
	 	pstmt.setInt(8, status); //slacktim
	 	pstmt.setString(9, resourcepoolkey );
	 	pstmt.setInt(10, fixedduration);
	 	pstmt.setInt(11, fixedstartdate);
	 	pstmt.setInt(12, displayOrder);
	 	pstmt.setString(13, activitykey);

				
	 	log.debug("Update State: "+ pstmt.toString());					  
				
	 	//Execture sql check result count
	 	resultCnt = pstmt.executeUpdate();
		   		  
	 	//Deal with predisesscors
	 	ActivityRefDAOtoDB actrefDB = new ActivityRefDAOtoDB();
	 	actrefDB.setList(act.getActivityKey(),act.getProjectKey(),1,act.getPredecessors());
		    
		    
	 	//Deal with successors
	 	//ActivityRefDAOtoDB actrefDB2 = new ActivityRefDAOtoDB();
	 	//actrefDB2.setList(act.getActivityKey(),act.getProjectKey(),0,act.getSuccessors());
		    
	 } 


	}catch (Exception ex) {
		String message = "Error trying to do update: " + ex.getMessage();
		   
		log.error(message);
	}finally {
			  
		//must clear connection upon execution
		if(db!= null){
			db.close();
		}
		if(db2!=null){
			db2.close();
		}
		db = null;
		db2 = null;
					  
	}
	}

	return resultCnt;
    }
			
    /**
     * <p>Deletes project from database based on key</p>
     * Returns -1 if failed otherwise 1 for success
     * @param PM275BaseKey pKey
     */
    public int delete (ActivityKey pKey) throws DAOException{
	PreparedStatement pstmt= null;
	DBConnection db = null;
	int resultCnt = -1;
	int count;
				
	//Base case: make sure key is not null
	if(pKey != null){
	    log.debug("Debug: Start Delete");
				
	    try{
		db = new DBConnection();
		db.connect();
			   			 
		//Prepare a statement to de a record
		sql = new String("delete from activity where activitykey = ?");	  
		pstmt = db.getPreparedPreparedStatement(sql);
		pstmt.setString(1, pKey.getKeyId());
				 			 	
		//check result count
		resultCnt = pstmt.executeUpdate();
				 
	    }catch (Exception ex) {
					
		String message = "Error trying to delete: " + ex.getMessage();
		log.debug(message);
		log.error(message);
	    }finally {
					  
		//must clear connection upon execution
		if(db!= null){
		    db.close();
		}
				 
		db = null;
				 
							  
	    }
	}
				
	return resultCnt;
    }
				
			
			
    /**
     * <p>Returns string</p>
     * 
     * @param string
     */
    public String toString(){
	return sql.toString();
    }	
			
			
}

/**
 * $Log: ActivityDAOtoDB.java,v $
 * Revision 1.25  2005/05/16 01:01:44  tlong2
 * cleaned up compile warnings
 *
 * Revision 1.24  2005/05/15 21:59:10  rcoutinh
 * Pred load is moved out to ProjectService.promoteActivites
 *
 * Revision 1.23  2005/05/15 20:40:29  ccochran
 * updated
 *
 * Revision 1.21  2005/05/14 22:42:51  ccochran
 * code to get order counts, Needs db change to activity table
 *
 * Revision 1.20  2005/05/14 06:03:38  ccochran
 * update for set resources
 *
 * Revision 1.19  2005/05/13 18:46:04  ccochran
 * cleaned up dangling integration points to other services
 *
 * Revision 1.18  2005/05/12 20:59:10  ccochran
 * update for getting DB pred succ working
 *
 * Revision 1.17  2005/05/10 21:57:53  ccochran
 * updated marshaling for project
 *
 * Revision 1.16  2005/05/10 20:07:46  ccochran
 * removed system outs
 *
 * Revision 1.15  2005/05/09 21:44:29  ccochran
 * updated addref
 *
 * Revision 1.14  2005/05/09 20:09:54  ccochran
 * cleaned up system outs
 *
 * Revision 1.13  2005/05/09 18:35:15  ccochran
 * removed to string
 *
 * Revision 1.12  2005/05/09 18:21:00  ccochran
 * testing
 *
 * Revision 1.11  2005/05/09 16:28:53  ccochran
 * patch for ui continued
 *
 * Revision 1.10  2005/05/09 13:08:59  ccochran
 * fixed comment
 *
 * Revision 1.9  2005/05/08 06:09:54  vchumako
 * Added load/save assignments
 *
 * Revision 1.8  2005/05/07 03:43:07  ccochran
 * added successor case
 *
 * Revision 1.7  2005/05/07 03:41:48  ccochran
 * added code to save succesors and predisessors
 *
 * Revision 1.6  2005/05/06 23:52:03  ccochran
 * fixed log name
 *
 * Revision 1.5  2005/05/06 23:35:56  ccochran
 * cleaneup to string, fixed bug in load activities
 *
 * Revision 1.4  2005/05/06 18:48:39  ccochran
 * got update  and insert  to work
 *
 * Revision 1.3  2005/05/04 12:09:01  ccochran
 * insert, update, delete skeleton
 *
 * Revision 1.2  2005/05/04 09:41:34  ccochran
 * added consturctor
 *
 * Revision 1.1  2005/05/04 05:54:29  ccochran
 * added footers
 *
 *
 * Updated footer to include CVS log.
 *
 **/
