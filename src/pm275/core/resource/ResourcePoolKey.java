/***************************************************
 * File:        $RCSfile: ResourcePoolKey.java,v $
 * @author      $Author: rcoutinh $,
 * @version     $Id: ResourcePoolKey.java,v 1.6 2005/05/15 20:21:17 rcoutinh Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents: A unique key for a resource pool.
 * </p>
 * <p>
 * Purpose:
 * The Reource Pool Key is a unique representation of a resource Pool
 * Using keys allows for lazy instatation and allowing services to
 * store only the keys.The Resource Pool Key represents the HashKey value of the project
 * that uniquely identifies a Resource Pool among all other resource pools.
 *
 * </p>
 */
package pm275.core.resource;
import org.apache.log4j.Logger;
import pm275.common.PM275BaseKey;

/**
 * <p></p> 
 */
public class ResourcePoolKey extends PM275BaseKey {
	
/**
 * <p>Represents a logging utility object.</p>
 */
	  private static final Logger log = Logger.getLogger( ResourcePoolKey.class );
	  private static final String ATTR = "ResourcePoolKey";
	  
	  
/**
 * Default constructor initializing a Resource Pool Key object.
 */  
	  public ResourcePoolKey(){
	  	super();
	  }

/**
 * Constructor initializing a Resource Pool Key object with a set key value.
 */
	  public ResourcePoolKey(String str){
	  	super(str);
	  }
/**
 * <p> Compares two keys equality </p>
 * 
 * @return true if 2 keys are equal, otherwise false
 * @param resource pool key
 */
    public boolean equals(Object key) {
        
    	return super.equals(key);
    }

/**
 * <p>Returns the string represntation of the key</p>
 *
 * @return String representation of key
 */
    public String toString() {
          return super.toString();
    }
 }

/**
 * $Log: ResourcePoolKey.java,v $
 * Revision 1.6  2005/05/15 20:21:17  rcoutinh
 * Corrected equals method signature
 *
 * Revision 1.5  2005/03/30 01:45:51  bbuchana
 * Added comments.
 *
 * Revision 1.4  2005/03/26 17:05:15  ccochran
 * Switched to using exted base key
 *
 * Revision 1.3  2005/03/26 12:13:59  ccochran
 * Cleanup CEC
 *
 * Revision 1.2  2005/03/26 10:20:45  achou
 * Fixed package designation.
 *
 * Revision 1.1  2005/03/26 09:02:30  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.5  2005/03/26 04:48:49  ccochran
 * Added comment
 *
 * Revision 1.4  2005/03/26 04:20:54  ccochran
 * CEC just added comment
 *
 * Revision 1.3  2005/03/25 14:56:46  ccochran
 * added some key code
 *
 * Revision 1.2  2005/03/23 01:47:49  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.1  2005/03/22 04:30:31  dnathan
 * New file.
 *
 **/
