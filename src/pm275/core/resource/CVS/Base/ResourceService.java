/***************************************************
 * File:        $RCSfile: ResourceService.java,v $
 * @author      $Author: tlong2 $,
 * @version     $Id: ResourceService.java,v 1.37 2005/05/14 22:19:32 tlong2 Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents: Resource managment operations and logic.
 * </p>
 * <p>
 * Purpose:
 * The Resource Service is responsible for managing an individual person which
 * we define as a resource, and a collection of people which we define as a
 * resource pool. This service will manage all the business logic for resources; like computing 
 * resource utilization. The Resource Service is a singleton, meaning only one per VM.
 * </p>
 */

package pm275.core.resource;

import org.apache.log4j.Logger;

import pm275.common.exception.InternalProjectException;
import pm275.common.exception.KeyNotFoundException;
import pm275.common.exception.ResourceException;
import pm275.core.project.ActivityInterface;
import pm275.core.project.ProjectInterface;
import pm275.core.project.ProjectKey;
import pm275.core.project.ProjectService;
import pm275.core.project.ActivityKey;
import pm275.core.project.Activity;
import pm275.core.calendar.Calendar;
import pm275.core.calendar.CalendarKey;
import pm275.core.calendar.CalendarService;
import pm275.common.persistance.*;


import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * <p></p>
 */
public class ResourceService {

/**
 * <p>Represents a logging utility object.</p>
 */
	  private static final Logger log = Logger.getLogger( ResourceService.class );
	  private static final String ATTR = "ResourceService";
/**
 * <p>Represents a ResourceService object.</p>
 */
    private static ResourceService resourceService;

/**
 * <p>Represents all resources</p>
 */
    private java.util.Map resources = new java.util.TreeMap();

/**
 * <p>Represents all assignments</p>
 */
    private java.util.Map assignments = new java.util.TreeMap();

/**
 * <p>Resource pool</p>
 *
 */
    private java.util.Map resourcePool = new java.util.TreeMap();

/**
 * <p></p>
 *
 * @poseidon-type TimeReport
 */
    public java.util.Collection timeReport = new java.util.TreeSet();


    
/**
 * <p>Returns the singlton instance for the Resource service.</p>
 *
 * @return ResourceService Instance
 */
    public static final ResourceService getInstance() {
    	if ( resourceService == null ) {
	          resourceService= new ResourceService();
		}
		return resourceService;
    }

/**
 * <p>Private constructor for the ResourceService class.</p>
 *
 * @return ResourceService Instance
 */
	private ResourceService(){
	}

/**
 * <p>Adds one new resource pool</p>
 *
 *@param none
 * @return void
 */
    public void addResourcePool(ResourcePool rp) throws ResourceException
    {
    	try
    	{
			resourcePool.put(rp.getResourcePoolKey(), rp);
    	}
		catch(Exception e)
		{
			log.error("ERROR - ResourceService: Could not add a ResourcePool to the pool with key " + rp.getResourcePoolKey());
			throw new ResourceException("ERROR - ResourceService: Could not create a new Resourse Pool");
		}
    }

/**
 * <p>Removes resource pool by key</p>
 *
 * @param resource pool key
 * @return void
 */
    public void removeResourcePool(ResourcePoolKey poolKey) throws ResourceException
    {
	try
	    {
		resourcePool.remove(poolKey);
	    }
	catch(Exception e)
	    {
		if(poolKey == null)
		    {
			log.error("ERROR - ResourceService: Could not remove a ResourcePool from the pool.  The  poolKey was null.");
			throw new ResourceException("ERROR - ResourceService: Could not remove a ResourcePool from the pool.  The  poolKey was null.");
		    }
		else
		    {
			log.error("ERROR - ResourceService: Could not remove a ResourcePool from the pool with key " + poolKey);
			throw new ResourceException("ERROR - ResourceService: Could not remove a ResourcePool from the pool with key " + poolKey);
		    }
	    }
    }
    
    /**
     * <p>Gets resource pool by key</p>
     *
     * @param resource pool key
     * @return resource pool
     */
    public ResourcePool getResourcePool(ResourcePoolKey poolKey) throws ResourceException
    {
    	try
	    {
    		if (poolKey != null)
		    return (ResourcePool) resourcePool.get(poolKey);
		else
		    return null;
	    }
    	catch(Exception e)
	    {
		e.printStackTrace();
		log.error("ERROR - ResourceService: Could retrieve a ResourcePool from the pool with key " + poolKey);
		throw new ResourceException("ERROR - ResourceService: Could not retrieve a ResourcePool from the pool with key " + poolKey);
	    }
    }
    
    /**
     * <p>Adds a ResourcePool to the list.</p>
     *
     * @param resource pool
     * @return void
     */
    public void setResourcePool(ResourcePool rp) throws ResourceException
    {
	try
	    {
		resourcePool.put(rp.getResourcePoolKey(), rp);
	    }
	catch(Exception e)
	    {
		if(rp == null)
		    {
			log.error("ERROR - ResourceService: Could not add a ResourcePool to the pool.  The new pool was null.");
			throw new ResourceException("ERROR: Could not remove resource from pool The new pool was null");
		    }
		else
		    {
			log.error("ERROR - ResourceService: Could not add a ResourcePool to the pool with key " + rp.getResourcePoolKey());
			throw new ResourceException("ERROR - ResourceService: Could not add a ResourcePool to the pool with key " + rp.getResourcePoolKey());
		    }
	    }
    }
    
    /**
     * <p>Returns an iterator to resourcesPools </p>
     *
     *@param none
     * @return Collection of resource pools
     */
    public Collection listResourcesPools() throws ResourceException
    {
      	try
	    {
      		return Collections.unmodifiableCollection(resourcePool.values());
	    }
      	catch(Exception e)
	    {
      		log.error("ERROR - ResourceService: Could not list ResourcePools.");
      		throw new ResourceException("ERROR - ResourceService: Could not list ResourcePools.");
	    }
    }
    
    /**
     * <p>Removes resource by key</p>
     *
     * @param resource key
     * @return void
     */
    public void removeResource(ResourceKey key) throws ResourceException
    {
	try
	    {
		resources.remove(key);
	    }
	catch(Exception e)
	    {
		if(key == null)
		    {
			log.error("ERROR - ResourceService: Could not remove a Resource from the pool.  The key was null.");
			throw new ResourceException("ERROR - ResourceService: Could not remove a Resource from the pool.  The key was null.");
		    }
		else
		    {
			log.error("ResourceService: Could not remove a Resource from the pool with key " + key + " Exception: " + e.getMessage());
			throw new ResourceException("ResourceService: Could not remove a Resource from the pool with key " + key);
		    }
	    }
	log.debug("Resource with key '" + key + "' was removed.");
    }
    
    /**
     * <p>Gets resource by key</p>
     *
     * @return resource
     * @param resource key
     */
    public ResourceInterface getResource(ResourceKey key) throws ResourceException
    {
	try
	    {
		if (key != null)
		    return (ResourceInterface) resources.get(key);
		else
		    return null;
	    }
	catch(Exception e)
	    {
		log.error("ERROR - ResourceService: Could retrieve a Resource from the pool with key " + key);
		throw new ResourceException("ERROR - ResourceService: Could not remove a Resource from the pool with key " + key);
	    }
    }
    
    
    public HashMap getResourcesDB(ProjectKey projKey, ActivityKey actKey){
      HashMap resMap = new HashMap();
      ResourceDAOtoDB resDB = new ResourceDAOtoDB();
      
      resMap  = resDB.getResourceAssignments(projKey,actKey);
      
      
      //Make sure the resources are add to the service cache
      resources.putAll(resMap);
        		      
      return resMap;
      
    }
    /**
     * <p>Adds a Resource to the ResourcePool.</p>
     *
     * @param resource
     * @return void
     */
    public void setResource(ResourceInterface resource) throws ResourceException
    {
	try
	    {
		resources.put(resource.getKey(), resource);
	    }
	catch(Exception e)
	    {
		if(resource == null)
		    {
			log.error("ERROR - ResourceService: Could not add a Resource to the resource pool.  The new resource was null.");
			throw new ResourceException("ERROR - ResourceService: Could not add a Resource to the resource pool.  The new resource was null.");
		    }
		else
		    {
			log.error("ERROR - ResourceService: Could not add a Resource to the resource pool with key " + resource.getKey());
			throw new ResourceException("ERROR - ResourceService: Could not add a Resource to the resource pool with key " + resource.getKey());
		    }
	    }
	log.debug("Resource '" + resource + "' was added to the sevice.");
    }
    
    
    /**
     * <p>Returns an iterator to resources</p>
     *
     * @param none
     * @return collection of resources
     */
    public Collection listResources() throws ResourceException
    {
	try
	    {
		return Collections.unmodifiableCollection(resources.values());
	    }
	catch(Exception e)
	    {
		log.error("ERROR - ResourceService: Could not list Resources.");
		throw new ResourceException("ERROR - ResourceService: Could not list Resources.");
	    }
    }
    /**
     * <p>Returns an iterator to assignments</p>
     *
     * @param none
     * @return collection of assignments
     */
    public Collection listAllAssignments() throws ResourceException
    {
	try
	    {
		return Collections.unmodifiableCollection(assignments.values());
	    }
	catch(Exception e)
	    {
		log.error("ERROR - ResourceService: Could not list AllAssignments.");
		throw new ResourceException("ERROR - ResourceService: Could not list AllAssignments.");
	    }
    }
    
    /**
     * <p>Returns an iterator to assignments</p>
     *
     * @param none
     * @return collection of assignments
     */
    public AssignmentInterface getAssignment(ResourceKey rKey, ActivityKey aKey) throws ResourceException
											
    {
	if(rKey == null)
	    {
		String msg = "ResourceService: listAssignments:,ResourceKey cannot be null"; 
		log.error(msg);
		throw new ResourceException(msg);
	    }
	if( aKey == null)
	    {
		String msg = "ResourceService: listAssignments:,ActivityKey cannot be null"; 
		log.error(msg);
		throw new ResourceException(msg);
		
	    }
	Iterator iterator = assignments.keySet().iterator();
	while(iterator.hasNext())
	    {
		Assignment assignment = (Assignment)assignments.get(iterator.next());
		if(aKey.equals(assignment.getActivityKey()))
		    {
			if(rKey.equals(assignment.getResource()))
			    {
				return assignment;
			    }
		    }
	    }
	return null;
    }
    
    /**
     * <p>Returns an iterator to assignments</p>
     *
     * @param none
     * @return an assignment
     */
    public AssignmentInterface getAssignment(AssignmentKey aKey) throws ResourceException
    {
	return (AssignmentInterface) this.assignments.get(aKey);
    }
    /**
     * <p>Assigns a resource to a particular activity at defined level</p>
     *
     * @param resource
     * @param activity
     * @param project
     * @param percent
     */
    public void assign(ResourceKey resourceKey, ActivityKey activityKey, ProjectKey pKey, double percent)
	throws ResourceException
    {
	AssignmentInterface assignment =new Assignment(activityKey, resourceKey);
	assignment.setPercent(percent);
	assignments.put(assignment.getAssignmentKey(),assignment);
	ProjectService ps = ProjectService.getInstance();
	
	try {
	    Activity thisActivity = ps.getActivity(pKey,activityKey);
	    //update activity end dates
	    thisActivity.addResource(resourceKey);
	} catch (KeyNotFoundException e) {
	    String msg = "ResourceService: assign, activity not found: "; 
	    log.error(msg,e);
	    throw new ResourceException(msg);
	    
	} catch (InternalProjectException e) {
	    String msg = "ResourceService: assign,internal project error: "; 
	    log.error(msg,e);
	    throw new ResourceException(msg);
	    
	}
	
	ResourceInterface res = this.getResource(resourceKey);
	res.assign( assignment.getAssignmentKey() );
	
	log.debug("ResourceKey '" + resourceKey + "' was assigned to '" 
		  + activityKey + "' activityKey");
	
    }
    /**
     * <p>Assigns a calendar (key) to a particular resource</p>
     *
     * @param resourceKey key indicating the resource
     * @param calendarKey key indicating the calendar
     */	    
    public void assignCalendar(ResourceKey resourceKey, CalendarKey calendarKey) {
	try {
	    
	    ResourceInterface resource = getResource(resourceKey);
	    resource.setCalendarKey(calendarKey);
	} catch (ResourceException re) {
	    String msg = "EXCEPTION: ResourceService.assignCalendar: "; 
	    log.error(msg,re);
	    
	}
	
    }
    
    /**
     * <p>Removes a resource from a particular activity at defined level</p>
     *
     * @param resource
     * @param activity
     * @param project
     */
    public void deassignAll(ActivityKey activityKey, ProjectKey pKey)
	throws ResourceException
    {
	ProjectService ps = ProjectService.getInstance();
	try {
	    Activity thisActivity = ps.getActivity(pKey,activityKey);
	    //update activity end dates
	    Collection resources = thisActivity.getResources();
	    Iterator iterator = resources.iterator();
	    while(iterator.hasNext())
		{
		    ResourceInterface rIfc = (ResourceInterface)iterator.next();
		    ResourceKey rKey = rIfc.getKey();
		    AssignmentInterface aIfc = getAssignment(rKey,activityKey);
		    if(aIfc == null)
			{
			    String msg = "ResourceService,deassign: Cannot find asssignment for Resource : " + rKey 
				+ "  and activity " + activityKey;
			    log.error(msg);
			    throw new ResourceException(msg);
			}
		    assignments.remove(aIfc.getAssignmentKey());
		    
		}
	    thisActivity.clearResources();
	    
	} catch (KeyNotFoundException e) {
	    String msg = "ResourceService: assign, activity not found: "; 
	    log.error(msg,e);
	    throw new ResourceException(msg);
	    
	} catch (InternalProjectException e) {
	    String msg = "ResourceService: assign,internal project error: "; 
	    log.error(msg,e);
	    throw new ResourceException(msg);
	    
	}			
    }
    
    /**
     * <p>Removes a resource from a particular activity at defined level</p>
     *
     * @param resource
     * @param activity
     * @param project
     */
    public void deassign(ResourceKey resourceKey, ActivityKey activityKey, ProjectKey pKey)
	throws ResourceException
    {
	AssignmentInterface assignment = getAssignment(resourceKey,activityKey);
	if(assignment == null)
	    {
		String msg = "ResourceService,deassign: Cannot find asssignment for Resource : " + resourceKey 
		    + "  and activity " + activityKey;
		log.error(msg);
		throw new ResourceException(msg);
	    }
	assignments.remove(assignment.getAssignmentKey());
	ProjectService ps = ProjectService.getInstance();
	
	try {
	    Activity thisActivity = ps.getActivity(pKey,activityKey);
	    //update activity end dates
	    thisActivity.removeResource(getResource(resourceKey));
	} catch (KeyNotFoundException e) {
	    String msg = "ResourceService: assign, activity not found: "; 
	    log.error(msg,e);
	    throw new ResourceException(msg);
	    
	} catch (InternalProjectException e) {
	    String msg = "ResourceService: assign,internal project error: "; 
	    log.error(msg,e);
	    throw new ResourceException(msg);
	    
	}
	
	log.debug("ResourceKey '" + resourceKey + "' was assigned to '" 
		  + activityKey + "' activityKey");
	
    }		
    /**
     * creates empty resource Pool and sets it for later use
     * @return
     * @throws ResourceException
     */
    public ResourcePool createResourcePool() throws ResourceException {
    	ResourcePool rp = new ResourcePool();
    	setResourcePool(rp);
    	return rp;
    }
    
    /**
     * @param line - first and last name/description of the resource
     * @return
     * @throws ResourceException
     */
    public Resource createResource(String line) throws ResourceException {
    	Resource res = new Resource(line);
    	setResource(res);
    	return res;
    }
    
    /**
     * @param key - resource key
     * @param line - first and last name/description of the resource
     * @return
     * @throws ResourceException
     */
    public Resource createResource(String key, String line) 
    	throws ResourceException {
	
    	
	    if(resources.get(new ResourceKey(key)) != null) {
	    	Resource res = (Resource)resources.get(new ResourceKey(key));
	    	// log.warn("Resource with this key already exists: " + key);
		    // throw new ResourceException("Resource with this key already exists: " + key);
	    	return res;
		} else {
	    	Resource res = new Resource(key, line);
	    	setResource(res);
	    	return res;
	    }
    
    }
    
    public double calculateHoursWorkedToday(ProjectInterface project, Resource thisResource, java.util.Calendar today) throws ResourceException 
    {
	try 
	    {
		double hoursWorkedToday = 0.0;
		Collection todaysActivities = project.listTodaysActivities(today); 
		CalendarKey calendarKey = project.getCalendarKey();
		Calendar calendar = CalendarService.getInstance().getCalendar(calendarKey);
		int hoursToday = calendar.getHoursPerDay();
		
		Iterator activityIterator = todaysActivities.iterator();
		while (activityIterator.hasNext())
		    {
			ActivityInterface activity = (ActivityInterface)activityIterator.next();
			Collection assignedResources = activity.getResources();
			Iterator resourceIterator = assignedResources.iterator();
			while(resourceIterator.hasNext())
			    {
				Resource resource = (Resource)resourceIterator.next();
				if (resource.getKey() == thisResource.getKey())
				    {
					AssignmentInterface assignment = ResourceService.getInstance().getAssignment(resource.getKey(), activity.getActivityKey());
					double todayTotal = (assignment.getPercent() * hoursToday);
					hoursWorkedToday = todayTotal + hoursWorkedToday;
				    }
			    }
		    }
		
		if (hoursWorkedToday > 0)
		    {
			String value = String.valueOf(hoursWorkedToday);
			int length = value.length();
			value = String.valueOf(value.subSequence(0, length-4 ));
			hoursWorkedToday = Double.parseDouble(value);
		    }
		
		return hoursWorkedToday;
	    } 
	catch (Exception e) 
	    {
		log.error("Error: Project.listTodaysResources - Failed to list todays resources.");
		throw new ResourceException("Error: Project.listTodaysResources - Failed to calculate today's resources.");
	    }
    }

    /**
     * <p>Method to return all activities of a resource.</p>
     *
     * @param rKey resource key
     * @return Collection list of activity keys
     */
    public Collection listActivities(ResourceKey rKey)
	throws ResourceException
    {
	if (rKey == null) {
	    String msg = "ResourceService: listActivities:,ResourceKey cannot be null"; 
	    log.error(msg);
	    throw new ResourceException(msg);
	}

	Collection ret_activities = new java.util.Vector();

	Iterator iterator = assignments.keySet().iterator();
	while(iterator.hasNext())
	    {
		Assignment assignment = 
		    (Assignment)assignments.get(iterator.next());
		
		if(rKey.equals(assignment.getResource()))
		    {
			ret_activities.add(assignment.getActivityKey()); 
		    }
	    }
	
	return ret_activities;
    }    
    
    
    public void loadDB(ProjectInterface proj){
    
    	PersistanceService persistanceService = PersistanceService.getInstance();  
    	
    	//Get Resources
    	//persistanceService.loadResourcesFromDBMS();
    	
    	ResourceDAOtoDB resDB = new ResourceDAOtoDB();
    	ArrayList resList = resDB.getAllResources();
    	
    	//Get Assignments
    	java.util.Iterator resIt =  resList.iterator();
        //cycle thru all the Activities
         while (resIt.hasNext()) {
             Resource res = (Resource) resIt.next();
             resources.put(res.getKey(),res);  
         }
    	
    	
   
    	//Get Assignments
    	java.util.Iterator actIt =  proj.getActivities().keySet().iterator();
        //cycle thru all the Activities
         while (actIt.hasNext()) {
             ActivityKey actKey = (ActivityKey) actIt.next();
             persistanceService.loadAssignmentsFromDBMS(actKey, proj.getProjectKey());  
         }
         
        
    }
    
    
    public void writeDB(ProjectInterface proj){
        	
        	ResourceDAOtoDB resDB = new ResourceDAOtoDB();
        	resDB.setList(resources.values());
        	
        	//Save Assignments
    	    PersistanceService persistanceService = PersistanceService.getInstance();
        	java.util.Iterator actIt =  proj.getActivities().keySet().iterator();
            //cycle thru all the Activities
             while (actIt.hasNext()) {
                 ActivityKey actKey = (ActivityKey) actIt.next();
                 persistanceService.saveAssignmentsToDBMS(actKey);  
             }
        	
    }
    
}

/**
 * $Log: ResourceService.java,v $
 * Revision 1.37  2005/05/14 22:19:32  tlong2
 * Removed println
 *
 * Revision 1.36  2005/05/14 06:40:00  ccochran
 * update for intergrating resources
 *
 * Revision 1.35  2005/05/14 06:03:37  ccochran
 * update for set resources
 *
 * Revision 1.34  2005/05/14 01:16:39  ccochran
 * *** empty log message ***
 *
 * Revision 1.33  2005/05/14 01:08:07  ccochran
 * added means to get resources via project and activity
 *
 * Revision 1.32  2005/05/13 22:54:25  ccochran
 * save assignments
 *
 * Revision 1.31  2005/05/13 21:00:51  ccochran
 * *** empty log message ***
 *
 * Revision 1.30  2005/05/13 20:20:13  ccochran
 * refactored the save to DB
 *
 * Revision 1.29  2005/05/13 18:02:53  ccochran
 * Refactored DB interaction to be owned by individual services invokeing presistance layer
 *
 * Revision 1.28  2005/05/09 18:34:21  tlong2
 * Fixed bug in createResource that would throw exception if dup found, now it returns the Resource instead
 *
 * Revision 1.27  2005/05/04 08:23:36  umangkus
 * Added method listActivities(ResourceKey) to get assignments of a resource
 *
 * Revision 1.26  2005/05/04 02:38:36  dnathan
 * Fixed bug where assignments were never told to Resources.
 *
 * Added helper method to simply get an assignment from a key.
 *
 * Revision 1.25  2005/05/02 23:52:34  vchumako
 * Added method to create Resource with specified Key.
 *
 * Revision 1.24  2005/05/02 19:58:43  bbuchana
 * Added VCT functionality: Iteration 3
 *
 * Revision 1.23  2005/05/02 02:30:50  tlong2
 * Added support for assigning a calendar to a resource
 *
 * Revision 1.22  2005/04/30 15:42:44  rcoutinh
 * Added method deassignAll
 *
 * Revision 1.21  2005/04/27 03:13:47  rcoutinh
 * Added method deassign  and getAssignment
 *
 * Revision 1.20  2005/04/27 02:02:10  rcoutinh
 * ResourceService.assign now accepts ProjectKey
 *
 * Revision 1.19  2005/04/18 06:39:30  tlong2
 * Addedd listAllAssignments for getting all resources assigned to individual activities.
 *
 * Revision 1.18  2005/04/17 23:25:25  vchumako
 * Implemented comparable for PM275BaseKey.
 * Now it's not required to use getKeyId() in order to put or remove keys from the Map.
 *
 * Revision 1.17  2005/04/17 20:05:32  vchumako
 * fix resource pool access
 *
 * Revision 1.16  2005/04/17 02:51:16  vchumako
 * Added some debug logging
 *
 * Revision 1.15  2005/04/17 02:20:27  vchumako
 * add createResourcePool() and createResource() methods to the ResourceService.
 * They should be used from now on to create Resources.
 *
 * Revision 1.14  2005/04/16 02:10:40  dnathan
 * Now all Maps are indexed by a String, not an Object.
 *
 * Revision 1.13  2005/04/16 01:24:30  dnathan
 * Corrected error text.
 *
 * Revision 1.12  2005/04/13 04:27:02  bbuchana
 * Changed the add ResourcePool to allow for flat file loading functionality
 *
 * Revision 1.11  2005/04/10 21:31:00  vchumako
 * Added Resource Interface
 *
 * Revision 1.10  2005/04/10 21:18:35  vchumako
 * Added AssignmentInterface whenever it's possible
 *
 * Revision 1.9  2005/03/30 01:47:28  bbuchana
 * Added comments.
 *
 * Revision 1.8  2005/03/29 22:50:52  bbuchana
 * Fixed defect.
 *
 * Revision 1.7  2005/03/29 21:45:22  bbuchana
 * Added code from my local to CVS.
 *
 * Revision 1.6  2005/03/29 13:15:27  ccochran
 * Removed Operation 168 generator bug, switched List methods to follow generic collection paradigm
 *
 * Revision 1.5  2005/03/28 01:23:37  vchumako
 * Implemented most of the functionality, still need to revisit assign methods
 *
 * Revision 1.4  2005/03/26 12:13:59  ccochran
 * Cleanup CEC
 *
 * Revision 1.3  2005/03/26 11:19:01  achou
 * Added import statements and fixed typos.
 *
 * Revision 1.2  2005/03/26 10:20:45  achou
 * Fixed package designation.
 *
 * Revision 1.1  2005/03/26 09:02:29  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.4  2005/03/26 04:58:05  ccochran
 * CEC Add comments
 *
 * Revision 1.3  2005/03/25 17:27:05  ccochran
 * CEC Added code for singleton instance
 *
 * Revision 1.2  2005/03/23 01:47:49  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.1  2005/03/22 04:30:31  dnathan
 * New file.
 *
 **/
