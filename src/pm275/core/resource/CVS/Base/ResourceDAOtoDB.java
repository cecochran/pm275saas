/*
 * Created on May 13, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pm275.core.resource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import org.apache.log4j.Logger;




import pm275.common.PM275BaseKey;
import pm275.common.exception.DAOException;
import pm275.common.persistance.DBConnection;
import pm275.gui.common.DateFormatter;
import pm275.core.project.*;

/***************************************************
* File:        $RCSfile: ResourceDAOtoDB.java,v $
* @author      $Author: ccochran $,
* @version     $Id: ResourceDAOtoDB.java,v 1.12 2005/05/15 01:15:30 ccochran Exp $
* Assignment:  CSCI E-275 Team 1 Project
* <p>
* Contents:
* </p>
* <p>
* Purpose:
* In the spirt of the layering pattern, this class provides a self contained 
* mechanism for all ActivityRef based presistance to DBMS. Assumes a predefined
* database structure that this class must mirror.
* 
CREATE TABLE `resource` (
  `ID` varchar(64) NOT NULL default '',
  `first` varchar(32) default NULL,
  `last` varchar(32) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
* * */


public class ResourceDAOtoDB {

		private static final Logger log = Logger.getLogger(ResourceDAOtoDB.class);
	    private static final String ATTR = "ResourceDAOtoDB";
	    String sql = null;
	    
		
	    
	    /**
	     * <p>Default constructor</p>
	     * 
	     * 
	     */
		  public ResourceDAOtoDB(){
		      super();
		    }
		   
		  
		  /**
		   * <p> Loads a project from the database based on project key
		   *     If the key doesnt exsist then it will return a null object
		   * @param ProjectInterface
		   */  
			public Resource get(ResourceKey resKey){
		        //ArrayList resultList = new ArrayList();
                Resource res = null;
                
				PreparedStatement pstmt= null;
				DBConnection db = null;
				DBConnection db2 = null;
				int resultCnt = -1;
				
				log.debug("Debug: Start Set");
				
				try{
				 db = new DBConnection();
				 db.connect();
			    
				 log.debug("Debug: Get Count");
		    	 StringBuffer sb= new StringBuffer();
		    	 //Prepare a statement to de a record
				 sql = new String("select ID, name from resource where ID = ? ");	  
	  		 	 pstmt = db.getPreparedPreparedStatement(sql);
				 pstmt.setString(1, resKey.getKeyId());

	     	     String id;
				 String first;
				 String last;
				   			   
				 //Execute statement
				 ResultSet rs = pstmt.executeQuery();
	         	 while (rs.next()) {
		     	 	
	         	 	/*activitykey = rs.getString("activitykey");
					projectkey = rs.getString("projectKey");
					predflag = rs.getInt("predflag");
					value = rs.getString("value");
					*/
	         	 	
					id = rs.getString(1);
					first = rs.getString(2);
					last = rs.getString(3);
						
					StringBuffer full = new StringBuffer(first);
					full.append(" ").append(last);
					
					 res = new Resource(id,full.toString());
	          	 }  
				
				}catch (Exception ex) {
				  	
				   String message = "Error building Resource  : " + resKey.toString() + ex.getMessage();
			       log.error(message);
			       log.debug(message); 
				}finally {
					  
				 //must clear connection upon execution
				  if(db!= null){
				    db.close();
				  }
				  if(db2!=null){
				    db2.close();
				  }
				  db = null;
				  db2 = null;
							  
				}
				
				return res;
				
			}
			
			
			
			  /**
			   * <p> Loads a project from the database based on project key
			   *     If the key doesnt exsist then it will return a null object
			   * @param ProjectInterface
			   */  
				public ArrayList getAllResources(){
					
					ArrayList resultList = new ArrayList();
	                Resource res = null;
	                
					PreparedStatement pstmt= null;
					DBConnection db = null;
					DBConnection db2 = null;
					int resultCnt = -1;
					
					log.debug("Debug: Start Set");
					
					try{
					 db = new DBConnection();
					 db.connect();
				    
					 log.debug("Debug: Get Count");
			    	 StringBuffer sb= new StringBuffer();
			    	 //Prepare a statement to de a record
					 sql = new String("select ID,first,last from resource");	  
		  		 	 pstmt = db.getPreparedPreparedStatement(sql);
					   
		  		 	String id;
					 String first;
					 String last;
					   			   
					 //Execute statement
					 ResultSet rs = pstmt.executeQuery();
		         	 while (rs.next()) {
			     	 	
		         	 	id = rs.getString(1);
						first = rs.getString(2);
						last = rs.getString(3);
							
						StringBuffer full = new StringBuffer(first);
						full.append(" ").append(last);
						
						 res = new Resource(id,full.toString());
						 resultList.add(res);
		          	 }
					
					}catch (Exception ex) {
					  	
					   String message = "Error loading allResources  : "  + ex.getMessage();
				       log.error(message);
				       log.debug(message); 
					}finally {
						  
					 //must clear connection upon execution
					  if(db!= null){
					    db.close();
					  }
					  if(db2!=null){
					    db2.close();
					  }
					  db = null;
					  db2 = null;
								  
					}
					
					return resultList;
					
				
					
				}
			
			public int setList(Collection inList ){
			   int cnt = 1;
				 
				   //get iterator
			        java.util.Iterator it =  inList.iterator();

			       try{ 
			        //cycle thru all the Activities
			        while (it.hasNext()) {
			        	Resource res = (Resource) it.next();
				    	set(res);
			            cnt++;
			        }
			       }catch(Exception ex){
			        String message = "Error trying to do insert: " + ex.getMessage();
				       log.error(message);	
			       }
			return cnt;
			}
			
			
			/**
			 * <p> Writes the project to the database  </p>
			 *  If the project currently exsists then it will be updated
			 *  otherwise it will be inserted
			 *  Returns -1 if failed otherwise 1 for success
			 * @param int  
			 */
	public int set(Resource res) throws DAOException
	{
				PreparedStatement pstmt= null;
				DBConnection db = null;
				DBConnection db2 = null;
				DBConnection db3 = null;
				int resultCnt = -1;
				int count = 0;
				
				//Base Case 
				//make sure the project is not null before the write 
      if(res != null){
				
				 
			try {
			
				 db = new DBConnection();
				 db.connect();
				 	
				 String sb  = new String("select count(*) from resource where ID = ? ");
				 pstmt = db.getPreparedPreparedStatement(sb);
				 pstmt.setString(1, res.getKey().getKeyId());
				 ResultSet rs = pstmt.executeQuery();
	         	 while (rs.next()) {
		    	   count = rs.getInt(1);
	          	 }  
			     db.close();		
			 	 db = null;
			 	 
			}catch(Exception ex){
				String message = "Error trying to do update Resource: " + res.toString() + ex.getMessage();
			 	log.error(message);
				return -1;
			}
			    
			
			
		   if(count == 0){
				 	System.out.println("Debug: Starting Insert"); 	
	 
				    //defensive coding technique to deal with nulls
				 	String idkey = new String();
					String first = new String();
					String last = new String();
			
				 	try{
				
				 	//now set up for second connection
					db2 = new DBConnection();
					db2.connect();
					 
			    	//Key does not exsist so do insert to project table
					String sql2 = new String("INSERT resource VALUES( ?, ?, ?)");	  
					PreparedStatement pstmt2 = db2.getPreparedPreparedStatement(sql2);
				 		
				     //INSERT CASE
				     idkey = res.getKey().getKeyId();
				     first = res.getFirstName();
					 last = res.getLastName();
					    
					 pstmt2.setString(1, idkey);
					 pstmt2.setString(2, first);
					 pstmt2.setString(3, last);
				     resultCnt = pstmt2.executeUpdate();
				     
				     db2.close();
				     db2 = null;
				     
				     
				     
				 	}catch (Exception ex) {
					   String message = "Error trying to do Insert under set Resource: "+ res.toString()+ ex.getMessage();
				       log.error(message);
				       log.debug(message);
					} finally{
						if(db2!=null) db2.close();
					}
				 	
				 	
   	     }else{
   	
	  	 	 try{
				
	
   	   			//now set up for second connection
   	     		db3 = new DBConnection();
 	     		db3.connect();
   	     	
   	    		//Key exsists so do an update
 	 			String sql3 = new String("update resource " +
 	 					 "set ID = ?,"+
 	 					 "first  = ?, " +
 	 					 "last   = ?"+
 	 					 " WHERE ID = ? ");    
	 				  
 	 				PreparedStatement pstmt3 = db3.getPreparedPreparedStatement(sql3);
	 	
	    			//defensive coding technique to deal with nulls
	    			String idkey = new String();
		   			String first = new String();
		   			String last = new String();
						 		
		   			//INSERT CASE
		   			idkey = res.getKey().getKeyId();
	     			first = res.getFirstName();
		 			last = res.getLastName();
	     
		 			//Prepare a statement to update a record
			    	pstmt3.setString(1, idkey);
			    	pstmt3.setString(2, first);
			    	pstmt3.setString(3, last); //start date
			    	pstmt3.setString(4, idkey); //start date
			    	
			    	System.out.println(pstmt3.toString());
			    	
	     			//Execture sql check result count
	     			resultCnt = pstmt3.executeUpdate();
	     			
	     			db3.close();
	     			db3 = null;
   	     	
			    
				 }catch (Exception ex) {
				 	String message = "Error trying to do update Resource: " + res.toString() + ex.getMessage();
				 	log.error(message);
				 }finally {
					if(db3!=null)
				    db2.close();
				  
				  db3 = null;
				 
				 } //finally
			 	
   	     } //else
		} //null
      
	return resultCnt;			
}
				
			
			/**
			 * <p>Deletes project from database based on key</p>
			 * Returns -1 if failed otherwise 1 for success
			 * @param PM275BaseKey pKey
			 */
			public int delete (ResourceKey resKey) throws DAOException{
				PreparedStatement pstmt= null;
				DBConnection db = null;
			    int resultCnt = -1;
				int count;
				
				//Base case: make sure key is not null
				if(resKey != null){
				log.debug("Debug: Start Delete");
				
				try{
				 db = new DBConnection();
				 db.connect();
			   			 
				 //Prepare a statement to de a record
				 sql = new String("delete from resource where id = ? ");	  
	  		 	 pstmt = db.getPreparedPreparedStatement(sql);
				 pstmt.setString(1, resKey.getKeyId());
				 
				 //check result count
				 resultCnt = pstmt.executeUpdate();
				 
				}catch (Exception ex) {
					
				   String message = "Resource: Error trying to delete: " + ex.getMessage();
			       log.debug(message);
				   log.error(message);
				}finally {
					  
				 //must clear connection upon execution
				  if(db!= null){
				    db.close();
				  }
				 
				  db = null;
				 
							  
				}
				}
				
				return resultCnt;
			}
				
			public HashMap getResourceAssignments(ProjectKey projKey,ActivityKey actKey){
				HashMap resMap = new HashMap();
				
				
		        //ArrayList resultList = new ArrayList();
                Resource res = null;
                
				PreparedStatement pstmt= null;
				DBConnection db = null;
				DBConnection db2 = null;
				int resultCnt = -1;
				
				log.debug("Debug: Start Set");
				
				try{
				 db = new DBConnection();
				 db.connect();
			    
				 log.debug("Debug: Get Count");
		    	 StringBuffer sb= new StringBuffer();
		    	 //Prepare a statement to de a record
				 sql = new String("select r.id,r.first, r.last,a.activityKey" +
				 		" from resource r, assignment a" +
				 		"  where a.resourceKey = r.ID and a.activitykey =  ?");
				 pstmt = db.getPreparedPreparedStatement(sql);
				 pstmt.setString(1, actKey.getKeyId());

	     	     String id;
				 String first;
				 String last;
				   			   
				 //Execute statement
				 ResultSet rs = pstmt.executeQuery();
	         	 while (rs.next()) {
					id = rs.getString(1);
					first = rs.getString(2);
					last = rs.getString(3);
						
					StringBuffer full = new StringBuffer(first);
					full.append(" ").append(last);
                     
					ResourceKey resKey = new ResourceKey(id);					
					res = new Resource(id,full.toString());
					resMap.put(resKey, res);
					
	          	 }  
				
				}catch (Exception ex) {
				  	
				   String message = "Error building Resource from Activity  for ActivityKey:  " + actKey.toString() + ex.getMessage();
			       log.error(message);
			       log.debug(message); 
				}finally {
					  
				 //must clear connection upon execution
				  if(db!= null){
				    db.close();
				  }
				  if(db2!=null){
				    db2.close();
				  }
				  db = null;
				  db2 = null;
							  
				}
				
				
				
				return resMap;
				
			}
			
			/**
			 * <p>Returns string</p>
			 * 
			 * @param string
			 */
			public String toString(){
				return "ResourceRefDAOtoDB";
			}	
			
			
			
	}

	/**
	
	* Revision 1.1  2005/05/04 05:54:29  ccochran
	* added footers
	*
	*
	* Updated footer to include CVS log.
	*
	**/

