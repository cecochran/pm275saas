/***************************************************
 * File:        $RCSfile: Resource.java,v $
 * @author      $Author: ccochran $,
 * @version     $Id: Resource.java,v 1.21 2005/05/13 22:11:41 ccochran Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * A resource is a core abastraction of PM-275 that represts a person
 * A resource can exsist outside of any project or assigment.
 * </p>
 */
package pm275.core.resource;

import org.apache.log4j.Logger;
import pm275.common.exception.ResourceException;
import pm275.common.persistance.ProjectVisitorInterface;
import pm275.core.calendar.CalendarKey;
import pm275.core.calendar.CalendarService;
/**
 * <p></p>
 */
public class Resource implements ResourceInterface {
    private static final Logger log = Logger.getLogger( Resource.class );
    private static final String ATTR = "Resource";
/**
 * <p>The name of the individual person, first and Last</p>
 */
    private String name = new String();

/**
 * <p>The key is a unique identifier for a resource</p>
 */
    private ResourceKey key;
    private CalendarKey calendarKey;


/**
 * <p>A collection of task assigments that this resource is allocated too</p>
 */
    private java.util.Collection assignments;

/**
 * default constructor
 * initializes a Resource object
 */
    protected Resource(String name) {
    	if(name != null){
    		this.name = name;
    	}else{
    		log.error("ERROR Resource Constructor name blank");
    	}
	
	this.key = new ResourceKey();

    	this.assignments = new java.util.ArrayList();
    	
    	CalendarService cs = CalendarService.getInstance();
	
    	/* TODO: use corporate calendar by default for now 
    	this.setCalendarKey(cs.getCalendar(this.getKey()).getKey());
	*/
    	this.setCalendarKey(cs.getCalendar((ResourceKey)null).getKey());

    }
    			
/**
 * default constructor
 * initializes a Resource object
 */
    public Resource(String key, String name) {
    	if(name != null){
    		this.name = name;
    	}else{
    		log.error("ERROR Resource Constructor name blank");
    	}
	
	this.key = new ResourceKey(key);

    	this.assignments = new java.util.ArrayList();
    	
    	CalendarService cs = CalendarService.getInstance();
	
    	/* TODO: use corporate calendar by default for now 
    	this.setCalendarKey(cs.getCalendar(this.getKey()).getKey());
	*/
    	this.setCalendarKey(cs.getCalendar((ResourceKey)null).getKey());
    }
    			
/**
 * <p>The name of the resource, first and last</p>
 *
 *
 *
 * @return String
 */
    public String getName() {
        return name;
    }
    
    
    public String getFirstName(){
    	int index = this.name.lastIndexOf(' ');
		String fName = null;
		
    	if(index >= 0)
	    {
			fName = name.substring(0, name.lastIndexOf(' '));
		}
		return fName.trim();
		
    }

    public String getLastName(){
    	
		String lName = "";
		
		int index = this.name.lastIndexOf(' ');
    	if(index >= 0)
    	{
    		lName = name.substring(index);
			
		}
		return lName.trim();
    }
    
/**
 * <p>Sets the Name for a given resource</p>
 * Ideally this would be unique but not enforced
 * The key will provide true uniqueness
 *
 * @param _name to be set
 */
    public void setName(String name) {
    	if(name != null){
    			this.name = name;
    		}else{
    			log.error("ERROR set name blank");
    		}
    }

/**
 * <p>The unique representation of a resource</p>
 *
 * @return resource key
 */
    public ResourceKey getKey() {
        return key;
    }

/**
 * <p>Assigns a key </p>
 *
 *
 *
 * @param key
 */
    public void assign(AssignmentKey key) {
    	if(key != null){
    		assignments.add(key);
	    }else{
	    	log.error("ERROR assign key NULL");
	    }
    }
    /**
     * <p>The key of the calendar assigned to the resource</p>
     *
     * @return calendar key
     */
    public CalendarKey getCalendarKey() {
    	return calendarKey;
    }
    
    
    /**
     * <p>Set the key of the calendar assigned to the resource</p>
     *
     * @param resource key
     */
    public void setCalendarKey(CalendarKey calKey) {
    	calendarKey = calKey;
    }
/**
 * <p>Removes an Assigment</p>
 *
 *
 *
 * @param key
 */
    public void deassign(AssignmentKey key) throws ResourceException
    {
	
		try 
		{
			if(key != null){
				assignments.remove(key);
			}else{
				log.error("ERROR deassign key NULL");
 			}
		} 
		catch(Exception e)
		{
			// Log Exception 
			log.error("ERROR: Failed to remove resouce from as assignment with key:  " + key);
			// make client aware that this operation could not be performed.
			throw new ResourceException("Failed to remove resouce from as assignment with key:  " + key);
		}
    }

/**
 * <p>Allows for easy access to see what assigments a resource has
 * been allocated too </p>
 *
 *
 *
 * @return list of Assigments
 */
    public java.util.Iterator listAssignments() {
	return assignments.iterator();
    }
    
    /**
     * <p>Helps determine number assigments</p>
     *
     *
     *
     * @return int number of Assigments
     */
    public int assignmentsSize(){
    	return assignments.size();
    }

    public String toString() {
    	
    	StringBuffer sb = new StringBuffer();
    	
    	sb.append("ID:").append(this.key);
    	sb.append(" First:").append(this.getFirstName());
    	sb.append(" Last:").append(this.getLastName());
    	sb.append("\n");
    	
    	return sb.toString();
    }
    
    /*
     * Trying out the visitor pattern for writing to XML
     * @param ProjectVisitorInterface visitor instance of visitor
     *  to gather informatoin about this class
     */
    public void acceptVisitor(ProjectVisitorInterface visitor) {
    	visitor.visitResource(this);
    }
    
 }

/**
 * $Log: Resource.java,v $
 * Revision 1.21  2005/05/13 22:11:41  ccochran
 * trims for get names
 *
 * Revision 1.20  2005/05/13 21:41:36  ccochran
 * fixed to string
 *
 * Revision 1.19  2005/05/13 21:33:30  ccochran
 * update to string
 *
 * Revision 1.18  2005/05/13 20:00:52  ccochran
 * added get first name last name
 *
 * Revision 1.17  2005/05/04 07:39:01  umangkus
 * Try modified but undone it.
 *
 * Revision 1.16  2005/05/02 23:48:56  vchumako
 * Added constructor to create resource with specified key.
 *
 * Revision 1.15  2005/05/02 18:02:32  vchumako
 * Fixed null pointer exception. Have to create the calendar for the resource
 * first, or use default/corporate by default. Now using corporate one.
 *
 * Revision 1.14  2005/05/02 01:20:40  tlong2
 * Addedd support for default calendar
 *
 * Revision 1.13  2005/04/29 01:10:30  tlong2
 * Added visitor pattern updates acceptVisitor methods, etc
 *
 * Revision 1.12  2005/04/17 03:22:23  vchumako
 * Added extra logging
 *
 * Revision 1.11  2005/04/17 02:51:17  vchumako
 * Added some debug logging
 *
 * Revision 1.10  2005/04/10 21:26:33  vchumako
 * Added Resource Interface
 *
 * Revision 1.9  2005/03/29 21:40:31  bbuchana
 * Added code from my local to CVS:  logic plus exception handling.
 *
 * Revision 1.8  2005/03/29 18:01:47  ccochran
 * Added size method
 *
 * Revision 1.7  2005/03/29 16:09:32  ccochran
 * Added some log4J errors and increases defensive coding
 *
 * Revision 1.6  2005/03/27 21:59:43  vchumako
 * Changed Hashtable to ArrayList.
 * Implemented assign, deassign, listAssignments.
 *
 * Revision 1.5  2005/03/26 11:45:15  ccochran
 * Log4j
 *
 * Revision 1.4  2005/03/26 11:27:00  ccochran
 * Log4j
 *
 * Revision 1.3  2005/03/26 11:18:38  achou
 * Fixed typos.
 *
 * Revision 1.2  2005/03/26 10:20:44  achou
 * Fixed package designation.
 *
 * Revision 1.1  2005/03/26 09:02:29  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.3  2005/03/26 06:31:16  ccochran
 * Added Comments
 *
 * Revision 1.2  2005/03/23 01:47:50  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.1  2005/03/22 04:30:31  dnathan
 * New file.
 *
 **/
