/***************************************************
 * File:        $RCSfile: AssignmentKey.java,v $
 * @author      $Author: rcoutinh $,
 * @version     $Id: AssignmentKey.java,v 1.10 2005/05/15 20:20:48 rcoutinh Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * Assignment Key is the unique key for all assignments 
 * </p>
 */
package pm275.core.resource;

import org.apache.log4j.Logger;
import pm275.common.PM275BaseKey;

/**
 * <p></p>
 */
public class AssignmentKey extends PM275BaseKey{
    private static final Logger log = Logger.getLogger( AssignmentKey.class );
    private static final String ATTR = "AssignmentKey";
    private String key;

/** default constructor
 *  use base unique id if no name provided
 */
    public AssignmentKey() {
      super();
    }

/** constructor
 *  use base unique id plus name provided
 */
    public AssignmentKey(String name) {
      super(name);
    }


/**
 * <p>Does ...</p>
 *
 *
 *
 * @return
 * @param key
 */
    public boolean equals(Object key) {
              return super.equals(key);
    }

/**
 * <p>Does ...</p>
 *
 *
 *
 * @return
 */
    public String toString() {
        return super.toString();
    }
 }

/**
 * $Log: AssignmentKey.java,v $
 * Revision 1.10  2005/05/15 20:20:48  rcoutinh
 * Corrected equals method signature
 *
 * Revision 1.9  2005/03/28 18:09:29  ccochran
 * Add class purpose
 *
 * Revision 1.8  2005/03/27 21:58:22  vchumako
 * Don't have to add a name to the key
 *
 * Revision 1.7  2005/03/26 19:34:17  vchumako
 * No need to import core.* classes
 *
 * Revision 1.6  2005/03/26 19:01:54  vchumako
 * Imports and package names fixed
 *
 * Revision 1.5  2005/03/26 18:30:51  vchumako
 * Added named constructor
 *
 * Revision 1.4  2005/03/26 17:05:15  ccochran
 * Switched to using exted base key
 *
 * Revision 1.3  2005/03/26 11:09:59  ccochran
 * Log4j
 *
 * Revision 1.2  2005/03/26 10:20:44  achou
 * Fixed package designation.
 *
 * Revision 1.1  2005/03/26 09:02:28  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.1  2005/03/26 04:18:14  vchumako
 * Initial revision AssignmentKey object
 *
 * Revision 1.2  2005/03/23 01:47:49  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.1  2005/03/22 04:30:30  dnathan
 * New file.
 *
 **/
