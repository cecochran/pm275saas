/*
 * File: $RCSfile: ResourceInterface.java,v $
 * @author $Author: tlong2 $,
 * @version $Id: ResourceInterface.java,v 1.2 2005/05/02 01:24:15 tlong2 Exp $
 * Assignment: CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * A resource is a core abastraction of PM-275 that represts a person or 
 * other resource(such as an equipment, etc)
 * A resource can exsist outside of any project or assigment.
 * </p>
 */
package pm275.core.resource;

import pm275.common.exception.ResourceException;
import pm275.common.persistance.ProjectVisitorInterface;
import pm275.core.calendar.CalendarKey;

/**
 * @author vc
 *
 * Allows to inquire for basic information 
 * and get a list of working assignments
 */
public interface ResourceInterface {
	/**
	 * <p>The name of the resource, first and last</p>
	 *
	 *
	 *
	 * @return String
	 */
	public abstract String getName();

	/**
	 * <p>Sets the Name for a given resource</p>
	 * Ideally this would be unique but not enforced
	 * The key will provide true uniqueness
	 *
	 * @param _name to be set
	 */
	public abstract void setName(String name);

	/**
	 * <p>The unique represtation of a resource</p>
	 *
	 *
	 *
	 * @return
	 */
	public abstract ResourceKey getKey();

	/**
	 * <p>Assigns a key </p>
	 *
	 *
	 *
	 * @param key
	 */
	public abstract void assign(AssignmentKey key);

	/**
	 * <p>Removes an Assigment</p>
	 *
	 *
	 *
	 * @param key
	 */
	public abstract void deassign(AssignmentKey key) throws ResourceException;

	/**
	 * <p>Allows for easy access to see what assigments a resource has
	 * been allocated too </p>
	 *
	 *
	 *
	 * @return list of Assigments
	 */
	public abstract java.util.Iterator listAssignments();

	/**
	 * <p>Helps determine number assigments</p>
	 *
	 *
	 *
	 * @return int number of Assigments
	 */
	public abstract int assignmentsSize();
	
	
    /**
     * <p>The key of the calendar assigned to the resource</p>
     *
     * @return calendar key
     */
    public CalendarKey getCalendarKey(); 
    
    /**
     * <p>Set the key of the calendar assigned to the resource</p>
     *
     * @param resource key
     */
    public void setCalendarKey(CalendarKey calKey);
   
    /*
     * The visitor pattern for writing to XML & DB
     * @param ProjectVisitorInterface visitor instance of visitor
     *  to gather informatoin about this class
     */
    public void acceptVisitor(ProjectVisitorInterface visitor);
	/**
	* $Log: ResourceInterface.java,v $
	* Revision 1.2  2005/05/02 01:24:15  tlong2
	* updated interface methods
	*
	* Revision 1.1  2005/04/10 21:26:33  vchumako
	* Added Resource Interface
	*
	*
	**/
}