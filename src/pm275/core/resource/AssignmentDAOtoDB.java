/*
 * Created on May 14, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package pm275.core.resource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import pm275.common.persistance.*;
import pm275.common.exception.DAOException;
import pm275.common.persistance.DBConnection;
import pm275.core.project.*;

/***************************************************
 * File:        $RCSfile: AssignmentDAOtoDB.java,v $
 * @author      $Author: tlong2 $,
 * @version     $Id: AssignmentDAOtoDB.java,v 1.5 2005/05/16 01:01:43 tlong2 Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * In the spirt of the layering pattern, this class provides a self contained
 * mechanism for all ActivityRef based presistance to DBMS. Assumes a predefined
 * database structure that this class must mirror.
 *
 CREATE TABLE `resource` (
 `ID` varchar(64) NOT NULL default '',
 `first` varchar(32) default NULL,
 `last` varchar(32) default NULL,
 PRIMARY KEY  (`ID`)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1
 * * */

public class AssignmentDAOtoDB {


	private static final Logger log = Logger.getLogger(AssignmentDAOtoDB.class);
	private static final String ATTR = "AssignmentDAOtoDB";
	String sql = null;



	/**
	 * <p>Default constructor</p>
	 *
	 *
	 */
	public AssignmentDAOtoDB() {
		super();
	}


	/**
	 * <p> Loads a project from the database based on project key
	 *     If the key doesnt exsist then it will return a null object
	 * @param ProjectInterface
	 */
	public Assignment get(AssignmentKey assKey){
		//ArrayList resultList = new ArrayList();
		Assignment ass = null;

		PreparedStatement pstmt= null;
		DBConnection db = null;
		DBConnection db2 = null;
		int resultCnt = -1;

		log.debug("Debug: Start Set");

		try{
			db = new DBConnection();
			db.connect();

			log.debug("Debug: Get Count");
			StringBuffer sb= new StringBuffer();
			//Prepare a statement to de a record
			sql = new String("select ID  from assignment where ID = ? ");
			pstmt = db.getPreparedPreparedStatement(sql);
			pstmt.setString(1, assKey.getKeyId());
			//defensive coding technique to deal with nulls
			String idkey = new String();
			String resourceKey = new String();
			String activityKey = new String();
			double effort = 0;


			//Execute statement
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {


				idkey = rs.getString(1);
				resourceKey = rs.getString(2);
				activityKey = rs.getString(3);
				effort = rs.getDouble(4);



				ass = new Assignment(idkey,new ActivityKey(activityKey),
						new ResourceKey(resourceKey),effort);
			}

		}catch (Exception ex) {

			String message = "Error building Resource  : " + assKey.toString() + ex.getMessage();
			log.error(message);
			log.debug(message);
		}finally {

			//must clear connection upon execution
			if(db!= null){
				db.close();
			}
			if(db2!=null){
				db2.close();
			}
			db = null;
			db2 = null;

		}

		return ass;

	}




	public int setList(Collection inList ){


		int cnt = 1;

		//get iterator
		java.util.Iterator it =  inList.iterator();

		try{
			//cycle thru all the Activities
			while (it.hasNext()) {
				Assignment ass = (Assignment) it.next();
				set(ass);
				cnt++;
			}
		}catch(Exception ex){
			String message = "Error trying to do insert: " + ex.getMessage();
			log.error(message);
		}
		return cnt;
	}



	public ArrayList loadAssignments (ActivityKey actKey,ProjectKey  projKey){
		ArrayList list = new ArrayList();

		PreparedStatement pstmt= null;
		DBConnection db = null;
		DBConnection db2 = null;
		int resultCnt = -1;

		log.debug("Debug: Start Set");

		try{
			db = new DBConnection();
			db.connect();

			pstmt = db.getPreparedPreparedStatement(PM275DBSchema.SELECT_ASSIGNMENT_BY_AKEY);
			pstmt.setString(1, actKey.getKeyId());

			ResultSet rs = pstmt.executeQuery();
			while(rs.next()){


				ResourceKey rKey = new ResourceKey(rs.getString(PM275DBSchema.ASSIGNMENT_RKEY));
				Assignment assignment = new Assignment (rs.getString(PM275DBSchema.ASSIGNMENT_ID),
						actKey,rKey,
						rs.getDouble(PM275DBSchema.ASSIGNMENT_EFFORT));

				list.add(assignment);





			}
		} catch(Exception sqle) {
			log.error("Can't load assignments from DB for: " + actKey);
		} finally {
			try {
				if( db != null) {
					db.close();
				}
			} catch(Exception e) {
			}
		}




		return list;


	}


	/**
	 * <p> Writes the project to the database  </p>
	 *  If the project currently exsists then it will be updated
	 *  otherwise it will be inserted
	 *  Returns -1 if failed otherwise 1 for success
	 * @param int
	 */
	public int set(Assignment ass) throws DAOException
	{
		PreparedStatement pstmt= null;
		DBConnection db = null;
		DBConnection db2 = null;
		DBConnection db3 = null;
		int resultCnt = -1;
		int count = 0;

		//Base Case
		//make sure the project is not null before the write
		if(ass != null && ass.getAssignmentKey() != null){


			try {

				db = new DBConnection();
				db.connect();

				String sb  = new String("select count(*) from assignment where ID = ? ");
				pstmt = db.getPreparedPreparedStatement(sb);
				pstmt.setString(1, ass.getAssignmentKey().getKeyId());
				ResultSet rs = pstmt.executeQuery();
				while (rs.next()) {
					count = rs.getInt(1);
				}
				db.close();
				db = null;

			}catch(Exception ex){
				String message = "Error trying to get count from assignment : " + ass.toString() + ex.getMessage();
				log.error(message);
				return -1;
			}



			if(count == 0){
				//defensive coding technique to deal with nulls
				String idkey = new String();
				String resourceKey = new String();
				String activityKey = new String();
				double effort = 0;

				try{

					//now set up for second connection
					db2 = new DBConnection();
					db2.connect();

					//Key does not exsist so do insert to project table
					String sql2 = new String("insert assignment (id,resourceKey,activityKey,effort ) VALUES( ?, ?, ?, ?)");
					PreparedStatement pstmt2 = db2.getPreparedPreparedStatement(sql2);

					//INSERT CASE
					idkey = ass.getAssignmentKey().getKeyId();
					resourceKey = ass.getResource().getKeyId();
					activityKey = ass.getActivityKey().getKeyId();
					effort  = ass.getPercent();

					pstmt2.setString(1, idkey);
					pstmt2.setString(2, resourceKey);
					pstmt2.setString(3, activityKey);
					pstmt2.setDouble(4, effort);
					resultCnt = pstmt2.executeUpdate();

					db2.close();
					db2 = null;



				}catch (Exception ex) {
					String message = "Error trying to do Insert under set Resource: "+ ass.toString()+ ex.getMessage();
					log.error(message);
					log.debug(message);
				} finally{
					if(db2!=null) db2.close();
				}


			}else{

				try{


					//now set up for second connection
					db3 = new DBConnection();
					db3.connect();

					//Key exsists so do an update
					String sql3 = new String("update resource " +
							"set ID = ?,"+
							"  resourceKey = ?, " +
							" activityKey   = ?,"+
							" effort   = ?"+
					" WHERE ID = ? ");

					PreparedStatement pstmt3 = db3.getPreparedPreparedStatement(sql3);

					//defensive coding technique to deal with nulls
					String idkey = new String();
					String resourceKey = new String();
					String activityKey = new String();
					double effort = 0;


					//INSERT CASE
					idkey = ass.getAssignmentKey().getKeyId();
					resourceKey = ass.getResource().getKeyId();
					activityKey = ass.getActivityKey().getKeyId();
					effort  = ass.getPercent();

					pstmt3.setString(1, idkey);
					pstmt3.setString(2, resourceKey);
					pstmt3.setString(3, activityKey);
					pstmt3.setDouble(4, effort);
					pstmt3.setString(5, idkey);

					//Execture sql check result count
					resultCnt = pstmt3.executeUpdate();

					db3.close();
					db3 = null;


				}catch (Exception ex) {
					String message = "Error trying to do update Assignment: " + ass.toString() + ex.getMessage();
					log.error(message);
				}finally {
					if(db3!=null)
						db2.close();

					db3 = null;

				} //finally

			} //else
		} //null

		return resultCnt;
	}


	/**
	 * <p>Deletes project from database based on key</p>
	 * Returns -1 if failed otherwise 1 for success
	 * @param PM275BaseKey pKey
	 */
	public int delete (AssignmentKey assKey) throws DAOException{
		PreparedStatement pstmt= null;
		DBConnection db = null;
		int resultCnt = -1;
		int count;

		//Base case: make sure key is not null
		if(assKey != null){
			log.debug("Debug: Start Delete");

			try{
				db = new DBConnection();
				db.connect();

				//Prepare a statement to de a record
				sql = new String("delete from assignment where id = ? ");
				pstmt = db.getPreparedPreparedStatement(sql);
				pstmt.setString(1, assKey.getKeyId());

				//check result count
				resultCnt = pstmt.executeUpdate();

			}catch (Exception ex) {

				String message = "Resource: Error trying to delete: " + ex.getMessage();
				log.debug(message);
				log.error(message);
			}finally {

				//must clear connection upon execution
				if(db!= null){
					db.close();
				}

				db = null;


			}
		}

		return resultCnt;
	}


	/**
	 * <p>Returns string</p>
	 *
	 * @param string
	 */
	public String toString(){
		return "AssignmentDAOtoDB";
	}



}

/**

 * Revision 1.1  2005/05/04 05:54:29  ccochran
 * added footers
 *
 *
 * Updated footer to include CVS log.
 *
 **/

