/*
 * File: $RCSfile: AssignmentInterface.java,v $
 * @author $Author: vchumako $,
 * @version $Id: AssignmentInterface.java,v 1.3 2005/05/08 06:12:43 vchumako Exp $
 * Assignment: CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 *  An Assigment associates an activity and a resource
 *  at a certain percentage. This is critical in calculating
 *  the amount of work required to finish a core activity
 * </p>
 */
package pm275.core.resource;

import pm275.common.persistance.ProjectVisitorInterface;
import pm275.core.project.ActivityKey;

/**
 * @author vc
 *
 * Keeps references of assigned resource
 */
public interface AssignmentInterface {
	/**
	 * <p>Represents an activity</p>
	 * 
	 * 
	 * 
	 * @return involved activity 
	 */
	public abstract ActivityKey getActivityKey();

	public abstract void setActivityKey(ActivityKey activity);

	/**
	 * <p>Represents resource</p>
	 * 
	 * 
	 * 
	 * @return resource key
	 */
	public abstract ResourceKey getResource();

	public abstract void setResource(ResourceKey resource);

	/**
	 * <p>Represents an assignement key</p>
	 * 
	 * 
	 * 
	 * @return 
	 */
	public abstract AssignmentKey getAssignmentKey();

	/**
	 * <p>Gets a percentage allocated for a given task</p>
	 * 
	 * 
	 * 
	 * @return 
	 */
	public abstract double getPercent();

	/**
	 * <p>Set the percentage allocated for a given task</p>
	 * 
	 * 
	 * 
	 * @param _percent 
	 */
	public abstract void setPercent(double _percent);

    /*
     * The visitor pattern for writing to XML & DB
     * @param ProjectVisitorInterface visitor instance of visitor
     *  to gather informatoin about this class
     */
    public void acceptVisitor(ProjectVisitorInterface visitor);
	
	/**
	* $Log: AssignmentInterface.java,v $
	* Revision 1.3  2005/05/08 06:12:43  vchumako
	* Added acceptVisitor to Assignment
	*
	* Revision 1.2  2005/04/10 21:26:33  vchumako
	* Added Resource Interface
	*
	* Revision 1.1  2005/04/10 21:18:39  vchumako
	* Added AssignmentInterface whenever it's possible
	*
	*
	**/
}
