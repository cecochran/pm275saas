/***************************************************
 * File:        $RCSfile: ResourcePool.java,v $
 * @author      $Author: rcoutinh $,
 * @version     $Id: ResourcePool.java,v 1.19 2005/05/15 16:56:27 rcoutinh Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents: The operstions and representation of a collection of Resource Pools.
 * </p>
 * <p>
 * Purpose:
 *   The resource pool is a collection of resources. Its purpose is to manage
 *   assignment and removal of resources from the pool
 *
 * </p>
 */

package pm275.core.resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Collection;

import org.apache.log4j.Logger;

import pm275.common.exception.ResourceException;
/**
 * <p></p>
 */
public class ResourcePool {

/**
 * <p>Represents a logging utility object.</p>
 */
    private static final Logger log = Logger.getLogger( ResourcePool.class );
    private static final String ATTR = "ResourcePool";

/**
 * <p>Resource Pool key</p>
 */
    private ResourcePoolKey resourcePoolKey;

/**
 * <p>The collection of resources</p>
 */

    private Map resources = new HashMap(12);

/**
 * Default constructor creating a Resource Pool
 */
    protected ResourcePool() {
        this.resourcePoolKey = new ResourcePoolKey();
    }
	
	protected ResourcePool(ResourcePoolKey rpKey) {
		this.resourcePoolKey = rpKey;
	}
/**
 * <p>Returns the resource key for this pool.</p>
 *
 *
 * @return resource pool key
 *
 */
    public ResourcePoolKey getResourcePoolKey() {
        return resourcePoolKey;
    }

/**
 * <p>Adds a resource to the collection</p>
 *
 * @param resource key
 * @return void
 *
 */
    public void addResource(ResourceInterface resource) throws ResourceException
    {
        ResourceKey key = resource.getKey();
        try
        {
            resources.put(key, resource);
        }
        catch(Exception e)
        {
            if(key == null)
            {
                log.error("ERROR - ResourcePool: Could not add a resource to the resource pool.  The  key was null." + " Exception: " + e.getMessage());
                throw new ResourceException("ERROR - ResourcePool: Could not add a resource to the resource pool.  The  key was null.");
            }
            else
            {
                log.error("ERROR - ResourcePool: Could not add a resource to the resource pool with key " + key + " Exception: " + e.getMessage());
                throw new ResourceException("ERROR - ResourcePool: Could not add a resource to the resource pool with key " + key);
            }
        }
        log.debug("Resource '" + resource + "' added!");
    }

/**
 * <p>Removes a resource from a pool given a key</p>
 *
 * @param resource key
 *
 */
    public void removeResouce(ResourceKey key) throws ResourceException
    {
        try
        {
            resources.remove(key);
        }
        catch(Exception e)
        {
            log.error("ERROR: Could not remove resource from pool with key: " + key + " Exception: " + e.getMessage());
            throw new ResourceException("ERROR: Could not remove resource from pool with key: " + key);
        }
        log.debug("Resource with key '" + key + "' removed");
    }

    /**
     * <p>Gets a resource from a pool given a key</p>
     *
     * @param resource key
     * @return resource
     */
    public ResourceInterface getResource(ResourceKey key) throws ResourceException
    {
        try
        {
            //get resource with specific key
            ResourceInterface rs = 
            		(ResourceInterface)resources.get(key);
            if (rs.getKey().equals(key))
            {
                //found it so return it
                return rs;
            }
        } catch (Exception e) {
            if (key == null) {
                log.error("ERROR - ResourcePool: Could not get a resource from the resource pool.  The  key was null."
                                + " Exception: " + e.getMessage());
                throw new ResourceException(
                        "ERROR - ResourcePool: Could not get a resource from the resource pool.  The  key was null.");
            } else {
                log.error("ERROR - ResourcePool: Could not get a resource from the resource pool with key "
                                + key + " Exception: " + e.getMessage());
                throw new ResourceException(
                        "ERROR - ResourcePool: Could not get a resource from the resource pool with key "
                                + key);
            }
        }
        log.debug("Resource with key '" + key + "' found");
        return null;
    }

    /**
     * @return
     */
    public Collection listResources() {
        return resources.values();
    }

    /**
     * @return
     */
    public Collection listResourceKeys() {
        return resources.keySet();
    }
 }

/**
 * $Log: ResourcePool.java,v $
 * Revision 1.19  2005/05/15 16:56:27  rcoutinh
 * New constructor
 *
 * Revision 1.18  2005/04/17 23:25:26  vchumako
 * Implemented comparable for PM275BaseKey.
 * Now it's not required to use getKeyId() in order to put or remove keys from the Map.
 *
 * Revision 1.17  2005/04/17 20:05:32  vchumako
 * fix resource pool access
 *
 * Revision 1.16  2005/04/17 11:25:54  achou
 * Changed listResources() and listResourceKeys() to return Collection instead of Iterator.
 *
 * Revision 1.15  2005/04/17 03:22:23  vchumako
 * Added extra logging
 *
 * Revision 1.14  2005/04/17 02:51:16  vchumako
 * Added some debug logging
 *
 * Revision 1.13  2005/04/17 02:20:27  vchumako
 * add createResourcePool() and createResource() methods to the ResourceService.
 * They should be used from now on to create Resources.
 *
 * Revision 1.12  2005/04/13 23:14:33  vchumako
 * fix Resource interfaces and test case
 *
 * Revision 1.11  2005/04/13 04:25:59  bbuchana
 * Changed resource ArrayList to HashMap
 *
 * Revision 1.10  2005/04/10 21:31:00  vchumako
 * Added Resource Interface
 *
 * Revision 1.9  2005/03/30 01:44:50  bbuchana
 * Added comments.
 *
 * Revision 1.8  2005/03/29 21:43:21  bbuchana
 * Added code from my local to CVS.
 *
 * Revision 1.7  2005/03/29 17:26:02  ccochran
 * added get resource method
 *
 * Revision 1.6  2005/03/28 01:22:31  vchumako
 * Minor name changes
 *
 * Revision 1.5  2005/03/27 22:30:51  vchumako
 * Implemented add, remove, and list resources methods.
 *
 * Revision 1.4  2005/03/26 12:13:58  ccochran
 * Cleanup CEC
 *
 * Revision 1.3  2005/03/26 11:32:42  ccochran
 * Log4j
 *
 * Revision 1.2  2005/03/26 10:20:44  achou
 * Fixed package designation.
 *
 * Revision 1.1  2005/03/26 09:02:29  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.4  2005/03/26 06:35:16  ccochran
 * CEC Added Comments
 *
 * Revision 1.3  2005/03/26 04:20:54  ccochran
 * CEC just added comment
 *
 * Revision 1.2  2005/03/23 01:47:52  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.1  2005/03/22 04:30:32  dnathan
 * New file.
 *
 **/
