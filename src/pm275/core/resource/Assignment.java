/***************************************************
 * File: 		$RCSfile: Assignment.java,v $ 
 * @author 		$Author: rcoutinh $ 
 * @version 	$Id: Assignment.java,v 1.13 2005/05/15 16:56:08 rcoutinh Exp $
 * Assignment:	CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 *  An Assigment associates an activity and a resource
 *  at a certain percentage. This is critical in calculating
 *  the amount of work require to finish a core activity
 * </p>
 */
package pm275.core.resource;
import org.apache.log4j.Logger;

import pm275.common.persistance.ProjectVisitorInterface;
import pm275.core.project.ActivityKey;
/**
 * <p></p>
 */
public class Assignment implements AssignmentInterface {
	 private static final Logger log = Logger.getLogger( Assignment.class );
	 private static final String ATTR = "Assignment";
/**
 * <p>Represents reference of the Activuty</p>
 */
	private ActivityKey activity;

/**
 * <p>Represents resource reference</p>
 */
    private ResourceKey resource;

/**
 * <p>Represents implementaiton</p>
 */
    private AssignmentKey assignmentKey;

/**
 * <p>Represents completion progress</p>
 */
    private double percent;

/**
 * default constructor
 * initializes an Assignment object
 */
    public Assignment(ActivityKey activity,
		      ResourceKey resource) {
    	setActivityKey(activity);
    	setResource(resource);
    	this.assignmentKey = new AssignmentKey();
    }
    			
/**
 * named constructor
 * initializes an Assignment object
 */
    public Assignment(ActivityKey activity,
		      ResourceKey resource,
		      String name) {
    	setActivityKey(activity);
    	setResource(resource);
    	this.assignmentKey = new AssignmentKey(name);
    }
    			

    
	
    /**
     * named constructor
     * initializes an Assignment object
     */
        public Assignment(String assKey,				
				ActivityKey activity,
				ResourceKey resource, 
    		    double eff) {
        	
        	this.assignmentKey = new AssignmentKey(assKey);
       		setActivityKey(activity);
        	setResource(resource);
        	percent = eff;
        }
        
/**
 * <p>Represents an activity</p>
 * 
 * 
 * 
 * @return involved activity 
 */
    public ActivityKey getActivityKey() {        
        return activity;
    } 
    
    public void setActivityKey(ActivityKey activity) {
    	if(activity!=null){
    	  this.activity = activity;
	   }else{
		log.error("Error: Set Activity key was set with NULL");
  	   }
    }

/**
 * <p>Represents resource</p>
 * 
 * 
 * 
 * @return resource key
 */
    public ResourceKey getResource() {        
        return resource;
    } 
    
    public void setResource(ResourceKey resource) {
    	if(resource != null){
    	   this.resource = resource;
    	}else{
    		log.error("Error: Set resource key was set with NULL");
    	}
    }
    


/**
 * <p>Represents an assignement key</p>
 * 
 * 
 * 
 * @return 
 */
    public AssignmentKey getAssignmentKey() {
    	return assignmentKey;
    } 

/**
 * <p>Gets a percentage allocated for a given task</p>
 * 
 * 
 * 
 * @return 
 */
    public double getPercent() {        
        return percent;
    } 

/**
 * <p>Set the percentage allocated for a given task</p>
 * 
 * 
 * 
 * @param _percent 
 */
    public void setPercent(double _percent) {        
       if(_percent != Double.NaN){
    	this.percent = _percent;
       }else{
       	log.error("Error: Set percent was set with double.NAN");
       }
    } 
    
    /*
     * Trying out the visitor pattern for writing to XML
     * @param ProjectVisitorInterface visitor instance of visitor
     *  to gather informatoin about this class
     */
    public void acceptVisitor(ProjectVisitorInterface visitor) {
    	visitor.visitAssignment(this);
    }

}

/**
 * $Log: Assignment.java,v $
 * Revision 1.13  2005/05/15 16:56:08  rcoutinh
 * Added new constructor
 *
 * Revision 1.12  2005/05/15 03:42:42  ccochran
 * added dao
 *
 * Revision 1.11  2005/05/08 06:12:43  vchumako
 * Added acceptVisitor to Assignment
 *
 * Revision 1.10  2005/05/01 22:15:02  tlong2
 * removed AssignmentVisitor()
 *
 * Revision 1.9  2005/05/01 21:18:38  tlong2
 * Added support for visitAssignment
 *
 * Revision 1.8  2005/04/10 21:18:39  vchumako
 * Added AssignmentInterface whenever it's possible
 *
 * Revision 1.7  2005/03/29 16:03:31  ccochran
 * added some Logging and increase defensive coding
 *
 * Revision 1.6  2005/03/28 18:09:29  ccochran
 * Add class purpose
 *
 * Revision 1.5  2005/03/28 01:14:17  vchumako
 * Minor naming fixes, default constructor fix
 *
 * Revision 1.4  2005/03/27 06:28:43  ccochran
 * Add Log4J id
 *
 * Revision 1.3  2005/03/26 19:33:25  vchumako
 * Reference ActivityKey from core.project
 *
 * Revision 1.2  2005/03/26 18:59:00  vchumako
 * string type fixed
 *
 * Revision 1.1  2005/03/26 18:31:22  vchumako
 * Restored Assignment.java in this directory
 *
 * Revision 1.5  2005/03/26 04:17:54  vchumako
 * Initial revision of Assignment object
 *
 * Revision 1.4  2005/03/23 01:47:49  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.3  2005/03/22 04:31:00  dnathan
 * Updated header to include CVS ID.
 *
 * Updated footer to include CVS log.
 *
 * Revision 1.2  2005/03/22 04:21:32  dnathan
 * Updated CVS footer.
 *
 **/
