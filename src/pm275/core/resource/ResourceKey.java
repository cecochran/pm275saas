/***************************************************
 * File:        $RCSfile: ResourceKey.java,v $
 * @author      $Author: rcoutinh $,
 * @version     $Id: ResourceKey.java,v 1.9 2005/05/15 20:21:10 rcoutinh Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents: A unique key for a resource.
 * </p>
 * <p>
 * Purpose:
 * The Resource Key is a unique representation of a resource
 * Using keys allows for lazy instatation and allowing services to
 * store only the keys. The Resource Key represents the HashKey value of the project
 * that uniquely identifies an Resource among all other projects.
 * </p>
 */
package pm275.core.resource;
import org.apache.log4j.Logger;
import pm275.common.PM275BaseKey;
/**
 * <p></p>
 */
public class ResourceKey extends PM275BaseKey{

/**
 * <p>Represents a logging utility object.</p>
 */
	  private static final Logger log = Logger.getLogger( ResourceKey.class );
	  private static final String ATTR = "ResourceKey";


/**
 * Default constructor initializing a Resource Key object.
 */  
	  public ResourceKey(){
	  	super();
	  }

/**
 * Constructor initializing a Resource Key object with a set key value.
 */
	  public ResourceKey(String str){
	  	super(str);
	  }
	  
/**
 * <p>Compare A key value to current value of this key ...</p>
 *
 * @return True if Key matches, False if not
 * @param key
 */
    public boolean equals(Object key) { 	
        return super.equals(key);
    	    	
    }

/**
 * <p>Return the value of the key as a string.</p>
 *
 * @return String
 */
    public String toString() {
        
        return super.toString();
    }
 }

/**
 * $Log: ResourceKey.java,v $
 * Revision 1.9  2005/05/15 20:21:10  rcoutinh
 * Corrected equals method signature
 *
 * Revision 1.8  2005/04/10 21:31:00  vchumako
 * Added Resource Interface
 *
 * Revision 1.7  2005/03/30 01:43:38  bbuchana
 * Added comments.
 *
 * Revision 1.6  2005/03/26 17:47:09  ccochran
 * fixed bug
 *
 * Revision 1.5  2005/03/26 17:05:15  ccochran
 * Switched to using exted base key
 *
 * Revision 1.4  2005/03/26 12:13:59  ccochran
 * Cleanup CEC
 *
 * Revision 1.3  2005/03/26 11:29:54  ccochran
 * Log 4J
 *
 * Revision 1.2  2005/03/26 10:20:44  achou
 * Fixed package designation.
 *
 * Revision 1.1  2005/03/26 09:02:30  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.5  2005/03/26 04:45:56  ccochran
 * Added Commnts
 *
 * Revision 1.4  2005/03/25 17:19:35  ccochran
 * CEC fixed stupid bug on key
 *
 * Revision 1.3  2005/03/25 14:56:46  ccochran
 * added some key code
 *
 * Revision 1.2  2005/03/23 01:47:46  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.1  2005/03/22 04:30:29  dnathan
 * New file.
 *
 **/
