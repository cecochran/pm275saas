/***************************************************
 * File: 		$RCSfile: CalendarKey.java,v $ 
 * @author 		$Author: rcoutinh $, 
 * @version 	$Id: CalendarKey.java,v 1.5 2005/05/15 20:20:16 rcoutinh Exp $
 * Assignment:	CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * The Calendar Key is a unique representation of a calendar
 * Using keys allows for lazy instatation and allowing services to
 * store only the keys.The Calendar Key represents the HashKey value of the project
 * that uniquely identifies an Calendar among all other calendar. 
 * 
 * </p>
 */
package pm275.core.calendar;
import org.apache.log4j.Logger;

import pm275.common.PM275BaseKey;


public class CalendarKey  extends PM275BaseKey
{
    private static final Logger log = Logger.getLogger( CalendarKey.class );
    private static final String ATTR = "CalendarKey";


	
	public CalendarKey() 
	{
		super();
	}
	
	/**
	 * 
	 * @param str - create  a key from this string
	 */
	public CalendarKey(String str)
	{
		super(str);
	}
	
	/**
	 * <p>Does given key match.</p>
	 * 
	 * @return boolean
	 * @param key 
	 */
    public boolean equals(Object key) {
    	
		return super.equals(key);
    } 

	/**
	 * <p>Method to return the string represnation of a key</p>
	 * 
	 * 
	 * 
	 * @return String
	 */
    public String toString() {        
		return super.toString();
    } 
 }






