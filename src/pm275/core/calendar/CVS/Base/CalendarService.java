/***************************************************
 * File:        $RCSfile: CalendarService.java,v $
 * @author      $Author: ccochran $,
 * @version     $Id: CalendarService.java,v 1.13 2005/05/13 02:46:51 ccochran Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * The Calendar Service is the application component responcible for
 * managing all calendars. There are 3 types of calendars Corporate,Personal and Project.
 * A Corportate calendar is universal across all projects and defines stanard holidays
 * A Personal calendar is unique to that individual and tracks personal time
 * A Project calendar is uniue to a project
 * There is some interaction between each of these calendars which is domain
 * of the business logic for this service.
 * The Calendar Service is a singleton, meaning only one per VM and arguably one per application
 * </p>
 */
package pm275.core.calendar;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.Collection;
import java.util.Map;
import java.util.Date;
import pm275.common.persistance.*;
import pm275.common.exception.KeyNotFoundException;
import pm275.core.resource.ResourceKey;

/**
 * <p>Services to store all calendars in PM-275</p>
 */
public class CalendarService
{
    private static final Logger log = Logger.getLogger( CalendarService.class );
    private static final String ATTR = "CalendarService";
/**
 * <p>Represents variable to point to a calendar service.</p>
 *
 */
    private static CalendarService calendarService;

/**
 * <p>Represents variable to store hashtable of calendars by key value pair. Key is ResourceKey, value is Calendar object.</p>
 *
 */
    private Map calendars;

/**
 * <p>Represents variable to store the corporate calendar</p>
 *
 *
 */
    private Calendar corporateCalendar;

/**
 * <p></p>
 *
 * @poseidon-type Calendar
 */
    public java.util.Collection calendar = new java.util.TreeSet();

    private CalendarService()
    {

    }

/**
 * <p>Method to instantiate new CalendarService, put corporate calendar into list of calendars.</p>
 * 
 *
 *
 * @return CalendarService
 */
    public static final CalendarService getInstance() {

        if ( calendarService == null ){

            calendarService = new CalendarService();
            calendarService.calendars = new java.util.Hashtable();
            calendarService.corporateCalendar = new Calendar();
            calendarService.corporateCalendar.setCalendarType(Enumeration.CORPORATECALENDAR);
	    calendarService.corporateCalendar.setName("Corporate Calendar");
            calendarService.corporateCalendar.setDaysPerWeek(5);
            calendarService.corporateCalendar.setHoursPerDay(8);

	    // add corporate calendar to list of calendars
	    calendarService.calendars.put(calendarService.corporateCalendar.getKey(), calendarService.corporateCalendar);
        }
        return calendarService;
    }
    
    /**
     * <p>Does given date is a working day for given resource.</p>
     *
     *
     *
     * @return
     * @param date
     * @param resource
     */
    public boolean isWorkDay(java.util.Date date, ResourceKey resource) {
	
        boolean isworkday = true;

	// check on corporate calendar
	// if non working day in corporate then not in personal
	//
	isworkday = corporateCalendar.isWorkDay(date);
	
	// check on resource calendar if resource is not null
        if (resource != null)
	    {
		Calendar cal = (Calendar)calendars.get(resource);
		isworkday = cal.isWorkDay(date);
	    }
	
        return isworkday;
    }

    // get calendar by key (resource or project)
    // list calendars
    //

    /**
     * <p>Method to get calendar by resourceKey, if resource is null then return corporate calendar.</p>
     *
     * @param resourceKey
     */
    public Calendar getCalendar(ResourceKey resource)
    {
    	
   	Calendar cal = null;
   	
	if (resource != null){
		
		if(calendars.containsKey(resource)){
		   cal = (Calendar)calendars.get(resource);
		}else{
		    CalendarKey calKey= null;
		    //PersistanceService.getInstance().loadCalendarFromDBMS(resource);
		    	cal = corporateCalendar;
		}
	}else{
		cal = corporateCalendar;
	}
	
	return cal;
	
	 
    }
    
    /**
     * <p>Method to add holiday to a personal calendar</p>
     *
     *
     * @param date
     * @param title
     * @param resource
     */
    public void addRestrictedDay(java.util.Date date, String title, ResourceKey resource) {
	
	// if resource is null then add to corporate calendar
	if (resource != null)
	    {
		// get calendar
		Calendar temp_cal = (Calendar)calendars.get(resource);
		
		temp_cal.addRestrictedDay(date, title);
	    }
	else // resource is null, add to corporate
	    {
		corporateCalendar.addRestrictedDay(date, title);
	    }
    }
    
    /**
     * <p>Method to add holiday to a personal calendar</p>
     *
     *
     *
     * @param date
     * @param resource
     */
    public void removeRestrictedDay(java.util.Date date, ResourceKey resource)
    {
	
	// if resource is null then add to corporate calendar
	if (resource != null)
	    {
		// get calendar
		Calendar temp_cal = (Calendar)calendars.get(resource);
		
		temp_cal.removeRestrictedDay(date);
		
	    }
	else // resource is null, add to corporate
	    {
		corporateCalendar.removeRestrictedDay(date);
	    }
    }
    
    /**
     * Method to add calendar
     * @param key
     * @return
     */
    public Calendar addCalendar(CalendarKey key){
        Calendar c = new Calendar(key);
        calendars.put(key,c);
        return c ;
    }
    
    /**
     * Method to remove calendar with this particular key
     * @param key
     */
    public Calendar removeCalendar(CalendarKey key)
        throws KeyNotFoundException
    {

        if(! calendars.containsKey(key))
	    {
		log.warn("CalendarService.removeCalendar() - Key not found " 
			 + key.toString());
		throw new KeyNotFoundException("CalendarService.removeCalendar() "
					       + "- Key not found " + key.toString());
        }
        return (Calendar)calendars.remove(key);

    }

    /**
     * Get the calendar with give key
     * @param key
     * @return
     */
    public Calendar getCalendar(CalendarKey cKey)
        throws KeyNotFoundException
    {
      	
       	Calendar cal = null;
       	
    	if (cKey!= null){
    		if(calendars.containsKey(cKey)){
    		   cal = (Calendar)calendars.get(cKey);
    		}else{
    			CalendarKey calKey = null;
    		    calKey = PersistanceService.getInstance().loadCalendarFromDBMS(cKey.getKeyId());
    		    cal = (Calendar)calendars.get(calKey);
    		}
    	}
    	
    	//if all else fails
    	if(cal == null){
	    	cal = corporateCalendar;
	    }

    	
    	return cal;
    	
    }

    /**
     * <p>Method to return list of calendars from the hashtable of calendars</p>
     *
     *
     *
     */
    public Collection listCalendars() {

        return Collections.unmodifiableCollection(calendars.values());

    }

    public CalendarInterface getCorporateCalendar() {

        return corporateCalendar;

    }

    /**
     *  Returns the next work day on or after the specified date.
     *  @date date Baseline date.  If null, set to today's date.
     *  @return Next work day on or after the specified date.
     */
    public Date getNextWorkDay(Date date)
    {
        return corporateCalendar.getNextWorkDay(date);
    }

}

/**
 * $Log: CalendarService.java,v $
 * Revision 1.13  2005/05/13 02:46:51  ccochran
 * defensive coding around empty calendar
 *
 * Revision 1.12  2005/05/09 18:49:24  vchumako
 * Removed debug statement
 *
 * Revision 1.11  2005/05/09 15:03:02  vchumako
 * Give warning instead of error when there is no calendar to remove.
 * Didn't find the other way to check if there is no calendar key without
 * throwing an exception.
 *
 * Revision 1.10  2005/05/03 02:44:10  umangkus
 * Modified Calendar.java to use core.calendar.Holiday object and
 * getRestrictedDays to return Collection.
 * Modified CalendarInterface.java getNumberOfWorkingDays method.
 * Modified CalendarService.java to include corporate calendar in list of
 * calendars.
 *
 * Revision 1.9  2005/05/02 23:45:22  umangkus
 * By default include corporate calendar in list of calendar in
 * CalendarService
 *
 * Revision 1.8  2005/04/19 09:28:06  achou
 * Removed obsolete methods computeFutureWorkDate() and getNextDay().  Added new method getNextWorkDay().
 *
 * Revision 1.7  2005/04/16 21:14:52  rcoutinh
 * computeFutureWorkDate - moved to calendar.
 * On calendarService -added method getCorporatecalendar that returns the default calendar.
 *
 * Revision 1.6  2005/04/16 20:37:48  dnathan
 * Fixed null pointer problems.
 *
 * Revision 1.5  2005/03/29 05:07:41  achou
 * Fixed bug that was causing null pointer exceptions.
 *
 * Revision 1.4  2005/03/29 04:41:44  rcoutinh
 * Added addCalendar,removeCalendar and getCalendar methods to the Service
 *
 * Revision 1.3  2005/03/26 11:18:13  achou
 * Added import statements and fixed copy-and-paste error.
 *
 * Revision 1.2  2005/03/26 09:26:29  ccochran
 * CEC : Added Log4j and XML support
 *
 * Revision 1.1  2005/03/26 09:02:33  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.9  2005/03/26 06:34:36  ccochran
 * Added comments
 *
 * Revision 1.8  2005/03/25 17:18:55  ccochran
 * CEC fixed the instance code
 *
 * Revision 1.7  2005/03/25 06:46:49  tlong2
 * Uncommented out computeFutureDate because it is being used by Activity.java
 *
 * Revision 1.6  2005/03/24 00:34:46  umangkus
 * Updated CalendarService.java, simplify hashtable. For more info, see Calendar.doc in yahoogroups repository.
 *
 * Revision 1.5  2005/03/23 04:44:48  tlong2
 * Minor updates..
 *
 * Revision 1.4  2005/03/23 01:47:51  dnathan
 * Updated header to reflect coding standards document.
 *
 * Added various classes into the right package.
 *
 * Revision 1.3  2005/03/23 00:45:15  umangkus
 * *** empty log message ***
 *
 * Revision 1.2  2005/03/22 04:31:00  dnathan
 * Updated header to include CVS ID.
 *
 * Updated footer to include CVS log.
 *
 **/
