/***************************************************
 * File:        $RCSfile$
 * @author      $Author$,
 * @version     $Id$
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 *  This class is responsible for reading/writing calendar from 
 *  Database.
 * </p>
 */
package pm275.core.calendar;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import pm275.core.resource.ResourceDAOtoDB;
import pm275.common.persistance.DBConnection;
import pm275.common.persistance.PM275DBSchema;
import pm275.core.project.ProjectKey;
import pm275.common.exception.KeyNotFoundException;

public class CalendarDAOToDB 
{
	

	private static final Logger log = Logger.getLogger(ResourceDAOtoDB.class);
    private static final String ATTR = "CalendarDAOtoDB";
    String sql = "";
	
	
	public CalendarDAOToDB()
	{
		super();
	}
	
	/**
	 * Looks up a calendar from Databse for a given ProjectKey
	 * Loads the calendar attributes from DB and adds it to 
	 * the calendar Service list of calendars.
	 * @param pKey
	 * @return
	 */
	public CalendarKey get(ProjectKey pKey)
	{
		CalendarService cs = CalendarService.getInstance();
		PreparedStatement ps  = null;
		PreparedStatement ps2 = null;
		DBConnection conn = new DBConnection();
		conn.connect();
		CalendarKey cKey = null;
		try {
			ps = conn.getPreparedPreparedStatement(PM275DBSchema.SELECT_CALENDAR_WITH_PROJID);
			ps.setString(1, pKey.toString());
			ResultSet rs = ps.executeQuery();
			if(rs.next()) 
			{
				
				ps2 = conn.getPreparedPreparedStatement(PM275DBSchema.SELECT_CALENDAR_WITH_ID);
				String calKeyStr = rs.getString(1);
				ps2.setString(1,calKeyStr);
				ResultSet rs2 = ps2.executeQuery();
				if(rs2.next())
				{
					cKey = new CalendarKey(calKeyStr);
					CalendarInterface ci = cs.addCalendar(cKey);
					ci.setName(rs2.getString(PM275DBSchema.CALENDAR_NAME));
					ci.setCalendarType(rs2.getInt(PM275DBSchema.CALENDAR_TYPE));
					ci.setHoursPerDay(rs2.getInt(PM275DBSchema.CALENDAR_HOURS));
					ci.setDaysPerWeek(rs2.getInt(PM275DBSchema.CALENDAR_DWEEK));
					log.debug("Calendar " + ci.getName() + " loaded successfully!");
				}
			}	
		} catch(SQLException sqle) {
			log.error("Can't load calendar from DB!");
		} finally {
			try {
				if( ps != null) {
					ps.close();
				}
				if( ps2 != null) {
					ps2.close();
				}
			} catch(Exception e) {
			}
		}
		if(cKey == null)
		{
			// default calendar;
			cKey = CalendarService.getInstance().getCorporateCalendar().getKey();
		}
		return cKey;
		
	}
	
	/**
	 * Save a calendar to DB.
	 * @param cKey -calendarKey to be saved.
	 * @return - int indicating number of rows updated
	 */
	public int set(CalendarKey cKey)
	{
		PreparedStatement ps = null;
		DBConnection conn = new DBConnection();
		conn.connect();
		int resultCnt = -1;
		try {
			CalendarService cs = CalendarService.getInstance();
			CalendarInterface c = cs.getCalendar(cKey);
			if(c.getName() == null) {
				c.setName("Corporate calendar");
			}
			ps = conn.getPreparedPreparedStatement(PM275DBSchema.SELECT_CALENDAR_WITH_ID);
			ps.setString(1, c.getKey().toString());
			ResultSet rs = ps.executeQuery();
			log.debug("VCDEBUG: cKey = " + c.getKey() + "; cName = " 
					+ c.getName());
			if(rs.next()) {
				log.debug("VCDEBUG: UPDATE");
				ps = conn.getPreparedPreparedStatement(PM275DBSchema.UPDATE_CALENDAR);
				ps.setString(1, c.getName());
				ps.setInt(2, c.getCalendarType());
				ps.setInt(3, c.getDaysPerWeek());
				ps.setInt(4, c.getHoursPerDay());
				
				ps.setString(5, c.getKey().toString());
			} else {
				log.debug("VCDEBUG: INSERT");
				ps = conn.getPreparedPreparedStatement(PM275DBSchema.INSERT_CALENDAR);
				ps.setString(1, c.getKey().toString());
				ps.setString(2, c.getName());
				ps.setInt(3, c.getCalendarType());
				ps.setInt(4, c.getDaysPerWeek());
				ps.setInt(5, c.getHoursPerDay());
				ps.setInt(6, 0);
				
			}
			resultCnt = ps.executeUpdate();
			log.debug("Saved calendar in db: " + c.getName());
		} catch(KeyNotFoundException ke) {
			log.error("Can't find calendar:" + cKey);
		}
		catch(SQLException sqle) {
			log.error("Can't save calendar:" + cKey 
					+ " into DB: " + sqle.getMessage() 
					+ "; SQLState: " + sqle.getSQLState() + "; err = " 
					+ sqle.getErrorCode());
		} finally {
			try {
				if( ps != null) {
					ps.close();
				}
				
			} catch(Exception e) {
			}
			
		}
		return resultCnt ; 
	}
	
	
}

/**
 * $Log$
 * Revision 1.1  2005/05/14 22:16:17  rcoutinh
 * Class responsible for writing/ reading calendar from dB
 *
 *
 */
