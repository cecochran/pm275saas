/***************************************************
 * File: 		$RCSfile: Enumeration.java,v $ 
 * @author 		$Author: ccochran $, 
 * @version 	$Id: Enumeration.java,v 1.4 2005/03/30 06:02:15 ccochran Exp $
 * Assignment:	CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 */

package pm275.core.calendar;
//import org.apache.log4j.Logger;
/**
 * <p>Enumeration value for core.</p>
 * 
 * @author UripMangkusubroto
 */

public interface Enumeration
{
    // type of calendar
    public final int CORPORATECALENDAR = 1;
    public final int PERSONALCALENDAR = 2;
    public final int PROJECTCALENDAR = 3;
    
    // type of day
    public final int SUNDAY = 0;
    public final int MONDAY = 1;
    public final int TUESDAY = 2;
    public final int WEDNESDAY = 3;
    public final int THURSDAY = 4;
    public final int FRIDAY = 5;
    public final int SATURDAY = 6;
}




/**
 * $Log: Enumeration.java,v $
 * Revision 1.4  2005/03/30 06:02:15  ccochran
 * Comment log4j import to get rid of warning
 *
 * Revision 1.3  2005/03/26 12:13:58  ccochran
 * Cleanup CEC
 *
 * Revision 1.2  2005/03/26 09:26:29  ccochran
 * CEC : Added Log4j and XML support
 *
 * Revision 1.1  2005/03/26 09:02:32  ccochran
 * CEC : Organized Core By Service
 *
 * Revision 1.9  2005/03/25 06:45:55  tlong2
 * Added CVS Header and Footer
 *
 

  >>>>>>> 1.6
*/