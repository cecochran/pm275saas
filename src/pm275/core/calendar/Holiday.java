/***************************************************
 * File:        $RCSfile: Holiday.java,v $
 * @author      $Author: jkeiser $,
 * @version     $Id: Holiday.java,v 1.2 2005/05/08 13:41:11 jkeiser Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose: Class store holiday. To be used by Calendar.java
 * </p>
 */

package pm275.core.calendar;

import java.util.Date;

import pm275.gui.common.DateFormatter;

public class Holiday
{
    private String name;
    private Date date;

    public Holiday()
    {
	name = null;
	date = null;
    }

    public Holiday(Date d, String s)
    {
	date = d;
	name = s;
    }

    public Holiday(String s, Date d)
    {
	date = d;
	name = s;
    }

    public void setDate(Date d)
    {
	date = d;
    }

    public void setName(String s)
    {
	name = s;
    }

    public String getName()
    {
	return name;
    }

    public Date getDate()
    {
	return date;
    }

    public String toString()
    {
        String holidayDateString = DateFormatter.getStringForDate( getDate() );   	
        return holidayDateString + getName();
    }
}

/**
 * $Log: Holiday.java,v $
 * Revision 1.2  2005/05/08 13:41:11  jkeiser
 * Added toString for display of Holidays in UI
 *
 * Revision 1.1  2005/05/02 23:39:12  umangkus
 * added Holiday.java
 *
 *
 **/
