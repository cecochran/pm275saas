/***************************************************
 * File:        $RCSfile: Calendar.java,v $
 * @author      $Author: jkeiser $,
 * @version     $Id: Calendar.java,v 1.25 2005/05/08 22:19:28 jkeiser Exp $
 * Assignment:  CSCI E-275 Team 1 Project
 * <p>
 * Contents:
 * </p>
 * <p>
 * Purpose:
 * Calendar a core abastraction of PM-275 that represts a calendar
 * More precisely a collection of dates and times over which events occur
 * How a calender is given context represents its true meaning.
 * There can be multiple kind of Calendars. Personal, Project and corporate.
 * The Calendar service shall manage the calnedars.
 * </p>
 */
package pm275.core.calendar;
import java.util.Date;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Collection;
import org.apache.log4j.Logger;
import pm275.common.persistance.ProjectVisitorInterface;

public class Calendar implements CalendarInterface
{

// TODO: Remove.
java.text.SimpleDateFormat SDF = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");


    private static final Logger log = Logger.getLogger( Calendar.class );
    private static final String ATTR = "Calendar";

    /**
     * <p>Represents type of calendars (corporate, resource, project).</p>
     */
    private int calendarType;

    /**
     * <p>Represents unique ID for calendar.</p>
     */
    private CalendarKey key;

    /**
     * <p>Represents name of the calendar.</p>
    */
    private String name;

    /**
     * <p>Represents number of working hours per day for this calendar.     Differentiate between full time or part time worker.</p>
     */
    private int hoursPerDay = 8;

    /**
     * <p>Represents number of working days per week for this calendar. Differentiate between full or part time week.</p>
     */
    private int daysPerWeek = 5;

    /**
     * <p>Represents enumeration for days per week.</p>
     */
    private ArrayList workWeek = new ArrayList();

    /**
     * <p>Represents list of holidays in key value pair. Key is java.util.Date, value is name of holliday.</p>
     */
    private Hashtable restrictedDays = new Hashtable();


    /**
     * <p>Non param constructor. Instantiate new key, cannot be changed. Set type to personal.</p>
     *
     */
    public Calendar()
    {
       this(new CalendarKey());
    }

    /**
     * Instatiate calendar using given key
     * @param key
     */
    public Calendar(CalendarKey key)
    {
        this.key = key;
        calendarType = Enumeration.PERSONALCALENDAR;
        restrictedDays = new java.util.Hashtable();
        int [] week = new int[5];
        week[0] = java.util.Calendar.MONDAY;
        week[1] = java.util.Calendar.TUESDAY;
        week[2] = java.util.Calendar.WEDNESDAY;
        week[3] = java.util.Calendar.THURSDAY;
        week[4] = java.util.Calendar.FRIDAY;
        setWorkWeek(week);
    }

    /**
     * <p>Does this calendar a corporate calendar?</p>
     *
     *
     * @return boolean
     */
    public boolean isCorporate() {

        if (calendarType == Enumeration.CORPORATECALENDAR)
            return true;

        return false;
    }

    /**
     * <p>Does this calendar a personal calendar?</p>
     *
     * @return boolean
     */
    public boolean isPersonal() {

        if (calendarType == Enumeration.PERSONALCALENDAR)
            return true;

        return false;
    }

    /**
     * <p>Does this calendar a project calendar?</p>
     *
     * @return boolean
     */
    public boolean isProject() {

        if (calendarType == Enumeration.PROJECTCALENDAR)
            return true;

        return false;
    }

    /**
     * <p>Method to return name of this calendar.</p>
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Method to set Name of Calendar</p>
     *
     * @param _name
     */
    public void setName(String _name) {
        name = _name;
    }

    /**
     * <p>The type of calendar this particular calendar is
     * Corportate
     * Project
     * Personal
     *
     * </p>
     *
     *
     *
     * @return calendar type
     */
    public int getCalendarType() {
        return calendarType;
    }

    /**
     * <p>Represents method to set type of calendar.
     * If param is bad then calendar will be personal
     *  calendar by default.</p>
     *
     *
     *
     * @param _calendarType
     */
    public void setCalendarType(int _calendarType) {

        if (calendarType != Enumeration.CORPORATECALENDAR ||
            calendarType != Enumeration.PERSONALCALENDAR ||
            calendarType != Enumeration.PROJECTCALENDAR )
            { calendarType = _calendarType; }
        else
            { calendarType = Enumeration.PERSONALCALENDAR; }
    }

    /**
     * <p>Represents method to return key for this calendar.</p>
     *
     *
     *
     * @return
     */
    public CalendarKey getKey() {
        return key;
    }

    /**
     * <p>Represents method to return number of hours per day.</p>
     *
     *
     *
     * @return
     */
    public int getHoursPerDay() {
        return hoursPerDay;
    }

    /**
     * <p>Represents method to set number of hours per day.</p>
     *
     *
     *
     * @param _hoursPerDay
     */
    public void setHoursPerDay(int _hoursPerDay) {
        hoursPerDay = _hoursPerDay;
    }

    /**
     * <p>Represents method to return number of days per week.</p>
     *
     *
     *
     * @return number of days per week
     */
    public int getDaysPerWeek() {
        return daysPerWeek;
    }

    /**
     * <p>Represents method to set number of days per week.</p>
     *
     *
     *
     * @param _daysPerWeek
     */
    public void setDaysPerWeek(int _daysPerWeek) {
        daysPerWeek = _daysPerWeek;
    }

    /**
     * <p>Represents method to add a holiday to this calendar</p>
     *
     * @param java.util.Date - date of holiday
     * @param String - name of holiday
     */
     public void addRestrictedDay(java.util.Date date, String name)
     {
        restrictedDays.put(date, new Holiday(name,date));
     }

    /**
     * <p>Represents method to add a holiday to this calendar</p>
     *                                                                   
     * @param holiday day to be added to list of restricted day
     */
    public void addRestrictedDay(Holiday holiday)
    {
        restrictedDays.put(holiday.getDate(),holiday);
    }
 

    /**
     * <p>Represents method to remove a holiday from this calendar</p>
     *
     * @param date Date of holiday
     */
     public void removeRestrictedDay(java.util.Date date)
     {
        restrictedDays.remove(date);
     }

    /**
     * <p>Represents method to remove a holiday from this calendar</p>
     * 
     * @param holiday Holiday to be removed
     */
    public void removeRestrictedDay(Holiday holiday)
    {
        restrictedDays.remove(holiday.getDate());
    }

     /**
      * <p>Represents method to list holidays in the calendar</p>
      *
      */
     public java.util.Collection listRestrictedDays()
     {
        return restrictedDays.values();
     }

     /**
      * <p>Represents method to return set of holidays in the calendar</p>
      *
      */
     public Hashtable getRestrictedDays()
     {
        return restrictedDays;
     }
     
     /**
      * <p>Represents method to remove all holidays in the calendar</p>
      *
      */
     public void clearRestrictedDays()
     {
        restrictedDays.clear();
     }
    /**
     * 
     *<p>Represents method to get name of holidays</p>
     *
     *@param date date of the restricted day
     *@return String name of the restricted day
     */
    public String getRestrictedDayName(java.util.Date date)
    {
	Holiday hday = (Holiday)restrictedDays.get(date);
	return hday.getName();
    }

     /**
      * <p>Represents method to check if given date is a work day (not in holidays)
      * @return false - if date is a holiday and non-working day
      *
      */
    public boolean isWorkDay(java.util.Date date)
    {
        if (restrictedDays.get(date) == null)
        {
            java.util.Calendar cal = java.util.Calendar.getInstance();
            cal.setTime(date);
            int dayOfWeek =  cal.get(java.util.Calendar.DAY_OF_WEEK);
            Integer dayOfWeekInt = new Integer(dayOfWeek);
            if(workWeek.contains(dayOfWeekInt))
            {
                return true;
            }
        }
        return false;
    }

    /**
     *  Adds/subtracts an arbitrary number of hours to/from the specified date.
     *
     *  Note that this method is performed within the context of the calendar,
     *  meaning that the result reflects the hours per day and work days
     *  constraints.  Calls addDaysToDate() during the calculation.
     *
     *  For example, if work day is 00:00 to 08:00 and work week is
     *  Monday through Friday:
     *
     *      // 2005-04-11 is Monday.
     *      [2005-04-11 00:00]  +6 hours  -->  [2005-04-11 06:00]
     *
     *      [2005-04-11 06:00]  +6 hours  -->  [2005-04-12 04:00]
     *
     *      // 2005-04-15 is Friday.
     *      [2005-04-15 04:00]  +8 hours  -->  [2005-04-18 04:00]
     *
     *  @param date Date on which to add/subtract.
     *  @param hours Number of hours to add/subtract from the date.  Positive
     *               means addition, negative means subtraction.
     *  @return Date with the specified number of hours added/subtracted.
     */
    public Date addHoursToDate(Date date, int hours)
    {
        // First, adjust by days.
        Date tmpDate = addDaysToDate(date, hours / hoursPerDay);

        // Direction of change.
        int direction = 1;
        if (hours < 0)
        {
            direction = -1;
            hours *= -1;
        }

        // Now handle the hours.
        hours %= hoursPerDay;
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(tmpDate);
        while (hours > 0)
        {
            cal.add(java.util.Calendar.HOUR_OF_DAY, direction);

            // If we wrap around the "boundaries" of a work day in either
            // direction, fix the hour and date.
            if (cal.get(java.util.Calendar.HOUR_OF_DAY) >= hoursPerDay)
            {
                cal.add(java.util.Calendar.HOUR_OF_DAY, (hoursPerDay * (0 - direction)));
                cal.setTime(addDaysToDate(cal.getTime(), direction));
            }

            hours--;
        }

        // Return the result.
        return cal.getTime();
    }

    /**
     *  Adds/subtracts an arbitrary number of days to/from the specified date.
     *
     *  Note that this method is performed within the context of the calendar,
     *  meaning that the result reflects work days constraints.
     *
     *  For example, if work week is Monday through Friday:
     *
     *      // 2005-04-11 is Monday.
     *      [2005-04-11 00:00]  +2 days  -->  [2005-04-13 00:00]
     *
     *      // 2005-04-14 is Thursday.
     *      [2005-04-14 00:00]  +3 days  -->  [2005-04-19 00:00]
     *
     *  @param date Date on which to add/subtract.
     *  @param hours Number of days to add/subtract from the date.  Positive
     *               means addition, negative means subtraction.
     *  @return Date with the specified number of days added/subtracted.
     */
    public Date addDaysToDate(Date date, int days)
    {
        // Direction of change.
        int direction = 1;
        if (days < 0)
        {
            direction = -1;
            days *= -1;
        }

        // Add/substract one day at a time.  Ignore days that are not work days.
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.setTime(date);
        while (days > 0)
        {
            cal.add(java.util.Calendar.DAY_OF_YEAR, direction);
            if (isWorkDay(cal.getTime()))
            {
                days--;
            }
        }

        // Return the result.
        return cal.getTime();
    }

    /**
     *  Returns the next work day on or after the specified date.
     *  @date date Baseline date.  If null, set to today's date.
     *  @return Next work day on or after the specified date.
     */
    public Date getNextWorkDay(Date date)
    {
        if (date == null)
        {
            date = new Date();
        }
        if (! isWorkDay(date))
        {
            date = addDaysToDate(date, 1);
        }
        return date;
    }

    public Collection getWorkWeek() {
        return workWeek;
    }

    public void setWorkWeek(int [] week)
    {
        this.workWeek = new ArrayList();
        for(int i = 0 ; i < week.length; i++)
        {
            this.workWeek.add(i,new Integer(week[i]));
        }
    }

    public int getNumberOfWorkingDays()
    {
	return this.workWeek.size();
    }

    /*
     * 
     * @author TLong
     *
     * Trying out the visitor pattern for writing to XML
     * @param ProjectVisitorInterface visitor instance of visitor
     *  to gather informatoin about this class
     */
    public void acceptVisitor(ProjectVisitorInterface visitor) {
    	visitor.visitCalendar(this);
    }
    
    public String toString() {
    	return this.name;
    }
    
}   // close calendar
/**
 * $Log: Calendar.java,v $
 * Revision 1.25  2005/05/08 22:19:28  jkeiser
 * Added clearRestrictedDays() to allow update of list of holidays.
 *
 * Revision 1.24  2005/05/08 14:23:12  jkeiser
 * Added getRestrictedDays to return Hashtable of Holiday objects, rather than just values.
 *
 * Revision 1.23  2005/05/07 16:52:28  umangkus
 * Added add and remove restricted methods with pm275.core.calendar.Holiday
 * param.
 *
 * Revision 1.22  2005/05/03 02:44:10  umangkus
 * Modified Calendar.java to use core.calendar.Holiday object and
 * getRestrictedDays to return Collection.
 * Modified CalendarInterface.java getNumberOfWorkingDays method.
 * Modified CalendarService.java to include corporate calendar in list of
 * calendars.
 *
 * Revision 1.21  2005/05/01 15:51:57  jkeiser
 * Added new method toString();
 *
 * Revision 1.20  2005/04/29 01:10:30  tlong2
 * Added visitor pattern updates acceptVisitor methods, etc
 *
 * Revision 1.19  2005/04/19 09:28:06  achou
 * Removed obsolete methods computeFutureWorkDate() and getNextDay().  Added new method getNextWorkDay().
 *
 * Revision 1.18  2005/04/19 08:41:18  achou
 * Implemented addHoursToDate() and addDaysToDate() to perform date arithmetics within the context of the calendar.
 *
 * Revision 1.17  2005/04/18 03:46:33  rcoutinh
 * Fixed bug where endDate always one day late
 *
 * Revision 1.16  2005/04/18 03:34:32  rcoutinh
 * corrected isWorkDay
 *
 * Revision 1.15  2005/04/18 02:07:10  rcoutinh
 * assign default values
 *
 * Revision 1.14  2005/04/18 02:00:18  rcoutinh
 * Updated to skip weekends.
 *
 */



