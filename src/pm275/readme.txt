All Java application code, and only Java application code, goes in this directory.  
Java code for tests should be placed in an appropriate subdirectory of 
the test directory. Other kinds of files, XML files, SQL scripts, etc will go into other directories.

The assumption is that code will belong to a package pm275.<group>, which can be further subdivided as the teams deem necessary.

